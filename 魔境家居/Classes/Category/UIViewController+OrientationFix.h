//
//  UIViewController+OrientationFix.h
//  魔境家居
//
//  Created by lumingliang on 15/12/16.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (OrientationFix)

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation;
- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
@end
