//
//  MJLoginCtler.m
//  魔境家居
//
//  Created by mojing on 15/11/11.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJLoginCtler.h"
#import "MJViewController.h"
#import "MJNetworkTool.h"
#import "MJLoginUserModel.h"
#import "MJUserDataModel.h"
#import "MJSupplierDataModel.h"
#import "MJBrandDataModel.h"
#import "MJDealerDataModel.h"
#import "MJGuideData.h"
#import "AppDelegate.h"
#import "APService.h"
#import "MBProgressHUD.h"
#import "MJVersionUpdateTool.h"
#import "UIImageView+WebCache.h"
#import "MJDesignerHomePageTabBarCtler.h"

#import "JenWebService.h"
#import "NSString+md5.h"
#define kLoginNotification @"kLoginNotification"
@interface MJLoginCtler ()

@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (copy, nonatomic) NSString *type;
@property (copy, nonatomic) NSString *updateString;
@end

@implementation MJLoginCtler

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   // self.title = @"登录";
    self.view.backgroundColor = [UIColor whiteColor];
    self.backgroundView.layer.cornerRadius = 5;
    NSString *loginUser = [MJUserDefaults objectForKey:@"userName"];
    if (loginUser.length > 1) {
        self.usernameField.text = loginUser;
    }
    NSString *password = [MJUserDefaults objectForKey:@"password"];
    if (password.length > 5) {
        self.passwordField.text = password;
    }
    
    NSString *url = [[NSString alloc] initWithFormat:@"http://itunes.apple.com/lookup?id=%@",@""];
    
    
    [self Postpath:url];
    
    MJVersionUpdateTool *update = [MJVersionUpdateTool shareInstance];
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    if ([self isConnectToNet]) {
        [net getMoJingURL:MJVersion parameters:nil success:^(id obj) {
            NSArray *array = obj[@"results"];
            NSDictionary *dict = [array lastObject];
            NSLog(@"当前版本为：%@", dict[@"version"]);
           // [update compareVersion:dict[@"version"] alertTitle:changeLog alertMessage:nil];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
        
    }
    __weak MJLoginCtler *ctl = self;
    
    update.update = ^{
        if ([ctl isConnectToNet]) {

            [[UIApplication sharedApplication] openURL:[NSURL        URLWithString:mjVersionUpdateURL]];
        }
    };
    
}

- (void)Postpath:(NSString *)url
{
    
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

- (IBAction)login:(id)sender {
//    MJViewController *vc = [[MJViewController alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
    [MJUserDefaults setObject:@"YES" forKey:@"serviceViewHidden"];
    [MJUserDefaults setObject:self.passwordField.text forKey:@"password"];
    NSDictionary *dict = @{@"userName" : self.usernameField.text,@"password" : self.passwordField.text};
    AppDelegate *myDelegate = [UIApplication sharedApplication].delegate;
    
    if ([self isConnectToNet]) {
        MJNetworkTool *net = [MJNetworkTool shareInstance];
        //用户登录接口调用
        [net getMoJingURL:MJUserLogin parameters:dict success:^(id obj) {
            
            MJLoginUserModel *loginModel = obj;

           
            MJUserDataModel *userModel = loginModel.data;
//            MJBrandDataModel *brandData = userModel.brandData;
//            MJSupplierDataModel *supplier = userModel.supplierData;
//            MJDealerDataModel *dealer = userModel.dealerData;
//            MJGuideData *guide = userModel.guideData;
            //保存用户头像，用户名，用户电话，用户ID
           // NSLog(@"%@",loginModel.data.dealerData.shopName);
            
            
            UIImageView *tempV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
            [self.view addSubview:tempV];
            NSURL *imgUrl = [NSURL URLWithString:userModel.userAvatarURL];
            [tempV sd_setImageWithURL:imgUrl completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                NSData *data = UIImageJPEGRepresentation(image, 0.7);
                [MJUserDefaults setObject:data forKey:@"userAvatarURL"];
            }];
            
            [MJUserDefaults setObject:userModel.brandData.name forKey:@"brandName"];
            
            [MJUserDefaults setObject:userModel.userName forKey:@"userName"];
            [MJUserDefaults setObject:userModel.nickName forKey:@"nickName"];
            [MJUserDefaults setObject:userModel.phoneNumber forKey:@"phoneNumber"];
            NSString *brandID = [NSString stringWithFormat:@"%ld",(long)userModel.brandData.idNum];
            [MJUserDefaults setObject:brandID forKey:@"brandID"];
            
            NSString *userID = [NSString stringWithFormat:@"%ld",(long)userModel.userID];
            _type = userModel.type;
            [APService setAlias:userID  callbackSelector:@selector(tagsAliasCallback:tags:alias:) object:self];
            [MJUserDefaults setObject:userID forKey:@"userID"];
             
            [MJUserDefaults synchronize];
            //全局品牌ID 用户ID参数
            myDelegate.brandIDParaDict = @{@"brandID" : brandID,@"userID":userID,};
    
            if ([userModel.type isEqualToString:@"1"]||[userModel.type isEqualToString:@"2"]||[userModel.type isEqualToString:@"3"]) {
                if (loginModel.code == 200 ) {
                    if ([userModel.type isEqualToString:@"2"] || [userModel.type isEqualToString:@"3"]) {
                        myDelegate.shopName = loginModel.data.dealerData.shopName;
                        myDelegate.type = userModel.type;
                    }
                    [self loginDUC:self.usernameField.text password:self.passwordField.text];
                    }else
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"用户名或密码错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alert show];
                    }

            }else if([userModel.type isEqualToString:@"4"]){
                if (loginModel.code == 200 ) {
                   
                    [self loginDUC:self.usernameField.text password:self.passwordField.text];
                    myDelegate.type = userModel.type;
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"用户名或密码错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alert show];
                }

            }
            else{
                UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您不是魔境家居商家用户,如需开通,请联系4008-530-588" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                            [alert1 show];
            }
        
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
        
    }else
    {
        
    }
 
}

- (void)tagsAliasCallback:(int)iResCode
                     tags:(NSSet *)tags
                    alias:(NSString *)alias {
    NSString *callbackString =
    [NSString stringWithFormat:@"%d, \ntags: %@, \nalias: %@\n", iResCode,
     [self logSet:tags], alias];
    
    NSLog(@"TagsAlias回调:%@", callbackString);
}

- (NSString *)logSet:(NSSet *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}

- (void)loginDUC:(NSString*)userName password:(NSString*)password
{
    NSString *token = [[NSString stringWithFormat:@"%@-ios", userName] md5];
    NSDictionary *params = @{@"from":@"4", @"token":token, @"username":userName, @"password":password};
    [JenWebService requestWithUrl:Login_DUC params:params method:POST userInfo:nil
                         callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
         if ([[data objectForKey:@"success"] boolValue]) {
             [[NSNotificationCenter defaultCenter] postNotificationName:kLoginNotification object:nil];
             self.passwordField.text = nil;
             if ([_type isEqualToString:@"1"]||[_type isEqualToString:@"2"]||[_type isEqualToString:@"3"]) {
                 [self pushVC];
             }else if ([_type isEqualToString:@"4"])
             {
                 [self pushTabBarCtler];
             }
             
         }else
         {
             
         }
     }];
}

- (void)pushVC
{
    MJViewController *vc = [[MJViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)pushTabBarCtler
{
    MJDesignerHomePageTabBarCtler *tab = [[MJDesignerHomePageTabBarCtler alloc] init];
    [self presentViewController:tab animated:YES completion:nil];
}
@end
