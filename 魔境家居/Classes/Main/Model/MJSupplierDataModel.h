//
//  MJSupplierDataModel.h
//  魔境家居
//
//  Created by mojing on 15/11/20.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJSupplierDataModel : NSObject

@property (nonatomic,assign) NSInteger userID;//:1001,
@property (nonatomic,copy) NSString *userName;//:;//用户名;//,
@property (nonatomic,copy) NSString *nickName;//:;//昵称;//,
@property (nonatomic,assign) NSInteger startDateTime;//:11321321313131,
@property (nonatomic,assign) NSInteger endDateTime;//:11321321313131,
@property (nonatomic,assign) NSInteger productCount;//:10,
@property (nonatomic,assign) NSInteger existProductCount;//:5,
@property (nonatomic,assign) NSInteger dealerCount;//:10,
@property (nonatomic,assign) NSInteger existDealerCount;//:5,
@property (nonatomic,copy) NSString *phoneNumber;//:;//12345678910;//,
@property (nonatomic,copy) NSString *address;//:;//详细地址;//

@end
