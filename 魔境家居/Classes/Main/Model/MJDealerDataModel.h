//
//  MJDealerDataModel.h
//  魔境家居
//
//  Created by mojing on 15/11/20.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJDealerDataModel : NSObject

@property (nonatomic,assign) NSInteger userID;//:1001,
@property (nonatomic,copy) NSString *userName;//:;//用户名;//,
@property (nonatomic,copy) NSString *nickName;//:;//昵称;//,
@property (nonatomic,assign) NSInteger guideCount;//:10,
@property (nonatomic,assign) NSInteger existGuideCount;//:5,
@property (nonatomic,copy) NSString *phoneNumber;//:;//12345678910;//,
@property (nonatomic,copy) NSString *address;//:;//详细地址;//
@property (nonatomic,copy) NSString *shopName;

@end
