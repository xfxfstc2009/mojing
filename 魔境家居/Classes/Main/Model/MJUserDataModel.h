//
//  MJUserDataModel.h
//  魔境家居
//
//  Created by mojing on 15/11/20.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJBrandDataModel.h"//
#import "MJSupplierDataModel.h"//
#import "MJDealerDataModel.h"//
#import "MJGuideData.h"
@interface MJUserDataModel : NSObject

@property (nonatomic,assign) NSInteger idNum;
@property (nonatomic,assign) NSInteger userID;//:1001,
@property (nonatomic,copy) NSString *userName;//:;//用户名;//,
@property (nonatomic,copy) NSString *nickName;
@property (nonatomic,copy) NSString *type;//:;//账号类型。“1”表示供应商、“2”表示经销商、“3”表示导购员。;//,
@property (nonatomic,copy) NSString *permission;//:;//00010，用于表示权限值;//,
@property (nonatomic,copy) NSString *phoneNumber;//:;//12345678910;//,
@property (nonatomic,copy) NSString *userAvatarURL;//:;//用户头像的URL地址，可以直接使用，如果没有值，请使用默认值;//,
@property (nonatomic,copy) NSString *isDeleted;//:;//0;//,
@property (nonatomic,copy) NSString *password;
@property (nonatomic,copy) NSString *oldPassword;
@property (nonatomic,strong) MJBrandDataModel *brandData;//:Object{...},
@property (nonatomic,strong) MJSupplierDataModel *supplierData;//:Object{...},
@property (nonatomic,strong) MJDealerDataModel *dealerData;//:Object{...},
@property (nonatomic,strong) MJGuideData *guideData;//:Object{...}

@end
