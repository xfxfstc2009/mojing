//
//  TrackViewController.h
//  PhotoAR
//
//  Created by 施正士 on 15/7/9.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <metaioSDK/GestureHandlerIOS.h>
#import <MetaioSDK/MetaioSDKViewController.h>
#import <UIKit/UIKit.h>

@interface Product3DViewCtrl : MetaioSDKViewController

- (void)addModel:(NSDictionary*)model;
- (id)initWithItem:(NSDictionary*)data;



@end
