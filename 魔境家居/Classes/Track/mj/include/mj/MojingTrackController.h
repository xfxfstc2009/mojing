//
//  MojingTrackController.h
//  PhotoAR
//
//  Created by 施正士 on 15/11/6.
//  Copyright © 2015年 施正士. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <metaioSDK/GestureHandlerIOS.h>
#import <MetaioSDK/MetaioSDKViewController.h>

@protocol MojingTrackDelegate <NSObject>


- (void)trackSDKReady;
- (void)loadModel;
- (void)saveTrackDelegate:(NSString*)data
                 products:(NSArray*)products
                    image:(UIImage*)image
                 sceneImg:(UIImage*)sceneImg
                sceneBgId:(NSString*)sceneBgId;

@end


@interface MojingTrackController : MetaioSDKViewController

@property (nonatomic, weak) id<MojingTrackDelegate> mdelegate;

- (id)init;
- (id)initWithItem:(NSDictionary*)data;

// take picture button pressed.
- (void)onTakePicture;
// save screenshot button pressed
- (void)onSaveScreen;

- (void)addModel:(NSDictionary*)model info:(NSDictionary*)info;

// 删除当前模型
- (void)removeCurrModel;
- (NSDictionary*)getCurrModelData;
// 换材质
- (void)changeModelTexture:(NSDictionary*)userInfo;
// 缩放 锁/解锁
- (void)lockZoom:(BOOL)isLock;
- (void)onClearScreen;
// 获取模型家居数据
- (NSArray*)getModelsWithRect;

@end
