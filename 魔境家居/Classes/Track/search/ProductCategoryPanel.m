//
//  ProductSearchSTypePanel.m
//  PhotoAR
//
//  Created by 施正士 on 15/8/10.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "ProductCategoryPanel.h"
#import "ServiceModel.h"

@interface ProductCategoryPanel ()
{
    NSInteger currentIndex;
    int categoryId;
}

@property (nonatomic, strong) NSArray *typeList;

@end

@implementation ProductCategoryPanel

@synthesize typeList=_typeList, delegate=_delegate;

- (id)initWithCategoryId:(int)index
{
    self = [super init];
    if (self) {
        categoryId = index;
    }
    return  self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[ServiceModel getInstance] getProductCategory:^(NSDictionary *data, NSDictionary *userInfo) {
        NSArray *list = [[data objectForKey:@"data"] objectForKey:@"productTypeList"];
        NSMutableArray *newList = [NSMutableArray arrayWithCapacity:[list count]+1];
        [newList addObject:@{@"id":[NSNumber numberWithInt:0], @"name":@"全部"}];
        [newList addObjectsFromArray:list];
        self.typeList = newList;
        
        for (int i=0; i<[_typeList count]; i++) {
            NSDictionary *item = [_typeList objectAtIndex:i];
            if ([[item objectForKey:@"id"] intValue]==categoryId) {
                currentIndex = i;
            }
        }
        [self.tableView reloadData];
    }];
    
//    self.typeList = @[@{@"id":@"0",@"name":@"全部"},@{@"id":@"1",@"name":@"客餐厅"},@{@"id":@"2",@"name":@"卧室"},
//                      @{@"id":@"3",@"name":@"卫生间"},@{@"id":@"4",@"name":@"厨房"},
//                      @{@"id":@"5",@"name":@"书房"},@{@"id":@"6",@"name":@"儿童房"},
//                      @{@"id":@"7",@"name":@"多功能房"},@{@"id":@"8",@"name":@"其他"}];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_typeList count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if(indexPath.row==currentIndex){
        return;
    }
    NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:currentIndex
                                                   inSection:0];
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    if (newCell.accessoryType == UITableViewCellAccessoryNone) {
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:oldIndexPath];
    if (oldCell.accessoryType == UITableViewCellAccessoryCheckmark) {
        oldCell.accessoryType = UITableViewCellAccessoryNone;
    }
    currentIndex=indexPath.row;
    
    [_delegate productCategorySelected:[_typeList objectAtIndex:currentIndex]];
    [self.navigationController popViewControllerAnimated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *TableSampleIdentifier = @"TableSampleIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             TableSampleIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:TableSampleIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
    }
    if(indexPath.row==currentIndex){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    NSUInteger row = [indexPath row];
    NSDictionary *item = [_typeList objectAtIndex:row];
    cell.textLabel.text = [item objectForKey:@"name"];
    if ([[item objectForKey:@"id"] integerValue]==0) {
        cell.textLabel.textColor = [UIColor redColor];
    }else{
        cell.textLabel.textColor = [UIColor blackColor];
    }
    return cell;
}



@end
