//
//  ProductSearchSTypePanel.m
//  PhotoAR
//
//  Created by 施正士 on 15/8/10.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "ProductBrandPanel.h"
#import "ServiceModel.h"

@interface ProductBrandPanel ()
{
    NSInteger currentIndex;
    int brandId;
}

@property (nonatomic, strong) NSArray *typeList;

@end

@implementation ProductBrandPanel

@synthesize typeList=_typeList, delegate=_delegate;

- (id)initWithBrandId:(int)index
{
    self = [super init];
    if (self) {
        brandId = index;
    }
    return  self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[ServiceModel getInstance] getBrand:^(NSDictionary *data, NSDictionary *userInfo) {
        NSArray *list = [[data objectForKey:@"data"] objectForKey:@"brandList"];
        NSMutableArray *newList = [NSMutableArray arrayWithCapacity:[list count]+1];
        [newList addObject:@{@"id":[NSNumber numberWithInt:0], @"name":@"全部"}];
        [newList addObjectsFromArray:list];
        self.typeList = newList;
        
        for (int i=0; i<[_typeList count]; i++) {
            NSDictionary *item = [_typeList objectAtIndex:i];
            if ([[item objectForKey:@"id"] intValue]==brandId) {
                currentIndex = i;
            }
        }
        
        [self.tableView reloadData];
    }];
    
//    self.typeList = @[@{@"id":@"0",@"name":@"全部"},@{@"id":@"1",@"name":@"顾家家居"},@{@"id":@"2",@"name":@"皇佳"},
//                      @{@"id":@"3",@"name":@"科翔"},@{@"id":@"4",@"name":@"立邦"},
//                      @{@"id":@"5",@"name":@"马可波罗"},@{@"id":@"6",@"name":@"柔然"},
//                      @{@"id":@"7",@"name":@"尚尚木莲"},@{@"id":@"8",@"name":@"其他"}];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_typeList count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if(indexPath.row==currentIndex){
        return;
    }
    NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:currentIndex
                                                   inSection:0];
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    if (newCell.accessoryType == UITableViewCellAccessoryNone) {
        newCell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:oldIndexPath];
    if (oldCell.accessoryType == UITableViewCellAccessoryCheckmark) {
        oldCell.accessoryType = UITableViewCellAccessoryNone;
    }
    currentIndex=indexPath.row;
    
    [_delegate productBrandSelected:[_typeList objectAtIndex:currentIndex]];
    [self.navigationController popViewControllerAnimated:YES];
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *TableSampleIdentifier = @"TableSampleIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
                             TableSampleIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:TableSampleIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:14];
    }
    if(indexPath.row==currentIndex){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    NSUInteger row = [indexPath row];
    NSDictionary *item = [_typeList objectAtIndex:row];
    cell.textLabel.text = [item objectForKey:@"name"];
    if ([[item objectForKey:@"id"] integerValue]==0) {
        cell.textLabel.textColor = [UIColor redColor];
    }else{
        cell.textLabel.textColor = [UIColor blackColor];
    }
    return cell;
}



@end
