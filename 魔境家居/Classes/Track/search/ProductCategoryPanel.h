//
//  ProductSearchSTypePanel.h
//  PhotoAR
//
//  Created by 施正士 on 15/8/10.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductSearchControl.h"

@interface ProductCategoryPanel : UITableViewController

@property (nonatomic, assign) id<ProductSearchDelegate> delegate;

- (id)initWithCategoryId:(int)index;

@end
