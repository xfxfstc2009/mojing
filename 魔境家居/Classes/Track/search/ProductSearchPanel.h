//
//  ProductSearchPanel.h
//  PhotoAR
//
//  Created by 施正士 on 15/8/4.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableCellItem.h"
#import "ProductSearchControl.h"

@interface ProductSearchPanel : UIViewController<TableCellItemDelegate>

@property (nonatomic, assign) id<ProductSearchDelegate> delegate;
@property (nonatomic, strong) UITextField *searchInput;

- (id)initWithFrame:(CGRect)frame;
- (void)setProductType:(NSDictionary*)data;
- (void)setProductBrand:(NSDictionary*)data;

@end
