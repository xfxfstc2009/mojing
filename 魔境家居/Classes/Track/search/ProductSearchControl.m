//
//  ProductSearchControl.m
//  PhotoAR
//
//  Created by 施正士 on 15/8/10.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "ProductSearchControl.h"
#import "ProductSearchPanel.h"
#import "AppDelegate.h"
#import "Global.h"
#import "ProductCategoryPanel.h"
#import "ProductBrandPanel.h"
#import "NSString+Encode.h"

@interface ProductSearchControl ()
{
    UINavigationController *navi;
    ProductSearchPanel *searchPanel;
    CGRect _frame;
}

@property (nonatomic, strong) NSDictionary *category;
@property (nonatomic, strong) NSDictionary *brand;
@property (nonatomic, strong) NSString *searchType;


@end

@implementation ProductSearchControl

@synthesize category=_category, brand=_brand, searchType=_searchType;
@synthesize delegate=_delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super init];
    if (self) {
        _frame = frame;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_frame.size.width!=0 && _frame.size.height!=0) {
        self.view.frame = _frame;
    }
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = self.view.bounds;
    [self.view addSubview:btn];
    [btn addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    
    if (_frame.size.width!=0 && _frame.size.height!=0) {
        searchPanel = [[ProductSearchPanel alloc] initWithFrame:self.view.bounds];
    }else{
        searchPanel = [[ProductSearchPanel alloc] init];
    }
    searchPanel.delegate = self;
    navi = [[UINavigationController alloc] initWithRootViewController:searchPanel];
    if (_frame.size.width!=0 && _frame.size.height!=0) {
        navi.view.frame = self.view.bounds;
    }
    else if (iPad) {
        navi.view.frame = CGRectMake(SCREEN_WIDTH-270, 0, 270, SCREEN_HEIGHT);
    }else{
        navi.view.frame = CGRectMake(50, 0, SCREEN_WIDTH-50, SCREEN_HEIGHT);
    }
    navi.navigationBar.translucent = NO;
    navi.navigationBar.barTintColor = UIColorFromRGB(0xffffff);
    navi.navigationBar.tintColor = [UIColor blackColor];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIColor blackColor], NSForegroundColorAttributeName,
                                [UIFont systemFontOfSize:20], NSFontAttributeName,
                                nil];
    [navi.navigationBar setTitleTextAttributes:attributes];
    
    [self addChildViewController:navi];
    [self.view addSubview:navi.view];
    
    UIButton *cancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancel setTitle:@"取消" forState:UIControlStateNormal];
    [cancel setTitleColor:UIColorFromRGB(0xa3a3a3) forState:UIControlStateNormal];
    cancel.frame = CGRectMake(10, 7, 50, 30);
    UIButton *submit = [UIButton buttonWithType:UIButtonTypeCustom];
    [submit setTitle:@"确定" forState:UIControlStateNormal];
    [submit setTitleColor:UIColorFromRGB(0xff0000) forState:UIControlStateNormal];
    submit.frame = CGRectMake(0, 7, 50, 30);
    [cancel addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    [submit addTarget:self action:@selector(submitHander) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:cancel];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:submit];
    searchPanel.navigationItem.leftBarButtonItem = left;
    searchPanel.navigationItem.rightBarButtonItem = right;
    searchPanel.navigationItem.title = @"筛选";
    if (_frame.size.width!=0 && _frame.size.height!=0) {
        searchPanel.navigationItem.title = @"";
    }
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(searchPanel.view.frame.origin.x, 0, 0.5, SCREEN_HEIGHT)];
    line.backgroundColor = UIColorFromRGB(0xe0e0df);
    [self.view addSubview:line];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hide) name:@"HideProductSearch" object:nil];
}


- (void)submitHander
{
    NSString *key = searchPanel.searchInput.text;
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithCapacity:1];
    if (![key isEqualToString:@""]) {
        [param setObject:[key encodeString] forKey:@"searchString"];
    }
//    if (_searchType) {
//        [param setObject:_searchType forKey:@"searchType"];
//    }
    if (_category) {
        NSString *bid = [[_category objectForKey:@"id"] stringValue];
        if (![bid isEqualToString:@"0"]) {
            [param setObject:bid forKey:@"productTypeID"];
        }
    }
    if (_brand) {
        NSString *bid = [[_brand objectForKey:@"id"] stringValue];
        if (![bid isEqualToString:@"0"]) {
            [param setObject:bid forKey:@"brandID"];
        }
    }
    
    [_delegate searchProductByDelegate:param];
    [self hide];
}

- (void)gotoProductCategory
{
    int index = 0;
    if (_category) {
        index = [[_category objectForKey:@"id"] intValue];
    }
    ProductCategoryPanel *panel = [[ProductCategoryPanel alloc] initWithCategoryId:index];
    panel.delegate = self;
    [navi pushViewController:panel animated:YES];
}

- (void)productCategorySelected:(NSDictionary *)data
{
    self.category = data;
    [searchPanel setProductType:data];
}

- (void)productSearchTypeSelected:(NSString*)type
{
    self.searchType = type;
}

- (void)gotoProductBrand
{
    int index = 0;
    if (_brand) {
        index = [[_brand objectForKey:@"id"] intValue];
    }
    ProductBrandPanel *panel = [[ProductBrandPanel alloc] initWithBrandId:index];
    panel.delegate = self;
    [navi pushViewController:panel animated:YES];
}

- (void)productBrandSelected:(NSDictionary*)data
{
    self.brand = data;
    [searchPanel setProductBrand:data];
}

- (void)show
{
    self.view.frame = CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    AppDelegate *delegate=[[UIApplication sharedApplication] delegate];
//    [delegate.window.rootViewController addChildViewController:self];
    [delegate.window.rootViewController.view addSubview:self.view];
    [UIView beginAnimations:@"searchProduct" context:nil];
    [UIView setAnimationDelay:0.3];
    self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    if(!iPad)self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [UIView commitAnimations];
}

- (void)hide
{
    [searchPanel.searchInput resignFirstResponder];
    if (_frame.size.width!=0 && _frame.size.height!=0) {
        [self removeFromParentViewController];
        [self.view removeFromSuperview];
    }else{
        [UIView beginAnimations:@"searchProduct" context:nil];
        [UIView setAnimationDelay:0.3];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationStop)];
        if(!iPad)self.view.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        self.view.frame = CGRectMake(SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
        [UIView commitAnimations];
    }
    
}

- (void)animationStop
{
//    [self removeFromParentViewController];
    [self.view removeFromSuperview];
}

- (void)dispose
{
    [self removeFromParentViewController];
    [self.view removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
