//
//  ProductSearchPanel.m
//  PhotoAR
//
//  Created by 施正士 on 15/8/4.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "ProductSearchPanel.h"
#import "Global.h"
#import "AppDelegate.h"
#import "BorderView.h"

@interface ProductSearchPanel ()
{
    TableCellItem *classItem;
    TableCellItem *brandItem;
    UIView *zuopBg;
    
    UIView *container;
    int totalHeight;
    int contentWidth;
    CGRect _frame;
}
@end

@implementation ProductSearchPanel

@synthesize delegate=_delegate, searchInput=_searchInput;


- (id)initWithFrame:(CGRect)frame
{
    self = [super init];
    if (self) {
        _frame = frame;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    int contentHeight = SCREEN_HEIGHT;
    contentWidth = iPad ? 270 : self.view.frame.size.width-50;
    if (_frame.size.width!=0 && _frame.size.height!=0) {
        contentWidth = _frame.size.width;
        contentHeight = _frame.size.height;
    }
    
    container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, contentWidth, contentHeight)];
    container.backgroundColor = UIColorFromRGB(0xf8f8f8);
    [self.view addSubview:container];
    
    [self create];
    
}

- (void)create
{
    totalHeight = 0;
    UIView *line;
    
    // 搜索输入框
    totalHeight += 10;
    BorderView *inputBg = [[BorderView alloc] initWithFrame:CGRectMake(10, totalHeight, contentWidth-20, 32)
                                                  lineColor:0xe8e8e8];
    [container addSubview:inputBg];
    CGRect sf = inputBg.frame;
    sf.origin.x += 5;
    UITextField *searchInput = [[UITextField alloc] initWithFrame:sf];
    searchInput.font = [UIFont systemFontOfSize:14];
    [container addSubview:searchInput];
    searchInput.placeholder = @"输入搜索关键词";
    self.searchInput = searchInput;
    totalHeight += inputBg.frame.size.height;
    
    // 作品类型选项
//    totalHeight += 10;
//    line = [[UIView alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, 0.5)];
//    line.backgroundColor = UIColorFromRGB(0xe0e0df);
//    [container addSubview:line];
//    totalHeight += 1;
//    
//    zuopBg = [[UIView alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, 45*3)];
//    zuopBg.backgroundColor = [UIColor whiteColor];
//    [container addSubview:zuopBg];
//    totalHeight += [self createZuoping:0];
//    
//    line = [[UIView alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, 0.5)];
//    line.backgroundColor = UIColorFromRGB(0xe0e0df);
//    [container addSubview:line];
//    totalHeight += 1;
    
    // 空间分类
    totalHeight += 10;
    line = [[UIView alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, 0.5)];
    line.backgroundColor = UIColorFromRGB(0xe0e0df);
    [container addSubview:line];
    totalHeight += 1;
    UIView *classBg = [[UIView alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, 91)];
    classBg.backgroundColor = [UIColor whiteColor];
    [container addSubview:classBg];
    brandItem = [[TableCellItem alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, 45)
                                                data:@{@"label":@"品牌", @"value":@"全部", @"id":@"4",@"detailIcon":@"icon_more"}];
    brandItem.delegate = self;
    [container addSubview:brandItem];
    totalHeight += 45;
    
    line = [[UIView alloc] initWithFrame:CGRectMake(10, totalHeight, contentWidth-10, 0.5)];
    line.backgroundColor = UIColorFromRGB(0xe0e0df);
    [container addSubview:line];
    totalHeight += 1;
    
    classItem = [[TableCellItem alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, 45)
                                                               data:@{@"label":@"分类", @"value":@"全部", @"id":@"5",@"detailIcon":@"icon_more"}];
    classItem.delegate = self;
    [container addSubview:classItem];
    totalHeight += 45;
    line = [[UIView alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, 0.5)];
    line.backgroundColor = UIColorFromRGB(0xe0e0df);
    [container addSubview:line];
    totalHeight += 1;
    
    // 搜索历史
    totalHeight += 10;
    line = [[UIView alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, 0.5)];
    line.backgroundColor = UIColorFromRGB(0xe0e0df);
    [container addSubview:line];
    totalHeight += 1;
    UIView *historyBg = [[UIView alloc] initWithFrame:CGRectMake(0, totalHeight, contentWidth, SCREEN_HEIGHT-totalHeight)];
    historyBg.backgroundColor = [UIColor whiteColor];
    [container addSubview:historyBg];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 200, 20)];
    label.font = [UIFont systemFontOfSize:14];
    label.text = @"搜索历史";
    [historyBg addSubview:label];

}

- (int)createZuoping:(NSInteger)selectdId
{
    while ([[zuopBg subviews] count]) {
        [[[zuopBg subviews] objectAtIndex:0] removeFromSuperview];
    }
    NSArray *list = @[
                      [NSMutableDictionary dictionaryWithDictionary:@{@"label":@"全部商品", @"id":@"1"}],
                      [NSMutableDictionary dictionaryWithDictionary:@{@"label":@"我的商品", @"id":@"2"}],
                      [NSMutableDictionary dictionaryWithDictionary:@{@"label":@"我收藏的商品", @"id":@"3"}]
                      ];
    NSMutableDictionary *item = [list objectAtIndex:selectdId];
    [item setObject:@"icon_selected" forKey:@"detailIcon"];
    
    UIView *line;
    int tuopHeight;
    for (int i=0; i<[list count]; i++) {
        TableCellItem *item = [[TableCellItem alloc] initWithFrame:CGRectMake(0, tuopHeight, contentWidth, 45)
                                                              data:[list objectAtIndex:i]];
        item.delegate = self;
        [zuopBg addSubview:item];
        tuopHeight += 45;
        
        if (i<[list count]-1) {
            line = [[UIView alloc] initWithFrame:CGRectMake(10, tuopHeight, contentWidth-10, 0.5)];
            line.backgroundColor = UIColorFromRGB(0xe0e0df);
            [zuopBg addSubview:line];
        }
    }
    
    return tuopHeight;
}

- (void)setProductType:(NSDictionary*)data
{
    TableCellItem *newClassItem = [[TableCellItem alloc] initWithFrame:classItem.frame
                                                                  data:@{@"label":@"分类", @"value":[data objectForKey:@"name"],
                                                                         @"id":@"5",@"detailIcon":@"icon_more"}];
    newClassItem.delegate = self;
    [container addSubview:newClassItem];
    if (classItem) {
        [classItem removeFromSuperview];
    }
    classItem = newClassItem;
}

- (void)setProductBrand:(NSDictionary*)data
{
    TableCellItem *newBrandItem = [[TableCellItem alloc] initWithFrame:brandItem.frame
                                                                  data:@{@"label":@"品牌", @"value":[data objectForKey:@"name"],
                                                                         @"id":@"4",@"detailIcon":@"icon_more"}];
    newBrandItem.delegate = self;
    [container addSubview:newBrandItem];
    if (brandItem) {
        [brandItem removeFromSuperview];
    }
    brandItem = newBrandItem;
}

- (void)selectedItem:(NSDictionary*)data
{
    NSInteger sid = [[data objectForKey:@"id"] integerValue];
    switch (sid) {
        case 1:
            [self createZuoping:sid-1];
            [_delegate productSearchTypeSelected:@"1"];
            break;
        case 2:
            [self createZuoping:sid-1];
            [_delegate productSearchTypeSelected:@"2"];
            break;
        case 3:
            [self createZuoping:sid-1];
            [_delegate productSearchTypeSelected:@"3"];
            break;
        case 4:
            [_delegate gotoProductBrand];
            break;
        case 5:
            [_delegate gotoProductCategory];
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
