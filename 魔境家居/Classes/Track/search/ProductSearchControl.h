//
//  SpaceSearchControl.h
//  PhotoAR
//
//  Created by 施正士 on 15/8/10.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProductSearchDelegate <NSObject>

@optional
- (void)gotoProductCategory;
@optional
- (void)gotoProductBrand;
@optional
- (void)productCategorySelected:(NSDictionary*)data;
@optional
- (void)productBrandSelected:(NSDictionary*)data;
@optional
- (void)productSearchTypeSelected:(NSString*)type;
@optional
- (void)searchProductByDelegate:(NSDictionary*)search;

@end

@interface ProductSearchControl : UIViewController<ProductSearchDelegate>

@property (nonatomic, strong) id<ProductSearchDelegate> delegate;

- (id)initWithFrame:(CGRect)frame;
- (void)show;
- (void)dispose;

@end
