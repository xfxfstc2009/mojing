//
//  AccountInfoItem.m
//  HuaTuo
//
//  Created by jenth on 14-3-19.
//  Copyright (c) 2014年 HuaTuo. All rights reserved.
//

#import "TableCellItem.h"
#import "Global.h"

@implementation TableCellItem

@synthesize delegate=_delegate;

- (id)initWithFrame:(CGRect)frame data:(NSDictionary*)data
{
    self = [super initWithFrame:frame];
    if (self) {
        _data = data;
        
        [self create];
    }
    return self;
}

- (void)create
{
    int labelX = 12;
    if ([_data objectForKey:@"icon"]) {
        UIImage *icon = [UIImage imageNamed:[_data objectForKey:@"icon"]];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:icon];
        CGRect f;
        f.origin = CGPointMake(labelX, (self.frame.size.height-icon.size.height)/2);
        f.size = icon.size;
        imgView.frame = f;
        labelX = icon.size.width +20;
        [self addSubview:imgView];
    }
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(labelX, (self.frame.size.height-20)/2, 300, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = UIColorFromRGB(0x333333);
    label.text = [_data objectForKey:@"label"];
    [self addSubview:label];
    
    UIFont *font = [UIFont systemFontOfSize:14];
    NSString *str = trimStr([_data objectForKey:@"value"]);
    if (str) {
        CGSize size = CGSizeMake(MAXFLOAT, 20);
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
        NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
        CGSize labelSize = [str boundingRectWithSize:size
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attributes context:nil].size;
        labelSize.height = ceil(labelSize.height);
        labelSize.width = ceil(labelSize.width);
        int ly = (self.frame.size.height-labelSize.height)/2;
        UILabel *value = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-labelSize.width-35, ly,
                                                                   labelSize.width, labelSize.height)];
        value.backgroundColor = [UIColor clearColor];
        value.numberOfLines = 1;
        value.font = font;
        value.textColor = [UIColor blackColor];
        value.text = str;
        [self addSubview:value];
    }
    
    if ([_data objectForKey:@"detailIcon"]) {
        UIImage *detailIcon = [UIImage imageNamed:[_data objectForKey:@"detailIcon"]];
        UIImageView *detailView = [[UIImageView alloc] initWithImage:detailIcon];
        detailView.frame = CGRectMake(self.frame.size.width-25, (self.frame.size.height-detailIcon.size.height)/2,
                                      detailIcon.size.width, detailIcon.size.height);
        [self addSubview:detailView];
    }
    if ([_data objectForKey:@"id"]) {
        UIButton *hotarea = [UIButton buttonWithType:UIButtonTypeCustom];
        hotarea.frame = self.bounds;
        [hotarea addTarget:self action:@selector(hotareaHander) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:hotarea];
    }
}

- (void)hotareaHander
{
    if (_delegate && [_delegate respondsToSelector:@selector(selectedItem:)]) {
        [_delegate selectedItem:_data];
    }
}

@end
