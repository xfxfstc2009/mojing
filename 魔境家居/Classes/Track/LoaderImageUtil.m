//
//  LoaderModelUtil.m
//  PhotoAR
//
//  Created by 施正士 on 15/7/14.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "LoaderImageUtil.h"
#import "ASIHTTPRequest.h"
#import "Global.h"

@implementation LoaderImageUtil

@synthesize imageURL=_imageURL;
@synthesize delegate=_delegate;


static NSMutableArray *queue;

+ (void)addQueue:(LoaderImageUtil*)item
{
    if (queue==nil) {
        queue = [NSMutableArray arrayWithCapacity:10];
    }
    [queue addObject:item];
}

+ (void)removeQueue:(LoaderImageUtil*)item
{
    if (queue && [queue containsObject:item]) {
        [queue removeObject:item];
    }
}

+ (id)loadImage:(NSString *)url delegate:(id)delegate
{
    LoaderImageUtil *item = [[self alloc] initWithImageUrl:url delegate:delegate];
    [LoaderImageUtil addQueue:item];
    return item;
}


- (id)initWithImageUrl:(NSString*)imageURL
              delegate:(id)delegate
{
    self = [super init];
    if (self) {
        self.imageURL = imageURL;
        self.delegate = delegate;
        [self initQueue];
    }
    return self;
}

- (void)initQueue
{
    //初始化Documents路径
    
    NSArray *ll = [_imageURL componentsSeparatedByString:@"/"];
    NSString *fileName  = [ll objectAtIndex:[ll count]-1];
    filePath = [NSString stringWithFormat:@"%@/%@",  NSTemporaryDirectory(), fileName];
    
    //创建文件管理器
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //判断temp文件夹是否存在
    BOOL fileExists = [fileManager fileExistsAtPath:filePath];
    if (!fileExists) {
        [self addRequest:filePath];
    }else{
        [_delegate loadImageComplete:filePath];
    }
}


- (void)addRequest:(NSString*)savePath
{
    NSString *tempPath = [savePath stringByAppendingString:@"_temp"];
    //初始下载路径
    NSURL *url = [NSURL URLWithString:_imageURL];
    //设置下载路径
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
    //设置ASIHTTPRequest代理
    request.delegate = self;
    request.downloadProgressDelegate = self;
    //设置文件保存路径
    [request setDownloadDestinationPath:savePath];
    //设置临时文件路径
    [request setTemporaryFileDownloadPath:tempPath];
    //设置是是否支持断点下载
    [request setAllowResumeForFileDownloads:YES];
    [request startSynchronous];
}

- (void)setProgress:(float)newProgress
{
    if ([_delegate respondsToSelector:@selector(loadProgress:)]) {
        [_delegate loadProgress:newProgress];
    }
}

//ASIHTTPRequestDelegate,下载完成时,执行的方法
- (void)requestFinished:(ASIHTTPRequest *)request {
    if ([_delegate respondsToSelector:@selector(loadImageComplete:)]) {
        [_delegate loadImageComplete:filePath];
    }
    
    [LoaderImageUtil removeQueue:self];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [LoaderImageUtil removeQueue:self];
    NSLog(@"load failed %@", request.error);
}


- (void)dispose
{
}


@end
