//
//  LoaderModelUtil.h
//  PhotoAR
//
//  Created by 施正士 on 15/7/14.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoaderModelDelegate <NSObject>

- (void)loadModelComplete:(NSString*)modelId userInfo:(NSDictionary*)userInfo;

@optional

- (void)loadProgress:(float)newProgress modelId:(NSString*)modelId;

@end

@interface LoaderModelUtil : NSObject
{
    NSString *filePath;
    NSString *folderPath;
}

@property (nonatomic, strong) NSString *modelURL, *modelId;
@property (nonatomic, weak) id<LoaderModelDelegate> delegate;
@property (nonatomic, strong) NSDictionary *userInfo;
@property (nonatomic, strong) NSMutableArray *otherDelegateList;

+ (NSString*)getFileName:(NSString*)filePath;
+ (id)loadModel:(NSString *)url modelId:(NSString*)modelId
       userInfo:(NSDictionary*)userInfo
       delegate:(id)delegate;
- (void)addOtherDelegate:(id)delegate userInfo:(NSDictionary*)userInfo;
- (id)initWithModelUrl:(NSString*)imageURL
               modelId:(NSString*)modelId
              userInfo:userInfo
           delegate:(id)delegate;
- (void)initQueue;
- (void)dispose;

@end
