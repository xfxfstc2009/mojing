//
//  Loading.h
//  ChildRoad_iPad
//
//  Created by jenth on 11-5-6.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Loading : NSObject {

}


+ (void) show:(NSString*)msg;
+ (void) hide;

@end
