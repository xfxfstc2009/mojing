//
//  ModelListViewCtl.m
//  PhotoAR
//
//  Created by 施正士 on 15/7/10.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "ModelListViewCtl.h"
#import "SBJson.h"
#import "Global.h"
#import "ModelItem.h"
#import "ServiceModel.h"
#import "UIButton+JUtil.h"
#import "BorderView.h"
#import "ProductSearchControl.h"
#import "LoadingView.h"
//#import "UserModel.h"

@interface ModelListViewCtl ()<ProductSearchDelegate, UIScrollViewDelegate>
{
    UIView *container;
    UITableView *tabelView;
    UIScrollView *scrollView;
    UIButton *productBtn;
    UIButton *favoriteBtn;
    UIButton *searchBtn;
    int itemIndex;
    int type;
    int currentPage;
    int totalPage;
    NSDictionary *seachParam;
    int totalHeight;
    LoadingView *loadingView;
    int screenHeight;
}

@property (nonatomic, strong) NSArray *dataList;
@property (nonatomic, strong) LoaderModelUtil *loaderUtil;
@property (nonatomic, strong) ProductSearchControl *searchPanel;

@end

@implementation ModelListViewCtl

@synthesize dataList=_dataList, searchPanel=_searchPanel;


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    int screenWidth = iPad?SCREEN_WIDTH:SCREEN_HEIGHT;
    screenHeight = iPad?SCREEN_HEIGHT:SCREEN_WIDTH;
    container = [[UIView alloc] initWithFrame:CGRectMake(40, 0, 317, screenHeight)];
    container.backgroundColor = UIColorFromRGB(0xe7e7e7);
    [self.view addSubview:container];
    
    UIImage *productSelected = [UIImage imageNamed:@"dp_pro_selected"];
    productBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    productBtn.frame = CGRectMake(0, 0, productSelected.size.width, productSelected.size.height);
    [productBtn setBackgroundImage:productSelected forState:UIControlStateNormal];
    favoriteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    favoriteBtn.frame = CGRectMake(0, 120, productSelected.size.width, productSelected.size.height);
    [favoriteBtn setBackgroundImage:[UIImage imageNamed:@"dp_fav_unselected"] forState:UIControlStateNormal];
    searchBtn = [UIButton imageNamed:@"dp_search" origin:CGPointMake(0, 260)];
    [productBtn addTarget:self action:@selector(productHandler) forControlEvents:UIControlEventTouchUpInside];
    [favoriteBtn addTarget:self action:@selector(favoriteHandler) forControlEvents:UIControlEventTouchUpInside];
    [searchBtn addTarget:self action:@selector(openHandler)
        forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:productBtn];
    [self.view addSubview:favoriteBtn];
    [self.view addSubview:searchBtn];
    
    scrollView = [[UIScrollView alloc] initWithFrame:container.bounds];
    scrollView.delegate = self;
    [container addSubview:scrollView];
    
    itemIndex = 0;
    type = 1;
    currentPage = 1;
    loadingView = [[LoadingView alloc] initWithFrame:CGRectMake(0, totalHeight+10,
                                                                scrollView.frame.size.width, 30)];
    [scrollView addSubview:loadingView];
    
    [self search:nil];
    
//    NSDictionary *user = [UserModel getInstance].user;
//    if (!user) {
//        favoriteBtn.hidden = YES;
//        [searchBtn paddingTop:120];
//    }
}

- (void)search:(NSDictionary*)param
{
    [self removeSearchPanel];
    
    if (type==1) {
        NSMutableDictionary *params;
        if (!param) {
            params = [NSMutableDictionary dictionaryWithCapacity:1];
        }else{
            params = [NSMutableDictionary dictionaryWithDictionary:param];
        }
        NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
        [params setObject:[NSNumber numberWithInt:currentPage] forKey:@"pageNumber"];
#warning sfkslkfd
        [params setObject:@"66" forKey:@"brandID"];
        [[ServiceModel getInstance] searchProductByParam:params
                                                callBack:^(NSDictionary *data, NSDictionary *userInfo)
         {
             currentPage = [[data objectForKey:@"pageNumber"] intValue];
             totalPage = [[data objectForKey:@"pageCount"] intValue];
             NSArray *models = [data objectForKey:@"productList"];
             self.dataList = models;
             
             if ([_dataList count]>0) {
                 [self createList];
             }
             
             if (totalPage>currentPage) {
                 loadingView.frame = CGRectMake(0, totalHeight+5, scrollView.frame.size.width, 30);
                 scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, totalHeight+30);
             }else{
                 [loadingView removeFromSuperview];
                 scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, totalHeight);
             }

         }];
    }else{
        [[ServiceModel getInstance] searchProduct:1 brandId:nil categoryId:nil
                                       searchType:@"2" key:nil callBack:^(NSDictionary *data, NSDictionary *userInfo)
         
         {
             currentPage = [[data objectForKey:@"pageNumber"] intValue];
             totalPage = [[data objectForKey:@"pageCount"] intValue];
             NSArray *models = [data objectForKey:@"productList"];
             self.dataList = models;
             [self createList];
             scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, totalHeight);
         }];
    }
}

- (void)createList
{
//    while ([[scrollView subviews] count]) {
//        [[[scrollView subviews] objectAtIndex:0] removeFromSuperview];
//    }
    int width = 150;
    for (int i=0; i<[_dataList count]; i++) {
        NSDictionary *it = [_dataList objectAtIndex:i];
        CGRect f = CGRectMake((itemIndex%2)*(width+5)+5, ((int)(itemIndex/2))*(width+5)+5, width, width);
        ModelItem *item = [[ModelItem alloc] initWithData:it frame:f];
        item.delegate = self;
        [scrollView addSubview:item];
        
        itemIndex ++;
    }
    totalHeight = ceilf(itemIndex/2.0)*(width+5);
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sv
{
    float distant= sv.contentSize.height-sv.contentOffset.y;
    if (distant-30<screenHeight*1.0 && totalPage>currentPage) {
        currentPage ++;
        [self search:seachParam];
    }
}

- (void)modelItemSelected:(NSDictionary*)data
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddModelNotif"
                                                        object:nil userInfo:data];
}

- (void)productHandler
{
    [productBtn setBackgroundImage:[UIImage imageNamed:@"dp_pro_selected"]
                          forState:UIControlStateNormal];
    [favoriteBtn setBackgroundImage:[UIImage imageNamed:@"dp_fav_unselected"]
                           forState:UIControlStateNormal];
    type = 1;
    totalHeight = 0;
    currentPage = 1;
    itemIndex = 0;
    searchBtn.hidden = NO;
    while ([[scrollView subviews] count]) {
        [[[scrollView subviews] objectAtIndex:0] removeFromSuperview];
    }
    [self search:nil];
}

- (void)favoriteHandler
{
    [favoriteBtn setBackgroundImage:[UIImage imageNamed:@"dp_fav_selected"]
                           forState:UIControlStateNormal];
    [productBtn setBackgroundImage:[UIImage imageNamed:@"dp_pro_unselected"]
                          forState:UIControlStateNormal];
    type = 2;
    totalHeight = 0;
    currentPage = 1;
    itemIndex = 0;
    searchBtn.hidden = YES;
    while ([[scrollView subviews] count]) {
        [[[scrollView subviews] objectAtIndex:0] removeFromSuperview];
    }
    [self search:nil];
}

- (void)openHandler
{
    if (!_searchPanel) {
        ProductSearchControl *searchPanel = [[ProductSearchControl alloc] initWithFrame:container.frame];
        searchPanel.delegate = self;
        self.searchPanel = searchPanel;
        [self addChildViewController:_searchPanel];
        [self.view addSubview:_searchPanel.view];
    }else{
        [self removeSearchPanel];
    }
    
}

- (void)removeSearchPanel
{
    if (_searchPanel) {
        [_searchPanel removeFromParentViewController];
        [_searchPanel.view removeFromSuperview];
        _searchPanel = nil;
    }
}

- (void)searchProductByDelegate:(NSDictionary*)search
{
    seachParam = search;
    while ([[scrollView subviews] count]) {
        [[[scrollView subviews] objectAtIndex:0] removeFromSuperview];
    }
    
    totalHeight = 0;
    currentPage = 1;
    itemIndex = 0;
    
    loadingView = [[LoadingView alloc] initWithFrame:CGRectMake(0, totalHeight+5,
                                                                scrollView.frame.size.width, 30)];
    [scrollView addSubview:loadingView];
    [self search:seachParam];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
