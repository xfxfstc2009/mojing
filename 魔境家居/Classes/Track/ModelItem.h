//
//  ModelItem.h
//  PhotoAR
//
//  Created by 施正士 on 15/7/16.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgressView.h"
#import "LoaderModelUtil.h"

@protocol ModelItemDelegate <NSObject>

- (void)modelItemSelected:(NSDictionary*)data;

@end

@interface ModelItem : UIView<LoaderModelDelegate>
{
    ProgressView *progress;
}

@property (nonatomic, strong) NSDictionary *data;
@property (nonatomic, strong) LoaderModelUtil *loadUtil ;
@property (nonatomic, assign) id<ModelItemDelegate> delegate;

- (id)initWithData:(NSDictionary*)data frame:(CGRect)frame;

@end
