//
//  LoadingView.m
//  HuaTuo
//
//  Created by jenth on 14-5-16.
//  Copyright (c) 2014年 HuaTuo. All rights reserved.
//

#import "LoadingView.h"
#import "Global.h"

@implementation LoadingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self create];
    }
    return self;
}

- (void)create
{
    UIActivityIndicatorView *iv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	[iv startAnimating];
	iv.frame = CGRectMake((CGRectGetWidth(self.frame)-20)/2-40, 0, 20, 20);

    UILabel *txt = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(iv.frame)+10, 0, SCREEN_WIDTH, 20)];
    txt.backgroundColor = [UIColor clearColor];
    txt.font = [UIFont systemFontOfSize:14];
    txt.textColor = UIColorFromRGB(0x666666);
    txt.text = @"正在加载...";
    
    [self addSubview:iv];
    [self addSubview:txt];
}

@end
