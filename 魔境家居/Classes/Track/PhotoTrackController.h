//
//  ViewController.h
//  PhotoAR
//
//  Created by 施正士 on 15/7/9.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoaderModelUtil.h"

@protocol PhotoTrackDelegate <NSObject>

- (void)dismissViewController;
- (void)savePanelRemove;

@end

@interface PhotoTrackController : UIViewController<LoaderModelDelegate, PhotoTrackDelegate>

- (id)initWithItem:(NSDictionary*)data;
- (void)loadModel;
- (id)initWithModels:(NSArray*)models;

@end

