//
//  HTButton.h
//  HuaTuo
//
//  Created by jenth on 14-3-12.
//  Copyright (c) 2014年 HuaTuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HTButton : UIView

@property (nonatomic, strong) UIButton *btn;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title fontSize:(int)fontSize
           detegate:(id)detegate selector:(SEL)selector;

@end
