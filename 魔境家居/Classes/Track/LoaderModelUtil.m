//
//  LoaderModelUtil.m
//  PhotoAR
//
//  Created by 施正士 on 15/7/14.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "LoaderModelUtil.h"
#import "ASIHTTPRequest.h"
#import "Global.h"
#import "ZipArchive.h"

@implementation LoaderModelUtil

@synthesize modelURL=_modelURL, userInfo=_userInfo;
@synthesize delegate=_delegate, modelId=_modelId;
@synthesize otherDelegateList=_otherDelegateList;

static NSMutableArray *queue;

+ (void)addQueue:(LoaderModelUtil*)item
{
    if (queue==nil) {
        queue = [NSMutableArray arrayWithCapacity:10];
    }
    [queue addObject:item];
}

+ (void)removeQueue:(LoaderModelUtil*)item
{
    if (queue && [queue containsObject:item]) {
        [queue removeObject:item];
    }
}

+ (id)loadModel:(NSString *)url modelId:(NSString*)modelId
       userInfo:(NSDictionary *)userInfo delegate:(id)delegate
{
    if (queue) {
        for (int i=0; i<[queue count]; i++) {
            LoaderModelUtil *item = [queue objectAtIndex:i];
            if ([item.modelId isEqualToString:modelId]) {
                [item addOtherDelegate:delegate userInfo:userInfo];
                return item;
            }
        }
    }
    
    LoaderModelUtil *item = [[self alloc] initWithModelUrl:url modelId:modelId
                                                  userInfo:userInfo
                                                  delegate:delegate];
    [LoaderModelUtil addQueue:item];
    [item initQueue];
    return item;
}

+ (NSString*)getFileName:(NSString*)filePath
{
    NSArray *ll = [filePath componentsSeparatedByString:@"/"];
    NSString *file  = [ll objectAtIndex:[ll count]-1];
    NSArray *filell = [file componentsSeparatedByString:@"."];
    return [filell objectAtIndex:0];
}

- (void)addOtherDelegate:(id)delegate userInfo:(NSDictionary*)userInfo
{
    if (!_otherDelegateList) {
        self.otherDelegateList = [NSMutableArray arrayWithCapacity:1];
    }
    if (userInfo) {
        [_otherDelegateList addObject:@{@"delegate":delegate, @"userInfo":userInfo}];
    }else{
        [_otherDelegateList addObject:@{@"delegate":delegate}];
    }
    
}

- (id)initWithModelUrl:(NSString*)modelURL
               modelId:(NSString*)modelId
              userInfo:(NSDictionary *)userInfo
              delegate:(id)delegate
{
    self = [super init];
    if (self) {
        self.modelId = modelId;
        self.modelURL = modelURL;
        self.userInfo = userInfo;
        self.delegate = delegate;
    }
    return self;
}

- (void)initQueue
{
    //初始化Documents路径
    NSArray *ll = [_modelURL componentsSeparatedByString:@"/"];
    NSString *fileName  = [ll objectAtIndex:[ll count]-1];
    NSArray *filell = [fileName componentsSeparatedByString:@"."];
    NSString *fileOnlyName = [filell objectAtIndex:0];
    filePath = [NSString stringWithFormat:@"%@/%@",  NSTemporaryDirectory(), fileName];
    
    NSString *folderTempPath = NSTemporaryDirectory();
    folderPath = [folderTempPath stringByAppendingPathComponent:fileOnlyName];
    //创建文件管理器
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //判断temp文件夹是否存在
    BOOL fileExists = [fileManager fileExistsAtPath:folderPath];
    if (!fileExists) {
        [self addRequest:filePath];
    }else{
        [self dispose];
        [_delegate loadModelComplete:_modelId userInfo:_userInfo];
    }
}

- (void)addRequest:(NSString*)savePath
{
    NSString *tempPath = [savePath stringByAppendingString:@"_temp"];
    //初始下载路径
    NSURL *url = [NSURL URLWithString:_modelURL];
    //设置下载路径
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:url];
    //设置ASIHTTPRequest代理
    request.delegate = self;
    request.downloadProgressDelegate = self;
    //设置文件保存路径
    [request setDownloadDestinationPath:savePath];
    //设置临时文件路径
    [request setTemporaryFileDownloadPath:tempPath];
    //设置是是否支持断点下载
    [request setAllowResumeForFileDownloads:YES];
    [request startAsynchronous];
}

- (void)setProgress:(float)newProgress
{
    NSLog(@"LoaderModelUtril %f", newProgress);
    if (_delegate && [_delegate respondsToSelector:@selector(loadProgress:modelId:)]) {
        [_delegate loadProgress:newProgress modelId:_modelId];
    }
}

//ASIHTTPRequestDelegate,下载完成时,执行的方法
- (void)requestFinished:(ASIHTTPRequest *)request {
    
    [self unzip:_modelId];
    if (_delegate ) {
        [_delegate loadModelComplete:_modelId userInfo:_userInfo];
    }
    if (_otherDelegateList) {
        for (int i=0; i<[_otherDelegateList count]; i++) {
            NSDictionary *item = [_otherDelegateList objectAtIndex:i];
            id delegate = [item objectForKey:@"delegate"];
            NSDictionary *userInfo = [item objectForKey:@"userInfo"];
            [delegate loadModelComplete:_modelId userInfo:userInfo];
        }
    }
    [request cancel];
    [self dispose];
    NSLog(@"load finish");
}

- (void)unzip:(NSString*)modelId
{
    ZipArchive* zip = [[ZipArchive alloc] init];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //判断temp文件夹是否存在
    BOOL fileExists = [fileManager fileExistsAtPath:filePath];
    if (fileExists) {
        if( [zip UnzipOpenFile:filePath] ){
            BOOL result = [zip UnzipFileTo:folderPath overWrite:YES];
            if( NO==result ){
                //添加代码
            }
            [zip UnzipCloseFile];
        }
        [fileManager removeItemAtPath:filePath error:nil];
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"load failed %@", request.error);
    [request cancel];
    [self dispose];
}

- (void)dispose
{
    [_otherDelegateList removeAllObjects];
    [LoaderModelUtil removeQueue:self];
}


@end
