//
//  LoaderModelUtil.h
//  PhotoAR
//
//  Created by 施正士 on 15/7/14.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoaderImageUtilDelegate <NSObject>

- (void)loadImageComplete:(NSString*)file;

@optional

- (void)loadProgress:(float)newProgress;

@end

@interface LoaderImageUtil : NSObject
{
    NSString *filePath;
}

@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, assign) id<LoaderImageUtilDelegate> delegate;

+ (id)loadImage:(NSString *)url delegate:(id)delegate;

- (id)initWithImageUrl:(NSString*)imageURL
           delegate:(id)delegate;
- (void)dispose;

@end
