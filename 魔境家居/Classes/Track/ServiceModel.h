//
//  ServiceModel.h
//  PhotoAR
//
//  Created by 施正士 on 15/7/17.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ServiceCallBack)(NSDictionary*, NSDictionary*);

@interface ServiceModel : UIView

@property (nonatomic, strong) NSArray *brandList;
@property (nonatomic, strong) NSArray *productCategoryList;
@property (nonatomic, strong) NSArray *spaceCategoryList;

+ (ServiceModel *)getInstance;

- (void)searchProduct:(int)page brandId:(NSString*)brandId categoryId:(NSString*)categoryId
           searchType:(NSString*)type
                  key:(NSString*)key callBack:(ServiceCallBack)callBack;
- (void)productDetail:(NSString*)proId callBack:(ServiceCallBack)callBack;
- (void)updateImage:(NSString*)name image:(UIImage*)image callBack:(ServiceCallBack)callBack;
- (void)getBrand:(ServiceCallBack)callBack;
- (void)getProductCategory:(ServiceCallBack)callBack;
- (void)searchProductByParam:(NSDictionary*)param callBack:(ServiceCallBack)callBack;
- (void)saveScene:(NSString*)title content:(NSString*)content spaceType:(NSString*)spaceType
             data:(NSString*)data products:(NSArray*)products
            image:(UIImage*)image sceneImg:(UIImage*)sceneImg
        sceneBgId:(NSString*)sceneBgId callBack:(ServiceCallBack)callBack;
- (void)getSpaceCategory:(ServiceCallBack)callBack;

@end
