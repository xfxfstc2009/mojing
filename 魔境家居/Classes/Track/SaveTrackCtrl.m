//
//  SaveTrackCtrl.m
//  PhotoAR
//
//  Created by 施正士 on 15/8/25.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "SaveTrackCtrl.h"
#import "Global.h"
#import "ServiceModel.h"
#import "BorderView.h"
#import "HTButton.h"
#import "NSString+Encode.h"
#import "Loading.h"
//#import "UserModel.h"
//#import "UIManager.h"

@interface SaveTrackCtrl ()
{
    NSString *_data;
    NSArray *_products;
    UIImage *_image;
    UIImage *_sceneImg;
    NSString *_sceneBgId;
    
    NSArray *categoryList;
    UITextField *titleInput;
    BorderView *currCategory;
    NSInteger categoryIndex;
    UIView *bgContainer;
}

@end

@implementation SaveTrackCtrl

@synthesize delegate=_delegate;

- (id)initWithData:(NSString*)data products:(NSArray*)products
             image:(UIImage*)image sceneImg:(UIImage*)sceneImg sceneBgId:(NSString*)sceneBgId
{
    self = [super init];
    if (self) {
        _data = data;
        _products = products;
        _image = image;
        _sceneBgId = sceneBgId;
        _sceneImg = sceneImg;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = self.view.bounds;
    [self.view addSubview:closeBtn];
    [closeBtn addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    
    int screenWidth;
    int screenHeight;
    screenWidth = CGRectGetWidth(self.view.frame);
    screenHeight = CGRectGetHeight(self.view.frame);
    
    int w = MIN(screenWidth-40, 500);
    int h = MIN(screenHeight-40, 300);
    bgContainer = [[UIView alloc] initWithFrame:CGRectMake((screenWidth-w)/2, (screenHeight-h)/2, w, h)];
    bgContainer.backgroundColor = [UIColor whiteColor];
    bgContainer.layer.cornerRadius = 5;
    bgContainer.clipsToBounds = YES;
    [self.view addSubview:bgContainer];
    UIButton *hotarea = [UIButton buttonWithType:UIButtonTypeCustom];
    hotarea.frame = bgContainer.bounds;
    [bgContainer addSubview:hotarea];
    [hotarea addTarget:self action:@selector(hotareaHander)
      forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(20, 38, 50, 30)];
    title.font = [UIFont systemFontOfSize:14];
    title.text = @"标题:";
    [bgContainer addSubview:title];
    titleInput = [[UITextField alloc] initWithFrame:CGRectMake(70, 39, 400, 30)];
    titleInput.font = [UIFont systemFontOfSize:14];
    titleInput.layer.borderColor = UIColorFromRGB(COLOR_FENGGE).CGColor;
    titleInput.layer.borderWidth = 0.5;
    [bgContainer addSubview:titleInput];

    UILabel *category = [[UILabel alloc] initWithFrame:CGRectMake(20, 100, 50, 30)];
    category.font = [UIFont systemFontOfSize:14];
    category.text = @"空间:";
    [bgContainer addSubview:category];
    
    int saveX = CGRectGetWidth(bgContainer.frame)/2-90;
    int saveY = CGRectGetHeight(bgContainer.frame)-50;
    HTButton *saveBtn = [[HTButton alloc] initWithFrame:CGRectMake(saveX, saveY, 80, 30) title:@"保存"
                                                fontSize:14 detegate:self
                                                selector:@selector(saveHandler)];
    saveBtn.backgroundColor = UIColorFromRGB(COLOR_BLUE);
    HTButton *cancelBtn = [[HTButton alloc] initWithFrame:CGRectMake(saveX+100,saveY, 80, 30)
                                                    title:@"关闭"
                                               fontSize:14 detegate:self
                                               selector:@selector(hide)];
    cancelBtn.backgroundColor = [UIColor whiteColor];
    [cancelBtn.btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cancelBtn.layer.borderWidth = 0.5;
    cancelBtn.layer.borderColor = UIColorFromRGB(COLOR_FENGGE).CGColor;
    [bgContainer addSubview:saveBtn];
    [bgContainer addSubview:cancelBtn];
    
    categoryIndex = -1;
    
    [[ServiceModel getInstance] getSpaceCategory:^(NSDictionary *data, NSDictionary *userInfo) {
        NSArray *list = [ServiceModel getInstance].spaceCategoryList;
        categoryList = list;
        int totalWidth = 70;
        int totalHeight = 100;
        for (int i=0; i<[list count]; i++) {
            NSDictionary *item = [list objectAtIndex:i];
            NSString *name = [item objectForKey:@"name"];
            UIFont *font = [UIFont systemFontOfSize:14];
            CGSize size = [Global getStringLength:font text:name];
            BorderView *itemBg = [[BorderView alloc] initWithFrame:CGRectMake(totalWidth, totalHeight, size.width+20, size.height+10)];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, size.width, size.height)];
            label.font = font;
            label.text = name;
            [itemBg addSubview:label];
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = itemBg.bounds;
            btn.tag = i+1;
            itemBg.tag = (i+1)*100;
            [itemBg addSubview:btn];
            [btn addTarget:self action:@selector(categoryHandler:) forControlEvents:UIControlEventTouchUpInside];
            [bgContainer addSubview:itemBg];
            
            totalWidth += size.width+20 + 15;
            if (totalWidth>bgContainer.frame.size.width-70-20) {
                totalWidth = 70;
                totalHeight += 40;
            }
        }
    }];
}

- (void)hotareaHander
{
    [titleInput resignFirstResponder];
}

- (void)categoryHandler:(id)sender
{
    if (currCategory) {
        currCategory.backgroundColor = [UIColor whiteColor];
    }
    NSInteger tag = [sender tag];
    BorderView *category = (BorderView*)[bgContainer viewWithTag:tag*100];
    category.backgroundColor = UIColorFromRGB(COLOR_BLUE);
    currCategory = category;
    
    categoryIndex = tag-1;
}

- (void)saveHandler
{
//    NSDictionary *user ;//= [UserModel getInstance].user;
//    if (!user) {
//        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"提示"
//                                                          message:@"请先登录再保存作品！"
//                                                         delegate:self
//                                                cancelButtonTitle:@"马上登录"
//                                                otherButtonTitles:@"关闭", nil];
//        message.tag = 1;
//        [message show];
//        return;
//    }
    
    if ([titleInput.text isEqualToString:@""]) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"提示"
                                                          message:@"请输入作品标题！"
                                                         delegate:nil
                                                cancelButtonTitle:@"确定"
                                                otherButtonTitles:nil];
        [message show];
        return;
    }
    if (categoryIndex==-1) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"提示"
                                                          message:@"请选择空间类型！"
                                                         delegate:nil
                                                cancelButtonTitle:@"确定"
                                                otherButtonTitles:nil];
        [message show];
        return;
    }
    
    NSString *title = [titleInput.text encodeString];
    NSDictionary *item = [categoryList objectAtIndex:categoryIndex];
    [Loading show:@"正在保存数据..."];
    [[ServiceModel getInstance] saveScene:title  content:@"c"
                                spaceType:[item objectForKey:@"id"]
                                     data:_data products:_products
                                    image:_image sceneImg:_sceneImg
                                sceneBgId:_sceneBgId
                                 callBack:^(NSDictionary *data, NSDictionary *userInfo)
    {
        [Loading hide];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"提示"
                                                          message:@"您的作品保存成功！"
                                                         delegate:self
                                                cancelButtonTitle:@"继续搭配"
                                                otherButtonTitles:@"回到首页", nil];
        message.tag = 2;
        [message show];
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSInteger tag = [alertView tag];
    if (tag==1)
    {
        if (buttonIndex==0) {
            [_delegate dismissViewController];
        }else{
            
        }
    }
    else if (tag==2)
    {
        if (buttonIndex==0) {
            [self hide];
        }else{
            [_delegate dismissViewController];
        }
    }
}

- (void)hide
{
    [_delegate savePanelRemove];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
