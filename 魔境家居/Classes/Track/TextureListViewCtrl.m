//
//  TextureListViewCtrl.m
//  PhotoAR
//
//  Created by 施正士 on 15/7/13.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "TextureListViewCtrl.h"
#import "EGOImageView.h"
#import "Global.h"
#import "UIImageView+JUtil.h"

@interface TextureListViewCtrl ()
{
    UIScrollView *scrolView;
    int totalWidth;
    UIView *bg;
}

@property (nonatomic, strong) NSDictionary *model;

@end

@implementation TextureListViewCtrl

@synthesize model=_model;

- (void)viewDidLoad {
    [super viewDidLoad];

    bg = [[UIView alloc] initWithFrame:self.view.bounds];
    bg.backgroundColor = [UIColor blackColor];
    bg.layer.borderWidth = 1.0f;
    bg.layer.borderColor = UIColorFromRGB(0xdddddd).CGColor;
    bg.layer.cornerRadius = 4;
    [self.view addSubview:bg];
    UIImageView *leftBg = [UIImageView imageNamed:@"dp_texture_left" origin:CGPointMake(-4, 25)];
    [self.view addSubview:leftBg];
    
    scrolView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:scrolView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textureHander:)
                                                 name:@"TextureNotification" object:nil];
}

- (void)textureHander:(NSNotification*)notif
{
    while ([[scrolView subviews] count]) {
        [[[scrolView subviews] objectAtIndex:0] removeFromSuperview];
    }
    totalWidth = 5;
    self.model = [notif userInfo];
    NSArray *textures = [_model objectForKey:@"texture"];
//    if ([textures count]<2) {
//        self.view.hidden = YES;
//    }else{
//        self.view.hidden = NO;
//    }
    for (int i=0; i<[textures count]; i++) {
        [self createTextureItem:[textures objectAtIndex:i] index:i];
    }
    
    scrolView.contentSize = CGSizeMake(totalWidth, 50);
    
    CGRect f = self.view.frame;
    f.size.width = MIN(SCREEN_HEIGHT-70, totalWidth);
    f.size.height = 60;
    self.view.frame = f;
    bg.frame = self.view.bounds;
    scrolView.frame = self.view.bounds;
}

- (void)createTextureItem:(NSDictionary*)texture index:(NSInteger)index
{
    EGOImageView *thumb = [[EGOImageView alloc] initWithFrame:CGRectMake(totalWidth, 5, 50, 50)];
    NSString *thumbUrl = [SEVER_URL stringByAppendingString:[texture objectForKey:@"thumbnail"]];
    thumb.imageURL = [NSURL URLWithString:thumbUrl];
    [scrolView addSubview:thumb];
    
//    UIImage *img = [UIImage imageNamed:[texture objectForKey:@"thumbnail"]];
//    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
//    imgView.frame = CGRectMake(totalWidth, 2, 80, 48);
//    [scrolView addSubview:imgView];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = thumb.frame;
    btn.tag = index;
    [scrolView addSubview:btn];
    [btn addTarget:self action:@selector(btnHander:) forControlEvents:UIControlEventTouchUpInside];
    
    totalWidth += 55;
}

- (void)btnHander:(id)sender
{
    NSInteger tag = [sender tag];
    NSArray *textures = [_model objectForKey:@"texture"];
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[_model objectForKey:@"id"], @"id",[textures objectAtIndex:tag], @"texture",  nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChangeModelTexture" object:nil userInfo:userInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
