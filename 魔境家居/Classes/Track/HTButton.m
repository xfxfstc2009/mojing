//
//  HTButton.m
//  HuaTuo
//
//  Created by jenth on 14-3-12.
//  Copyright (c) 2014年 HuaTuo. All rights reserved.
//

#import "HTButton.h"
#import "Global.h"

@implementation HTButton

@synthesize btn=_btn;

- (id)initWithFrame:(CGRect)frame title:(NSString *)title fontSize:(int)fontSize
           detegate:(id)detegate selector:(SEL)selector
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 3;
        self.layer.masksToBounds = YES;
        
        self.backgroundColor = UIColorFromRGB(COLOR_BLUE);
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btn = btn;
        CGRect f;
        f.origin = CGPointZero;
        f.size = frame.size;
        btn.frame = f;
        [btn.titleLabel setFont:[UIFont systemFontOfSize:fontSize]];
        [btn.titleLabel setTextColor:[UIColor whiteColor]];
        [btn setTitle:title forState:UIControlStateNormal];
        [btn addTarget:detegate action:selector forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
