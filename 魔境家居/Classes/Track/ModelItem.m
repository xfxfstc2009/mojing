//
//  ModelItem.m
//  PhotoAR
//
//  Created by 施正士 on 15/7/16.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "ModelItem.h"
#import "EGOImageView.h"
#import "Global.h"

@implementation ModelItem


@synthesize data=_data, delegate=_delegate;
@synthesize loadUtil=_loadUtil;

- (id)initWithData:(NSDictionary*)data frame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.data = data;
        [self create];
    }
    return self;
}

- (void)create
{
    EGOImageView *thumb = [[EGOImageView alloc] initWithFrame:self.bounds];
    //    NSString *thumbUrl = [_data objectForKey:@"thumb"];
    //    NSString *thumbUrl = [SEVER_URL stringByAppendingString:[_data objectForKey:@"thumbnail"]];
    NSString *suffix = [_data objectForKey:@"imageSuffix"];
    NSString *thumbName = [_data objectForKey:@"imageName"];
    NSString *thumbUrl = [self resImageUrl:thumbName suffix:suffix type:@"300_300"];
    thumb.imageURL = [NSURL URLWithString:thumbUrl];
    [self addSubview:thumb];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = self.bounds;
    [self addSubview:btn];
    [btn addTarget:self action:@selector(clickHander) forControlEvents:UIControlEventTouchUpInside];
}

- (void)clickHander
{
    [self showLoading];
}

- (void)showLoading
{
    progress = [[ProgressView alloc]initWithFrame:CGRectMake((CGRectGetWidth(self.frame)-60)/2, (CGRectGetHeight(self.frame)-60)/2, 60, 60)];
    progress.backgroundColor = [UIColor clearColor];
    progress.centerColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.8];
    progress.arcFinishColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    progress.arcUnfinishColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.8];
    progress.arcBackColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    progress.percent = 0;
    [self addSubview:progress];
    
    NSString *modelName = [_data objectForKey:@"modelName"];
    NSString *modelSuffix = [_data objectForKey:@"modelSuffix"];
    NSString *modelUrl = [self resImageUrl:modelName suffix:modelSuffix type:@"1"];
    [LoaderModelUtil loadModel:modelUrl
                       modelId:[[_data objectForKey:@"id"] stringValue]
                      userInfo:_data delegate:self];
}

- (void)loadProgress:(float)newProgress modelId:(NSString *)modelId
{
    progress.percent = newProgress;
}

- (void)loadModelComplete:(NSString *)modelId userInfo:(NSDictionary *)userInfo
{
    [progress removeFromSuperview];
    [_delegate modelItemSelected:_data];
}

- (NSString*)resImageUrl:(NSString *)name suffix:(NSString *)suffix type:(NSString *)type
{
    NSString *str = [NSString stringWithFormat:@"%@upload/%@/%@/%@_%@.%@", mainURL, suffix, name, name, type, suffix];
    return str;
}

@end
