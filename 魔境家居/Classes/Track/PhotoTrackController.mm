//
//  ViewController.m
//  PhotoAR
//
//  Created by 施正士 on 15/7/9.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "PhotoTrackController.h"
#import "ModelListViewCtl.h"
#import "Global.h"
#import "TextureListViewCtrl.h"
#import "UIButton+JUtil.h"
//#import "WebBrowerCtrl.h"
//#import "SpaceDetailContrl.h"
#import "BorderView.h"
#import "SBJson.h"
#import "SaveTrackCtrl.h"
#import "UIImageView+JUtil.h"
#import "ServiceModel.h"
#import "MojingTrackController.h"
#import "Loading.h"
#import "LoaderImageUtil.h"
#import "Alert.h"

@interface PhotoTrackController () <MojingTrackDelegate>
{
    MojingTrackController *trackCtrl;
    ModelListViewCtl *modelList;
    TextureListViewCtrl *textureList;
    BorderView *baseInfo;
    
    UIButton *modelBtn;
    BOOL isModelOpen;
    BOOL isTextureOpen;
    
    UIButton *infoBtn;
    UIButton *saveBtn;
    UIButton *deleteBtn;
    UIButton *photoBtn;
    UIButton *backBtn;
    UIButton *lockBtn;
    UIButton *unlockBtn;
    UIButton *textureBtn;
    NSMutableDictionary *_data;
    NSDictionary *trackData;
    NSArray *_modelList;
    
    NSMutableArray *needLoadModels;
    int screenWidth;
    int screenHeight;
    
    UIView *loadingContainer;
    
    UIImageView *roationView;
    BOOL startGuide;
}


@end

@implementation PhotoTrackController

- (id)initWithItem:(NSDictionary*)data
{
    self = [super init];
    if (self) {
        _data = [NSMutableDictionary dictionaryWithDictionary:data];
    }
    return self;
}


- (id)initWithModels:(NSArray*)models
{
    self = [super init];
    if (self) {
        _modelList = models;
    }
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    if (iPad) {
        screenWidth = SCREEN_WIDTH;
        screenHeight = SCREEN_HEIGHT;
    }else{
        screenWidth = SCREEN_HEIGHT;
        screenHeight = SCREEN_WIDTH;
    }
    
    
    if (_data && [_data objectForKey:@"collocationData"]) {
        NSString *str = [_data objectForKey:@"collocationData"];
        NSDictionary *data = [str JSONValue];
        [_data setObject:data forKey:@"collocationData"];
        
        //{@"sceneBgImageUrl":@"背景图片地址", @"collocationData":@"场景配置数据"}
        NSString *suffix = [_data objectForKey:@"bgImageSuffix"];
        NSString *thumbName = [_data objectForKey:@"bgImageName"];
        NSString *thumbUrl = [self resImageUrl:thumbName suffix:suffix type:@"1"];
        NSString *bgImageID = [_data objectForKey:@"bgImageID"];
        NSDictionary *mpdata = [data objectForKey:@"data"];
        trackData = @{@"sceneBgImageUrl":thumbUrl,@"sceneBgImageID":bgImageID, @"collocationData":mpdata};
    }else if(_data){
        NSString *suffix = [_data objectForKey:@"imageSuffix"];
        NSString *thumbName = [_data objectForKey:@"imageName"];
        NSString *thumbUrl = [self resImageUrl:thumbName suffix:suffix type:@"1"];
        NSString *bgImageID = [_data objectForKey:@"imageID"];
        NSString *str = [_data objectForKey:@"sceneData"];
        NSDictionary *mpdata = [str JSONValue];
        trackData = @{@"sceneBgImageUrl":thumbUrl,@"sceneBgImageID":bgImageID, @"collocationData":mpdata};
    }
    
    
    if (trackData) {
        [Loading show:@"正在加载"];
        [LoaderImageUtil loadImage:[trackData objectForKey:@"sceneBgImageUrl"] delegate:self];
    }else{
        [self loadImageComplete:nil];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(texturePlaneOpen:)
                                                 name:@"TexturePlaneOpen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSpace:)
                                                 name:@"SpaceInfo" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addModelNotfi:)
                                                 name:@"AddModelNotif" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeModelTexture:)
                                                 name:@"ChangeModelTexture" object:nil];
    
    
    if (!iPad) {
        roationView = [UIImageView imageNamed:@"gd_roation.png" origin:CGPointZero];
        roationView.layer.cornerRadius = 5;
        roationView.layer.masksToBounds = YES;
        roationView.alpha = 1.0f;
        [self.view addSubview:roationView];
        [roationView paddingTop:(screenHeight-CGRectGetHeight(roationView.frame))/2];
        [roationView paddingLeft:(screenWidth-CGRectGetWidth(roationView.frame))/2];
        
        [self performSelector:@selector(roationGuideStop) withObject:nil afterDelay:3];
    }
}

- (void)loadImageComplete:(NSString *)file
{
    [Loading hide];
    MojingTrackController *vc = [[MojingTrackController alloc] initWithItem:trackData];
    vc.mdelegate = self;
    [self addChildViewController:vc];
    [self.view addSubview:vc.view];
    trackCtrl = vc;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到当前view
    [vc.view addGestureRecognizer:tapGestureRecognizer];
    
    [self showCollocationUI];
}

- (void)roationGuideStop
{
    [roationView removeFromSuperview];
}

- (void)trackSDKReady
{
    [self performSelector:@selector(startLoadModel) withObject:nil afterDelay:0.3];
}

- (void)startLoadModel
{
//    if ([_data objectForKey:@"productList"]) {
//        NSDictionary *data = [_data objectForKey:@"collocationData"];
//        NSArray *models = [data objectForKey:@"models"];
//        needLoadModels = [NSMutableArray arrayWithArray:models];
//        if ([needLoadModels count]>0) {
//            [self showLoading];
//        }
//        [self loadModel];
//    }
    
    if ([_data objectForKey:@"productList"]) {
        NSDictionary *data = [_data objectForKey:@"collocationData"];
        NSArray *models = [data objectForKey:@"models"];
        needLoadModels = [NSMutableArray arrayWithArray:models];
        if ([needLoadModels count]>0) {
            [self showLoading];
        }
        [self loadModel];
    }
    else if (_modelList)
    {
        needLoadModels = [NSMutableArray arrayWithArray:_modelList];
        if ([needLoadModels count]>0) {
            [self showLoading];
        }
        [self loadModel];
    }
}

- (void)loadModel
{
    if ([needLoadModels count]>0) {
        NSDictionary *model = [needLoadModels objectAtIndex:0];
        [needLoadModels removeObjectAtIndex:0];
        if (_data) { // 编辑作品
            NSString *mid = [[model objectForKey:@"id"] stringValue];
            NSArray *goods = [_data objectForKey:@"productList"];
            for(int j=0; j<[goods count]; j++)
            {
                NSDictionary *goodsDic = [goods objectAtIndex:j];
                NSString *goodId = [[goodsDic objectForKey:@"id"] stringValue];
                //            NSLog(@"ggooo %@ %@", goodId, mid);
                if ([goodId isEqualToString:mid]) {
                    NSString *modelName = [goodsDic objectForKey:@"modelName"];
                    NSString *modelSuffix = [goodsDic objectForKey:@"modelSuffix"];
                    NSString *modelUrl = [self resImageUrl:modelName suffix:modelSuffix type:@"1"];
                    [LoaderModelUtil loadModel:modelUrl
                                       modelId:[[goodsDic objectForKey:@"id"] stringValue]
                                      userInfo:@{@"model":goodsDic, @"info":model} delegate:self];
                    break;
                }
            }
        }else{ // 从商品详情入口进入
            NSString *modelName = [model objectForKey:@"modelName"];
            NSString *modelSuffix = [model objectForKey:@"modelSuffix"];
            NSString *modelUrl = [self resImageUrl:modelName suffix:modelSuffix type:@"1"];
            [LoaderModelUtil loadModel:modelUrl
                               modelId:[[model objectForKey:@"id"] stringValue]
                              userInfo:@{@"model":model} delegate:self];
        }
    }else{
        [self hideLoading];
    }
}

- (void)showLoading
{
    if (!loadingContainer) {
        // loading动画
        loadingContainer = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH-80, SCREEN_HEIGHT-30,  70, 20)];
//        loadingContainer.backgroundColor = UIColorFromRGB(0x666666);
//        loadingContainer.layer.cornerRadius = 6;
//        loadingContainer.layer.masksToBounds = YES;
        UIActivityIndicatorView *iv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [iv startAnimating];
        iv.frame = CGRectMake(0, 0, 20, 20);
        [loadingContainer addSubview:iv];
        UILabel *txt = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, 60, 20)];
        txt.backgroundColor = [UIColor clearColor];
        txt.font = [UIFont systemFontOfSize:12];
        txt.textColor = [UIColor whiteColor];
        txt.text = @"加载模型";
        [loadingContainer addSubview:txt];
        [self.view addSubview:loadingContainer];
    }
    loadingContainer.hidden = NO;
}

- (void)hideLoading
{
    if (loadingContainer) {
        loadingContainer.hidden = YES;
    }
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)prefersStatusBarHidden
{
    return YES; // 返回NO表示要显示，返回YES将hiden
}

- (void)loadModelComplete:(NSString *)modelId userInfo:(NSDictionary *)userInfo
{
    [trackCtrl addModel:[userInfo objectForKey:@"model"] info:[userInfo objectForKey:@"info"]];
    [self performSelector:@selector(loadModel) withObject:nil afterDelay:0.3];
}

// 添加模型事件
- (void)addModelNotfi:(NSNotification*)notif
{
    NSDictionary *userInfo = [notif userInfo];
    [trackCtrl addModel:userInfo info:nil];
}


// 换材质
- (void)changeModelTexture:(NSNotification*)notif
{
    NSDictionary *userInfo = [notif userInfo];
    [trackCtrl changeModelTexture:userInfo];
}

- (void)photoHander
{
    [photoBtn removeFromSuperview];
    [trackCtrl onTakePicture];
    saveBtn.hidden = NO;
}

- (void)showCollocationUI
{
    // 按钮
    backBtn = [UIButton imageNamed:@"match_back" origin:CGPointMake(20, 20)];
    [self.view addSubview:backBtn];
    [backBtn addTarget:self action:@selector(backHandler) forControlEvents:UIControlEventTouchUpInside];
    
    // 材质列表按钮
    textureBtn = [UIButton imageNamed:@"dp_texture.png" origin:CGPointMake(20, screenHeight+10)];
    [self.view addSubview:textureBtn];
    [textureBtn addTarget:self action:@selector(textureHandler) forControlEvents:UIControlEventTouchUpInside];
    textureBtn.hidden = YES;
    
    // 按钮
    infoBtn = [UIButton imageNamed:@"match_details" origin:CGPointMake(screenWidth-290, -50)];
    deleteBtn = [UIButton imageNamed:@"match_delete" origin:CGPointMake(screenWidth-230, -50)];
    lockBtn = [UIButton imageNamed:@"match_amplifier" origin:CGPointMake(screenWidth-170, 20)];
    unlockBtn = [UIButton imageNamed:@"match_amplifier_clear"
                              origin:CGPointMake(screenWidth-170, 20)];
    saveBtn = [UIButton imageNamed:@"match_storage" origin:CGPointMake(screenWidth-110, 20)];
    unlockBtn.hidden = YES;
//    infoBtn.hidden = YES;
    
    [self.view addSubview:infoBtn];
    [self.view addSubview:lockBtn];
    [self.view addSubview:unlockBtn];
    [self.view addSubview:deleteBtn];
    [self.view addSubview:saveBtn];
    
    [infoBtn addTarget:self action:@selector(infoHandler) forControlEvents:UIControlEventTouchUpInside];
    [lockBtn addTarget:self action:@selector(lockHandler) forControlEvents:UIControlEventTouchUpInside];
    [unlockBtn addTarget:self action:@selector(unlockHandler) forControlEvents:UIControlEventTouchUpInside];
    [deleteBtn addTarget:self action:@selector(deleteHandler) forControlEvents:UIControlEventTouchUpInside];
    [saveBtn addTarget:self action:@selector(saveHandler) forControlEvents:UIControlEventTouchUpInside];
    
    
    if (!_data) {
        photoBtn = [UIButton imageNamed:@"dp_photo1.png" origin:CGPointMake(screenWidth-110, 20)];
        [self.view addSubview:photoBtn];
        [photoBtn addTarget:self action:@selector(photoHander) forControlEvents:UIControlEventTouchUpInside];
        photoBtn.hidden = YES;
    }
    
    textureList = [[TextureListViewCtrl alloc] init];
    textureList.view.frame = CGRectMake(70, screenHeight-70, screenWidth-70, 70);
    [self addChildViewController:textureList];
    [self.view addSubview:textureList.view];
    textureList.view.hidden = YES;
    
    // 模型列表
    modelBtn = [UIButton imageNamed:@"match_product_list" origin:CGPointMake(screenWidth-35, 15)];
    [self.view addSubview:modelBtn];
    [modelBtn addTarget:self action:@selector(openHandler)
       forControlEvents:UIControlEventTouchUpInside];
    
    modelList = [[ModelListViewCtl alloc] init];
    modelList.view.frame = CGRectMake(screenWidth, 0, 205, screenHeight);
    [self addChildViewController:modelList];
    [self.view addSubview:modelList.view];
    isModelOpen = NO;

//    NSString *value = [LocalData getLocalDataWithKey:@"track" localData:GUIDE_APP];
//    if (!value) {
//        startGuide = YES;
//        [LocalData setLocalDataWithKey:@"track" value:@"1" localData:GUIDE_APP];
//    }
//    if (startGuide) {
//        GuideMainCtroller *guide = [[GuideMainCtroller alloc] init];
//        [guide popGuide:@"gd_dp_opengoods.png" rect:modelBtn.frame
//               fromView:self.view toViewCtrl:self direct:1 hasNext:NO];
//    }
}

- (void)textureHandler
{
    [self refreshTexturePlane];
}

- (void)backHandler
{
    [self dismissViewControllerAnimated:YES completion:nil];
   // [self.navigationController popViewControllerAnimated:YES];
}

- (void)dismissViewController
{
    [self backHandler];
//    [UIManager gotoHome:YES];
}

//- (void)saveHandler
//{
//    if (!_data) {
//        [trackCtrl onTakePicture];
//    }else{
//        [trackCtrl onSaveScreen];
//    }
//}

- (void)saveHandler
{
    if ([[trackCtrl getModelsWithRect] count]==0)
    {
        [Alert show:@"提示" msg:@"您还没有添加任何商品哦，请试试在右侧的商品栏中选择商品吧！" delegate:nil];
    }
    else
    {
        if (!_data) {
            [trackCtrl onTakePicture];
        }else{
            [trackCtrl onSaveScreen];
        }
    }
}

- (void)infoHandler
{
//    NSDictionary *modelItem = [trackCtrl getCurrModelData];
//    NSString *buyUrl = [modelItem objectForKey:@"buyUrl"];
    if (baseInfo) {
        [baseInfo removeFromSuperview];
        baseInfo = nil;
    }
    if (!baseInfo) {
        baseInfo = [[BorderView alloc] initWithFrame:CGRectMake(12, 12, 250, 0)];
        baseInfo.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        baseInfo.layer.cornerRadius = 3;
        [self.view addSubview:baseInfo];
        
        NSString *info ;
        NSDictionary *modelData = [trackCtrl getCurrModelData];
        if (modelData) {
            NSDictionary *brand = [modelData objectForKey:@"brandData"];
            info = [NSString stringWithFormat:@"%@\n品牌：%@\n价格：￥%@", [modelData objectForKey:@"name"], [brand objectForKey:@"name"], [modelData objectForKey:@"price"]];
        }
        
        UIFont *font = [UIFont systemFontOfSize:14];
        CGSize labelSize = [Global getStringHeight:font text:info maxWidth:250];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, labelSize.width, labelSize.height)];
        label.font = font;
        label.textColor = [UIColor whiteColor];
        label.numberOfLines = 0;
        label.text = info;
        [baseInfo addSubview:label];
        baseInfo.frame = CGRectMake((screenWidth-270)/2, (screenHeight-labelSize.height)/2,
                                    250, labelSize.height+20);
        [baseInfo refresh];
    }
//    
//    baseInfo.hidden = NO;

//    WebBrowerCtrl *webBrower = [[WebBrowerCtrl alloc] initWIthWebURL:buyUrl];
//    [self presentViewController:webBrower animated:YES completion:nil];
}

- (void)lockHandler
{
    lockBtn.hidden = YES;
    unlockBtn.hidden = NO;
    [trackCtrl lockZoom:NO];
}

- (void)unlockHandler
{
    lockBtn.hidden = NO;
    unlockBtn.hidden = YES;
    [trackCtrl lockZoom:YES];
}

- (void)deleteHandler
{
    [trackCtrl removeCurrModel];
}

- (void)texturePlaneOpen:(NSNotification*)noti
{
    NSDictionary *userInfo = [noti userInfo];
    BOOL isOpenTexture = [[userInfo objectForKey:@"open"] boolValue];
//    NSDictionary *model = [userInfo objectForKey:@"model"];
    
    isTextureOpen = isOpenTexture;
//    [self refreshTexturePlane];
    if(!isTextureOpen) textureList.view.hidden = YES;
    
    [self refreshButton];
    
    isModelOpen = NO;
    [self refreshModelPlane];
    
//    if (isOpenTexture && startGuide) {
//        GuideMainCtroller *guide = [[GuideMainCtroller alloc] init];
//        guide.delegate = self;
//        guide.index = 1;
//        [guide popGuide:@"gd_dp_info.png" rect:infoBtn.frame
//               fromView:self.view toViewCtrl:self direct:1 hasNext:YES];
//        
//        startGuide = NO;
//    }
}

- (void)guideRemove:(int)index
{
//    if (index==1) {
//        GuideMainCtroller *guide = [[GuideMainCtroller alloc] init];
//        guide.delegate = self;
//        guide.index = 2;
//        [guide popGuide:@"gd_dp_delete.png" rect:deleteBtn.frame
//               fromView:self.view toViewCtrl:self direct:1  hasNext:YES];
//    }else if (index==2){
//        GuideMainCtroller *guide = [[GuideMainCtroller alloc] init];
//        guide.delegate = self;
//        guide.index = 3;
//        [guide popGuide:@"gd_dp_zoom.png" rect:unlockBtn.frame
//               fromView:self.view toViewCtrl:self direct:1  hasNext:YES];
//    }else if (index==3){
//        GuideMainCtroller *guide = [[GuideMainCtroller alloc] init];
//        guide.delegate = self;
//        guide.index = 4;
//        [guide popGuide:@"gd_dp_save.png" rect:saveBtn.frame
//               fromView:self.view toViewCtrl:self direct:1  hasNext:YES];
//    }else if(index==4){
//        GuideMainCtroller *guide = [[GuideMainCtroller alloc] init];
//        guide.delegate = self;
//        guide.index = 5;
//        [guide popView:@"gd_guster.png" toViewCtrl:self];
//    }
}


// 材质面板
- (void)refreshTexturePlane
{
    
    textureList.view.hidden = !textureList.view.hidden;
//    [UIView beginAnimations:@"textureopen" context:nil];
//    [UIView setAnimationDuration:0.3];
//    textureList.view.frame = CGRectMake(0, screenHeight-(isTextureOpen?50:0), screenWidth, 50);
//    [UIView commitAnimations];
}

// 模型面板
- (void)refreshModelPlane
{
    int w = 355;
    modelBtn.hidden = isModelOpen;
    [UIView beginAnimations:@"modelopen" context:nil];
    [UIView setAnimationDuration:0.3];
    modelList.view.frame = CGRectMake(screenWidth-(isModelOpen?w:0), 0, w, screenHeight);
    [UIView commitAnimations];
}

// 按钮面板
- (void)refreshButton
{
    [UIView beginAnimations:@"buttonopen" context:nil];
    [UIView setAnimationDuration:0.3];
    [infoBtn paddingTop:(isTextureOpen?20:-50)];
    [deleteBtn paddingTop:(isTextureOpen?20:-50)];
    [textureBtn paddingBottom:(isTextureOpen?20:-50)];
    [UIView commitAnimations];
    
    if (baseInfo) {
//        baseInfo.hidden = YES;
        [baseInfo removeFromSuperview];
        baseInfo = nil;
    }
}

- (void)openHandler
{
    isModelOpen = !isModelOpen;
    modelBtn.hidden = isModelOpen;
    [self refreshModelPlane];
}

- (void)showSpace:(NSNotification*)userInfo
{
//    SpaceDetailContrl *space = [[SpaceDetailContrl alloc] init];
//    [self addChildViewController:space];
//    [self.view addSubview:space.view];
}

- (void)saveTrackDelegate:(NSString*)data
                 products:(NSArray*)products
                    image:(UIImage*)image
                 sceneImg:(UIImage*)sceneImg
                sceneBgId:(NSString*)sceneBgId;
{
    SaveTrackCtrl *saveCtrl = [[SaveTrackCtrl alloc] initWithData:data products:products
                                                            image:image sceneImg:sceneImg
                                                        sceneBgId:sceneBgId];
    saveCtrl.delegate = self;
    [self addChildViewController:saveCtrl];
    [self.view addSubview:saveCtrl.view];
}

- (void)savePanelRemove
{
//    [trackCtrl onClearScreen];
}

- (void)viewDidDisappear:(BOOL)animated
{
   
}

-(void)keyboardHide:(UITapGestureRecognizer*)tap{
    [self.view endEditing:YES];
}

- (NSString*)resImageUrl:(NSString *)name suffix:(NSString *)suffix type:(NSString *)type
{
    NSString *str = [NSString stringWithFormat:@"%@upload/%@/%@/%@_%@.%@", mainURL, suffix, name, name, type, suffix];
    return str;
}

@end
