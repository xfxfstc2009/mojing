//
//  SaveTrackCtrl.h
//  PhotoAR
//
//  Created by 施正士 on 15/8/25.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoTrackController.h"

@interface SaveTrackCtrl : UIViewController

@property (nonatomic, weak) id<PhotoTrackDelegate> delegate;

- (id)initWithData:(NSString*)data products:(NSArray*)products
             image:(UIImage*)image sceneImg:(UIImage*)sceneImg sceneBgId:(NSString*)sceneBgId;

@end
