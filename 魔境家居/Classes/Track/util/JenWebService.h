//
//  WebService.h
//  ChildRoad_iPad
//
//  Created by jenth on 11-5-1.
//  Copyright 2011 ChildRoad. All rights reserved.
//

#import <Foundation/Foundation.h>

#define GET @"GET"
#define POST @"POST"

@class ASIFormDataRequest;

typedef void(^Completion)(BOOL, NSDictionary*, NSDictionary*);

@interface JenWebService : NSObject {
    Completion _callBack;
    ASIFormDataRequest *formDataRequest;
     NSMutableData *_data;
}

@property (nonatomic, retain) NSDictionary *userInfo;

+ (id)requestWithUrl:(NSString *)url
              params:(NSDictionary *)params
              method:(NSString*)method
            userInfo:(NSDictionary*)userInfo
            callBack:(Completion)callBack;

- (id)initWithUrl:(NSString *)url
           params:(NSDictionary *)params
           method:(NSString*)method
         userInfo:(NSDictionary*)userInfo
         callBack:(Completion)callBack;

@end
