//
//  Global.h
//  YouShelf
//
//  Created by jenth on 13-11-21.
//  Copyright (c) 2013年 ChildRoad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define App_Version @"1.3.1"

// 判断机型 是否是iphone5
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

//放大版的iphone6等于Iphone5的分辨率
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size)) : NO)

#define iPhone6plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? (CGSizeEqualToSize(CGSizeMake(1125, 2001), [[UIScreen mainScreen] currentMode].size) || CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size)) : NO)


// 判断是否是iPad 
#define iPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? YES : NO)

// ios6及以下系统版本
#define isIOS6_1 (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)

// RGB
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

// 获取屏幕尺寸
#define SCREEN_WIDTH  [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT  [[UIScreen mainScreen] bounds].size.height

#define MAIN_WINDOW [[UIApplication sharedApplication].delegate window]

// 头部不透明状态下，需计算高度
#define TOP_BAR_HEGIHT (iPad ? 63 : 63)

#define RIGHT_BARITEM_FONT 15

#define HOME_FRAME iPad ? CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-63) : CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-TOP_BAR_HEGIHT-50)
#define PERSONAL_FRAME CGRectMake(301, 0, SCREEN_WIDTH-301, SCREEN_HEIGHT-50)

#define SEVER_URL @"http://maichang.duc.cn/maichang/testSZS/"

#define COLOR_BLUE 0x4fbda0

#define COLOR_BG 0xf0f0f0

#define COLOR_BOTTOM 0xf5f5f5

#define COLOR_LINE 0xb2b2b2

#define COLOR_FENGGE 0xe0e0df

@interface Global : NSObject

+ (CGSize)getStringLength:(UIFont*)font text:(NSString*)text;
+ (CGSize)getStringHeight:(UIFont*)font text:(NSString*)text maxWidth:(int)maxWidth;
+ (NSString*)getRes:(NSString *)str;

extern BOOL validResp(BOOL b, NSDictionary *data);
extern NSString* trimStr(NSString *str);
extern NSString* getRes(NSString *str);

@end
