//
//  UIImageView+JUtil.m
//  ChildRoad_Single
//
//  Created by jenth on 12-8-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "UIImageView+JUtil.h"

@implementation UIImageView(UIImageView_JUtil)

// 图片名称生成图片
+ (UIImageView *)imageNamed:(NSString *)name origin:(CGPoint)origin
{
    UIImage *img = [UIImage imageNamed:name];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    CGRect f ;
    f.origin = origin;
    f.size = img.size;
    imgView.frame = f;
    
    return imgView;
}

- (void)paddingLeft:(float)value
{
    CGRect f = self.frame;
    f.origin.x = value;
    self.frame = f;
}

- (void)paddingRight:(float)value
{
    if ([self superview]) {
        CGRect rect=[[self superview] bounds];
        CGRect f = self.frame;
        f.origin.x = rect.size.width-f.size.width-value;
        self.frame = f;
    }
}

- (void)paddingTop:(float)value
{
    CGRect f = self.frame;
    f.origin.y = value;
    self.frame = f;
}

- (void)paddingBottom:(float)value
{
    if ([self superview]) {
        CGRect rect=[[self superview] bounds];
        CGRect f = self.frame;
        f.origin.y = rect.size.height-f.size.height-value;
        self.frame = f;
    }
}

- (void)horizontalCenter:(float)value
{
    if ([self superview]) {
        CGRect rect=[[self superview] bounds];
        CGRect f = self.frame;
        f.origin.x = (rect.size.width-f.size.width)/2+value;
        self.frame = f;
    }
}

- (void)verticalCenter:(float)value
{
    if ([self superview]) {
        CGRect rect=[[self superview] bounds];
        CGRect f = self.frame;
        f.origin.y = (rect.size.height-f.size.height)/2+value;
        self.frame = f;
    }
}


@end
