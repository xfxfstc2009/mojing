//
//  UIButton+JUtil.h
//  ChildRoad_Single
//
//  Created by jenth on 12-8-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton(UIButton_JUtil)

+ (UIButton *)imageNamed:(NSString *)name origin:(CGPoint)origin;
- (void)paddingLeft:(float)value;
- (void)paddingRight:(float)value;
- (void)paddingTop:(float)value;
- (void)paddingBottom:(float)value;
- (void)horizontalCenter:(float)value;
- (void)verticalCenter:(float)value;

@end
	