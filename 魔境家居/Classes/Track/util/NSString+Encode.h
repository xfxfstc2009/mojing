//
//  NSString+md5.h
//  Ojia_iPad
//
//  Created by jenth on 12-2-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_Encode){
    
    
}

- (NSString *) unicodeDecode;
- (NSString *) decodeString;
- (NSString*) encodeString;

@end
