//
//  BorderView.h
//  HuaTuo
//
//  Created by jenth on 14-3-19.
//  Copyright (c) 2014年 HuaTuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BorderView : UIView
{
    int _color;
    UIView *line1;
    UIView *line2;
    UIView *line3;
    UIView *line4;
}

- (void)refresh;
- (id)initWithFrame:(CGRect)frame lineColor:(int)color;
- (void)setLineColor:(int)color;

@end
