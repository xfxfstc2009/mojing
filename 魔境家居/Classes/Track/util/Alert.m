//
//  Alert.m
//  ChildRoad_iPad
//
//  Created by jenth on 11-8-23.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Alert.h"

@implementation Alert


+(void) show:(NSString *)title msg:(NSString *)msg delegate:(id)delegate
{
    
	UIAlertView *alert = [[UIAlertView alloc]
						  initWithTitle:title
						  message:msg
						  delegate:delegate
						  cancelButtonTitle:@"确定"
						  otherButtonTitles:nil];
    for(UIView *subview in alert.subviews)
    {
        if([[subview class] isSubclassOfClass:[UILabel class]])
        {
            UILabel *label = (UILabel*)subview;
            label.textAlignment = NSTextAlignmentLeft;
        }
    }
	[alert show];
//	[alert release];
	
}

+(void) show:(NSString *)title msg:(NSString *)msg btnTitle:(NSString *)btnTitle delegate:(id)delegate
{
	UIAlertView *alert = [[UIAlertView alloc]
						  initWithTitle:title
						  message:msg
						  delegate:delegate
						  cancelButtonTitle:btnTitle
						  otherButtonTitles:nil];
	[alert show];
//	[alert release];
	
}

+(void) confirm:(NSString *)title msg:(NSString *)msg otherTitle:(NSString *)ot delegate:(id)delegate
{
	UIAlertView *alert = [[UIAlertView alloc] 
						  initWithTitle:title
						  message:msg
						  delegate:delegate
						  cancelButtonTitle:ot
						  otherButtonTitles:@"取消", nil];
	[alert show];
//	[alert release];
	
}


+(void) confirmAlignment:(NSString *)title msg:(NSString *)msg otherTitle:(NSString *)ot delegate:(id)delegate
{
	UIAlertView *alert = [[UIAlertView alloc] 
						  initWithTitle:title
						  message:msg
						  delegate:delegate
						  cancelButtonTitle:ot
						  otherButtonTitles:@"取消", nil];
    
    int i=0;
    for(UIView *subview in alert.subviews)
    {
        if([[subview class] isSubclassOfClass:[UILabel class]])
        {
            if (i!=0) {
                UILabel *label = (UILabel*)subview;
                label.textAlignment = NSTextAlignmentLeft;
            }
            i++;
        }
    }
	[alert show];
//	[alert release];
	
}

@end
