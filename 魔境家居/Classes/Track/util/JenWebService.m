/** 
   后台通讯类 
   此类底层通讯,无需要请勿修改
**/ 
//  WebService.m
//  ChildRoad_iPad
//
//  Created by jenth on 11-5-1.
//  Copyright 2011 childroad. All rights reserved.
//

#import "JenWebService.h"
#import "SBJson.h"
#import "ASIFormDataRequest.h"
#import "MBProgressHUD.h"
@implementation JenWebService

@synthesize userInfo=_userInfo;


static NSMutableArray *queue;

+ (void)addQueue:(JenWebService*)item
{
    if (queue==nil) {
        queue = [NSMutableArray arrayWithCapacity:10];
    }
    [queue addObject:item];
}

+ (void)removeQueue:(JenWebService*)item
{
    if (queue && [queue containsObject:item]) {
        [queue removeObject:item];
    }
}


/** 静态方法  向后台请求数据 
  url       请求地址
  postData  post数据
  delegate  请求操作结束后代理
  param     携带的参数, 请求中没用到,用于代理操作
**/ 
+ (id)requestWithUrl:(NSString *)url
              params:(NSDictionary *)params
              method:(NSString*)method
            userInfo:(NSDictionary*)userInfo
            callBack:(Completion)callBack
{
    JenWebService *item = [[self alloc] initWithUrl:url
                                             params:params
                                             method:method
                                        userInfo:userInfo
                                        callBack:callBack];
    [JenWebService addQueue:item];
	return item;
}


/***** 向后台请求数据内部方法
   类内部方法 参数和静态方法一样
 
 *****/
- (id)initWithUrl:(NSString *)url 
           params:(NSDictionary *)params
           method:(NSString*)method
         userInfo:(NSDictionary*)userInfo
         callBack:(Completion)callBack
{
    _callBack = callBack;
    self.userInfo = userInfo;
    float r = arc4random();

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    if (params) {
        NSString *postStr=@"";
        NSArray *allKeys = [params allKeys];
        for(int i=0; i<[allKeys count]; i++){
            NSString *key = [allKeys objectAtIndex:i];
            if (i==0) {
                postStr = [NSString stringWithFormat:@"%@=%@", key, [params objectForKey:key]];
            }else{
                postStr = [NSString stringWithFormat:@"%@&%@=%@", postStr, key, [params objectForKey:key]];
            }
        }
        
        if ([method isEqualToString:POST]) {
            NSData *postBody = [postStr dataUsingEncoding:NSASCIIStringEncoding
                                     allowLossyConversion:YES];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:postBody];
            [request setTimeoutInterval:10.0];
        }else{
            url = [NSString stringWithFormat:@"%@%@%@", url, [url rangeOfString:@"?"].location==NSNotFound?@"?":@"&", postStr];
        }
        
    }
    
    url = [NSString stringWithFormat:@"%@%@%f", url, [url rangeOfString:@"?"].location==NSNotFound?@"?":@"&", r];
    request.URL = [NSURL URLWithString:url];

    NSLog(@"request url：%@",url);
//    NSURL *reqUrl = [NSURL URLWithString:url];
//    NSURLRequest *request = [NSURLRequest requestWithURL:reqUrl];
    NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];
    
    [conn start];
    
    return self;
}

#pragma mark -
#pragma mark Connection DelegateMethod

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
//    NSLog(@"收到回应");
    if (!_data)
    {
        _data = [NSMutableData data];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
//    NSLog(@"收到数据");
    [_data appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *responseString = [[NSString alloc] initWithData:_data encoding:NSUTF8StringEncoding];
     _data = nil;
    NSLog(@"requestFinished:%@", responseString);
    
	NSMutableDictionary *response = [responseString JSONValue];
    if (response) {
        _callBack(YES, response, _userInfo);
    }else{
//        [Alert show:@"" msg:@"请求数据失败，请稍后重试。" delegate:nil];
        _callBack(NO, nil, _userInfo);
    }
    
    [JenWebService removeQueue:self];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"error:%@",error);
    _callBack(NO, nil, _userInfo);
    
    [self connectTimeOutWithError:error];
    
    [JenWebService removeQueue:self];
//    assert(error);
}


#pragma mark 连接服务器超时
- (void)connectTimeOutWithError:(NSError *)error
{
    if ((error.code == -1004) || (error.code == -1001)) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
        hud.removeFromSuperViewOnHide =YES;
        hud.mode = MBProgressHUDModeCustomView;
        hud.labelText = NSLocalizedString(@"连接服务器超时,请稍后重试", nil);
        // hud.minSize = CGSizeMake(100, 100);
        hud.layer.cornerRadius = 50;
        [hud hide:YES afterDelay:1.5];
    }
}


///*** 
//  请求完成
//  由ASIFormDataRequest 调用的回调
// 
// ****/
//- (void)requestFinished:(ASIHTTPRequest *)request
//{	
//	NSString *responseString = [request responseString];
//	
////	responseString = [responseString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
////    responseString = [responseString stringByReplacingOccurrencesOfString:@"\t" withString:@""];
////    responseString = [responseString stringByReplacingOccurrencesOfString:@"\"id\":," withString:@""];
////    responseString = [responseString stringByReplacingOccurrencesOfString:@",]" withString:@"]"];
////    responseString = [responseString stringByReplacingOccurrencesOfString:@"\\" withString:@"\\\\"];
//    NSLog(@"requestFinished:%@", responseString);
//    
//	NSMutableDictionary *data = [responseString JSONValue];
//    _callBack(YES, data, _userInfo);
//    
//    [JenWebService removeQueue:self];
//}
//
///*** 
// 请求错误
// 由ASIFormDataRequest 调用的回调
//****/
//- (void)requestFailed:(ASIHTTPRequest *)request
//{
//	NSError *error = [request error];
//	NSLog(@"error:%@",error);
//    _callBack(NO, nil, _userInfo);
//    
//    [JenWebService removeQueue:self];
//}

@end
