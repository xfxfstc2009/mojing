//
//  Alert.h
//  ChildRoad_iPad
//
//  Created by jenth on 11-8-23.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Alert : NSObject<UIAlertViewDelegate> {

}

+(void) show:(NSString *)title msg:(NSString *)msg delegate:(id)delegate;
+(void) show:(NSString *)title msg:(NSString *)msg btnTitle:(NSString *)btnTitle delegate:(id)delegate;
+(void) confirm:(NSString *)title msg:(NSString *)msg otherTitle:(NSString *)ot delegate:(id)delegate;
+(void) confirmAlignment:(NSString *)title msg:(NSString *)msg otherTitle:(NSString *)ot delegate:(id)delegate;

@end
