//
//  ImageUpload.h
//  PhotoAR
//
//  Created by 施正士 on 15/8/15.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^Completion)(BOOL, NSDictionary*, NSDictionary*);

@interface ImageUpload : NSObject
{
     NSMutableData *_data;
     Completion _callBack;
}

@property (nonatomic, strong) NSDictionary *userInfo;

+ (id)requestWithUrl:(NSString *)url
            postData:(NSDictionary *)postData
             imgList:(NSArray*)imgList
            userInfo:(NSDictionary*)userInfo
            callBack:(Completion)callBack;
- (id)initRequestWithURL: (NSString *)url
              postParems: (NSDictionary *)postParems
                picFiles:(NSArray*)picFiles
                userInfo:(NSDictionary*)userInfo
                callBack:(Completion)callBack;

@end
