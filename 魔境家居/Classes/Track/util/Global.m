//
//  Global.m
//  YouShelf
//
//  Created by jenth on 13-11-21.
//  Copyright (c) 2013年 ChildRoad. All rights reserved.
//

#import "Global.h"
#import "Alert.h"
#import "Loading.h"

@implementation Global

+ (CGSize)getStringLength:(UIFont*)font text:(NSString*)text
{
    CGSize size = CGSizeMake(MAXFLOAT, 20);
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    CGSize labelSize;
    //    if (isIOS6_1) {
    //        labelSize = [text sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping];
    //    }else{
    labelSize = [text boundingRectWithSize:size
                                   options:NSStringDrawingUsesLineFragmentOrigin
                                attributes:attributes context:nil].size;
    labelSize.height = ceil(labelSize.height);
    labelSize.width = ceil(labelSize.width);
    //    }
    
    return labelSize;
}

+ (CGSize)getStringHeight:(UIFont*)font text:(NSString*)text maxWidth:(int)maxWidth
{
    CGSize size = CGSizeMake(maxWidth, MAXFLOAT);
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
    CGSize labelSize;
    //    if (isIOS6_1) {
    //        labelSize = [text sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping];
    //    }else{
    labelSize = [text boundingRectWithSize:size
                                   options:NSStringDrawingUsesLineFragmentOrigin
                                attributes:attributes context:nil].size;
    labelSize.height = ceil(labelSize.height);
    labelSize.width = ceil(labelSize.width);
    //    }
    
    return labelSize;
}

+ (NSString*)getRes:(NSString *)str
{
    if (iPad) {
        NSArray *ll = [str componentsSeparatedByString:@"."];
        str = [NSString stringWithFormat:@"%@_ipad", [ll objectAtIndex:0]];
    }
    return str;
}

extern BOOL validResp(BOOL b, NSDictionary *data)
{
    if (b) {
        if ([[data objectForKey:@"code"] intValue]==200) {
            return YES;
        }else{
            [Loading hide];
            [Alert show:@"提示" msg:[data objectForKey:@"msg"] delegate:nil];
            return NO;
        }
    }else{
        [Alert show:@"提示" msg:@"网络不给力，请重新尝试。" delegate:nil];
        return NO;
    }
    
}

extern NSString* getRes(NSString *str)
{
    if (iPad) {
        NSArray *ll = [str componentsSeparatedByString:@"."];
        str = [NSString stringWithFormat:@"%@_ipad", [ll objectAtIndex:0]];
    }
    return str;
}

extern NSString* trimStr(NSString *str)
{
    if (str==nil) {
        return @"";
    }
    return [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}


@end
