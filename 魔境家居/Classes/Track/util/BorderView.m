//
//  BorderView.m
//  HuaTuo
//
//  Created by jenth on 14-3-19.
//  Copyright (c) 2014年 HuaTuo. All rights reserved.
//

#import "BorderView.h"
#import "Global.h"

@implementation BorderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _color = 0xe7e5e0;
        self.backgroundColor = [UIColor whiteColor];
        [self refresh];
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame lineColor:(int)color
{
    self = [super initWithFrame:frame];
    if (self) {
        _color = color;
        self.backgroundColor = [UIColor whiteColor];
        [self refresh];
        
    }
    return self;
}

- (void)setLineColor:(int)color
{
    _color = color;
    if (!line1) {
        line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0.5)];
        line1.backgroundColor = UIColorFromRGB(_color);
        [self addSubview:line1];
    }else{
        line1.frame = CGRectMake(0, 0, self.frame.size.width, 0.5);
        line1.backgroundColor = UIColorFromRGB(_color);
    }
    
    if (!line2) {
        line2 = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 0.5)];
        line2.backgroundColor = UIColorFromRGB(_color);
        [self addSubview:line2];
    }else{
        line2.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, 0.5);
        line2.backgroundColor = UIColorFromRGB(_color);
    }
    
    if (!line3) {
        line3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, self.frame.size.height)];
        line3.backgroundColor = UIColorFromRGB(_color);
        [self addSubview:line3];
    }else{
        line3.frame = CGRectMake(0, 0, 0.5, self.frame.size.height);
        line3.backgroundColor = UIColorFromRGB(_color);
    }
    
    if (!line4) {
        line4 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width, 0, 0.5, self.frame.size.height)];
        line4.backgroundColor = UIColorFromRGB(_color);
        [self addSubview:line4];
    }else{
        line4.frame = CGRectMake(self.frame.size.width, 0, 0.5, self.frame.size.height);
        line4.backgroundColor = UIColorFromRGB(_color);
    }
}

- (void)refresh
{
    if (!line1) {
        line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0.5)];
        line1.backgroundColor = UIColorFromRGB(_color);
        [self addSubview:line1];
    }else{
        line1.frame = CGRectMake(0, 0, self.frame.size.width, 0.5);
    }
    
    if (!line2) {
        line2 = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 0.5)];
        line2.backgroundColor = UIColorFromRGB(_color);
        [self addSubview:line2];
    }else{
        line2.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, 0.5);
    }
    
    if (!line3) {
        line3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.5, self.frame.size.height)];
        line3.backgroundColor = UIColorFromRGB(_color);
        [self addSubview:line3];
    }else{
        line3.frame = CGRectMake(0, 0, 0.5, self.frame.size.height);
    }
    
    if (!line4) {
        line4 = [[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width, 0, 0.5, self.frame.size.height)];
        line4.backgroundColor = UIColorFromRGB(_color);
        [self addSubview:line4];
    }else{
        line4.frame = CGRectMake(self.frame.size.width, 0, 0.5, self.frame.size.height);
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
