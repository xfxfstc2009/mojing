//
//  ImageUpload.m
//  PhotoAR
//
//  Created by 施正士 on 15/8/15.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "ImageUpload.h"
#import "SBJson.h"
#import <UIKit/UIKit.h>

@implementation ImageUpload

@synthesize userInfo=_userInfo;

static NSMutableArray *queue;

+ (void)addQueue:(ImageUpload*)item
{
    if (queue==nil) {
        queue = [NSMutableArray arrayWithCapacity:10];
    }
    [queue addObject:item];
}

+ (void)removeQueue:(ImageUpload*)item
{
    if (queue && [queue containsObject:item]) {
        [queue removeObject:item];
    }
}


/** 静态方法  向后台请求数据
 url       请求地址
 postData  post数据
 delegate  请求操作结束后代理
 param     携带的参数, 请求中没用到,用于代理操作
 **/
+ (id)requestWithUrl:(NSString *)url
            postData:(NSDictionary *)postData
             imgList:(NSArray*)imgList
            userInfo:(NSDictionary*)userInfo
            callBack:(Completion)callBack
{
    ImageUpload *item = [[self alloc] initRequestWithURL: (NSString *)url
                                              postParems: (NSDictionary *)postData
                                                picFiles:(NSArray*)imgList
                                                userInfo:(NSDictionary*)userInfo
                                                callBack:(Completion)callBack];
    [ImageUpload addQueue:item];
    return item;
}


- (id)initRequestWithURL: (NSString *)url
                postParems: (NSDictionary *)postParems
                  picFiles:(NSArray*)picFiles
                userInfo:(NSDictionary*)userInfo
                  callBack:(Completion)callBack
{
    self = [super init];
    
    self.userInfo = userInfo;
    _callBack = callBack;
    
    NSDictionary *params = postParems;//@{ @"name": @"默认相册", @"comments": @"描述" };
    //分界线的标识符
    NSString *TWITTERFON_FORM_BOUNDARY = @"AaB03x";
    //根据url初始化request
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:10];
    //分界线 --AaB03x
    NSString *MPboundary=[[NSString alloc]initWithFormat:@"--%@",TWITTERFON_FORM_BOUNDARY];
    //结束符 AaB03x--
    NSString *endMPboundary=[[NSString alloc]initWithFormat:@"%@--",MPboundary];
    
    //http body的字符串
    NSMutableString *body=[[NSMutableString alloc]init];
    //参数的集合的所有key的集合
    NSArray *keys= [params allKeys];
    
    //遍历keys
    for(int i=0;i<[keys count];i++)
    {
        //得到当前key
        NSString *key=[keys objectAtIndex:i];
        //添加分界线，换行
        [body appendFormat:@"%@\r\n",MPboundary];
        //添加字段名称，换2行
        [body appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
        //添加字段的值
        [body appendFormat:@"%@\r\n",[params objectForKey:key]];
    }
    
    //声明结束符：--AaB03x--
    NSString *end=[[NSString alloc]initWithFormat:@"\r\n%@",endMPboundary];
    //声明myRequestData，用来放入http body
    NSMutableData *myRequestData=[NSMutableData data];
    //将body字符串转化为UTF8格式的二进制
    [myRequestData appendData:[body dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    for (int i=0; i<[picFiles count]; i++) {
        NSDictionary *imgItem = [picFiles objectAtIndex:i];
        UIImage *img = [imgItem objectForKey:@"image"];
        NSString *fileName = [imgItem objectForKey:@"name"];
        //http body的字符串
        NSMutableString *fileStr=[[NSMutableString alloc]init];
        ////添加分界线，换行
        [fileStr appendFormat:@"%@\r\n",MPboundary];
        //声明pic字段，文件名为boris.png
        [fileStr appendFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@\"\r\n", fileName];
        //声明上传文件的格式
        [fileStr appendFormat:@"Content-Type: %@\r\n\r\n", [fileName rangeOfString:@"png"].location!=NSNotFound ? @"image/png":@"image/jpeg"];
        [myRequestData appendData:[fileStr dataUsingEncoding:NSUTF8StringEncoding]];
        
        //要上传的图片
        //得到图片的data
        NSData* data = [fileName rangeOfString:@"png"].location!=NSNotFound ? UIImagePNGRepresentation(img) : UIImageJPEGRepresentation(img, 1);
        //将image的data加入
        [myRequestData appendData:data];
        
        [myRequestData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //加入结束符--AaB03x--
    [myRequestData appendData:[end dataUsingEncoding:NSUTF8StringEncoding]];
    
    //设置HTTPHeader中Content-Type的值
    NSString *content=[[NSString alloc]initWithFormat:@"multipart/form-data; boundary=%@",TWITTERFON_FORM_BOUNDARY];
    //设置HTTPHeader
    [request setValue:content forHTTPHeaderField:@"Content-Type"];
    //设置Content-Length
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[myRequestData length]] forHTTPHeaderField:@"Content-Length"];
    //设置http body
    [request setHTTPBody:myRequestData];
    //http method
    [request setHTTPMethod:@"POST"];
    
    //    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    //    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    //
    //    NSLog(@"%@", returnString);
    NSURLConnection *conn = [NSURLConnection connectionWithRequest:request delegate:self];
    [conn start];
    
    return self;
}

#pragma mark -
#pragma mark Connection DelegateMethod

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    //    NSLog(@"收到回应");
    if (!_data)
    {
        _data = [NSMutableData data];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    //    NSLog(@"收到数据");
    [_data appendData:data];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSString *responseString = [[NSString alloc] initWithData:_data encoding:NSUTF8StringEncoding];
    _data = nil;
    NSLog(@"requestFinished:%@", responseString);
    
    NSMutableDictionary *data = [responseString JSONValue];
    _callBack(YES, data, nil);
    [ImageUpload removeQueue:self];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"error:%@",error);
    _callBack(NO, nil, nil);
    [ImageUpload removeQueue:self];
}

@end
