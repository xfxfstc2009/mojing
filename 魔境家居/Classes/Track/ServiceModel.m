//
//  ServiceModel.m
//  PhotoAR
//
//  Created by 施正士 on 15/7/17.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import "ServiceModel.h"
#import "JenWebService.h"
//#import "SeverUrl.h"
#import "SBJson.h"
//#import "NSString+md5.h"
#import "Global.h"
#import "ImageUpload.h"
//#import "UserModel.h"
#import "NSString+Encode.h"

@implementation ServiceModel

@synthesize brandList=_brandList, productCategoryList=_productCategoryList;
@synthesize spaceCategoryList=_spaceCategoryList;

+ (ServiceModel *)getInstance
{
    static ServiceModel *instance;
    
    @synchronized(self)
    {
        if (!instance)
            instance = [[ServiceModel alloc] init];
        
        return instance;
    }
}

- (id)init
{
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (NSDictionary*)getBrandById:(NSString*)bid
{
    if (_brandList) {
        for (int i=0; i<[_brandList count]; i++) {
            NSDictionary *item = [_brandList objectAtIndex:i];
            if ([[[item objectForKey:@"id"] stringValue] isEqualToString:bid]) {
                return item;
            }
        }
    }
    return nil;
}

- (NSDictionary*)getCategoryById:(NSString*)cid
{
    if (_spaceCategoryList) {
        for (int i=0; i<[_spaceCategoryList count]; i++) {
            NSDictionary *item = [_spaceCategoryList objectAtIndex:i];
            if ([[[item objectForKey:@"id"] stringValue] isEqualToString:cid]) {
                return item;
            }
        }
    }
    return nil;
}


// 商品查询
- (void)searchProduct:(int)page brandId:(NSString*)brandId categoryId:(NSString*)categoryId
           searchType:(NSString*)type
                  key:(NSString*)key callBack:(ServiceCallBack)callBack
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                 @"pageNumber":[NSNumber numberWithInt:page],
                                                 @"pageSize":@"20"}];
    if(brandId) [params setObject:brandId forKey:@"brandID"];
    if(categoryId) [params setObject:categoryId forKey:@"productTypeID"];
    if(key) [params setObject:key forKey:@"searchString"];
    if(type) [params setObject:type forKey:@"type"];
    
    [JenWebService requestWithUrl:productListURL params:params method:POST userInfo:nil
                         callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
         callBack(data, userInfo);
     }];
}

- (void)searchProductByParam:(NSDictionary*)param callBack:(ServiceCallBack)callBack
{
    NSMutableDictionary *params;
    if (!param) {
        params = [NSMutableDictionary dictionaryWithCapacity:1];
    }else{
        params = [NSMutableDictionary dictionaryWithDictionary:param];
    }
    
    [params setObject:@"20" forKey:@"pageSize"];
    [JenWebService requestWithUrl:productListURL params:params method:POST userInfo:nil
                         callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
         if (validResp(b, data)) {
            callBack([data objectForKey:@"data"], userInfo); 
         }
         
     }];
}

- (void)productDetail:(NSString*)proId callBack:(ServiceCallBack)callBack
{
    NSDictionary *params = @{@"id":proId};
    
    [JenWebService requestWithUrl:productDetailURL params:params method:POST userInfo:nil
                         callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
         callBack(data, userInfo);
     }];
}

- (void)updateImage:(NSString*)name image:(UIImage*)image callBack:(ServiceCallBack)callBack
{
//    NSDictionary *param = @{@"name":name,
//                            @"panoramaName":[NSString stringWithFormat:@"%@.jpg",name]};
    NSDictionary *img = @{@"image":image, @"name":@"aa.jpg"};
    [ImageUpload requestWithUrl:fileUploadURL postData:nil imgList:@[img] userInfo:nil
                       callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
    {
        if (!validResp(b, data)) return;
        callBack(data, userInfo);
        
    }];
}

- (void)saveScene:(NSString*)title content:(NSString*)content spaceType:(NSString*)spaceType
             data:(NSString*)data products:(NSArray*)products
            image:(UIImage*)image sceneImg:(UIImage*)sceneImg sceneBgId:(NSString*)sceneBgId
         callBack:(ServiceCallBack)callBack
{
    __block NSString *imageid, *loadSceneBgId;
    
    void (^saveFunc)(void) = ^(void){
        
        if (imageid && loadSceneBgId) {
            NSString *productStr = [products componentsJoinedByString:@","];
            NSDictionary *params = @{@"name":title, @"spaceTypeID":spaceType, @"name":content,
                                     @"collocationData":data, @"productIDs":productStr, @"collocationImageID":imageid, @"bgImageID":loadSceneBgId};
            [JenWebService requestWithUrl:worksSaveURL params:params method:POST userInfo:nil
                                 callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
             {
                 callBack(data, userInfo);
             }];
        }
    };
    
    
    [[ServiceModel getInstance] updateImage:@"aa" image:image
                                   callBack:^(NSDictionary *data, NSDictionary *userInfo)
    {
        imageid = [[data objectForKey:@"data"] objectForKey:@"id"];
        saveFunc();
        
    }];
    
    // 图片已存在，编辑状态或选择场景的
    if (!sceneBgId) {
        [[ServiceModel getInstance] updateImage:@"bb" image:sceneImg
                                       callBack:^(NSDictionary *data, NSDictionary *userInfo)
         {
             loadSceneBgId = [[data objectForKey:@"data"] objectForKey:@"id"];
             saveFunc();
         }];
    }else{
        loadSceneBgId = sceneBgId;
    }
    
}


- (void)getBrand:(ServiceCallBack)callBack
{
    [JenWebService requestWithUrl:brandListURL params:nil method:GET userInfo:nil
                         callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
         if (!validResp(b, data)) return;
         self.brandList = [data objectForKey:@"data"];
         callBack(data, userInfo);
     }];
}


- (void)getProductCategory:(ServiceCallBack)callBack
{
//    NSDictionary *param = @{@"parentId":@"10026"};
    [JenWebService requestWithUrl:productTypeListURL params:nil method:GET userInfo:nil
                         callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
         callBack(data, userInfo);
     }];
}

- (void)getSpaceCategory:(ServiceCallBack)callBack
{
    //    NSDictionary *param = @{@"parentId":@"2"};
    [JenWebService requestWithUrl:spaceTypeListURL params:nil method:GET userInfo:nil
                         callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
         NSDictionary *obj = [data objectForKey:@"data"];
         self.spaceCategoryList = [obj objectForKey:@"spaceTypeList"];
         callBack(data, userInfo);
     }];
}


@end
