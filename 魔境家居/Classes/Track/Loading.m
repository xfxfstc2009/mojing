//
//  Loading.m
//  ChildRoad_iPad
//
//  Created by jenth on 11-5-6.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Loading.h"
#import "Global.h"

//static UIWindow *_stage;
static UIView *_loading;
static UIView *_loadingContainer;

@implementation Loading


+ (void) show:(NSString*)msg
{
	[self hide];
	// loading 透明背景
	UIButton *view = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
	UIColor *backColor = [[UIColor alloc] initWithWhite:0.4 alpha:0.4];
	[view setBackgroundColor:backColor];
    [MAIN_WINDOW addSubview:view];
    [view addTarget:self action:@selector(backHander) forControlEvents:UIControlEventTouchUpInside];
    
	// loading动画
    UIView *loadingContainer = [[UIView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-120)/2, (SCREEN_HEIGHT-120)/2,  120, 80)];
    loadingContainer.backgroundColor = UIColorFromRGB(0x666666);
    loadingContainer.layer.cornerRadius = 6;
    loadingContainer.layer.masksToBounds = YES;
    
	UIActivityIndicatorView *iv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	[iv startAnimating];
	iv.frame = CGRectMake((120-40)/2, 10, 40, 40);
	[loadingContainer addSubview:iv];
    UILabel *txt = [[UILabel alloc] initWithFrame:CGRectMake(0, 55, 120, 20)];
    txt.backgroundColor = [UIColor clearColor];
    txt.font = [UIFont systemFontOfSize:13];
    txt.textColor = [UIColor whiteColor];
    txt.textAlignment = NSTextAlignmentCenter;
    txt.text = msg?msg:@"正在加载数据...";
    [loadingContainer addSubview:txt];
	[MAIN_WINDOW addSubview:loadingContainer];
	
    _loading = view;
    _loadingContainer = loadingContainer;
}

+ (void)backHander
{
    [self hide];
}

+ (void) hide{
	if (_loading) {
		[_loading removeFromSuperview];
		_loading = nil;
	}
    if (_loadingContainer) {
        [_loadingContainer removeFromSuperview];
        _loadingContainer = nil;
    }
}




@end
