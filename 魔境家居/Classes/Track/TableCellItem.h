//
//  AccountInfoItem.h
//  HuaTuo
//
//  Created by jenth on 14-3-19.
//  Copyright (c) 2014年 HuaTuo. All rights reserved.
//

#import <UIKit/UIKit.h>

#define Table_CELL_HEIGHT 45

@protocol TableCellItemDelegate <NSObject>

- (void)selectedItem:(NSDictionary*)data;

@end

@interface TableCellItem : UIView
{
    NSDictionary *_data;
}

@property (assign, nonatomic) id<TableCellItemDelegate> delegate;

- (id)initWithFrame:(CGRect)frame data:(NSDictionary*)data;

@end
