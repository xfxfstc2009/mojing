//
//  ModelListViewCtl.h
//  PhotoAR
//
//  Created by 施正士 on 15/7/10.
//  Copyright (c) 2015年 施正士. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelItem.h"

@interface ModelListViewCtl : UIViewController<ModelItemDelegate>

@end
