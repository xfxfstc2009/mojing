//
//  MJVersionUpdateTool.m
//  魔境家居
//
//  Created by lumingliang on 15/12/19.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJVersionUpdateTool.h"

static MJVersionUpdateTool *tool = nil;

@implementation MJVersionUpdateTool

+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        tool = [[MJVersionUpdateTool alloc] init];
    });
    return tool;
}

- (void)compareVersion:(NSString *)version alertTitle:(NSString *)title alertMessage:(NSString *)message 
{
    // 1.获取当前的版本号
    NSString *currentVersion1 = [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"];
    
    NSString *currentVersion = [NSString stringWithFormat:@"%.1f",[currentVersion1 floatValue]];
    // 2.获取上一次的版本号
   // NSString *lastVersion = [MJUserDefaults objectForKey:MJVersionKey];
    
    // 判断当前是否有新的版本
    if ([currentVersion floatValue] < [version floatValue]) {
        // 有最新的版本号
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"立即更新" otherButtonTitles:@"暂不更新", nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        if (_update) {
            _update();
        }
    }else if (buttonIndex == 1)
    {
        
    }
}

@end
