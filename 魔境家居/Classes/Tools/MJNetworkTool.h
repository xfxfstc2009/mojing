//
//  MJNetworkTool.h
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
typedef void (^Success)(id obj);
typedef void (^Failure)(NSError *error);
@interface MJNetworkTool : NSObject

+ (instancetype)shareInstance;

// 获取所有魔境数据的请求
- (void)getMoJingURL:(MJURLEnum)type parameters:(id)parameters success:(Success)success failure:(Failure)failure;

// 是否有网络连接
-(BOOL)isConnectionAvailableInView:(UIView *)view;


@end
