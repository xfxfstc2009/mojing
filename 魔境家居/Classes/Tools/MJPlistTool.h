//
//  MJPlistTool.h
//  魔境家居
//
//  Created by mojing on 15/10/26.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJPlistTool : NSObject

+ (NSArray *)categories;


+ (NSArray *)matchs;

@end
