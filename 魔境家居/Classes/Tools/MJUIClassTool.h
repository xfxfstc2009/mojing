//
//  MJUIClassTool.h
//  魔境家居
//
//  Created by mojing on 15/10/29.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJUIClassTool : NSObject

// 创建按钮
+ (UIButton *)createBtnWithFrame:(CGRect)rect title:(NSString *)title image:(UIImage *)image target:(id)target action:(SEL)action;
// 创建UILabel
+ (UILabel *)createLbWithFrame:(CGRect)rect title:(NSString *)title aliment:(NSTextAlignment)aliment color:(UIColor *)color size:(CGFloat)size;
// 创建iamgeView
+ (UIImageView *)createImgViewWithFrame:(CGRect)rect imageName:(NSString *)imageName;
// 自适应高度
+ (CGSize)caculateText:(NSString *)str fontSize:(CGFloat)size maxSize:(CGSize)maxSize;

// 创建UIBarButtonItem
+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(NSString *)image highImage:(NSString *)highImage;

@end
