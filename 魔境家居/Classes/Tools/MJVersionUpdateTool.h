//
//  MJVersionUpdateTool.h
//  魔境家居
//
//  Created by lumingliang on 15/12/19.
//  Copyright © 2015年 mojing. All rights reserved.
//  版本更新

#import <Foundation/Foundation.h>

typedef void (^versionUpdate)();
@interface MJVersionUpdateTool : NSObject<UIAlertViewDelegate>
- (void)compareVersion:(NSString *)version alertTitle:(NSString *)title alertMessage:(NSString *)message;
+ (instancetype)shareInstance;
@property (nonatomic,copy) versionUpdate update;
@end
