//
//  NetConnectURL.h
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//


#ifndef NetConnectURL_h
#define NetConnectURL_h
typedef NS_ENUM(NSInteger, MJURLEnum) {

    MJUserLogin = 0,//用户登录
    MJUserLogoff,//用户注销
    MJSpaceTypeList,//获取空间类型列表
    MJProductTypeList,//商品类型列表
    MJProductList,//获取商品列表
    MJProductDetail,//获取商品详情
    MJWorksList,//获取搭配作品列表
    MJWorksDetail,//获取搭配作品详情
    MJWorksDelete,//搭配作品删除
    MJProductTotalStatistics,//商品总体统计
    MJProductStatisticsList,//商品统计列表
    MJCustomerStatisticsList,//客户统计列表
    MJBrandList,//获取品牌列表
    MJBrandDetail,//获取品牌详情
    MJBrandDealerList,//获取品牌经销商列表
    MJCustomerList,//获取客户列表
    MJCustomerBrowseDetail,//获取客户浏览详情
    MJBrowseShopSave,//保存客户进/离店信息
    MJBrowseProductSave,//保存客户浏览详情
    MJUserDetailData,//获取用户详情
    MJUserByIDList,//通过id获取信息
    MJUsersDetailData,//获取多个用户详细信息
    MJVersion,//版本控制更新
    MJVersionUpdate,//版本更新
    MJSceneList,//场景列表
    MJSceneDetail,//场景详情
    MJProductNew,//设置新品
    MJUpdateNickName,//修改昵称
    MJPlaneWorksSave,//保存选材搭配
    MJPlaneWorksGetList,//获取选材搭配作品列表
    MJFileUpload,//文件上传
    MJResourceFavorite,
    //..
} ;
//static inline int add(int a, int b){
//    return a+b;
//}
static inline UIColor *RGB(CGFloat r,CGFloat g,CGFloat b){
    
  return   [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:1];
    
}
#endif /* NetConnectURL_h */
