//
//  MJUIClassTool.m
//  魔境家居
//
//  Created by mojing on 15/10/29.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJUIClassTool.h"
#import "UIView+Frame.h"
@implementation MJUIClassTool

+ (UIButton *)createBtnWithFrame:(CGRect)rect title:(NSString *)title image:(UIImage *)image target:(id)target action:(SEL)action
{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = rect;
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setBackgroundImage:image forState:UIControlStateNormal];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return btn;
}

+ (UILabel *)createLbWithFrame:(CGRect)rect title:(NSString *)title aliment:(NSTextAlignment)aliment color:(UIColor *)color size:(CGFloat)size
{
    UILabel * lb = [[UILabel alloc] initWithFrame:rect];
    lb.text = title;
    lb.textAlignment = aliment;
    lb.textColor = color;
    lb.font = [UIFont systemFontOfSize:size];
    return lb;
}

+ (UIImageView *)createImgViewWithFrame:(CGRect)rect imageName:(NSString *)imageName
{
    UIImageView *v = [[UIImageView alloc] initWithFrame:rect];
    v.image = [UIImage imageNamed:imageName];
    return v;
}

+ (CGSize)caculateText:(NSString *)str fontSize:(CGFloat)size maxSize:(CGSize)maxSize
{
    UIFont * font = [UIFont systemFontOfSize:size];
    if (Sys_Ver >= 7.0) {
        return [str boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil].size;
    }else{
        return [str sizeWithFont:font constrainedToSize:maxSize];
    }
}

+ (UIBarButtonItem *)itemWithTarget:(id)target action:(SEL)action image:(NSString *)image highImage:(NSString *)highImage
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    // 设置图片
    [btn setImage:[UIImage imageNamed:image] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:highImage] forState:UIControlStateHighlighted];
    // 设置尺寸
    btn.size = btn.currentImage.size;
    return [[UIBarButtonItem alloc] initWithCustomView:btn];
}

@end
