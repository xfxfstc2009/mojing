//
//  MJPlistTool.m
//  魔境家居
//
//  Created by mojing on 15/10/26.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJPlistTool.h"
#import "MJCategory.h"
#import "MJExtension.h"

@implementation MJPlistTool

static NSArray *_categories;
+ (NSArray *)categories
{
    if (_categories == nil) {
        _categories = [MJCategory mj_objectArrayWithFilename:@"categories.plist"];;
    }
    return _categories;
}

static NSArray *_matchs;
+ (NSArray *)matchs
{
    if (_matchs == nil) {
        _matchs = [MJCategory mj_objectArrayWithFilename:@"matchs.plist"];;
    }
    return _matchs;
}
@end
