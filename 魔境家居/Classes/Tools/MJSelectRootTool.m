//
//  MJSelectRootTool.m
//  魔境家居
//
//  Created by mojing on 15/10/26.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJSelectRootTool.h"
#import "MJNewFeatureCtler.h"
#import "MJLoginCtler.h"


#define MJVersionKey @"versionKey"

@implementation MJSelectRootTool
+ (void)chooseRootViewController:(UIWindow *)window
{
    // 1.获取当前的版本号
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"];
    
    // 2.获取上一次的版本号
    NSString *lastVersion = [MJUserDefaults objectForKey:MJVersionKey];
    
    // v1.0
    // 判断当前是否有新的版本
    if ([currentVersion isEqualToString:lastVersion]) { // 没有最新的版本号
        
        // 创建tabBarVc
        MJLoginCtler *brand = [[MJLoginCtler alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:brand];
       
        // 设置窗口的根控制器
        window.rootViewController = nav;
        
        
    }else{ // 有最新的版本号
        
        // 进入新特性界面
        // 如果有新特性，进入新特性界面
        MJNewFeatureCtler *new = [[MJNewFeatureCtler alloc] init];
        window.rootViewController = new;
        
        // 保持当前的版本，用偏好设置
        [MJUserDefaults setObject:currentVersion forKey:MJVersionKey];
        
        [MJUserDefaults synchronize];
    }
}

@end
