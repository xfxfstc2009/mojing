//
//  MJTransform.h
//  魔境家居
//
//  Created by Mac on 15/11/26.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJTransform : NSObject
+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;
+ (BOOL)isContainsEmoji:(NSString *)string;
@end
