//
//  MJNetworkTool.m
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJNetworkTool.h"
#import "MJExtension.h"
#import "MJCustomerModel.h"
#import "MJProductListModel.h"
#import "MJProductListDataModel.h"
#import "MJSpaceTypeModel.h"
#import "MJMatchModel.h"
#import "MJBrandModel.h"
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "MJLoginUserModel.h"
#import "MJCustomerListModel.h"
#import "MJCustomerModel.h"
#import "MJBrandDetailModel.h"
#import "MJBrandDetailCaseModel.h"
#import "MJBrandDetailAlbumModel.h"
#import "MJMatchUserDataModel.h"
#import "MJProductModel.h"
#import "MJCustomerScanDetailsModel.h"

static MJNetworkTool *netWork = nil;

@implementation MJNetworkTool
{
    AFHTTPSessionManager *_manager;
}

+ (instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        netWork = [[MJNetworkTool alloc] init];
    });
    return netWork;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _manager = [AFHTTPSessionManager manager];
        // 服务器返回的数据类型全部都是二进制
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    return self;
}

#pragma mark - 是否有网络连接
-(BOOL)isConnectionAvailableInView:(UIView *)view {
    
    BOOL isExistenceNetwork = YES;
    Reachability *reach = [Reachability reachabilityWithHostName:@"https://www.hao123.com"];
    switch ([reach currentReachabilityStatus]) {
        case NotReachable:
            isExistenceNetwork = NO;
            //NSLog(@"notReachable");
            break;
        case ReachableViaWiFi:
            isExistenceNetwork = YES;
            //NSLog(@"WIFI");
            break;
        case ReachableViaWWAN:
            isExistenceNetwork = YES;
            //NSLog(@"3G");
            break;
    }
    
    if (!isExistenceNetwork) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        hud.removeFromSuperViewOnHide =YES;
        hud.mode = MBProgressHUDModeCustomView;
        hud.labelText = NSLocalizedString(@"当前网络不可用,加载失败", nil);
       // hud.minSize = CGSizeMake(100, 100);
        hud.layer.cornerRadius = 50;
        [hud hide:YES afterDelay:2];
        
        return NO;
    }
    
    return isExistenceNetwork;
}

#pragma mark 连接服务器超时
- (void)connectTimeOutWithError:(NSError *)error
{
    if ((error.code != -1004) || (error.code != -1001)) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
        hud.removeFromSuperViewOnHide =YES;
        hud.mode = MBProgressHUDModeCustomView;
        hud.labelText = NSLocalizedString(@"连接服务器超时,请稍后重试", nil);
        // hud.minSize = CGSizeMake(100, 100);
        hud.layer.cornerRadius = 50;
        [hud hide:YES afterDelay:1.5];
    }
}

#pragma mark - 对外调用接口
- (void)getMoJingURL:(MJURLEnum)type parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    //数据请求链接
    NSArray *urlArr = @[userLoginURL,userLogoffURL,spaceTypeListURL,productTypeListURL,productListURL,productDetailURL,worksListURL,worksDrtailURL,worksDeleteURL,productTotalStatisticsURL,productStatisticsListURL,customerStatisticsListURL,brandListURL,brandDetailURL,brandDealerListURL,customerListURL,customerBrowseDetailURL,browseShopSaveURL,browseProductSavelURL,userDetailURL,userByIDListURL,usersDetailDataURL,mjVersionURL,mjVersionUpdateURL,sceneListURL,sceneDetailURL,productNewURL,userNickNameUpdateURL,planeWorksSaveURL,planeWorksGetListURL,fileUploadURL,resourceFavoriteURL];
    
    switch (type) {
        case MJUserLogin:
        {//用户登录
            [self postUserLoginURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJUserLogoff:
        {//用户注销
            [self postUserLogoffURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJSpaceTypeList:
        {//获取空间类型列表
            [self postSpaceTypeListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJProductTypeList:
        {//获取商品类型列表
            [self postProductTypeListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJProductList:
        {//获取商品列表
            [self postProductListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJProductDetail:
        {//获取商品详情
            [self postProductDetailURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJWorksList:
        {//获取搭配作品列表
            [self postWorksListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJWorksDetail:
        {//获取搭配作品详情
            [self postWorksDetailURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJWorksDelete:
        {//删除搭配作品
            [self postWorksDeleteURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJProductTotalStatistics:
        {//商品总体统计
            [self postProductTotalStatisticsURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJProductStatisticsList:
        {//商品统计列表
            [self postProductStatisticsListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJCustomerStatisticsList:
        {//客户统计列表
            [self postCustomerStatisticsListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJBrandList:
        {//获取品牌列表
            [self getBrandListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJBrandDetail:
        {//获取品牌详情
            [self postBrandDetailURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJBrandDealerList:
        {//获取经销商列表
            [self postDealerListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJCustomerList:
        {//获取客户列表
            [self postCustomerListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJCustomerBrowseDetail:
        {//获取客户浏览详情
            [self postCustomerBrowseDetailURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJBrowseShopSave:
        {//保存客户进店/离店
            [self postBrowseShopSaveURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJBrowseProductSave:
        {//保存客户进店/离店
            [self postBrowseProductSaveURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJUserDetailData:
        {//获取用户详情
            [self postUserDetailDataURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
        case MJUserByIDList:
        {//获取用户id列表
            [self postUserByIDListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
        case MJUsersDetailData:
        {//获取多个用户详情
            [self getUsersDetailDataURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJVersion:
        {//获取版本号
            [self getVersionURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJVersionUpdate:
        {//版本更新
            [self getVersionUpdateURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJSceneList:
        {//场景列表
            [self postSceneListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJSceneDetail:
        {//场景详情
            [self postSceneDetailURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJProductNew:
        {//置新/取消置新
            [self postProductNewURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJUpdateNickName:
        {//修改用户昵称
            [self postUserNickNameUpdateURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJPlaneWorksSave:
        {//选材搭配作品保存
            [self postPlaneWorksSaveURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJPlaneWorksGetList:
        {//选材搭配作品列表
            [self postPlaneWorksGetListURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJFileUpload:
        {//文件上传
            [self postFileUploadURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        case MJResourceFavorite:
        {//商品收藏
            [self postResourceFavoriteURL:urlArr[type] parameters:parameters success:success failure:failure];
        }
            break;
        default:
            break;
    }
}

#pragma mark - 用户登录
- (void)postUserLoginURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        // NSLog(@"%@",url);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        MJLoginUserModel *model = [MJLoginUserModel mj_objectWithKeyValues:dict];
        //   NSLog(@"%@",dict);
        //回调成功的数据
        if (model.code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(model.msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(model);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 用户注销
- (void)postUserLogoffURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        // NSLog(@"%@",dict);
        
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 获取空间类型列表
- (void)postSpaceTypeListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure {
    [_manager GET:url parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSMutableArray *dataArr = dict[@"data"][@"spaceTypeList"];
        // 把字典数组转换成模型数组
        NSMutableArray *arr = [MJSpaceTypeModel mj_objectArrayWithKeyValuesArray:dataArr];
        // NSLog(@"%ld",arr.count);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(arr);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];

}

#pragma mark - 获取商品类型列表
- (void)postProductTypeListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager GET:url parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSMutableArray *dataArr = dict[@"data"][@"productTypeList"];
        // 把字典数组转换成模型数组
        NSMutableArray *arr = [MJSpaceTypeModel mj_objectArrayWithKeyValuesArray:dataArr];
        //  NSLog(@"%ld",arr.count);
        //  NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(arr);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 获取商品列表
- (void)postProductListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",parameters);
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        MJProductListModel *model = [MJProductListModel mj_objectWithKeyValues:dict];
        //  NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(model);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 获取商品详情
- (void)postProductDetailURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSDictionary *productDic = dict[@"data"];
        
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        MJProductModel *model = [MJProductModel mj_objectWithKeyValues:productDic];
        NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(model);
            }
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 获取搭配作品列表
- (void)postWorksListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        // NSDictionary *productDic = dict[@"data"];
        
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        MJMatchModel *model = [MJMatchModel mj_objectWithKeyValues:dict];
        //   NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(model);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 获取搭配作品详情
- (void)postWorksDetailURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        //        NSDictionary *productDic = dict[@"data"];
        //        NSArray *arr = [productDic objectForKey:@"productList"];
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        //  MJMatchModel *model = [MJMatchModel mj_objectWithKeyValues:dict];
        //    NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 搭配作品删除
- (void)postWorksDeleteURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        //        NSDictionary *productDic = dict[@"data"];
        //        NSArray *arr = [productDic objectForKey:@"productList"];
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        //  MJMatchModel *model = [MJMatchModel mj_objectWithKeyValues:dict];
        NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 商品总体统计
- (void)postProductTotalStatisticsURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 商品统计列表
- (void)postProductStatisticsListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        // NSDictionary *productDic = dict[@"data"];
        
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        //  MJMatchModel *model = [MJMatchModel mj_objectWithKeyValues:dict];
        // NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
 
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 客户统计列表
- (void)postCustomerStatisticsListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        
        NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}


#pragma mark - 获取品牌列表
- (void)getBrandListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        //  MJBrandModel *model = [MJBrandModel mj_objectWithKeyValues:dict];
        
        //     NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 获取品牌详情
- (void)postBrandDetailURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:nil];
        // NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 获取客户列表
- (void)postCustomerListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSDictionary *productDic = dict[@"data"];
        MJCustomerDataModel *dataModel = [MJCustomerDataModel mj_objectWithKeyValues:productDic];
        
        
        NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dataModel);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 获取客户浏览详情
- (void)postCustomerBrowseDetailURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        //     NSLog(@"%@",dict);
        
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 保存客户进店/离店
- (void)postBrowseShopSaveURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        //   NSLog(@"%@",dict);
        //回调成功的数据
        if (success) {
            success(dict);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 保存客户浏览详情
- (void)postBrowseProductSaveURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        //  NSDictionary *browseDict = dict[@"data"];
        //   NSArray *arr = [browseDict objectForKey:@"productBrowseList"];
        // NSLog(@"%@",parameters);
        //  NSArray *modelArr = [MJCustomerScanDetailsModel mj_objectArrayWithKeyValuesArray:arr];
        NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 热销信息保存
- (void)postBrandDetailSpreadSaveURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure image:(UIImage *)image
{
    [_manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyyMMddHHmmss";
        NSString *str = [formatter stringFromDate:[NSDate date]];
        NSString *fileName = [NSString stringWithFormat:@"%@.png", str];
        
        // 上传图片，以文件流的格式
        [formData appendPartWithFileData:imageData name:@"hotProducts" fileName:fileName mimeType:@"image/png"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        //   NSLog(@"%@",parameters);
        if (success) {
            success(dict);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 获取用户详情
- (void)postUserDetailDataURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark idList列表
- (void)postUserByIDListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 获取多个用户详情
- (void)getUsersDetailDataURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager GET:url parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",dict);
        NSArray *arr = [dict objectForKey:@"data"];
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        NSMutableArray *array = [MJMatchUserDataModel mj_objectArrayWithKeyValuesArray:arr];
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(array);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 获取版本号
- (void)getVersionURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark - 版本更新
- (void)getVersionUpdateURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager GET:url parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",dict);
        //回调成功的数据
        if (success) {
            success(dict);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 获取场景列表
- (void)postSceneListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        // NSDictionary *productDic = dict[@"data"];
        
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        MJMatchModel *model = [MJMatchModel mj_objectWithKeyValues:dict];
        //   NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(model);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 获取场景详情
- (void)postSceneDetailURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure {
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        //        NSDictionary *productDic = dict[@"data"];
        //        NSArray *arr = [productDic objectForKey:@"productList"];
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        //  MJMatchModel *model = [MJMatchModel mj_objectWithKeyValues:dict];
        //   NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 置新/取消置新
- (void)postProductNewURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
     [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
         
     } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
         
         NSLog(@"%@",dict);
         //回调成功的数据
         NSInteger code = [[dict objectForKey:@"code"] integerValue];
         NSString *msg = [dict objectForKey:@"msg"];
         //回调成功的数据
         if (code == -1) {
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
             hud.removeFromSuperViewOnHide =YES;
             hud.mode = MBProgressHUDModeCustomView;
             hud.labelText = NSLocalizedString(msg, nil);
             // hud.minSize = CGSizeMake(100, 100);
             hud.layer.cornerRadius = 50;
             [hud hide:YES afterDelay:1.5];
         }else
         {
             if (success) {
                 success(dict);
             }
         }
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         if (failure) {
             failure(error);
             [self connectTimeOutWithError:error];
         }
     }];
}

#pragma mark 修改用户昵称
- (void)postUserNickNameUpdateURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        //  NSLog(@"%@",dict);
        //回调成功的数据
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 获取品牌对应的经销商列表
- (void)postDealerListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
    
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code == -1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 搭配作品保存
- (void)postPlaneWorksSaveURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"%@",dict);
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code != 200) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 选材搭配作品列表
- (void)postPlaneWorksGetListURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"%@",dict);
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code != 200) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}

#pragma mark 文件上传
- (void)postFileUploadURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"%@",dict);
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code != 200) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }

    }];
}

#pragma mark - 商品收藏
- (void)postResourceFavoriteURL:(NSString *)url parameters:(id)parameters success:(Success)success failure:(Failure)failure
{
    [_manager POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        NSLog(@"%@",dict);
        NSInteger code = [[dict objectForKey:@"code"] integerValue];
        NSString *msg = [dict objectForKey:@"msg"];
        //回调成功的数据
        if (code != 200) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(msg, nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1.5];
        }else
        {
            if (success) {
                success(dict);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
            [self connectTimeOutWithError:error];
        }
    }];
}




@end