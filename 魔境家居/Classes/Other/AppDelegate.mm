//
//  AppDelegate.m
//  魔境家居
//
//  Created by mojing on 15/10/26.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "AppDelegate.h"
#import "MJSelectRootTool.h"
#import "MJNetworkTool.h"
#import "MJBrandModel.h"
#import "MJBrandDataModel.h"
#import "MJExtension.h"
#import "MJLoginCtler.h"
#import <KSCrash/KSCrashInstallationStandard.h>
#import "UIImageView+WebCache.h"
#import "MJDesignerHomePageTabBarCtler.h"


#import "APService.h"

#import <AFNetworking.h>
#import <MJDBManager.h>
#import <MJTransform.h>
#import "MJChatView.h"
#import "Contack.h"
#import "Chat.h"
#import "MJContackModel.h"

#define myID [MJUserDefaults objectForKey:@"userID"]
#define kMessageRefresh @"kMessageRefresh"
#define kLoginNotification @"kLoginNotification"
#define kCountDidChangeNofication @"kCountDidChangeNofication"


@interface AppDelegate ()<UIApplicationDelegate>
{
    NSMutableArray* _arrayM;
}
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    self.window = [[UIWindow alloc] init];
    
    self.window.frame = [UIScreen mainScreen].bounds;
    
    [self.window makeKeyAndVisible];
    
    self.hotListParaArr= [NSMutableArray array];
    self.hotUploadParaArr = [NSMutableArray array];
    self.casePhotoArr = [NSMutableArray array];
    self.brandIDParaDict = [NSDictionary dictionary];
    NSString *str = @"";
    [MJUserDefaults setObject:str forKey:@"browseID"];
    //开始不存在客户
    [MJUserDefaults setObject:@"NO" forKey:@"isExist"];
    [MJUserDefaults synchronize];
    //键盘监听相关初始化
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.shouldResignOnTouchOutside = YES;
    manager.shouldToolbarUsesTextFieldTintColor = YES;
    manager.enableAutoToolbar = NO;
   
   self.window.rootViewController =[[UINavigationController alloc] initWithRootViewController:[[MJLoginCtler alloc] init]];
    
    
//    MJDesignerHomePageTabBarCtler *tabBar = [[MJDesignerHomePageTabBarCtler alloc] init];

//   self.window.rootViewController =tabBar;
   self.window.rootViewController =[[UINavigationController alloc] initWithRootViewController:[[MJLoginCtler alloc] init]];
    
   //是否是初次进入
   // [MJSelectRootTool chooseRootViewController:self.window];
    
    
    // Required
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        [APService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                       UIUserNotificationTypeSound |
                                                       UIUserNotificationTypeAlert)
                                           categories:nil];
    }else {
        //categories 必须为nil
        [APService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                       UIRemoteNotificationTypeSound |
                                                       UIRemoteNotificationTypeAlert)
                                           categories:nil];
    }
    
    [APService setupWithOption:launchOptions];
    
    //进入下面这个表示app未运行，且通过点击通知打开应用
  //  NSDictionary *remoteNotification = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    //    if (remoteNotification) {
    //        [self getUnreadInformation];
    //    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUnreadInformation) name:kLoginNotification object:nil];
    
    //崩溃日志分析
    KSCrashInstallationStandard* installation = [KSCrashInstallationStandard sharedInstance];
    installation.url = [NSURL URLWithString:@"https://collector.bughd.com/kscrash?key=1f7d10dbfd12e8aaba521e26ec439643"];
    [installation install];
    [installation sendAllReportsWithCompletion:nil];
    
    return YES;
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -push
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // Required
    [APService registerDeviceToken:deviceToken];
    
    
    
}


- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    
    
    [APService handleRemoteNotification:userInfo];
    
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    if (application.applicationState == UIApplicationStateActive) {
        [self getUnreadInformation];
        [[NSNotificationCenter defaultCenter] postNotificationName:kJPFNetworkDidReceiveMessageNotification object:nil];
    }
    
    
    
    [APService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNoData);
    
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //应用处于后台，把旧的推送通知从通知中心移除
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    [APService setBadge:0];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
    [self getUnreadInformation];
    [[NSNotificationCenter defaultCenter] postNotificationName:kJPFNetworkDidReceiveMessageNotification object:nil];
    
    [APService setBadge:0];
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
    
}

- (void)getUnreadInformation{

    [MJContackModel getInstance].unreadMsgCount =0;
    if (!myID) {
        return ;
    }
    _arrayM = [[NSMutableArray alloc]init];
    [_arrayM removeAllObjects];
    //打开数据库
    MJDBManager* managerData = [MJDBManager shareInstanceWithCoreDataID:myID];
    
    NSArray *obj = [[NSMutableArray alloc]initWithArray:[managerData query:@"Contack" predicate:nil]];
    //联系人
    NSMutableArray* contackArray = [[NSMutableArray alloc]initWithArray:obj];
    
    for (Contack* cont in contackArray) {
        [MJContackModel getInstance].unreadMsgCount = [MJContackModel getInstance].unreadMsgCount + [cont.msgcount integerValue];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kCountDidChangeNofication object:nil];
    
    
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    session.responseSerializer = [AFHTTPResponseSerializer serializer];
    [session GET:CHAT_UNREAD parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",CHAT_SEND);

        if ([[dict objectForKey:@"code"] isEqualToNumber:@(200)])
        {
            NSDictionary* messageDic =[dict objectForKey:@"data"];
            if ([messageDic isKindOfClass:[NSNull class] ]) {
                return ;
            }
            
            //收到信息可以弄个全局数
            NSArray* arrayChat = [messageDic objectForKey:@"chatList"];
            if (arrayChat == nil) {
                NSLog(@"获取失败");
                return ;
            }
            if ([arrayChat count] != 0)
            {
                NSMutableArray *contackIdArr = [[NSMutableArray alloc]init];
                NSMutableArray *LocalContact = [[NSMutableArray alloc]init];
                for (Contack* contack in obj) {
                    [LocalContact addObject:contack.contack_id];
                    
                }
                for (NSDictionary* chat in arrayChat)
                {
                    NSNumber*  senderUserID =  [chat valueForKey:@"senderUserID"];
                    if ([LocalContact count] ==0) {
                        [contackIdArr addObject:senderUserID];
                    }else
                    {
                        
                        if ([LocalContact indexOfObject:senderUserID] == NSNotFound)
                        {
//                            _arrayM addObject
                            [contackIdArr addObject:senderUserID];
                        }
                    };
                }
                if ([contackIdArr count] != 0)
                {
                    
                    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
                    session.responseSerializer = [AFHTTPResponseSerializer serializer];
                    NSMutableDictionary *contackDic1 = [[NSMutableDictionary alloc]init];
                    
                    for (NSNumber* x in contackIdArr)
                    {
                        [contackDic1 setObject:x forKey:@"ids"];
                    }
                    
                    [session GET:CHAT_CONTACK parameters:contackDic1 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                        
                        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                        
                        if ([[dict objectForKey:@"code"] isEqualToNumber:@(0)])
                        {
                            NSArray *contackInfo =[dict objectForKey:@"data"];
                            for (NSDictionary* dic in contackInfo)
                            {
                                NSNumber *contack_id = [dic objectForKey:@"id"];
                                NSString *headImgUrl = [dic objectForKey:@"avatarPic"];
                                
                                Contack *contack1 = (Contack *)[managerData createMO:@"Contack"];
                                contack1.image =headImgUrl;
                                contack1.contack_id = contack_id;
                                contack1.isSelect =0;
                                
                                for (NSDictionary* chat in arrayChat)
                                {
                                    
                                    NSNumber*  senderUserID =  [chat valueForKey:@"senderUserID"];
                                    NSString *displayName;
                                    
                                    if ([[chat objectForKey:@"senderUserVO"] isKindOfClass:[NSNull class]])
                                    {
                                        NSLog(@"senderUserVO是空的");
                                    }else
                                    {
                                        NSDictionary* userVo =[chat objectForKey:@"senderUserVO"];
                                        NSString* tpContack =[userVo objectForKey:@"type"];
                                        if ([tpContack isEqualToString:@"1"])
                                        {
                                            NSDictionary* brandDic =[userVo objectForKey:@"brandData"];
                                            if ([brandDic  isKindOfClass:[NSNull class]]) {
                                                displayName = [userVo objectForKey:@"nickName"];
                                            }
                                            else
                                            {
                                                displayName = [brandDic objectForKey:@"name"];
                                            }
                                        }
                                        else if ([tpContack isEqualToString:@"2"])
                                        {
                                            
                                            NSDictionary* dealerDic = [userVo objectForKey:@"dealerData"];
                                            if ([dealerDic  isKindOfClass:[NSNull class]]) {
                                                displayName = [userVo objectForKey:@"nickName"];
                                            }
                                            else
                                            {
                                                displayName = [dealerDic objectForKey:@"shopName"];
                                            }
                                        }
                                        else if ([tpContack isEqualToString:@"3"])
                                        {
                                            
                                            NSDictionary* dealerDic = [userVo objectForKey:@"dealerData"];
                                            if ([dealerDic  isKindOfClass:[NSNull class]]) {
                                                displayName = [userVo objectForKey:@"nickName"];
                                            }
                                            else
                                            {
                                                NSString* shopName = [dealerDic objectForKey:@"shopName"];
                                                displayName = [NSString stringWithFormat:@"%@(%@)",[userVo objectForKey:@"nickName"],shopName];
                                            }
                                        }
                                        else{
                                            displayName = [userVo objectForKey:@"nickName"];
                                            
                                        }
                                    }
                                    if ([contack_id isEqualToNumber:senderUserID] )
                                    {
                                        contack1.nickname =displayName;
                                        [managerData saveManagerObj:contack1];
                                        [contackArray addObject:contack1];
                                        
                                    }
                                    
                                        
                                    NSNumber* senderDateTime = [chat valueForKey:@"sendDateTime"];
                                    NSString* content = [chat valueForKey:@"content"];
                                    NSDictionary *contentDic =[MJTransform dictionaryWithJsonString:content];
                                    
                                    NSString *x =myID;
                                    NSString *y = [NSString stringWithFormat:@"%@",senderUserID];
                                    BOOL isself = [x isEqualToString:y];
                                    NSNumber* isSelf = [NSNumber numberWithBool:isself];
                                    
                                    if ([[contentDic valueForKey:@"type"] isEqualToString:@"1"]) {
                                        
                                        [MJChatView addChatWithName:displayName andContent:content andIsSelf:isSelf andImage:nil andContack_id:[senderUserID integerValue] andTime:senderDateTime];
                                        
                                    }else
                                    {
                                        //图片
                                        [MJChatView addChatWithName:displayName andContent:content andIsSelf:isSelf andImage:nil andContack_id:[senderUserID integerValue] andTime:senderDateTime];
                                    }

                                }
                            }
                        }
                        else
                        {
                            NSLog(@"%@",[dict objectForKey:@"msg"]);
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"msg"] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                            [alert show];
                        }
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        NSLog(@"获取联系人信息失败%@",error);
                        
                        
                    }];
                }else//联系人已经存在
                {

                    NSString *displayName;

                    for (NSDictionary* chat in arrayChat)
                    {
                        NSNumber*  senderUserID =  [chat valueForKey:@"senderUserID"];


                        if ([[chat objectForKey:@"senderUserVO"] isKindOfClass:[NSNull class]]) {
                            NSLog(@"senderUserVO是空的");
                        }else{
                            NSDictionary* userVo =[chat objectForKey:@"senderUserVO"];
                            NSString* tpContack =[userVo objectForKey:@"type"];
                            if ([tpContack isEqualToString:@"1"])
                            {
                                NSDictionary* brandDic =[userVo objectForKey:@"brandData"];
                                if ([brandDic  isKindOfClass:[NSNull class]]) {
                                    displayName = [userVo objectForKey:@"nickName"];
                                }
                                else
                                {
                                    displayName = [brandDic objectForKey:@"name"];
                                }
                            }
                            else if ([tpContack isEqualToString:@"2"])
                            {
                                
                                NSDictionary* dealerDic = [userVo objectForKey:@"dealerData"];
                                if ([dealerDic  isKindOfClass:[NSNull class]]) {
                                    displayName = [userVo objectForKey:@"nickName"];
                                }
                                else
                                {
                                    displayName = [dealerDic objectForKey:@"shopName"];
                                }
                            }
                            else if ([tpContack isEqualToString:@"3"])
                            {
                                
                                NSDictionary* dealerDic = [userVo objectForKey:@"dealerData"];
                                if ([dealerDic  isKindOfClass:[NSNull class]]) {
                                    displayName = [userVo objectForKey:@"nickName"];
                                }
                                else
                                {
                                    NSString* shopName = [dealerDic objectForKey:@"shopName"];
                                    displayName = [NSString stringWithFormat:@"%@(%@)",[userVo objectForKey:@"nickName"],shopName];
                                }
                            }
                            else{
                                displayName = [userVo objectForKey:@"nickName"];
                                
                            }
                            
                        }
                        NSNumber* senderDateTime = [chat valueForKey:@"sendDateTime"];
                        NSString* content = [chat valueForKey:@"content"];
                        NSDictionary *contentDic =[MJTransform dictionaryWithJsonString:content];
                        
                        NSString *x =myID;
                        NSString *y = [NSString stringWithFormat:@"%@",senderUserID];
                        BOOL isself = [x isEqualToString:y];
                        NSNumber* isSelf = [NSNumber numberWithBool:isself];
                        
                        
                        
                        if ([[contentDic valueForKey:@"type"] isEqualToString:@"1"]) {
                            
                            [MJChatView addChatWithName:displayName andContent:content andIsSelf:isSelf andImage:nil andContack_id:[senderUserID integerValue] andTime:senderDateTime];
                            
                        }else
                        {
                            //图片
                            [MJChatView addChatWithName:displayName andContent:content andIsSelf:isSelf andImage:nil andContack_id:[senderUserID integerValue] andTime:senderDateTime];
                        }
                        
                    }
                }
            }
        }
        else
        {
            NSLog(@"%@",[dict objectForKey:@"msg"]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[dict objectForKey:@"msg"] message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            [alert show];
        }
    
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return UIInterfaceOrientationMaskAll;
}

// 接收到内存警告的时候调用
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    // 停止所有的下载
    [[SDWebImageManager sharedManager] cancelAll];
    // 删除缓存
    [[SDWebImageManager sharedManager].imageCache clearMemory];
    
}
@end
