//
//  AppDelegate.h
//  魔境家居
//
//  Created by mojing on 15/10/26.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardManager.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
/** 热销推荐图片数组*/
@property (strong, nonatomic) NSMutableArray *hotListParaArr;
/** 热销推荐图片上传参数数组*/
@property (strong, nonatomic) NSMutableArray *hotUploadParaArr;
/** 图片案例数组*/
@property (strong, nonatomic) NSMutableArray *casePhotoArr;
/**
 *  全局品牌ID 用户ID参数
 */
@property (strong, nonatomic) NSDictionary *brandIDParaDict;
@property (nonatomic, copy) NSString *shopName;
@property (nonatomic, copy) NSString *type;

- (void)getUnreadInformation;

@end

