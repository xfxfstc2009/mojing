//
//  MJProductDetailsCtler.m
//  魔境家居
//
//  Created by mojing on 15/11/7.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJProductDetailsCtler.h"
#import "MJAddProductNoteView.h"
#import "KeyboardManager.h"
#import "UIImageView+WebCache.h"
#import "MJNetworkTool.h"
#import "MJProductListModel.h"
#import "MJProductListDataModel.h"
#import "MJProductModel.h"
#import "MJBrandDataModel.h"
#import "MJCreatedDateModel.h"
#import "MJModifiedDateModel.h"
#import "MJExtension.h"
#import "MJUIClassTool.h"
#import "LCVoice.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "Product3DViewCtrl.h"
#import "UIButton+JUtil.h"
#import "ProgressView.h"
#import "LoaderModelUtil.h"
#import "UIBarButtonItem+Extension.h"
//弹出添加view的宽 高
#define kproductNoteViewWidth 450
#define kproductNoteViewHeight 350
//弹出二维码view的宽 高
#define kQrcodeViewWidth 260
#define kQrcodeViewHeight 260
//弹出时间
#define kproductNoteViewShowTime 0.5
@interface MJProductDetailsCtler ()<UIAlertViewDelegate,UIScrollViewDelegate, UIWebViewDelegate>
{
    /* 大图*/
    Product3DViewCtrl *view3d;
    UIButton *btn3d;
    ProgressView *progress;
    UIImageView *_singleProductImgView;
    
    /* 名称*/
     UILabel *_nameLabel;
    /* 价格*/
    UILabel *_priceLabel1;
    UILabel *_priceLabel;
    /* 品牌*/
     UILabel *_brandLabel;
    /* 二维码*/
    UILabel *_orderNumLabel;
    /* 型号*/
     UILabel *_typeNumLabel;
    /* 规格*/
    UILabel *_standardLabel;
    /* 滚动View*/
    UIScrollView *_scrollView;
    
}
/* 录音*/
@property(nonatomic,retain) LCVoice * voice;
@property (nonatomic,strong) NSURL *urlPath;
@property (nonatomic,copy) NSString *path;
/** 弹出view*/
@property (nonatomic,strong) MJAddProductNoteView *productNoteView;
/** 遮盖*/
@property (nonatomic,strong) UIButton *coverViewDetail;
/** 键盘监听*/
@property (nonatomic, strong) IQKeyboardReturnKeyHandler *returnKeyHandler;
/** 数据源*/
@property (nonatomic, strong) NSMutableArray *dataArr;
/** 二维码图片View*/
@property (nonatomic, strong) UIView *qrcodeView;
@property (nonatomic,strong) MJProductModel *model;
@end

@implementation MJProductDetailsCtler

- (NSMutableArray *)dataArr
{
    if (_dataArr == nil) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UIView *)qrcodeView
{
    if (_qrcodeView == nil) {
        
        _qrcodeView = [[UIView alloc] initWithFrame:CGRectMake(765-40, 245-58, kQrcodeViewWidth, kQrcodeViewHeight)];
        _qrcodeView.backgroundColor = [UIColor whiteColor];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, kQrcodeViewWidth-40, kQrcodeViewHeight-40)];
        NSURL *qrUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/png/qr/product/%ld_qr_1.png",mainURL,self.idNum]];
      //  NSLog(@"%@",qrUrl);
        [imgView sd_setImageWithURL:qrUrl placeholderImage:nil];
        [_qrcodeView addSubview:imgView];
    }
    return _qrcodeView;
}


//弹出的遮盖view
- (UIButton *)coverViewDetail
{
    if (!_coverViewDetail) {
        _coverViewDetail = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Screen_Width/*-kMoreViewWidth*/, Screen_Height)];
        _coverViewDetail.backgroundColor = [UIColor grayColor];
        _coverViewDetail.alpha = 0.5;
        [_coverViewDetail addTarget:self action:@selector(coverClickView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _coverViewDetail;
}

- (MJAddProductNoteView *)productNoteView
{
    if (_productNoteView == nil) {
        
        _productNoteView = [MJAddProductNoteView note];
        _productNoteView.backgroundColor = RGB(246, 246, 246);
        _productNoteView.frame = CGRectMake(Screen_Width/2-kproductNoteViewWidth/2, Screen_Height, kproductNoteViewWidth, kproductNoteViewHeight);
      
        __weak MJProductDetailsCtler *ctl = self;
        _productNoteView.click = ^(NSInteger index){
            switch (index) {
                case 1:
                {//第一颗星
                    
                    break;
                }
                case 2:
                {//第二颗星
                    
                    break;
                }
                case 3:
                {//第三颗星
                    
                    break;
                }
                case 4:
                {//第四颗星
                    
                    break;
                }
                case 5:
                {//第五颗星
                    
                    break;
                }
                case 6:
                {//确认添加并移除view
                  //  NSLog(@"确定添加");
                    [UIView animateWithDuration:kproductNoteViewShowTime animations:^{
                         [ctl.productNoteView setFrame:CGRectMake(Screen_Width/2-kproductNoteViewWidth/2, Screen_Height, kproductNoteViewWidth, kproductNoteViewHeight)];
                    }completion:^(BOOL finished) {
                        [ctl.coverViewDetail removeFromSuperview];
                        [ctl.productNoteView setBtnIsSelected];
                    }];
                   // NSLog(@"%ld",ctl.productNoteView.degree);
                     NSString *browseID = [MJUserDefaults objectForKey:@"browseID"];
                    
                    if (![browseID isEqualToString:@""]) {
                        
                        NSDictionary *dict = @{@"type" : @1,@"data" : ctl.productNoteView.inputTextView.text,@"sound" :@""};
                      // NSDictionary *para1 = @{@"browseID" : browseID,@"degree" : [NSString stringWithFormat:@"%ld",ctl.productNoteView.degree],@"remark" : dict};
                        
                        NSError *error1;
                        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error1];
                        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                        
                        NSDictionary  *para = @{@"productID" : [NSString stringWithFormat:@"%ld",(long)ctl.idNum],@"browseID" : browseID,@"degree" : [NSString stringWithFormat:@"%ld",ctl.productNoteView.degree],@"remark":jsonString};
                     //   NSLog(@"%@",para1);
                        MJNetworkTool *net = [MJNetworkTool shareInstance];
                        
                        [net getMoJingURL:MJBrowseProductSave parameters:para success:^(id obj) {
                            
                          //  NSLog(@"%@",obj);
                            
                        } failure:^(NSError *error) {
                            
                        }];

                    }else
                    {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"当前没有顾客,添加失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                    
                    break;
                }
                case 7:
                {//取消
                    [UIView animateWithDuration:kproductNoteViewShowTime animations:^{
                        [ctl.productNoteView setFrame:CGRectMake(Screen_Width/2-kproductNoteViewWidth/2, Screen_Height, kproductNoteViewWidth, kproductNoteViewHeight)];
                    }completion:^(BOOL finished) {
                        [ctl.coverViewDetail removeFromSuperview];
                        [ctl.productNoteView setBtnIsSelected];
                    }];

                    break;
                }
                    
                default:
                    break;
            }
        };
    }
    return _productNoteView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)initUI
{
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    
    [self setTitle:@"单品详情名称"];
    [self.view addSubview:self.productNoteView];
    [self.view addSubview:self.qrcodeView];
    self.returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    self.returnKeyHandler.lastTextFieldReturnKeyType = UIReturnKeyDefault;
    //滚动视图
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, Screen_Width, Screen_Height-64)];
    _scrollView.contentSize = CGSizeMake(0, Screen_Height*2);
    _scrollView.delegate = self;
    [self.view addSubview:_scrollView];
    // 3d展示
    view3d = [[Product3DViewCtrl alloc] initWithItem:nil];
    view3d.view.frame = CGRectMake(15, 64+15, 674-40, 604-40);
    view3d.view.hidden = YES;
    [self addChildViewController:view3d];
    [self.view addSubview:view3d.view];
    
    
    //获取数据
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    NSDictionary *para = @{@"id" : [NSString stringWithFormat:@"%ld",(long)self.idNum]};
  //  NSLog(@"%@",para);
    [netTool getMoJingURL:MJProductDetail parameters:para success:^(id obj) {
        
        self.model = obj;
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_1.%@",mainURL,_model.imageSuffix,_model.imageName,_model.imageName,_model.imageSuffix]];
        CGFloat x = 667;
        //图片
        _singleProductImgView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 674-40, 604-40)];
        [_singleProductImgView sd_setImageWithURL:url placeholderImage:nil];
        UIColor *LBColor = [UIColor blackColor];
        
        btn3d = [UIButton imageNamed:@"icon_3d.png" origin:CGPointMake(CGRectGetMaxX(_singleProductImgView.frame)-60, CGRectGetMaxY(_singleProductImgView.frame)-55)];
        [btn3d addTarget:self action:@selector(loadModel) forControlEvents:UIControlEventTouchUpInside];
        
        //名称
        _nameLabel = [MJUIClassTool createLbWithFrame:CGRectMake(x, 82-64, 300, 44) title:_model.name aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
        [self setTitle:_model.name];
        //价格
        NSString *priceStr = [NSString stringWithFormat:@"￥%ld",(long)_model.price];
        _priceLabel1 = [MJUIClassTool createLbWithFrame:CGRectMake(x, 134-64, 45, 29) title:@"价格：" aliment:NSTextAlignmentLeft color:LBColor size:15];
        _priceLabel = [MJUIClassTool createLbWithFrame:CGRectMake(x+50, 134-64, 70, 29) title:priceStr aliment:NSTextAlignmentLeft color:RGB(255, 122, 76) size:17];
        
        //品牌名称
        NSString *brandStr = [NSString stringWithFormat:@"品牌：%@",_model.brandData.name];
        _brandLabel = [MJUIClassTool createLbWithFrame:CGRectMake(x, 171-64, 300, 29) title:brandStr aliment:NSTextAlignmentLeft color:LBColor size:15];
        
        //规格
        NSString *standStr = [NSString stringWithFormat:@"规格：%@",_model.productTypeName];
        _standardLabel = [MJUIClassTool createLbWithFrame:CGRectMake(x, 208-64, 300, 29) title:standStr aliment:NSTextAlignmentLeft color:LBColor size:15];
        
        //二维码
        _orderNumLabel = [MJUIClassTool createLbWithFrame:CGRectMake(x, 245-64, 80, 29) title:@"二维码：" aliment:NSTextAlignmentLeft color:LBColor size:15];
        
        
        //按钮文字记录
        UIButton *textBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(x, 713-64-40-80, 125, 40) title:@"" image:[UIImage imageNamed:@"store_text_add"] target:self action:@selector(addBtnClick:)];
        
        //按钮语音记录
        self.voice = [[LCVoice alloc] init];
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"store_sound_add"] forState:UIControlStateNormal];
        button.frame = CGRectMake(850-20, 713-64-40-80, 125, 40);
        [self.view addSubview:button];
        // Set record start action for UIControlEventTouchDown
        [button addTarget:self action:@selector(recordStart) forControlEvents:UIControlEventTouchDown];
        // Set record end action for UIControlEventTouchUpInside
        [button addTarget:self action:@selector(recordEnd) forControlEvents:UIControlEventTouchUpInside];
        // Set record cancel action for UIControlEventTouchUpOutside
            [button addTarget:self action:@selector(recordCancel) forControlEvents:UIControlEventTouchUpOutside];
        
        //竖线view
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(15, 713-64-40-80+60, 2, 15)];
        lineView.backgroundColor = RGB(255, 122, 76);
        //商品详情
        UILabel *detailLb = [MJUIClassTool createLbWithFrame:CGRectMake(20, 713-64-40-80+60, 100, 15) title:@"商品详情" aliment:NSTextAlignmentLeft color:LBColor size:15];
        
        //webView
        UIWebView *detailWebView = [[UIWebView alloc] initWithFrame:CGRectMake(15, 713-64-40-80+60+25, Screen_Width-30, 80)];
        detailWebView.scrollView.userInteractionEnabled = NO;
        detailWebView.delegate = self;
        detailWebView.scalesPageToFit = YES;
        NSMutableString *htmlCode = [NSMutableString stringWithString:@"<html><head>"];
        [htmlCode appendFormat:@"<style type=\"text/css\">img{max-width:%fpx;}</style>", CGRectGetWidth(detailWebView.frame)-15];
        [htmlCode appendString:@"<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no' />"];
        [htmlCode appendString:@"</head><body>"];
        NSString *str = _model.desc;
        if (str) {
            [htmlCode appendString:str];//这个给那个content里的内容。
        }
        
        [htmlCode appendString:@"</body></html>"];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //            NSURL *url = [NSURL URLWithString:@"http://www.hao123.com"];
            //            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            dispatch_async(dispatch_get_main_queue(), ^{
                //加载一个请求
                [detailWebView loadHTMLString:htmlCode baseURL:nil];
                //                [detailWebView loadRequest:request];
            });
        });
        
        [_scrollView addSubview:_singleProductImgView];
        [_scrollView addSubview:btn3d];
        [_scrollView addSubview:_nameLabel];
        [_scrollView addSubview:_priceLabel];
        [_scrollView addSubview:_priceLabel1];
        [_scrollView addSubview:_brandLabel];
        [_scrollView addSubview:_standardLabel];
        [_scrollView addSubview:_orderNumLabel];
        [_scrollView addSubview:self.qrcodeView];
        [_scrollView addSubview:textBtn];
        [_scrollView addSubview:button];
        [_scrollView addSubview:lineView];
        [_scrollView addSubview:detailLb];
        [_scrollView addSubview:detailWebView];
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
    
    //型号
//    _typeNumLabel.text = @"";
//    if ([_typeNumLabel.text isEqualToString:@""]) {
//        _typeNumLabel.hidden = YES;
//    }
     //规格名称
//    _standardLabel.text = @"";
//    if ([_standardLabel.text isEqualToString:@""]) {
//        _standardLabel.hidden = YES;
//    }
   
}

#pragma mark - 返回
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
    ;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *height_str= [webView stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight"];
    int height = [height_str intValue]+20;
    height = MAX(height, 80);
    CGRect f = webView.frame;
    f.size.height = height;
    webView.frame = f;
    _scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame), CGRectGetMaxY(webView.frame)+10);
}

#pragma mark - 录音事件
- (void)recordStart
{
    NSString *browseID = [MJUserDefaults objectForKey:@"browseID"];
    if ([browseID isEqualToString:@""]) {

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"当前没有顾客,添加失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }else
    {
    _path = [NSString stringWithFormat:@"%@/Documents/%@_%ld.caf",NSHomeDirectory(),browseID,self.idNum];
    [self.voice startRecordWithPath:_path];
        self.urlPath = [NSURL fileURLWithPath:_path];
     NSLog(@"%@",_urlPath);
    }
   
}

- (void)recordEnd
{
    [self.voice stopRecordWithCompletionBlock:^{
        
        if (self.voice.recordTime > 0.0f) {
           // NSString *infoStr = [NSString stringWithFormat:@"\nrecord finish ! \npath:%@ \nduration:%f",self.voice.recordPath,self.voice.recordTime];
            NSString *str = @"确定要保存吗";
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"提示" message:str delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
            [alert show];
        }
        
    }];
}

- (void)recordCancel
{
    [self.voice cancelled];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"取消了" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    [alert show];
}


#pragma mark - 获取数据
- (void)loadProductDetailData
{
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    NSDictionary *para = @{@"id" : [NSString stringWithFormat:@"%ld",self.model.idNum]};
    [netTool getMoJingURL:MJProductDetail parameters:para success:^(id obj) {
        MJProductListModel *listModel = obj;
        MJProductListDataModel *model = listModel.data;
        // 把字典数组转换成模型数组
        _dataArr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - 确认添加
- (void)addBtnClick:(UIButton *)sender {
    [UIView animateWithDuration:kproductNoteViewShowTime animations:^{
          self.productNoteView.frame = CGRectMake(Screen_Width/2-kproductNoteViewWidth/2, Screen_Height/2-kproductNoteViewHeight/2, kproductNoteViewWidth, kproductNoteViewHeight);
    } completion:^(BOOL finished) {
        [self.view addSubview:self.coverViewDetail];
        [self.view bringSubviewToFront:self.productNoteView];
        
    }];
}


#pragma mark - coverClick
- (void)coverClickView
{
    [UIView animateWithDuration:kproductNoteViewShowTime animations:^{
        [self.productNoteView setFrame:CGRectMake(Screen_Width/2-kproductNoteViewWidth/2, Screen_Height, kproductNoteViewWidth, kproductNoteViewHeight)];
       // self.qrcodeView.frame = CGRectMake(Screen_Width/2-kQrcodeViewWidth/2, Screen_Height, kQrcodeViewWidth, kQrcodeViewHeight);
    }completion:^(BOOL finished) {
        [self.view endEditing:YES];
        [self.coverViewDetail removeFromSuperview];
        [self.productNoteView setBtnIsSelected];
    }];
}

#pragma mark - <UIAlertViewDelegate>

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        //确定保存到服务器
        NSString *browseID = [MJUserDefaults objectForKey:@"browseID"];
        
        if (![browseID isEqualToString:@""]) {
            NSString *pathStr = [NSString stringWithFormat:@"%@_%ld",browseID,self.idNum];
            NSDictionary *dict = @{@"type" : @2,@"data" :@"",@"sound" :pathStr};
            // NSDictionary *para1 = @{@"browseID" : browseID,@"degree" : [NSString stringWithFormat:@"%d",1],@"remark" : dict};

            NSError *error1;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error1];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSDictionary  *para = @{@"productID" : [NSString stringWithFormat:@"%ld",self.idNum],@"browseID" : browseID,@"degree" : @"0",@"remark":jsonString};
            //   NSLog(@"%@",para1);
            MJNetworkTool *net = [MJNetworkTool shareInstance];
            
            [net getMoJingURL:MJBrowseProductSave parameters:para success:^(id obj) {
                
                
            } failure:^(NSError *error) {
                
                
            }];
            
        }
        
    }else if(buttonIndex == 1)
    {
        //把保存好的删除
        [self deleteFile];
    }
}

- (void)deleteFile
{
    NSFileManager* fileManager=[NSFileManager defaultManager];
    
    BOOL blHave=[[NSFileManager defaultManager] fileExistsAtPath:_path];
    if (!blHave) {
        NSLog(@"no  have");
        return ;
    }else {
        NSLog(@" have");
        BOOL blDele= [fileManager removeItemAtPath:_path error:nil];
        if (blDele) {
            NSLog(@"dele success");
        }else {
            NSLog(@"dele fail");
        }
        
    }
}

- (void)loadModel
{
    btn3d.hidden = YES;
    int x = CGRectGetMinX(view3d.view.frame)+(CGRectGetWidth(view3d.view.frame)-60)/2;
    int y = CGRectGetMinY(view3d.view.frame)+(CGRectGetHeight(view3d.view.frame)-60)/2;
    progress = [[ProgressView alloc]initWithFrame:CGRectMake(x, y, 60, 60)];
    progress.backgroundColor = [UIColor clearColor];
    progress.centerColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.8];
    progress.arcFinishColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    progress.arcUnfinishColor = [UIColor colorWithRed:255 green:255 blue:255 alpha:0.8];
    progress.arcBackColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    progress.percent = 0;
    [_scrollView addSubview:progress];
    
    NSString *modelName = _model.modelName;
    NSString *modelSuffix = _model.modelSuffix;
    NSString *modelUrl = [NSString stringWithFormat:@"%@upload/%@/%@/%@_%@.%@", mainURL, modelSuffix, modelName, modelName, @"1", modelSuffix];
    [LoaderModelUtil loadModel:modelUrl
                       modelId:[NSString stringWithFormat:@"%li", (long)_model.idNum]
                      userInfo:nil delegate:self];
}

- (void)loadProgress:(float)newProgress modelId:(NSString *)modelId
{
    progress.percent = newProgress;
}

- (void)loadModelComplete:(NSString *)modelId userInfo:(NSDictionary *)userInfo
{
    view3d.view.hidden = NO;
    _singleProductImgView.hidden = YES;
    btn3d.hidden = YES;
    NSDictionary *model = @{@"modelName":_model.modelName};
    [view3d addModel:model];
    [progress removeFromSuperview];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (view3d)
    {
        CGRect f = view3d.view.frame;
        f.origin.y = -scrollView.contentOffset.y+64+15;
        view3d.view.frame = f;
        
        f = progress.frame;
        f.origin.y = (CGRectGetHeight(view3d.view.frame)-60)/2-scrollView.contentOffset.y;
        progress.frame = f;
    }
}


- (void)dealloc
{
    self.returnKeyHandler = nil;
}
@end
