//
//  MJStoreCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/26.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJStoreCtler.h"
#import "MJConmon.h"
#import "MJSearchBar.h"
#import "MJGoodsDropdownView.h"
#import "MJNewProductsCell.h"
#import "MJProductDetailsCtler.h"
#import "MJProductGroupDetailsCtler.h"
#import "MJNetworkTool.h"
#import "MJProductListModel.h"
#import "MJProductListDataModel.h"
#import "MJProductModel.h"
#import "MJBrandDataModel.h"
#import "MJCreatedDateModel.h"
#import "MJModifiedDateModel.h"
#import "MJExtension.h"
#import "MJRefresh.h"
#import "MJSpaceTypeModel.h"
#import "GMDCircleLoader.h"
#import "MJUIClassTool.h"
#import "MJSetHotProductView.h"
#import "MJBrandDetailModel.h"
#import "MJBrandDetailDataModel.h"
#import "MJBrandDetailCaseModel.h"
#import "AppDelegate.h"
#import "MJSelectEditImageView.h"
#import "MJSelectEditImageCell.h"
#import "MJGroupSelectImageView.h"
#import "MJGroupSelectImageCell.h"
#import "UIImageView+WebCache.h"
#import "AFNetworking.h"
#import "UIBarButtonItem+Extension.h"
#import <CHTCollectionViewWaterfallLayout.h>
#import "MJEditNewProductView.h"
#import "MBProgressHUD.h"
@interface MJStoreCtler ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UIAlertViewDelegate,CHTCollectionViewDelegateWaterfallLayout>

#define kEditImageViewWidth 650
#define kEditImageViewHeight 520
//更多view宽度
#define kMoreViewWidth 266

/* 顶部按钮所在的view*/
@property (weak, nonatomic) IBOutlet UIView *selectView;
/* 热销推荐*/
@property (weak, nonatomic) IBOutlet UIButton *hotListButton;
/* 全部商品按钮*/
@property (weak, nonatomic) IBOutlet UIButton *allGoodsButton;
/* 新品上市按钮*/
@property (weak, nonatomic) IBOutlet UIButton *hotNewBtn;
/* 热销商品编辑按钮*/
@property (strong, nonatomic) UIButton *hotListEditButton;

/* 新品置新按钮*/
@property (strong, nonatomic) UIButton *hotNewEditButton;
/* 取消置新按钮*/
@property (strong, nonatomic) UIButton *cancelNewEditButton;

/* 热销商品所在的view*/
@property (weak, nonatomic) IBOutlet UIView *bottomBigView;
/* 热销商品imageView*/
@property (weak, nonatomic) IBOutlet UIImageView *firstImgView;
@property (weak, nonatomic) IBOutlet UIImageView *secondImgView;
@property (weak, nonatomic) IBOutlet UIImageView *thirdImgView;
@property (weak, nonatomic) IBOutlet UIImageView *fourthImgView;
@property (weak, nonatomic) IBOutlet UIImageView *fifthImgView;
@property (weak, nonatomic) IBOutlet UIImageView *sixImgView;
@property (weak, nonatomic) IBOutlet UIImageView *sevenImgView;
/* 新品上市*/
@property (nonatomic,strong) UICollectionView *collectionView;
/* 弹出视图*/
@property (nonatomic,strong) MJGoodsDropdownView *dropdownView;
/* 商品列表*/
@property (nonatomic,strong) NSMutableArray *productListArr;
/* 商品类型列表*/
@property (nonatomic,strong) NSMutableArray *productTypeArr;
/* 设置热销商品view*/
@property (nonatomic,strong) MJSetHotProductView *setHotProductView;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 热销弹出选择商品View*/
@property (nonatomic,strong) MJSelectEditImageView *editImageView;
/* 热销弹出选择商品View*/
@property (nonatomic,strong) MJGroupSelectImageView *groupImageView;
/* 弹出覆盖View*/
@property (nonatomic,strong) UIButton *storeCoverView;
/* alertView(搭配)*/
@property (nonatomic,strong) UIAlertView *matchAlertView;
/* alertView(单品)*/
@property (nonatomic,strong) UIAlertView *singleAlertView;
/* 保存点击哪副图上的编辑按钮*/
@property (nonatomic,assign) NSInteger index;
/* 所有imageView数组*/
@property (nonatomic,strong) NSMutableArray *imageViewArr;
/* 所有imageView数据数组*/
@property (nonatomic,strong) NSMutableArray *spreadDataArr;
/* 所有imageViewUrl的数组*/
@property (nonatomic,strong) NSMutableArray *imageViewUrlArr;
/* 点击确定上传后的URL*/
@property (nonatomic,strong) NSURL *uploadImageUrl;
/* 上传字典参数数组*/
@property (nonatomic,strong) NSMutableArray *uploadParaArr;
/* 要上传的搭配模型*/
@property (nonatomic,strong) MJMatchProductModel *matchModel;
/* 要上传的单品模型*/
@property (nonatomic,strong) MJProductModel *singleProductModel;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
/* 商品选项下标*/
@property (nonatomic,assign) NSInteger dropdownIndex;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
/* 设置选中按钮*/
@property (nonatomic,strong) UIButton *selectBtn;
/* 设置新品上市View*/
@property (nonatomic,strong) MJEditNewProductView *editNewView;
@end

@implementation MJStoreCtler

static NSString * const reuseIdentifier = @"Cell";

#pragma mark - 设置新品
- (MJEditNewProductView *)editNewView
{
    if (!_editNewView) {
        _editNewView = [[MJEditNewProductView alloc] initWithFrame:
        CGRectMake(0, 0, kEditImageViewWidth, kEditImageViewHeight)];
        _editNewView.backgroundColor = [UIColor whiteColor];
        _editNewView.center = self.view.center;
        
    }
    return _editNewView;
}

- (NSMutableArray *)spreadDataArr
{
    if (_spreadDataArr == nil) {
        _spreadDataArr = [NSMutableArray array];
    }
    return _spreadDataArr;
}

- (NSMutableArray *)imageViewArr
{
    if (_imageViewArr == nil) {
        _imageViewArr = [NSMutableArray array];
        [_imageViewArr addObject:_firstImgView];
        [_imageViewArr addObject:_secondImgView];
        [_imageViewArr addObject:_thirdImgView];
        [_imageViewArr addObject:_fourthImgView];
        [_imageViewArr addObject:_fifthImgView];
        [_imageViewArr addObject:_sixImgView];
        [_imageViewArr addObject:_sevenImgView];
    }
    return _imageViewArr;
}


- (NSMutableArray *)imageViewUrlArr
{
    if (_imageViewUrlArr == nil) {
        _imageViewUrlArr = [NSMutableArray array];
    }
    return _imageViewUrlArr;
}

#pragma mark - 单品编辑
- (MJSelectEditImageView *)editImageView
{
    if (_editImageView == nil) {
        _editImageView = [[MJSelectEditImageView alloc] initWithFrame:CGRectMake(0, 0, kEditImageViewWidth, kEditImageViewHeight)];
        _editImageView.backgroundColor = [UIColor whiteColor];
        _editImageView.center = self.view.center;
        __weak MJStoreCtler *ctl = self;
        _editImageView.btnClick = ^(NSInteger index){
            switch (index) {
                case 0:
                {//确定
                    NSMutableArray *arr = [NSMutableArray array];
                    for (MJProductModel *productModel in ctl.editImageView.productListArr) {
                        if (productModel.isSelected) {
                            [arr addObject:productModel];
                        }
                    }
                  //  NSLog(@"%ld",arr.count);
                    if (arr.count>1) {
                        
                        ctl.singleAlertView = [[UIAlertView alloc] initWithTitle:@"提示只能选一种类型" message:@"请重新选择" delegate:ctl cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        
                        [ctl.singleAlertView show];
                        
                    }else if(arr.count == 1)
                    {//上传
                        MJProductModel *model = arr[0];
                        model.selected = NO;
                        ctl.singleProductModel = model;
                        ctl.uploadImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
                        [ctl.imageViewArr[ctl.index] sd_setImageWithURL:ctl.uploadImageUrl placeholderImage:nil];
                        [ctl.setHotProductView.imageViewArr[ctl.index] sd_setImageWithURL:ctl.uploadImageUrl placeholderImage:nil];
                        //NSLog(@"%@",ctl.uploadImageUrl);
                       
                 NSDictionary *dict1 = @{@"id":[NSString stringWithFormat:@"%ld",model.idNum],@"type":@"2",@"imageID":[NSString stringWithFormat:@"%ld",model.imageID],@"imageName":model.imageName,@"imageSuffix":model.imageSuffix};
                        //表示使用哪一套模板
                        NSString *template = @"1";
                        NSLog(@"%ld",ctl.uploadParaArr.count);
                        [ctl.uploadParaArr replaceObjectAtIndex:ctl.index withObject:dict1];
                       // NSLog(@"%ld",ctl.uploadParaArr.count);
                        NSArray *dataList1 = [NSArray arrayWithArray:ctl.uploadParaArr];
                        
                        [MJUserDefaults setObject:dataList1 forKey:@"uploadParaArr"];
                        [MJUserDefaults synchronize];
                        NSDictionary *data = @{@"template":template,@"dataList":dataList1};
                      
                        NSError *error1;
                        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error1];
                        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                        NSDictionary *para = @{@"spreadData":jsonString};
                         //  NSLog(@"%@",para);
                        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
       
                        [manager POST:brandDetailSpreadSaveURL parameters:para constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                            //上传图片，以文件流的格式
                            //   [formData appendPartWithFileData:imageData name:@"file" fileName:model.name mimeType:@"image/png"];
                            //  NSLog(@"%@",formData);
                        } progress:^(NSProgress * _Nonnull uploadProgress) {
                            
                        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                            NSDictionary *resDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                            
                            NSLog(@"%@",resDict);

                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                            NSLog(@"%@",error);
                            if (error.code == -1004) {
                                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
                                hud.removeFromSuperViewOnHide =YES;
                                hud.mode = MBProgressHUDModeCustomView;
                                hud.labelText = NSLocalizedString(@"连接服务器超时,请稍后重试", nil);
                                // hud.minSize = CGSizeMake(100, 100);
                                hud.layer.cornerRadius = 50;
                                [hud hide:YES afterDelay:1.5];
                            }
                        }];
                        
                        [ctl.editImageView removeFromSuperview];
                        [ctl.chatCoverView removeFromSuperview];
                         ctl.editImageView = nil;
                       
                    }else
                    {
                        [ctl.editImageView removeFromSuperview];
                        [ctl.chatCoverView removeFromSuperview];
                         ctl.editImageView = nil;
                    }
                  
                }
                    break;
                case 1:
                {//取消
                    [ctl.editImageView removeFromSuperview];
                    [ctl.chatCoverView removeFromSuperview];
                     ctl.editImageView = nil;
                    
                }
                    break;
                default:
                    break;
            }
        };

    }
    return _editImageView;
}

#pragma mark - 组合编辑
- (MJGroupSelectImageView *)groupImageView
{
    if (_groupImageView == nil) {
        _groupImageView = [[MJGroupSelectImageView alloc] initWithFrame:CGRectMake(0, 0, kEditImageViewWidth, kEditImageViewHeight)];
        _groupImageView.backgroundColor = [UIColor whiteColor];
        _groupImageView.center = self.view.center;
        __weak MJStoreCtler *ctl = self;
        _groupImageView.btnClick = ^(NSInteger index){
            switch (index) {
                case 0:
                {//确定
                    NSMutableArray *arr = [NSMutableArray array];
                    for (MJMatchProductModel *matchModel in ctl.groupImageView.matchProductArr) {
                        if (matchModel.isSelected) {
                            [arr addObject:matchModel];
                        }
                    }
                    if (arr.count>1) {
                        ctl.matchAlertView = [[UIAlertView alloc] initWithTitle:@"提示只能选择一种类型" message:@"请重新选择" delegate:ctl cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [ctl.matchAlertView show];
                    }else if(arr.count == 1){
                    //上传
                        MJMatchProductModel *model = arr[0];
                        model.selected = NO;
                        ctl.matchModel = model;
                        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,model.collocationImageSuffix,model.collocationImageName,model.collocationImageName,model.collocationImageSuffix]];
                     //   NSLog(@"%@",url);
                        [ctl.imageViewArr[ctl.index] sd_setImageWithURL:url placeholderImage:nil];
                        [ctl.setHotProductView.imageViewArr[ctl.index] sd_setImageWithURL:url placeholderImage:nil];
                        
                    NSDictionary *dict1 = @{@"id":[NSString stringWithFormat:@"%ld",model.idNum],@"type":@"1",@"imageID":[NSString stringWithFormat:@"%ld",model.userID],@"imageName":model.collocationImageName,@"imageSuffix":model.collocationImageSuffix};
                        
                    [ctl.uploadParaArr replaceObjectAtIndex:ctl.index withObject:dict1];
                     //   NSLog(@"%ld",ctl.uploadParaArr.count);
                        
                    NSArray *dataList1 = [NSArray arrayWithArray:ctl.uploadParaArr];
                    [MJUserDefaults setObject:dataList1 forKey:@"uploadParaArr"];
                    [MJUserDefaults synchronize];
                    //表示使用哪一套模板
                    NSString *template = @"1";
                 //   NSLog(@"%ld",ctl.uploadParaArr.count);
                    NSDictionary *data = @{@"template":template,@"dataList":ctl.uploadParaArr};
                    NSError *error1;
                    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error1];
                    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                      //  NSLog(@"%@",jsonString);
                    NSDictionary *para = @{@"spreadData":jsonString};
                        
                 //   NSLog(@"%@",para);
                    
                    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                     manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                        [manager POST:brandDetailSpreadSaveURL parameters:para constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                            //上传图片，以文件流的格式
                            //   [formData appendPartWithFileData:imageData name:@"file" fileName:model.name mimeType:@"image/png"];
                            //  NSLog(@"%@",formData);
                        } progress:^(NSProgress * _Nonnull uploadProgress) {
                            
                        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                            // NSDictionary *resDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                            
                            // NSLog(@"%@",resDict);
                            // [ctl getBrandSpreadData];
                        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                            NSLog(@"%@",error);
                            if (error.code == -1004) {
                                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
                                hud.removeFromSuperViewOnHide =YES;
                                hud.mode = MBProgressHUDModeCustomView;
                                hud.labelText = NSLocalizedString(@"连接服务器超时,请稍后重试", nil);
                                // hud.minSize = CGSizeMake(100, 100);
                                hud.layer.cornerRadius = 50;
                                [hud hide:YES afterDelay:1.5];
                            }
                        }];
                        [ctl.groupImageView removeFromSuperview];
                        [ctl.chatCoverView removeFromSuperview];
                         ctl.groupImageView = nil;
                      
                    }else
                    {
                        [ctl.groupImageView removeFromSuperview];
                        [ctl.chatCoverView removeFromSuperview];
                         ctl.groupImageView = nil;

                    }
                    
                }
                    break;
                case 1:
                {//取消
                   [ctl.groupImageView removeFromSuperview];
                   [ctl.chatCoverView removeFromSuperview];
                    ctl.groupImageView = nil;
                }
                    break;
                default:
                    break;
            }
        };
    }
    return _groupImageView;
}

- (NSMutableArray *)productTypeArr
{
    if (!_productTypeArr) {
        _productTypeArr = [NSMutableArray array];
    }
    return _productTypeArr;
}

- (NSMutableArray *)productListArr
{
    if (_productListArr == nil) {
        _productListArr = [NSMutableArray array];
    }
    return _productListArr;
}

- (UIButton *)storeCoverView
{
    if (!_storeCoverView) {
        _storeCoverView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Screen_Width/*-kMoreViewWidth*/, Screen_Height)];
        _storeCoverView.backgroundColor = [UIColor grayColor];
        _storeCoverView.alpha = 0.5;
       // [_coverView addTarget:self action:@selector(coverClick) forControlEvents:UIControlEventTouchUpInside];
        // [self.view addSubview:_coverView];
    }
    return _storeCoverView;
}

#pragma mark - collectionView
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        
        CGFloat inset = 2;
        layout.sectionInset = UIEdgeInsetsMake(inset, inset, inset, inset);
        layout.minimumColumnSpacing = inset;
        layout.minimumInteritemSpacing = inset;
        layout.columnCount = 4;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 114, Screen_Width, Screen_Height-114) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = RGB(225, 225, 225);
        [_collectionView registerClass:[MJNewProductsCell class] forCellWithReuseIdentifier:reuseIdentifier];
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}


#pragma mark - 全部商品下拉弹窗
- (MJGoodsDropdownView *)dropdownView
{
    if (_dropdownView == nil) {
        _dropdownView = [MJGoodsDropdownView dropdown];
        _dropdownView.frame = CGRectMake(0, 114, Screen_Width, 50);
        _dropdownView.allGoodsBtn.selected = YES;
        _dropdownView.alpha = 0.5;
        __weak MJStoreCtler *ctl = self;
        
        //商品类型列表点击
        _dropdownView.click = ^(NSInteger index){
             NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
            NSDictionary *dict = @{@"productTypeID" : [NSString stringWithFormat:@"%ld",index+1-30],@"pageNumber" : [NSString stringWithFormat:@"%ld",ctl.pageNumber],@"pageSize" :@12,@"brandID" : brandID,@"type":@"0"};
            ctl.dropdownIndex = index+1-30;
            //NSLog(@"%@",dict);
            switch (index-30) {
                case 0:
                {
                    
                    [ctl dropdownClickIndex:0 dictionary:dict];
                   // NSLog(@"%@",dict);
                    break;
                }
                case 1:
                {
                  
                    [ctl dropdownClickIndex:1 dictionary:dict];
                    break;
                }
                case 2:
                {
                      [ctl dropdownClickIndex:2 dictionary:dict];
                    break;
                }
                case 3:
                {
                      [ctl dropdownClickIndex:3 dictionary:dict];
                    break;
                }
                case 4:
                {
                      [ctl dropdownClickIndex:4 dictionary:dict];
                    break;
                }
                case 5:
                {
                      [ctl dropdownClickIndex:5 dictionary:dict];
                    break;
                }
                case 6:
                {
                      [ctl dropdownClickIndex:6 dictionary:dict];
                    break;
                }
                case 7:
                {
                      [ctl dropdownClickIndex:7 dictionary:dict];
                    break;
                }
                case 8:
                {
                    NSDictionary *dict1 = @{@"productTypeID" : [NSString stringWithFormat:@"%d",0],@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)ctl.pageNumber],@"brandID" : brandID,@"pageSize" :@12,@"type":@"0"};
                    [ctl dropdownClickIndex:8 dictionary:dict1];
                    break;
                }
                default:
                    break;
            }
        };
    }
    return _dropdownView;
}

#pragma mark - 热销推荐商品设置
- (MJSetHotProductView *)setHotProductView
{
    if (_setHotProductView == nil) {
        _setHotProductView = [MJSetHotProductView hotView];
        _setHotProductView.frame = CGRectMake(0, 114, Screen_Width, Screen_Height-114);
        [_setHotProductView setButtonHidden:NO];
        [_setHotProductView setCoverHidden:YES];
        _setHotProductView.hidden = YES;
        __weak MJStoreCtler *ctl = self;
        _setHotProductView.editImage = ^(NSInteger index){
            ctl.index = index;
            
          //  NSLog(@"%ld点击",ctl.index);
            switch (index) {
                case 0:
                {
                    [ctl.view addSubview:ctl.chatCoverView];
                    [ctl.view addSubview:ctl.groupImageView];
                    [ctl.groupImageView.collectionView reloadData];
                    [ctl.view bringSubviewToFront:ctl.groupImageView];
                }
                    break;
                case 1:
                {
                    [ctl.view addSubview:ctl.chatCoverView];
                    [ctl.view addSubview:ctl.groupImageView];
                    [ctl.groupImageView.collectionView reloadData];
                    [ctl.view bringSubviewToFront:ctl.groupImageView];
                }
                    break;
                case 2:
                {
                    
                    [ctl.view addSubview:ctl.chatCoverView];
                    [ctl.view addSubview:ctl.editImageView];
                    [ctl.editImageView.collectionView reloadData];
                    [ctl.view bringSubviewToFront:ctl.editImageView];
                    
                }
                    break;
                case 3:
                {
                    [ctl.view addSubview:ctl.chatCoverView];
                    [ctl.view addSubview:ctl.editImageView];
                    [ctl.editImageView.collectionView reloadData];
                    [ctl.view bringSubviewToFront:ctl.editImageView];
                    
                }
                    break;
                case 4:
                {
                    [ctl.view addSubview:ctl.chatCoverView];
                    [ctl.view addSubview:ctl.editImageView];
                    [ctl.editImageView.collectionView reloadData];
                    [ctl.view bringSubviewToFront:ctl.editImageView];
                }
                    break;
                case 5:
                {
                    [ctl.view addSubview:ctl.chatCoverView];
                    [ctl.view addSubview:ctl.editImageView];
                    [ctl.editImageView.collectionView reloadData];
                    [ctl.view bringSubviewToFront:ctl.editImageView];
                }
                    break;
                case 6:
                {
                    [ctl.view addSubview:ctl.chatCoverView];
                    [ctl.view addSubview:ctl.editImageView];
                    [ctl.editImageView.collectionView reloadData];
                    [ctl.view bringSubviewToFront:ctl.editImageView];
                }
                    break;
                case 7:
                {//组合跳转
                    MJProductGroupDetailsCtler *group = [[MJProductGroupDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJMatchProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        group.idNum = model.idNum;
                        
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:group animated:YES];
                        }
                        
                    }
                    
                }
                    break;
                case 8:
                {
                    MJProductGroupDetailsCtler *group = [[MJProductGroupDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJMatchProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        group.idNum = model.idNum;
                        
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:group animated:YES];
                        }
                        
                    }
                }
                    break;
                case 9:
                {//单品跳转
                    MJProductDetailsCtler *detail = [[MJProductDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                    MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                    detail.idNum = model.idNum;
                         if (model.idNum > 0) {
                             [ctl.navigationController pushViewController:detail animated:YES];
                         }
                    }
                }
                    break;
                case 10:
                {
                    MJProductDetailsCtler *detail = [[MJProductDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        detail.idNum = model.idNum;
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:detail animated:YES];
                        }
                    }
                }
                    break;
                case 11:
                {
                    MJProductDetailsCtler *detail = [[MJProductDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        detail.idNum = model.idNum;
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:detail animated:YES];
                        }
                    }
                }
                    break;
                case 12:
                {
                    MJProductDetailsCtler *detail = [[MJProductDetailsCtler alloc] init];
                    
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                    MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                    
                        detail.idNum = model.idNum;
                         if (model.idNum > 0) {
                             [ctl.navigationController pushViewController:detail animated:YES];
                         }
                    }
                    
                }
                    break;
                case 13:
                {
                    MJProductDetailsCtler *detail = [[MJProductDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        detail.idNum = model.idNum;
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:detail animated:YES];
                        }
                    }
                }
                    break;
                default:
                    break;
            }

        };
        _setHotProductView.hidden = YES;
    [self.view addSubview:_setHotProductView];
    }
    return _setHotProductView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    __weak MJStoreCtler *ctl = self;
    self.uploadParaArr = [NSMutableArray array];
    //获取商品类型列表
    [net getMoJingURL:MJProductTypeList parameters:nil success:^(id obj) {
        ctl.productTypeArr = obj;
        NSMutableArray *btnNameArr = [NSMutableArray array];
        for (MJSpaceTypeModel *m in ctl.productTypeArr) {
            NSString *name = m.name;
            [btnNameArr addObject:name];
        }
        [ctl.dropdownView setBtnTitleWithArray:btnNameArr];
    } failure:^(NSError *error) {
         NSLog(@"%@",error);
    }];
}

- (void)initUI
{
    //通知
    [MJNotificationCenter addObserver:self selector:@selector(mjVcHiddenDidChange:) name:@"mjStartService" object:nil];
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    //self.automaticallyAdjustsScrollViewInsets=NO;
    self.pageNumber = 1;
    [self setTitle:@"商城列表页"];
    self.view.backgroundColor = RGB(246, 246, 246);
    //添加搜索框
    MJSearchBar *searchBar = [[MJSearchBar alloc] initWithFrame:CGRectMake(Screen_Width - 268, 10, 258, 30)];
    searchBar.placeholder = @"搜商品";
    searchBar.delegate = self;
    searchBar.returnKeyType = UIReturnKeySearch;
    [self.selectView addSubview:searchBar];
    
    //热销编辑按钮
    self.hotListEditButton = [MJUIClassTool createBtnWithFrame:CGRectMake(Screen_Width-268-100, 10, 75, 30) title:@"" image:[UIImage imageNamed:@"store_edit_button"] target:self action:@selector(selectEditBtnClick:)];
    _hotListEditButton.tag = 23;
    [self.selectView addSubview:self.hotListEditButton];
    
    //新品编辑按钮
    self.hotNewEditButton = [MJUIClassTool createBtnWithFrame:CGRectMake(Screen_Width-268-100, 10, 75, 30) title:@"" image:[UIImage imageNamed:@"store_edit_button"] target:self action:@selector(selectEditBtnClick:)];
  //  self.hotNewEditButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_hotNewEditButton setBackgroundColor:RGB(0, 146, 221)];
//    [_hotNewEditButton setTitle:@"商品置新" forState:UIControlStateNormal];
//    [_hotNewEditButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    _hotNewEditButton.frame = CGRectMake(Screen_Width-268-100-85-70, 10, 110, 30);
//    [_hotNewEditButton addTarget:self action:@selector(selectEditBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//     _hotNewEditButton.layer.cornerRadius = 3;
    _hotNewEditButton.tag = 24;
    [self.selectView addSubview:self.hotNewEditButton];
    
    //取消置新
    self.cancelNewEditButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cancelNewEditButton setBackgroundColor:RGB(0, 146, 221)];
    [_cancelNewEditButton setTitle:@"取消置新" forState:UIControlStateNormal];
    [_cancelNewEditButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _cancelNewEditButton.frame = CGRectMake(Screen_Width-268-100-35, 10, 110, 30);
    _cancelNewEditButton.tag = 25;
    _cancelNewEditButton.layer.cornerRadius = 3;
    [_cancelNewEditButton addTarget:self action:@selector(selectEditBtnClick:) forControlEvents:UIControlEventTouchUpInside];
  //  [self.selectView addSubview:self.cancelNewEditButton];
    
    self.hotNewEditButton.hidden = YES;
    self.hotListButton.selected = YES;
    self.cancelNewEditButton.hidden = YES;
    //self.setHotProductView.hidden = YES;
    [self setHotImageView];
  
}

#pragma mark - 隐藏服务客户
-(void)mjVcHiddenDidChange:(NSNotification *)notification
{
    BOOL hidden = [notification.userInfo[@"mjVcHidden"] boolValue];
    self.moreView.serviceView.hidden = hidden;
    
}

#pragma mark - 返回
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
}

#pragma mark - 设置热销图片从服务器解析数据读取
- (void)setHotImageView
{
    //设置热销图片
    [self.imageViewUrlArr removeAllObjects];
    NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
    NSString *userID  = [MJUserDefaults objectForKey:@"userID"];
    NSDictionary *dict = @{@"brandID" : brandID,@"userID":userID};
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    if ([self isConnectToNet]) {
        [net getMoJingURL:MJBrandDetail parameters:dict success:^(id obj) {
            MJBrandDetailModel *detailModel = [MJBrandDetailModel mj_objectWithKeyValues:obj];
            //完整信息模型
            MJBrandDetailDataModel *dataModel = [MJBrandDetailDataModel mj_objectWithKeyValues:detailModel.data];
            
            //热销信息模型数组
            MJBrandDetailSpreadModel *spreadModel = [MJBrandDetailSpreadModel mj_objectWithKeyValues:dataModel.spreadData];
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJBrandDetailSpreadDataModel mj_objectArrayWithKeyValuesArray:spreadModel.dataList];
            self.spreadDataArr = arr;
            if (arr.count>0) {
                
                for (MJBrandDetailSpreadDataModel *model in arr) {
                    NSDictionary *spreadDict = model.mj_keyValues;
                    NSLog(@"%@",spreadDict);
                    if ([spreadDict isKindOfClass:[NSNull class]]) {
                        spreadDict = @{@"id":@1001,@"type":@"0",@"imageID":@1001,@"imageName":[NSString stringWithFormat:@"1234%d",3],@"imageSuffix":@"png"};
                    }
                    [_uploadParaArr addObject:spreadDict];
                    
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
                    
                    //  NSLog(@"%@ %@",url,model.imageSuffix);
                    [self.imageViewUrlArr addObject:url];
                }
            }else
            {
                //没数据
                 UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-100, 147, 360, 30) title:@"点击工具条上的编辑按钮设置热销推荐商品" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                [self addNoDataViewWith:nil];
                [_noDataView addSubview:lb];
                
                for (int i = 0; i < 7; i++) {
                    
                    NSDictionary *dict = @{@"id":@1001,@"type":@"0",@"imageID":@1001,@"imageName":[NSString stringWithFormat:@"1234%d",i],@"imageSuffix":@"png"};
                    
                    [_uploadParaArr addObject:dict];
                }
            }
            //  NSLog(@"%ld",self.imageViewUrlArr.count);
            if (self.imageViewUrlArr.count > 0) {
                for (int i = 0; i < 7; i++) {
     //               NSLog(@"%ld",self.spreadDataArr.count);
                    UIImageView *imgV = self.imageViewArr[i];
                    imgV.backgroundColor = [UIColor whiteColor];
                    imgV.contentMode = UIViewContentModeScaleAspectFit;
                    [imgV sd_setImageWithURL:self.imageViewUrlArr[i] placeholderImage:nil];
                    
                    // NSLog(@"%@",self.imageViewUrlArr[i]);
                }
                
            }
        } failure:^(NSError *error) {
            
        }];

    }
    
}


#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_circulateBackgroungView];
    self.selectView.userInteractionEnabled = NO;
    self.dropdownView.userInteractionEnabled = NO;
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    self.selectView.userInteractionEnabled = YES;
    self.dropdownView.userInteractionEnabled = YES;
    [self.circulateBackgroungView removeFromSuperview];
    //[GMDCircleLoader hideFromView:self.view animated:YES];
  
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 下拉弹窗点击封装
- (void)dropdownClickIndex:(NSInteger)index dictionary:(NSDictionary *)dict
{
     self.setHotProductView.hidden = YES;
     __weak MJStoreCtler *ctl = self;
   
        self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [ctl loadNewDataWithPara:dict];
        }];
    [self loadingData];
    [self.collectionView.mj_header beginRefreshing];
    //[self.dropdownView removeFromSuperview];
    [self.collectionView reloadData];
    self.allGoodsButton.selected = YES;
    self.collectionView.hidden = NO;
    self.bottomBigView.hidden = YES;
    [self.view addSubview:self.dropdownView];
}

- (void)addRefreshWithDict:(NSDictionary *)dict
{
    self.setHotProductView.hidden = YES;
    __weak MJStoreCtler *ctl = self;
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [ctl loadNewDataWithPara:dict];
    }];
    [self loadingData];
    [self.collectionView.mj_header beginRefreshing];
    //[self.dropdownView removeFromSuperview];
    [self.collectionView reloadData];
    self.allGoodsButton.selected = NO;
    self.collectionView.hidden = NO;
    self.bottomBigView.hidden = YES;
    [self.dropdownView removeFromSuperview];
}

#pragma mark - 下拉加载新数据
- (void)loadNewDataWithPara:(NSDictionary *)dict
{
    self.pageNumber = 1;
    
    NSString *type = [dict objectForKey:@"type"];
    //获取商品列表数据
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    __weak MJStoreCtler *ctl = self;
    [self.noDataView removeFromSuperview];
    [self.productListArr removeAllObjects];
    if ([self isConnectToNet]) {
        
            [net getMoJingURL:MJProductList parameters:dict success:^(id obj) {
           //     NSLog(@"%@",dict);
            MJProductListModel *listModel = obj;
            MJProductListDataModel *model = listModel.data;
                self.pageCount = model.pageCount;
            // 把字典数组转换成模型数组
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
                
           // for (MJProductModel *m in ctl.productListArr) {
//                MJBrandDataModel *brand = m.brandData;
//                MJCreatedDateModel *create = m.createdDate;
//                MJModifiedDateModel *modify = m.modifiedDate;
             //   NSLog(@"%ld %@ %@ %@",brand.idNum,m.userID,create.time,modify.day);
          //  }
              
                [ctl.productListArr addObjectsFromArray:arr];
               
                if (ctl.productListArr.count == 0) {
                    if ([type isEqualToString:@"3"]) {
                        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-40, 147, 260, 30) title:@"请登陆mj.duc.cn设置新品上市" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                         [ctl addNoDataViewWith:nil];
                         [_noDataView addSubview:lb];
                    }else{
                        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-5, 147, 180, 30) title:@"没有搜到相关的商品" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                        [ctl addNoDataViewWith:nil];
                        [_noDataView addSubview:lb];
                    }
                }
              
              //  NSLog(@"%ld",ctl.productListArr.count);
                [ctl stopCircleLoader];
                
                if (ctl.productListArr.count >= 12) {
                    ctl.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        // 进入刷新状态后会自动调用这个block
                        [ctl loadMoreData:dict];
                    }];
                    // 默认先隐藏footer
                    ctl.collectionView.mj_footer.hidden = YES;
                }else{
                    ctl.collectionView.mj_footer = nil;
                }

            [ctl.collectionView reloadData];
            [ctl.collectionView.mj_header endRefreshing];
            [ctl.view endEditing:YES];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [ctl stopCircleLoader];
            [ctl.collectionView.mj_header endRefreshing];
        }];
     
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_header endRefreshing];
    }
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
   // [self.productListArr removeAllObjects];
    //获取商品列表数据
    _pageNumber++;
  //  NSLog(@"%ld",_pageNumber);
    NSString *str = [dict objectForKey:@"productTypeID"];
    NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
    NSString *type = [dict objectForKey:@"type"];
    
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSDictionary *moreDict = @{@"productTypeID" : str,@"pageNumber" : [NSString stringWithFormat:@"%ld",_pageNumber],@"pageSize" : @12,@"brandID" : brandID,@"type" : type};
    __weak MJStoreCtler *ctl = self;
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            [net getMoJingURL:MJProductList parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                MJProductListModel *listModel = obj;
                 NSLog(@"%@",listModel.mj_keyValues);
                MJProductListDataModel *model = listModel.data;
                // 把字典数组转换成模型数组
                NSMutableArray *arr = [NSMutableArray array];
                arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
                
//                for (MJProductModel *m in ctl.productListArr) {
//                    MJBrandDataModel *brand = m.brandData;
//                    MJCreatedDateModel *create = m.createdDate;
//                    MJModifiedDateModel *modify = m.modifiedDate;
//                    //  NSLog(@"%ld %@ %@ %@",brand.idNum,m.userID,create.time,modify.day);
//                }
                [ctl.productListArr addObjectsFromArray:arr];
             //   NSLog(@"%ld",ctl.productListArr.count);
                [UIView animateWithDuration:0.1 animations:^{
                    [ctl.collectionView reloadData];
                }];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            }];
            
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
    }
}


#pragma mark - 顶部按钮点击
- (IBAction)selectBtnClick:(UIButton *)button {
    __weak MJStoreCtler *ctl = self;
    self.hotListButton.selected = NO;
    self.selectBtn.selected = NO;
    button.selected = YES;
    self.selectBtn = button;

    switch (button.tag) {
        case 20:
        {
           // NSLog(@"热销推荐");
            [self stopCircleLoader];
          //  [self getBrandSpreadData];
            self.hotNewEditButton.hidden = YES;
            self.hotNewEditButton.selected = NO;
            self.cancelNewEditButton.hidden = YES;
            self.cancelNewEditButton.selected = NO;
            self.hotListEditButton.hidden = NO;
            self.bottomBigView.hidden = NO;
            self.allGoodsButton.selected = NO;
            self.hotNewBtn.selected = NO;
            self.collectionView.hidden = YES;
            self.hotListEditButton.selected = NO;
            self.setHotProductView.hidden = YES;
            [self.dropdownView removeFromSuperview];
            [self.noDataView removeFromSuperview];
            if (button.isSelected) {
                [self setHotImageView];
            }
            break;
        }
        case 21:
        {
           // NSLog(@"新品上市");
           // [self.bottomBigView addSubview:self.collectionView];
            
           // button.selected = !button.isSelected;
            [self.collectionView reloadData];
            self.hotListEditButton.hidden = YES;
            self.hotNewEditButton.hidden = NO;
            self.cancelNewEditButton.hidden = NO;
            self.bottomBigView.hidden = YES;
            self.allGoodsButton.selected = NO;
            self.hotListButton.selected = NO;
            self.collectionView.hidden = NO;
            self.setHotProductView.hidden = YES;
            self.hotListEditButton.selected = NO;
            [self.dropdownView removeFromSuperview];
            self.pageNumber = 1;
            NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
          //  if (button.isSelected) {
                NSDictionary *footerDict = @{@"productTypeID":@0,@"type" : @"3",@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"pageSize" :@12,@"brandID" : brandID};
                self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
                    [ctl loadNewDataWithPara:footerDict];
                }];
                
                // 马上进入刷新状态
                [self loadingData];
                [self.collectionView.mj_header beginRefreshing];
           // }
            break;
        }
        case 22:
        {//全部
            //self.bottomBigView.hidden = NO;
            [self.allGoodsButton setImage:[UIImage imageNamed:@"store_dropdown_down"] forState:UIControlStateSelected];
            self.hotNewBtn.selected = NO;
            self.hotListButton.selected = NO;
            self.hotListEditButton.hidden = YES;
            self.cancelNewEditButton.selected = NO;
            self.cancelNewEditButton.hidden = YES;
            self.hotNewEditButton.hidden = YES;
            self.setHotProductView.hidden = YES;
            NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
            self.pageNumber = 1;
          //  button.selected = !button.isSelected;
            if (button.isSelected == YES) {
                [self.view addSubview:self.dropdownView];
               // self.dropdownView.allGoodsBtn.selected = YES;
                NSDictionary *dict = @{@"productTypeID" : [NSString stringWithFormat:@"%d",0],@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)ctl.pageNumber],@"brandID" : brandID,@"pageSize" :@12,@"type" : @"0"};
                [self dropdownClickIndex:0 dictionary:dict];
            }else
            {
                [self.dropdownView removeFromSuperview];
            }
            break;
        }
          
        default:
            break;
    }
}

#pragma mark - 编辑
- (void)selectEditBtnClick:(UIButton *)button
{
     button.selected = !button.isSelected;
    if (button.tag == 23) {
        //NSLog(@"热销编辑");
        [self stopCircleLoader];
       
        self.bottomBigView.hidden = YES;
        self.allGoodsButton.selected = NO;
        self.hotListButton.selected = NO;
        self.hotNewBtn.selected = NO;
        self.collectionView.hidden = YES;
        self.setHotProductView.hidden = NO;
        [self.noDataView removeFromSuperview];
        NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
        NSString *userID  = [MJUserDefaults objectForKey:@"userID"];
        NSDictionary *dict = @{@"brandID" : brandID,@"userID":userID,};
        [self.setHotProductView setImageWithDict:dict];
        [self.dropdownView removeFromSuperview];
        
        if (button.selected) {
            [self.setHotProductView setButtonHidden:NO];
            [button setImage:[UIImage imageNamed:@"store_quit_edit"] forState:UIControlStateSelected];
            [self.setHotProductView setCoverHidden:YES];
            // NSLog(@"编辑");
        }else
        {
            //隐藏图片上的编辑按钮
            NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
            NSString *userID  = [MJUserDefaults objectForKey:@"userID"];
            NSDictionary *dict = @{@"brandID" : brandID,@"userID":userID,};
            
            [self.setHotProductView setImageWithDict:dict];
            [self.setHotProductView setButtonHidden:YES];
            [self.setHotProductView setCoverHidden:NO];
        }
    }else if (button.tag == 24)
    {
        if (button.selected) {
          
           // [button setImage:[UIImage imageNamed:@"store_quit_edit"] forState:UIControlStateSelected];
             NSLog(@"新品编辑");
            [self.view addSubview:self.chatCoverView];
            [self.view addSubview:self.editNewView];
            
            self.pageNumber = 1;
            
            [self.editNewView.collectionView reloadData];
            [self.view bringSubviewToFront:self.editNewView];
            __weak MJStoreCtler *ctl = self;
            self.editNewView.btnClick = ^(NSInteger index){
            
                button.selected = NO;
                switch (index) {
                    
                    case 1:
                    {//确定
                        [ctl.editNewView removeFromSuperview];
                        [ctl.chatCoverView removeFromSuperview];
                        ctl.allGoodsButton.selected = NO;
                        ctl.editNewView = nil;
                        NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
                        //  if (button.isSelected) {
                        
                        [ctl.productListArr removeAllObjects];
                        NSDictionary *footerDict = @{@"productTypeID":@0,@"type" : @"3",@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)ctl.pageNumber],@"pageSize" :@8,@"brandID" : brandID};
                        ctl.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
                            [ctl loadNewDataWithPara:footerDict];
                        }];
                        
                        // 马上进入刷新状态
                        [ctl loadingData];
                        [ctl.collectionView.mj_header beginRefreshing];

                    }
                        break;
                    default:
                        break;
                }
            };
        }else
        {
            
        }

    }
}

#pragma mark - 遮盖点击
- (void)coverBtnClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.navigationController.navigationBar.frame =  CGRectMake(0, 0, Screen_Width, 64);
        [self.moreView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        [self.mineView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        [self.coverView removeFromSuperview];
    }completion:^(BOOL finished) {
        self.replaceBarFrame = CGRectMake(0, 0, Screen_Width, 64);
        self.cameraItem.enabled = YES;
        self.chatItem.enabled = YES;
    }];
}

#pragma mark  <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.productListArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MJNewProductsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (self.productListArr.count > 0) {
         MJProductModel *model = self.productListArr[indexPath.row];
        
        NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
        [cell.singleProductImgView  sd_setImageWithURL:bgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //            if (image) {
            //                if (!CGSizeEqualToSize(model.imageSize, image.size)) {
            //                    model.imageSize = image.size;
            //                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            //                }
            //            }
        }];
        cell.singleProductName.text = model.name;
        cell.singleProductPrice.text = [NSString stringWithFormat:@"￥%ld",model.price];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        
    }
    
    return cell;
}

#pragma mark  <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MJProductDetailsCtler *ctl = [[MJProductDetailsCtler alloc] init];
    MJProductModel *model = self.productListArr[indexPath.row];
    ctl.idNum = model.idNum;
   
    [self.navigationController pushViewController:ctl animated:YES];
    //NSLog(@"详情点击%ld",indexPath.row);
}

#pragma mark - 图片手势
- (IBAction)imageViewClick:(UITapGestureRecognizer *)tap {
    NSInteger index = tap.view.tag-40;
    NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
    NSString *userID  = [MJUserDefaults objectForKey:@"userID"];
    NSDictionary *dict = @{@"brandID" : brandID,@"userID":userID,};
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    if ([self isConnectToNet]) {
        [net getMoJingURL:MJBrandDetail parameters:dict success:^(id obj) {
            MJBrandDetailModel *detailModel = [MJBrandDetailModel mj_objectWithKeyValues:obj];
            //完整信息模型
            MJBrandDetailDataModel *dataModel = [MJBrandDetailDataModel mj_objectWithKeyValues:detailModel.data];
            
            //热销信息模型数组
            MJBrandDetailSpreadModel *spreadModel = [MJBrandDetailSpreadModel mj_objectWithKeyValues:dataModel.spreadData];
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJBrandDetailSpreadDataModel mj_objectArrayWithKeyValuesArray:spreadModel.dataList];
            self.spreadDataArr = arr;
            
            MJProductGroupDetailsCtler *groupCtl = [[MJProductGroupDetailsCtler alloc] init];
            MJMatchProductModel *matchModel = [[MJMatchProductModel alloc] init];
            MJProductDetailsCtler *detail = [[MJProductDetailsCtler alloc] init];
            
            if (self.spreadDataArr.count > 0) {
                 MJBrandDetailSpreadDataModel *model = self.spreadDataArr[index];
                matchModel.idNum = model.idNum;
                
                if ([model.type isEqualToString:@"1"]) {
                    groupCtl.idNum = matchModel.idNum;
                    [self.navigationController pushViewController:groupCtl animated:YES];
                    
                }else if([model.type isEqualToString:@"2"]){
                    [self.navigationController pushViewController:detail animated:YES];
                    detail.idNum = model.idNum;
                }else
                {
                    
                }

            }
           
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];

    }
}

#pragma mark - <UITextFieldDelegate>
//文本框开始输入的时候
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];

}// became first responder

//结束输入的时候
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
   
    self.hotListButton.selected = NO;
    self.allGoodsButton.selected = NO;
    self.hotNewBtn.selected = NO;
    self.cancelNewEditButton.selected = NO;
    self.hotNewEditButton.selected = NO;
    [self.dropdownView removeFromSuperview];
    NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
    NSString *productTypeID = [NSString stringWithFormat:@"%d",0];
    NSDictionary *footerDict = @{@"productTypeID" : productTypeID,@"searchString" : textField.text,@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"pageSize" : @12,@"brandID" : brandID,@"type":@"0"};
    if (textField.text.length == 0) {
        [self.view endEditing:YES];
    }else{
        [self addRefreshWithDict:footerDict];
    }
        return YES;
}

#pragma mark - noDataView
- (void)addNoDataViewWith:(NSString *)string
{
    _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
    _noDataView.image = [UIImage imageNamed:@"no_data"];
    _noDataView.center = self.view.center;
    
    
    [self.view addSubview:_noDataView];
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
    //    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
    //        return model.imageSize;
    //    }
    return CGSizeMake(256, 275);
}

#pragma mark - <UIAlertViewDelegate>


@end
