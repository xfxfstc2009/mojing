//
//  MJProductDetailsCtler.h
//  魔境家居
//
//  Created by mojing on 15/11/7.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJBaseCtler.h"
#import "MJProductModel.h"
#import "MJExtension.h"
#import "MJUIClassTool.h"

@interface MJProductDetailsCtler : MJBaseCtler

@property (nonatomic,assign) NSInteger idNum;
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) MJProductModel *model;


- (void)back;
- (void)createButton;
@end
