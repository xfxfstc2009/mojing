//
//  MJProductGroupDetailsCtler.m
//  魔境家居
//
//  Created by mojing on 15/11/9.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJProductGroupDetailsCtler.h"
#import "MJNetworkTool.h"
#import "MJExtension.h"
#import "MJMatchProductModel.h"
#import "MJProductModel.h"
#import "UIImageView+WebCache.h"
#import "MJMatchUserDataModel.h"
#import "MJProductDetailsCtler.h"
#import "PhotoTrackController.h"
#import "UIBarButtonItem+Extension.h"
#import "MJUIClassTool.h"

@interface MJProductGroupDetailsCtler ()
{
    __weak IBOutlet UIImageView *_matchedImageView;
    __weak IBOutlet UILabel *_productGroupNameLabel;
    __weak IBOutlet UILabel *_productPriceLabel;
    __weak IBOutlet UILabel *_productTypeLabel;
    __weak IBOutlet UILabel *_productDesignerLabel;
    __weak IBOutlet UIImageView *_groupQRCodeImgView;
    __weak IBOutlet UIButton *_beginMatchBtn;
    
    
    __weak IBOutlet UILabel *_totalPrice;
    __weak IBOutlet UILabel *_spaceType;
    __weak IBOutlet UILabel *_designer;
    __weak IBOutlet UILabel *_qrcode;
    __weak IBOutlet UIImageView *_orangeView;
    __weak IBOutlet UILabel *_matchProduct;
    
    NSDictionary *_productDic;
}
@property (strong, nonatomic)  UIScrollView *bottomScrollView;
@property (strong, nonatomic) NSMutableArray *matchProductArr;
@end

@implementation MJProductGroupDetailsCtler

- (NSMutableArray *)matchProductArr
{
    if (_matchProductArr == nil) {
        _matchProductArr = [NSMutableArray array];
    }
    return _matchProductArr;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //通知
    [MJNotificationCenter addObserver:self selector:@selector(mjVcHiddenDidChange:) name:@"mjStartService" object:nil];
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    
    _beginMatchBtn.layer.cornerRadius = 20;
    _beginMatchBtn.clipsToBounds = YES;
    
    [self getMatchDetailData];
}

#pragma mark - 隐藏服务客户
-(void)mjVcHiddenDidChange:(NSNotification *)notification
{
    BOOL hidden = [notification.userInfo[@"mjVcHidden"] boolValue];
    self.moreView.serviceView.hidden = hidden;
}

#pragma mark - 返回
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 获取搭配详情数据
- (void)getMatchDetailData
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSDictionary *dict = @{@"id" : [NSString stringWithFormat:@"%ld",(long)self.idNum]};
    if ([self isConnectToNet]){
    [net getMoJingURL:MJWorksDetail parameters:dict success:^(id obj) {
       
        NSDictionary *productDic = obj[@"data"];
        _productDic = productDic;
      //  NSLog(@"%@",productDic);
        MJMatchProductModel *matchModel = [MJMatchProductModel mj_objectWithKeyValues:productDic];
        if ([matchModel.isDeleted isEqualToString:@"0"]) {
            
        NSArray *arr = [productDic objectForKey:@"productList"];
        
        self.matchProductArr = [MJProductModel mj_objectArrayWithKeyValuesArray:arr];
        CGFloat imgW = (Screen_Width-15)/6;
        CGFloat padding = 15;
        
        //组合图片,二维码
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,matchModel.collocationImageSuffix,matchModel.collocationImageName,matchModel.collocationImageName,matchModel.collocationImageSuffix]];
            NSURL *qrUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/png/qr/works/%ld_qr_1.png",mainURL,self.idNum]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_matchedImageView sd_setImageWithURL:bgUrl placeholderImage:nil];
                [_groupQRCodeImgView sd_setImageWithURL:qrUrl placeholderImage:nil];
            });
        });
        
        //底部滚动图
            CGFloat bottomScrollViewH = 225;
        _bottomScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 543, Screen_Width, bottomScrollViewH)];
       // _bottomScrollView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:self.bottomScrollView];
        //组合名称
        _productGroupNameLabel.text = matchModel.name;
        [self setTitle:matchModel.name];
        //组合总价
        NSInteger price = 0;
        for (MJProductModel *m in self.matchProductArr) {
            price += m.price*m.countInWorks;
        }
        _productPriceLabel.text = [NSString stringWithFormat:@"￥%ld",(long)price];
        //空间类型
        _productTypeLabel.text = [NSString stringWithFormat:@"空间类型：%@",matchModel.spaceTypeName];
        
        //设置底部搭配单品图片
        for (NSInteger i = 0; i < self.matchProductArr.count; i++) {
            MJProductModel *model = self.matchProductArr[i];
            if (self.matchProductArr.count < 5) {
                self.bottomScrollView.contentSize = CGSizeMake(Screen_Width, 0);
                
            }else
            {
                self.bottomScrollView.contentSize = CGSizeMake(imgW * self.matchProductArr.count+padding/**(self.matchProductArr.count-2)-(self.matchProductArr.count-3)*15*/, 0);
                
            }

          //  NSLog(@"%ld",_matchProductArr.count);
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(padding+imgW*i, 0, imgW-padding, bottomScrollViewH-60)];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            //imgView.clipsToBounds = YES;
            imgView.tag = 20 + i;
            imgView.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgTap:)];
            [imgView addGestureRecognizer:tap];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSString *urlStr = [NSString stringWithFormat:@"%@upload/%@/%@/%@_200_200.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix];
                urlStr = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url = [NSURL URLWithString:urlStr];
             //   NSLog(@"%@",url);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [imgView sd_setImageWithURL:url placeholderImage:nil];
                });
            });
            [self.bottomScrollView bringSubviewToFront:imgView];
            [self.bottomScrollView addSubview:imgView];
            
            UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(padding+imgW*i, bottomScrollViewH - 65, imgW-padding, 50)];
            nameLabel.text = model.name;
            nameLabel.numberOfLines = 0;
            nameLabel.font = [UIFont systemFontOfSize:13];
            [self.bottomScrollView addSubview:nameLabel];
            if (model.countInWorks>1) {
                UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(padding+imgW*i, bottomScrollViewH - 20, imgW-padding, 20)];
                priceLabel.text = [NSString stringWithFormat:@"￥%ld",model.price];
                [priceLabel setTextColor:[UIColor orangeColor]];
                [self.bottomScrollView addSubview:priceLabel];
                UILabel *countInWorksLb = [[UILabel alloc] initWithFrame:CGRectMake(priceLabel.frame.size.width-25, 0, 25, 20)];
                countInWorksLb.textColor = RGB(90, 166, 197);
                countInWorksLb.text = [NSString stringWithFormat:@"X%ld",model.countInWorks];
                [priceLabel addSubview:countInWorksLb];
                
            }else
            {
                UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(padding+imgW*i, bottomScrollViewH - 20, imgW-padding, 20)];
                priceLabel.text = [NSString stringWithFormat:@"￥%ld",(long)model.price];
                [priceLabel setTextColor:[UIColor orangeColor]];
                
                [self.bottomScrollView addSubview:priceLabel];
            }
            
        }
        NSDictionary *para = @{@"ids":[NSString stringWithFormat:@"%ld",(long)matchModel.userID]};
        [net getMoJingURL:MJUsersDetailData parameters:para success:^(id obj) {
             //设计师
            MJMatchUserDataModel *userModel = obj[0];
            _productDesignerLabel.text = [NSString stringWithFormat:@"设计师：%@",userModel.name];
        } failure:^(NSError *error) {
            
        }];
         }else
         {
        
             //改商品已删除
             UIImageView  *noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
             noDataView.image = [UIImage imageNamed:@"no_data"];
             noDataView.center = self.view.center;
             UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(3, 147, 180, 30) title:@"该搭配作品已删除" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
             [noDataView addSubview:lb];
             [self.view addSubview:noDataView];
             [self.view bringSubviewToFront:noDataView];
            
             _groupQRCodeImgView.hidden = YES;
             _beginMatchBtn.hidden = YES;
             _totalPrice.hidden = YES;
             _spaceType.hidden = YES;
             _designer.hidden = YES;
             _qrcode.hidden = YES;
             _orangeView.hidden = YES;
             _matchProduct.hidden = YES;
             
         }
      //  NSLog(@"%ld",_matchProductArr.count);
    } failure:^(NSError *error) {
        
    }];
    }
}

#pragma mark - 开始搭配
- (IBAction)startMatch:(UIButton *)sender {
    PhotoTrackController *photo = [[PhotoTrackController alloc] initWithItem:_productDic];
    [self.navigationController presentViewController:photo animated:YES completion:nil];
}

#pragma mark - 底部单品点击
- (void)imgTap:(UITapGestureRecognizer *)tap
{
    NSInteger index = tap.view.tag-20;
    
    MJProductDetailsCtler *ctl = [[MJProductDetailsCtler alloc] init];
    
    MJProductModel *m = self.matchProductArr[index];
    
    ctl.idNum = m.idNum;
    
    [self.navigationController pushViewController:ctl animated:YES];
}

@end
