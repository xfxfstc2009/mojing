//
//  MJProductListDataModel.m
//  魔境家居
//
//  Created by mojing on 15/11/7.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJProductListDataModel.h"
#import "MJExtension.h"
#import "MJProductModel.h"
@implementation MJProductListDataModel

- (NSDictionary *)objectClassInArray
{
    return @{@"productList":[MJProductModel class]};
}

@end
