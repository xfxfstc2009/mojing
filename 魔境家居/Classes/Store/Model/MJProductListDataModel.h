//
//  MJProductListDataModel.h
//  魔境家居
//
//  Created by mojing on 15/11/7.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJProductListDataModel : NSObject

@property (nonatomic,assign) NSInteger pageCount ;//20,
@property (nonatomic,assign) NSInteger pageNumber ;//3,
@property (nonatomic,assign) NSInteger pageSize ;//15,
@property (nonatomic,assign) NSInteger resourceCount ;//300,
@property (nonatomic,strong) NSArray * productList ;//Array[1]

@end
