//
//  MJCollocationDataModel.h
//  魔境家居
//
//  Created by mojing on 15/12/1.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJCollocationDataModel : NSObject
/*
{
    "v":"1.0",
    "data":{
        "state":2,
        "quality":1,
        "rotation":[
                    -0.17514365911483765,
                    -0.9842851161956787,
                    -0.022535324096679688,
                    0.42459025979042053,
                    -0.09616333246231079,
                    0.9002643823623657,
                    -0.8882838487625122,
                    0.1481071412563324,
                    0.4347600042819977
                    ],
        "timestampInSeconds":109094.1875,
        "sensor":"GPSCompassSensorSource",
        "cosName":"OrientationFloorTracker",
        "translation":{
            "x":29.292835235595703,
            "y":-1170.343505859375,
            "z":-565.1851196289062
        },
        "viewport":{
            "x":1733,
            "y":1849
        },
        "additionalValues":"",
        "trackingTimeMs":0.01799999177455902,
        "timeElapsed":32173.240234375,
        "coordinateSystemID":1,
        "rect":{
            "width":1136,
            "height":640
        }
    },
    "models":[
              {
                  "rotation":[
                              -0.539757490158081,
                              -0.00001519918441772461,
                              -0.8418205380439758,
                              -0.8418205380439758,
                              -0.00002658367156982422,
                              0.539757490158081,
                              -0.00003057718276977539,
                              1,
                              0.0000015497207641601562
                              ],
                  "id":1,
                  "scale":{
                      "x":0.4589087963104248,
                      "y":0.4589087963104248,
                      "z":0.4589087963104248
                  },
                  "name":"1_0",
                  "location":{
                      "x":801,
                      "y":529
                  },
                  "translation":{
                      "x":1550.630126953125,
                      "y":-752.2701416015625,
                      "z":0
                  }
              }
              ]
}
*/
@end
