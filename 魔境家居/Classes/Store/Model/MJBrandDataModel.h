//
//  MJBrandDataModel.m
//  魔境家居
//
//  Created by mojing on 15/11/7.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJBrandDataModel  : NSObject

@property (nonatomic,assign) NSInteger idNum;
@property (nonatomic,copy) NSString *isDeleted;
@property (nonatomic,copy) NSString *isTop;
@property (nonatomic,assign) NSInteger logoID;
@property (nonatomic,copy) NSString *logoName;
@property (nonatomic,copy) NSString *logoSuffix;
@property (nonatomic,copy) NSString *name;
/*是否选中打钩*/
@property (nonatomic,assign,getter=isSelected) BOOL selected;
@end
