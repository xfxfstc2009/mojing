//
//  MJProductModel.h
//  魔境家居
//
//  Created by mojing on 15/11/7.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJBrandDataModel.h"
#import "MJCreatedDateModel.h"
#import "MJModifiedDateModel.h"
@interface MJProductModel  : NSObject

@property (nonatomic,strong) MJBrandDataModel *brandData;
@property (nonatomic,assign) NSInteger  brandID;
@property (nonatomic,strong) MJCreatedDateModel *createdDate;
@property (nonatomic,copy) NSString *createdDateTime ;
@property (nonatomic,strong) NSArray *dealerList;
@property (nonatomic,copy) NSString *desc;
@property (nonatomic,assign) NSInteger  favoriteCount;
@property (nonatomic,assign) NSInteger  idNum;
@property (nonatomic,assign) NSInteger  imageID;
@property (nonatomic,copy) NSString *imageName;
@property (nonatomic,copy) NSString *imageSuffix;
@property (nonatomic,copy) NSString *isDeleted;
@property (nonatomic,copy) NSString *isFavorited;
@property (nonatomic,copy) NSString *isNew;
@property (nonatomic,assign) NSInteger modelID;
@property (nonatomic,copy) NSString *modelName;
@property (nonatomic,copy) NSString *modelSuffix;
@property (nonatomic,strong) MJModifiedDateModel *modifiedDate;
@property (nonatomic,copy) NSString *modifiedDateTime;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *nickName;
@property (nonatomic,assign) NSInteger price;
@property (nonatomic,assign) NSInteger productTypeID;
@property (nonatomic,copy) NSString *productTypeName;
@property (nonatomic,assign) NSInteger usedCount;
@property (nonatomic,assign) NSInteger userID;
@property (nonatomic,copy) NSString *userName;
@property (nonatomic,assign) NSInteger countInWorks;
/*是否选中打钩*/
@property (nonatomic,assign,getter=isSelected) BOOL selected;
/*是否处于编辑状态*/
@property (nonatomic,assign,getter=isEdited) BOOL edit;
/*type类型*/
@property (nonatomic,copy) NSString *type;

/*旋转度*/
@property (nonatomic,assign) CGFloat rotation;
/*放大比例*/
@property (nonatomic,assign) CGFloat scale;
/*坐标*/
@property (nonatomic,assign) CGPoint location;
/*所在图层*/
@property (nonatomic,assign) int layer;

/*跳转经销商 还是热销商品*/
@property (nonatomic,assign) int pushIndex;
@end
