//
//  MJProductListModel.h
//  魔境家居
//
//  Created by mojing on 15/11/7.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJProductListDataModel.h"
@interface MJProductListModel  : NSObject

@property (nonatomic,copy) NSString * code  ;//200,
@property (nonatomic,copy) NSString * msg  ;// 可以直接向用户提示的错误信息 ,
@property (nonatomic,copy) NSString * debugMsg  ;// 用于确定错误原因的调试错误信息，不能向用户提示 ,
@property (nonatomic,strong) MJProductListDataModel *data;
//@property (nonatomic,strong) NSDictionary *data;

@end
