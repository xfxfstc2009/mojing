//
//  MJSelectEditImageView.h
//  魔境家居
//
//  Created by mojing on 15/11/24.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CHTCollectionViewWaterfallLayout.h>
typedef void (^editBtnClick)(NSInteger index);
@interface MJSelectEditImageView : UIView<UICollectionViewDataSource,UICollectionViewDelegate>

/** 获取商品统计列表数组*/
@property (nonatomic, strong) NSMutableArray *productListArr;
@property (nonatomic,strong) UICollectionView *collectionView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
@property (nonatomic,copy) editBtnClick btnClick;
@end
