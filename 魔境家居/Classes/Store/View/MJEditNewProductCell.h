//
//  MJEditNewProductCell.h
//  魔境家居
//
//  Created by lumingliang on 15/12/29.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJProductModel.h"

@interface MJEditNewProductCell : UICollectionViewCell

@property (nonatomic,strong) MJProductModel *model;
@property (nonatomic,strong) UIImageView *singleProductImgView;
@property (nonatomic,strong) UIImageView *selectImgView;
@property (nonatomic,strong) UILabel *singleProductName;
@property (nonatomic,strong) UILabel *singleProductPrice;

@end
