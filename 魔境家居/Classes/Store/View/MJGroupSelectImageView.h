//
//  MJGroupSelectImageView.h
//  魔境家居
//
//  Created by mojing on 15/11/24.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^groupBtnClick)(NSInteger index);
@interface MJGroupSelectImageView : UIView<UICollectionViewDataSource,UICollectionViewDelegate>

/** 搭配作品列表数组 */
@property (nonatomic,strong) NSMutableArray *matchProductArr;
@property (nonatomic,strong) UICollectionView *collectionView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
@property (nonatomic,copy) groupBtnClick btnClick;
@end
