//
//  MJGroupSelectImageView.m
//  魔境家居
//
//  Created by mojing on 15/11/24.
//  Copyright © 2015年 mojing. All rights reserved.

#import "MJGroupSelectImageView.h"
#import "MJGroupSelectImageCell.h"
#import "MJMatchProductModel.h"
#import "MJExtension.h"
#import "MJRefresh.h"
#import "GMDCircleLoader.h"
#import "MJNetworkTool.h"
#import "MJMatchModel.h"
#import "MJUIClassTool.h"
#import "UIImageView+WebCache.h"
#define kEditImageViewWidth 650
#define kEditImageViewHeight 520
@interface MJGroupSelectImageView()

/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
@end
@implementation MJGroupSelectImageView

static NSString * const reuseIdentifier = @"groupCell";

- (UIImageView *)noDataView
{
    if (_noDataView == nil) {
        
        _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
        _noDataView.image = [UIImage imageNamed:@"no_data"];
        _noDataView.center = self.center;
        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-20, 147, 200, 30) title:@"没有搜到相关的搭配作品" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
        [_noDataView addSubview:lb];
        //   [self.view addSubview:_noDataView];
        
    }
    return _noDataView;
}

- (NSMutableArray *)matchProductArr
{
    if (_matchProductArr == nil) {
        _matchProductArr = [NSMutableArray array];
    }
    return _matchProductArr;
}

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        
        CGFloat inset = 10;
        layout.sectionInset = UIEdgeInsetsMake(inset, inset, inset, inset);
        // 设置每一行之间的间距
        layout.minimumLineSpacing = inset;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kEditImageViewWidth, kEditImageViewHeight-70) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = RGB(255, 255, 255);
        [_collectionView registerClass:[MJGroupSelectImageCell class] forCellWithReuseIdentifier:reuseIdentifier];
    }
    return _collectionView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
       [self addSubview:self.collectionView];
        self.pageNumber = 1;
        UIButton *confirmBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [confirmBtn setImage:[UIImage imageNamed:@"store_edit_confirm"] forState:UIControlStateNormal];
        confirmBtn.frame = CGRectMake(kEditImageViewWidth/2-80-5, kEditImageViewHeight-50, 80, 40);
        confirmBtn.tag = 50;
        [confirmBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *cancelBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setImage:[UIImage imageNamed:@"store_edit_cancel"] forState:UIControlStateNormal];
        cancelBtn.frame = CGRectMake(kEditImageViewWidth/2+5, kEditImageViewHeight-50, 80, 40);
        cancelBtn.tag = 51;
        [cancelBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:confirmBtn];
        [self addSubview:cancelBtn];
      
        NSDictionary *dict = @{@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber],@"pageSize" : @6,@"type" : @1};
        [self addRefreshHeader:dict];
    }
    return self;
}


#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_circulateBackgroungView];
    
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    [self.circulateBackgroungView removeFromSuperview];
    [GMDCircleLoader hideFromView:self animated:YES];
}

#pragma mark - 添加上下拉刷新
- (void)addRefreshHeader:(NSDictionary *)dict
{
    __weak MJGroupSelectImageView *ctl = self;
    [self loadingData];
    //下拉刷新
    self.collectionView.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        [ctl loadNewData:dict];
    }];
    // 马上进入刷新状态
    [self.collectionView.mj_header beginRefreshing];
}

#pragma mark - 下拉加载数据
- (void)loadNewData:(NSDictionary *)dict
{
    self.pageNumber = 1;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJGroupSelectImageView *ctl = self;
    [self.noDataView removeFromSuperview];
    [self.matchProductArr removeAllObjects];
    //获取搭配作品列表
    if ([self isConnectToNet]) {
        
            [netTool getMoJingURL:MJWorksList parameters:dict success:^(id obj) {
                
                MJMatchModel *model = obj;
                self.pageCount = model.data.pageCount;
                NSMutableArray *arr= [NSMutableArray array];
                arr = [MJMatchProductModel mj_objectArrayWithKeyValuesArray:model.data.worksList];
            
                     [ctl.matchProductArr addObjectsFromArray:arr];
                if (ctl.matchProductArr.count == 0) {
                  //  [ctl addSubview:ctl.noDataView];
                }
                // 上拉刷新
                if (self.matchProductArr.count >= 6) {
                    self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        // 进入刷新状态后会自动调用这个block
                        [ctl loadMoreData:dict];
                    }];
                    // 默认先隐藏footer
                    self.collectionView.mj_footer.hidden = YES;
                }

                [ctl.collectionView reloadData];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_header endRefreshing];
                
                for (MJMatchProductModel *m in ctl.matchProductArr) {
                    NSDictionary *dict = @{@"id" : [NSString stringWithFormat:@"%ld",m.idNum]};
                    [netTool getMoJingURL:MJWorksDetail parameters:dict success:^(id obj) {
                        
                    } failure:^(NSError *error) {
                        NSLog(@"%@",error);
                        [ctl.collectionView.mj_header endRefreshing];
                        [ctl stopCircleLoader];
                    }];
                    
                }
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.collectionView.mj_header endRefreshing];
                [ctl stopCircleLoader];
            }];
        
    }else
    {
        [ctl stopCircleLoader];
        [ctl.collectionView.mj_header endRefreshing];
        
    }
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.collectionView];
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    _pageNumber++;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJGroupSelectImageView *ctl = self;
    NSDictionary *moreDict = @{@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber],@"pageSize" : @6,@"type" : @1};
    //[self.matchProductArr removeAllObjects];
    //获取搭配作品列表
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            [netTool getMoJingURL:MJWorksList parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                MJMatchModel *model = obj;
                NSMutableArray *arr= [NSMutableArray array];
                arr = [MJMatchProductModel mj_objectArrayWithKeyValuesArray:model.data.worksList];
                [ctl.matchProductArr addObjectsFromArray:arr];
                [ctl.collectionView reloadData];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
                
                //                for (MJMatchProductModel *m in ctl.matchProductArr) {
                //                    NSDictionary *dict = @{@"id" : [NSString stringWithFormat:@"%ld",m.idNum]};
                //                    [netTool getMoJingURL:MJWorksDetail parameters:dict success:^(id obj) {
                //
                //                    } failure:^(NSError *error) {
                //                        NSLog(@"%@",error);
                //                        [ctl.collectionView.mj_footer endRefreshing];
                //                        [ctl stopCircleLoader];
                //                    }];
                
                // }
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.collectionView.mj_footer endRefreshing];
                [ctl stopCircleLoader];
            }];
 
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
        self.collectionView.mj_footer.hidden = YES;
    }
    
}


- (void)btnClick:(UIButton *)button
{
    if (_btnClick) {
        _btnClick(button.tag-50);
    }
}



#pragma mark  <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.matchProductArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MJGroupSelectImageCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];

    if (self.matchProductArr.count>0) {
        MJMatchProductModel *model = self.matchProductArr[indexPath.row];
        cell.model = model;
        NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.collocationImageSuffix,model.collocationImageName,model.collocationImageName,model.collocationImageSuffix]];
        [cell.singleProductImgView  sd_setImageWithURL:bgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //            if (image) {
            //                if (!CGSizeEqualToSize(model.imageSize, image.size)) {
            //                    model.imageSize = image.size;
            //                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            //                }
            //            }
        }];
        cell.singleProductName.text = model.name;
      //  cell.singleProductPrice.text = [NSString stringWithFormat:@"￥%ld",model.price];
        // 根据模型属性来控制打钩的显示和隐藏
        cell.selectImgView.hidden = !model.isSelected;
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

#pragma mark  <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
    //    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
    //        return model.imageSize;
    //    }
    return CGSizeMake(203, 207);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MJGroupSelectImageCell*cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    CGRect cellRect = cell.contentView.frame;
    UIImage *img = [UIImage imageNamed:@"store_edit_selected"];
    UIImageView *v = [[UIImageView alloc] initWithFrame:CGRectMake(cellRect.size.width-23, 0, 23, 23)];
    v.image = img;
    [cell.contentView addSubview:v];
    NSLog(@"%@",NSStringFromCGRect(v.frame));
    NSLog(@"详情点击%ld",indexPath.row);
}

@end
