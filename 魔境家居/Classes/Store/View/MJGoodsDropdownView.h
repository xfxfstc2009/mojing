//
//  MJGoodsDropdownView.h
//  魔境家居
//
//  Created by mojing on 15/11/6.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^dropdownClick)(NSInteger index);
@interface MJGoodsDropdownView : UIView
@property (weak, nonatomic) IBOutlet UIButton *allGoodsBtn;
@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (nonatomic,copy) dropdownClick click;
@property (nonatomic,strong) UIButton *selectBtn;
+ (instancetype)dropdown;
- (void)setBtnTitleWithArray:(NSArray *)arr;
@end
