//
//  MJAddProductNoteView.m
//  魔境家居
//
//  Created by mojing on 15/11/7.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJAddProductNoteView.h"

@implementation MJAddProductNoteView
{
    __weak IBOutlet UIButton *_firstStarView;
    __weak IBOutlet UIButton *_secondStarView;
    __weak IBOutlet UIButton *_thirdStarView;
    __weak IBOutlet UIButton *_fourthStarView;
    __weak IBOutlet UIButton *_fifthStarView;
}
+ (instancetype)note
{
    return [[[NSBundle mainBundle] loadNibNamed:@"MJAddProductNoteView" owner:nil options:nil] firstObject];
}

- (IBAction)starClick:(UIButton *)button {
    button.selected = !button.isSelected;
    [self setSelectStar:button.tag];
    
    if (_fifthStarView.isSelected)
    {
        self.degree = 5;
    }else if (_fifthStarView.isSelected == NO && _fourthStarView.isSelected == YES)
    {
        self.degree = 4;
    }else if (_fifthStarView.isSelected == NO && _fourthStarView.isSelected == NO && _thirdStarView.isSelected == YES)
    {
        self.degree = 3;
    }else if (_fifthStarView.isSelected == NO && _fourthStarView.isSelected == NO && _thirdStarView.isSelected == NO && _secondStarView.isSelected == YES)
    {
        self.degree = 2;
    }else if (_fifthStarView.isSelected == NO && _fourthStarView.isSelected == NO && _thirdStarView.isSelected == NO && _secondStarView.isSelected == NO && _firstStarView.isSelected == YES)
    {
        self.degree = 1;
    }else if (_fifthStarView.isSelected == NO && _fourthStarView.isSelected == NO && _thirdStarView.isSelected == NO && _secondStarView.isSelected == NO && _firstStarView.isSelected == NO)
    {
        self.degree = 0;
    }
    
    if(_click)
    {
        _click(button.tag-60);
    }
}
- (IBAction)addClick:(UIButton *)sender {
    
    if (_click) {
        _click(sender.tag-60);
    }
}

- (void)setBtnIsSelected
{
    _inputTextView.text = @"";
    _firstStarView.selected = NO;
    _secondStarView.selected = NO;
    _thirdStarView.selected = NO;
    _fourthStarView.selected = NO;
    _fifthStarView.selected = NO;
}
/* 选择星星*/
- (void)setSelectStar:(NSInteger)index
{
    switch (index) {
        case 61:
        {
            _secondStarView.selected = NO;
            _thirdStarView.selected = NO;
             _fourthStarView.selected = NO;
            _fifthStarView.selected = NO;
            break;
        }
        case 62:
        {
            _firstStarView.selected = YES;
            //_secondStarView.selected = NO;
            _thirdStarView.selected = NO;
            _fourthStarView.selected = NO;
            _fifthStarView.selected = NO;
            break;
        }
        case 63:
        {
            _firstStarView.selected = YES;
            _secondStarView.selected = YES;
            _fourthStarView.selected = NO;
            _fifthStarView.selected = NO;
            break;
        }
        case 64:
        {
            _firstStarView.selected = YES;
            _secondStarView.selected = YES;
            _thirdStarView.selected = YES;
           // _fourthStarView.selected = NO;
            _fifthStarView.selected = NO;
            break;
        }
        case 65:
        {
            _firstStarView.selected = YES;
            _secondStarView.selected = YES;
            _thirdStarView.selected = YES;
            _fourthStarView.selected = YES;
            break;
        }
        default:
            break;
    }
}

@end
