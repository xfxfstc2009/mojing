//
//  MJSelectEditImageCell.h
//  魔境家居
//
//  Created by mojing on 15/11/24.
//  Copyright © 2015年 mojing. All rights reserved.
//  单品选择cell

#import <UIKit/UIKit.h>
#import "MJProductModel.h"
@interface MJSelectEditImageCell : UICollectionViewCell

@property (nonatomic,strong) MJProductModel *model;
@property (nonatomic,strong) UIImageView *singleProductImgView;
@property (nonatomic,strong) UIImageView *selectImgView;
@property (nonatomic,strong) UILabel *singleProductName;
@property (nonatomic,strong) UILabel *singleProductPrice;

@end
