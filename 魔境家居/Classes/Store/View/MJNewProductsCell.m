//
//  MJNewProductsCell.m
//  魔境家居
//
//  Created by mojing on 15/10/27.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJNewProductsCell.h"
#import "UIImageView+WebCache.h"
@implementation MJNewProductsCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self uiConfig];
    }
    return self;
}

-(void)uiConfig
{
    CGFloat width = 255;
    CGFloat height = 275;
    CGFloat w = 60;
    _singleProductImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width,height-w-20)];
    _singleProductImgView.contentMode = UIViewContentModeScaleAspectFit;
  //  NSLog(@"%@",NSStringFromCGSize(self.contentView.frame.size));
    _singleProductName = [[UILabel alloc] initWithFrame:CGRectMake(2, height-w-20, width, w/2+30)];
    _singleProductName.numberOfLines = 0;
    _singleProductName.textAlignment = NSTextAlignmentLeft;
    _singleProductName.font = [UIFont systemFontOfSize:17];
    
    _singleProductPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, height-w/2+8, width-2, w/2-10)];
    _singleProductPrice.textAlignment = NSTextAlignmentLeft;
    _singleProductPrice.font = [UIFont systemFontOfSize:17];
    [_singleProductPrice setTextColor:RGB(255, 122, 76)];
    [self.contentView addSubview:_singleProductImgView];
    [self.contentView addSubview:_singleProductName];
    [self.contentView addSubview:_singleProductPrice];
}


- (void)awakeFromNib {
    // Initialization code
}

@end
