//
//  MJGoodsDropdownView.m
//  魔境家居
//
//  Created by mojing on 15/11/6.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJGoodsDropdownView.h"

@implementation MJGoodsDropdownView
{
  
    __weak IBOutlet UIButton *_secondBtn;
    __weak IBOutlet UIButton *_thirdBtn;
    __weak IBOutlet UIButton *_fourthBtn;
    __weak IBOutlet UIButton *_fifthBtn;
    __weak IBOutlet UIButton *_sixthBtn;
    __weak IBOutlet UIButton *_seventhBtn;
    __weak IBOutlet UIButton *_eighthBtn;
}

+ (instancetype)dropdown
{
    return [[[NSBundle mainBundle] loadNibNamed:@"MJGoodsDropdownView" owner:nil options:nil] firstObject];
}

- (IBAction)dropdownBtnClick:(UIButton *)button
{
    self.allGoodsBtn.selected = NO;
    self.selectBtn.selected = NO;
    button.selected = YES;
    self.selectBtn = button;
    
    if (_click) {
        _click(button.tag);
    }
}

#pragma mark - 设置按钮名称
- (void)setBtnTitleWithArray:(NSArray *)arr
{
    if (arr.count > 0) {
        [_firstBtn setTitle:arr[0] forState:UIControlStateNormal];
        [_secondBtn setTitle:arr[1] forState:UIControlStateNormal];
        [_thirdBtn setTitle:arr[2] forState:UIControlStateNormal];
        [_fourthBtn setTitle:arr[3] forState:UIControlStateNormal];
        [_fifthBtn setTitle:arr[4] forState:UIControlStateNormal];
        [_sixthBtn setTitle:arr[5] forState:UIControlStateNormal];
        [_seventhBtn setTitle:arr[6] forState:UIControlStateNormal];
        [_eighthBtn setTitle:arr[7] forState:UIControlStateNormal];
    }
}
@end
