//
//  MJEditNewProductView.h
//  魔境家居
//
//  Created by lumingliang on 15/12/29.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CHTCollectionViewWaterfallLayout.h>
typedef void (^editNewProductClick)(NSInteger index);
@interface MJEditNewProductView : UIView<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>

/** 获取商品统计列表数组*/
@property (nonatomic, strong) NSMutableArray *productListArr;
@property (nonatomic,strong) UICollectionView *collectionView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
@property (nonatomic,copy) editNewProductClick btnClick;
/* 参数字典*/
@property (nonatomic,strong) NSDictionary *para;
/* 参数字典*/
@property (nonatomic,strong) UIButton *confirmBtn;
@end
