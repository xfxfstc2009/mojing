//
//  MJEditNewProductView.m
//  魔境家居
//
//  Created by lumingliang on 15/12/29.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJEditNewProductView.h"
#import "MJEditNewProductCell.h"
#import "MJExtension.h"
#import "MJRefresh.h"
#import "GMDCircleLoader.h"
#import "MJNetworkTool.h"
#import "MJProductListModel.h"
#import "MJUIClassTool.h"
#import "UIImageView+WebCache.h"

#define kEditImageViewWidth 650
#define kEditImageViewHeight 520
@interface MJEditNewProductView()
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
@end
@implementation MJEditNewProductView


static NSString * const reuseIdentifier = @"Cell";

- (UIImageView *)noDataView
{
    if (_noDataView == nil) {
        
        _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(kEditImageViewWidth/2-73, kEditImageViewHeight/2-73, 147, 147)];
        _noDataView.image = [UIImage imageNamed:@"no_data"];
        // _noDataView.center = self.center;
        
        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-5, 147, 180, 30) title:@"没有搜到相关的商品" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
        
        [_noDataView addSubview:lb];
        
        //   [self.view addSubview:_noDataView];
        
    }
    return _noDataView;
}

- (NSMutableArray *)productListArr
{
    if (_productListArr == nil) {
        _productListArr = [NSMutableArray array];
    }
    return _productListArr;
}



- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.collectionView];
        self.pageNumber = 1;
//        self.confirmBtn =[UIButton buttonWithType:UIButtonTypeCustom];
//       // [confirmBtn setImage:[UIImage imageNamed:@"store_edit_confirm"] forState:UIControlStateNormal];
//        _confirmBtn.frame = CGRectMake(kEditImageViewWidth/2-110-5, kEditImageViewHeight-50, 110, 40);
//        [_confirmBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//       
//        [_confirmBtn setBackgroundColor:RGB(110, 181, 205)];
//        _confirmBtn.layer.cornerRadius = 3;
//        _confirmBtn.tag = 54;
//        [_confirmBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *cancelBtn =[UIButton buttonWithType:UIButtonTypeCustom];
        //[cancelBtn setImage:[UIImage imageNamed:@"store_edit_cancel"] forState:UIControlStateNormal];
        
         cancelBtn.frame = CGRectMake(kEditImageViewWidth/2-100, kEditImageViewHeight-50, 200, 40);
        [cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [cancelBtn setTitle:@"确定" forState:UIControlStateNormal];
        [cancelBtn setBackgroundColor:RGB(110, 181, 205)];
         cancelBtn.layer.cornerRadius = 3;
         cancelBtn.tag = 55;
        [cancelBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
       // [self addSubview:_confirmBtn];
        [self addSubview:cancelBtn];
        __weak MJEditNewProductView *ctl = self;
        
        NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
        //  if (button.isSelected) {
         NSDictionary *dict = @{@"productTypeID" : [NSString stringWithFormat:@"%d",0],@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)ctl.pageNumber],@"brandID" : brandID,@"pageSize" :@6};
        
        self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [ctl loadNewDataWithPara:dict];
        }];
        // 马上进入刷新状态
        [self loadingData];
        [self.collectionView.mj_header beginRefreshing];
        
        [MJNotificationCenter addObserver:self selector:@selector(reloadData:) name:@"newProductChange" object:nil];
        
    }
    return self;
}

- (void)btnClick:(UIButton *)button
{
    if (_btnClick) {
        _btnClick(button.tag - 54);
    }
}

- (UICollectionView *)collectionView
{
    if (_collectionView == nil) {
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        //  layout.minimumLineSpacing = inset;
        layout.columnCount = 3;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kEditImageViewWidth, kEditImageViewHeight-70) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = RGB(225, 225, 225);
        [_collectionView registerClass:[MJEditNewProductCell class] forCellWithReuseIdentifier:reuseIdentifier];
    }
    return _collectionView;
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_circulateBackgroungView];
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    [self.circulateBackgroungView removeFromSuperview];
    //[GMDCircleLoader hideFromView:self.view animated:YES];
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self];
}


#pragma mark - 下拉加载新数据
- (void)loadNewDataWithPara:(NSDictionary *)dict
{
    //获取商品列表数据
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    self.pageNumber = 1;
    __weak MJEditNewProductView *ctl = self;
    [self.noDataView removeFromSuperview];
    [ctl.productListArr removeAllObjects];
    if ([self isConnectToNet]) {
        
        [net getMoJingURL:MJProductList parameters:dict success:^(id obj) {
            
            MJProductListModel *listModel = obj;
            MJProductListDataModel *model = listModel.data;
            self.pageCount = model.pageCount;
            // 把字典数组转换成模型数组
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
            
            //            for (MJProductModel *m in ctl.productListArr) {
            //                MJBrandDataModel *brand = m.brandData;
            //                MJCreatedDateModel *create = m.createdDate;
            //                MJModifiedDateModel *modify = m.modifiedDate;
            //                //   NSLog(@"%ld %@ %@ %@",brand.idNum,m.userID,create.time,modify.day);
            //            }
            [ctl.productListArr addObjectsFromArray:arr];
            
            if (ctl.productListArr.count == 0) {
                //   [ctl addSubview:ctl.noDataView];
            }
            
            // 上拉刷新
            if (ctl.productListArr.count >= 6) {
                ctl.collectionView.mj_footer.hidden = YES;
                ctl.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                    // 进入刷新状态后会自动调用这个block
                    [ctl loadMoreData:dict];
                }];
            }
            
            [ctl.collectionView reloadData];
            [ctl stopCircleLoader];
            [ctl.collectionView.mj_header endRefreshing];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [ctl stopCircleLoader];
            [ctl.collectionView.mj_header endRefreshing];
        }];
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_header endRefreshing];
    }
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    // [self.productListArr removeAllObjects];
    _pageNumber++;
    //获取商品列表数据
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
    NSDictionary *moreDict = @{@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber],@"pageSize" : @6,@"brandID" : brandID};
    __weak MJEditNewProductView *ctl = self;
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            [net getMoJingURL:MJProductList parameters:moreDict success:^(id obj) {
                
                MJProductListModel *listModel = obj;
                MJProductListDataModel *model = listModel.data;
                // 把字典数组转换成模型数组
                NSMutableArray *arr = [NSMutableArray array];
                arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
                [ctl.productListArr addObjectsFromArray:arr];
                [UIView animateWithDuration:0.1 animations:^{
                    [ctl.collectionView reloadData];
                }];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            }];
            
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
    }
}

#pragma mark  <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.productListArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MJEditNewProductCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (self.productListArr.count > 0) {
        MJProductModel *model = self.productListArr[indexPath.row];
        cell.model = model;
        NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
        [cell.singleProductImgView  sd_setImageWithURL:bgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //            if (image) {
            //                if (!CGSizeEqualToSize(model.imageSize, image.size)) {
            //                    model.imageSize = image.size;
            //                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            //                }
            //            }
        }];
        cell.singleProductName.text = model.name;
        cell.singleProductPrice.text = [NSString stringWithFormat:@"￥%ld",model.price];
       // 根据模型属性来控制打钩的显示和隐藏
       cell.selectImgView.hidden = !model.isSelected;
        if ([cell.model.isNew isEqualToString:@"1"]) {
            cell.selectImgView.hidden = NO;
            model.selected = YES;
        }else
        {
            cell.selectImgView.hidden = YES;
            model.selected = NO;
        }
       cell.contentView.backgroundColor = [UIColor whiteColor];
    }

    // Configure the cell
    
    return cell;
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
    //    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
    //        return model.imageSize;
    //    }
    return CGSizeMake(203, 207);
}

#pragma mark  <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"详情点击%ld",indexPath.row);
}

- (void)reloadData:(NSNotification *)notification
{
    MJProductModel *model = notification.userInfo[@"newProduct"];
    
   // NSLog(@"%@ %@",model.mj_keyValues,notification.userInfo);
    
    for (MJProductModel *m in self.productListArr) {
        if (m.idNum == model.idNum) {
            m.isNew = notification.userInfo[@"isNew"];
        }
    }
    
    [self.collectionView reloadData];
}
@end
