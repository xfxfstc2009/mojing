//
//  MJSelectEditImageCell.m
//  魔境家居
//
//  Created by mojing on 15/11/24.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJSelectEditImageCell.h"
#import "UIImageView+WebCache.h"
@implementation MJSelectEditImageCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self uiConfig];
    }
    return self;
}

-(void)uiConfig
{
    CGFloat width = 203;
    CGFloat height = 207;
    _singleProductImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width,height-30)];
    //  NSLog(@"%@",NSStringFromCGSize(self.contentView.frame.size));
    _singleProductName = [[UILabel alloc] initWithFrame:CGRectMake(0, height-30, width, 30)];
    _singleProductName.textAlignment = NSTextAlignmentLeft;
    _singleProductName.font = [UIFont systemFontOfSize:13];
    _singleProductName.backgroundColor = [UIColor blackColor];
    _singleProductName.alpha = 0.5;
    [_singleProductName setTextColor:[UIColor whiteColor]];
    
    _singleProductPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, height-30, width-2, 30)];
    _singleProductPrice.textAlignment = NSTextAlignmentRight;
    _singleProductPrice.font = [UIFont systemFontOfSize:13];
    [_singleProductPrice setTextColor:RGB(255, 122, 76)];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.backgroundColor = [UIColor whiteColor];
    btn.frame = CGRectMake(0, 0, width, height);
    btn.alpha = 0.1;
    [btn addTarget:self action:@selector(coverClick) forControlEvents:UIControlEventTouchUpInside];
    
    _selectImgView = [[UIImageView alloc] initWithFrame:CGRectMake(width-23, 0, 23, 23)];
    _selectImgView.image = [UIImage imageNamed:@"store_edit_selected"];
    [self.contentView addSubview:_singleProductImgView];
    [self.contentView addSubview:_singleProductName];
  //  [self.contentView addSubview:_singleProductPrice];
    [self.contentView addSubview:btn];
    [self.contentView addSubview:_selectImgView];
}

- (void)coverClick
{
    // 设置模型选中状态
    self.model.selected = !self.model.isSelected;
    // 直接修改状态
    _selectImgView.hidden = !_selectImgView.isHidden;
}

- (void)awakeFromNib {
    // Initialization code
}



@end
