//
//  MJSetHotProductView.h
//  魔境家居
//
//  Created by mojing on 15/11/21.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^imgViewClick)(NSInteger index);
typedef void (^editImage)(NSInteger index);
@interface MJSetHotProductView : UIView

@property (nonatomic,copy) imgViewClick click;
@property (nonatomic,copy) editImage editImage;
@property (nonatomic,strong) NSMutableArray *imageViewArr;
@property (nonatomic,strong) NSMutableArray *imageViewUrlArr;
@property (nonatomic,strong) NSMutableArray *spreadDataArr;
@property (weak, nonatomic) IBOutlet UIImageView *firstImageView;
@property (weak, nonatomic) IBOutlet UIImageView *secondImageView;
@property (weak, nonatomic) IBOutlet UIImageView *thirdImageView;
@property (weak, nonatomic) IBOutlet UIImageView *fourthImageView;
@property (weak, nonatomic) IBOutlet UIImageView *fifthImageView;
@property (weak, nonatomic) IBOutlet UIImageView *sixImageView;
@property (weak, nonatomic) IBOutlet UIImageView *seventhImageView;
@property (weak, nonatomic) IBOutlet UIButton *coverBtn1;
@property (weak, nonatomic) IBOutlet UIButton *coverBtn2;
@property (weak, nonatomic) IBOutlet UIButton *coverBtn3;
@property (weak, nonatomic) IBOutlet UIButton *coverBtn4;
@property (weak, nonatomic) IBOutlet UIButton *coverBtn5;
@property (weak, nonatomic) IBOutlet UIButton *coverBtn6;
@property (weak, nonatomic) IBOutlet UIButton *coverBtn7;
@property (weak, nonatomic) IBOutlet UIButton *btn3;


+ (instancetype)hotView;
- (void)setButtonHidden:(BOOL)hidden;
- (void)setCoverHidden:(BOOL)hidden;
- (void)setImageWithDict:(NSDictionary *)dict;
@end
