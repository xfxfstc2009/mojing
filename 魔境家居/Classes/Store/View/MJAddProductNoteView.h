//
//  MJAddProductNoteView.h
//  魔境家居
//
//  Created by mojing on 15/11/7.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^noteViewBtnClick)(NSInteger index);
@interface MJAddProductNoteView : UIView

@property (weak, nonatomic) IBOutlet UITextView *inputTextView;
@property (assign,nonatomic) NSInteger degree;
- (void)setBtnIsSelected;
+ (instancetype)note;
@property (nonatomic,copy) noteViewBtnClick click;
@end
