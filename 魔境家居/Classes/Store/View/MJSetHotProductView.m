//
//  MJSetHotProductView.m
//  魔境家居
//
//  Created by mojing on 15/11/21.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJSetHotProductView.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
#import "MJBrandDetailSpreadDataModel.h"
#import "MJExtension.h"
#import "MJBrandDetailModel.h"
@implementation MJSetHotProductView
{
    __weak IBOutlet UIButton *btn1;
    __weak IBOutlet UIButton *btn2;
   // __weak IBOutlet UIButton *btn3;
    __weak IBOutlet UIButton *btn4;
    __weak IBOutlet UIButton *btn5;
    __weak IBOutlet UIButton *btn6;
    __weak IBOutlet UIButton *btn7;
  
}
+ (instancetype)hotView
{
    return [[[NSBundle mainBundle] loadNibNamed:@"MJSetHotProductView" owner:nil options:nil] firstObject];
}

- (NSMutableArray *)imageViewUrlArr
{
    if (_imageViewUrlArr == nil) {
        _imageViewUrlArr = [NSMutableArray array];
    }
    return _imageViewUrlArr;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        _spreadDataArr =[NSMutableArray array];
    }
    return self;
}

- (NSMutableArray *)imageViewArr
{
    if (_imageViewArr == nil) {
        _imageViewArr = [NSMutableArray array];
        [_imageViewArr addObject:self.firstImageView];
        [_imageViewArr addObject:self.secondImageView];
        [_imageViewArr addObject:self.thirdImageView];
        [_imageViewArr addObject:self.fourthImageView];
        [_imageViewArr addObject:self.fifthImageView];
        [_imageViewArr addObject:self.sixImageView];
        [_imageViewArr addObject:self.seventhImageView];
    }
    return _imageViewArr;
}

- (void)setButtonHidden:(BOOL)hidden
{
    btn1.hidden = hidden;
    btn2.hidden = hidden;
    self.btn3.hidden = hidden;
    btn4.hidden = hidden;
    btn5.hidden = hidden;
    btn6.hidden = hidden;
    btn7.hidden = hidden;
}

- (void)setCoverHidden:(BOOL)hidden
{
    _coverBtn1.hidden = hidden;
    _coverBtn2.hidden = hidden;
    _coverBtn3.hidden = hidden;
    _coverBtn4.hidden = hidden;
    _coverBtn5.hidden = hidden;
    _coverBtn6.hidden = hidden;
    _coverBtn7.hidden = hidden;
    
}
//编辑按钮
- (IBAction)editImage:(UIButton *)button {
    
    if (_editImage) {
        _editImage(button.tag - 80);
    }
}

- (IBAction)imgViewClick:(UITapGestureRecognizer *)sender {
    NSLog(@"%ld",sender.view.tag - 90);
    if (_click) {
        _click(sender.view.tag - 90);
    }
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self];
}

#pragma mark - 重新解析数据设置图片
- (void)setImageWithDict:(NSDictionary *)dict
{

    MJNetworkTool *net = [MJNetworkTool shareInstance];
    if ([self isConnectToNet]) {
        [net getMoJingURL:MJBrandDetail parameters:dict success:^(id obj) {
            MJBrandDetailModel *detailModel = [MJBrandDetailModel mj_objectWithKeyValues:obj];
            //完整信息模型
            MJBrandDetailDataModel *dataModel = [MJBrandDetailDataModel mj_objectWithKeyValues:detailModel.data];
            
            //热销信息模型数组
            MJBrandDetailSpreadModel *spreadModel = [MJBrandDetailSpreadModel mj_objectWithKeyValues:dataModel.spreadData];
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJBrandDetailSpreadDataModel mj_objectArrayWithKeyValuesArray:spreadModel.dataList];
            self.spreadDataArr = arr;
            if (arr.count>0) {
                [self.imageViewUrlArr removeAllObjects];
                for (MJBrandDetailSpreadDataModel *model in arr) {
                    
                    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
                    
                    //  NSLog(@"%@ %@",url,model.imageSuffix);
                    [self.imageViewUrlArr addObject:url];
                }
            }else
            {
                [MJNotificationCenter postNotificationName:@"noSpreadData"object:nil];
            }
            //  NSLog(@"%ld",self.imageViewUrlArr.count);
            if (self.imageViewUrlArr.count > 0) {
                for (int i = 0; i < 7; i++) {
                    
                    UIImageView *imgV = self.imageViewArr[i];
                    imgV.backgroundColor = [UIColor whiteColor];
                    imgV.contentMode = UIViewContentModeScaleAspectFit;
                   // imgV.clipsToBounds = YES;
                    [imgV sd_setImageWithURL:self.imageViewUrlArr[i] placeholderImage:nil];
                    
                    // NSLog(@"%@",self.imageViewUrlArr[i]);
                }
                
            }
        } failure:^(NSError *error) {
            
        }];

    }
    
}

@end
