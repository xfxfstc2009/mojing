//
//  MJBaseCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/28.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJBaseCtler.h"
#import "UIView+Frame.h"
#import "MJRichScanCtler.h"
#import "MJCustomerCtler.h"
#import "MJStatisticsCtler.h"
#import "MJMineCtler.h"
#import "MJChatCtler.h"
#import "PhotoTrackController.h"
#import "ImageUpload.h"
#import "UIViewController+OrientationFix.h"
#import "MJCameraCtler.h"
#import "AppDelegate.h"
#import "MJMaterialMatchCtler.h"

#import "APService.h"
#import "MJDBManager.h"
#import "UIBarButtonItem+MJBadge.h"
#import "AppDelegate.h"
#import "MJLoginCtler.h"
#import "MJContackModel.h"
#import "MJNickNameChangeCtler.h"
#import <objc/runtime.h>

#define kMoreViewWidth 266
#define kCountDidChangeNofication @"kCountDidChangeNofication"
#define kMessageRefresh @"kMessageRefresh"

static char photoKey;
static char assetsKey;
@interface MJBaseCtler ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>


/** 更多item*/
@property (nonatomic,strong) UIBarButtonItem *moreItem;

/** 点击相机时的popover*/
@property (nonatomic, strong) UIPopoverController *cameraPopover;
@end
//更多view宽度
#define kMoreViewWidth 266
/** 聊天view*/

#define kNoteViewWidth 984
#define kNoteViewHeight 728
//弹出时间
#define kNoteViewShowTime 0.5


@implementation MJBaseCtler

#pragma mark - 单例
+ (instancetype)sharedInstance {
    static MJBaseCtler *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[MJBaseCtler alloc] init];
    });
    
    return sharedInstance;
}


- (UIButton *)coverView
{
    if (!_coverView) {
        _coverView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        _coverView.backgroundColor = [UIColor grayColor];
        _coverView.alpha = 0.5;
        [_coverView addTarget:self action:@selector(coverBtnClick) forControlEvents:UIControlEventTouchUpInside];
        // [self.view addSubview:_coverView];
    }
    return _coverView;
}

- (MJMoreView *)moreView
{
    if (_moreView == nil) {
        _moreView = [[MJMoreView alloc] initWithFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        __weak MJBaseCtler *ctl = self;
        self.automaticallyAdjustsScrollViewInsets=NO;
       // [self.view addSubview:_moreView];
        MJRichScanCtler *richScan = [[MJRichScanCtler alloc] init];
         UINavigationController *richScanNav = [[UINavigationController alloc] initWithRootViewController:richScan];
        //__weak MJRichScanCtler *richScanCtl = richScan;
        MJCustomerCtler *customer = [[MJCustomerCtler alloc] init];
        // __weak MJCustomerCtler *customerCtl = customer;
        MJStatisticsCtler *statistics = [[MJStatisticsCtler alloc] init];
        __weak typeof(self)weakSelf = self;
        _moreView.close = ^{
          //  NSLog(@"关闭点击");
            [UIView animateWithDuration:0.25 animations:^{
                ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
                [ctl.moreView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth,Screen_Height)];
                [ctl.coverView removeFromSuperview];
            } completion:^(BOOL finished) {
                ctl.view.frame = CGRectMake(0, 0, Screen_Width, Screen_Height);
                ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
                _replaceBarFrame = CGRectMake(0, 0, Screen_Width, 64);
                ctl.cameraItem.enabled = YES;
                ctl.chatItem.enabled = YES;
                ctl.materialMatchItem.enabled = YES;
                weakSelf.tabBarController.tabBar.hidden = NO;
            }];
        };
        _moreView.indexPathBlock = ^(NSInteger index){
            switch (index) {
                case 0:
                {//扫一扫
                    [ctl presentModalViewController:richScanNav animated:YES];
                   
                    break;
                }
                case 1:
                {//我的客户
                    [ctl.navigationController pushViewController:customer animated:YES];
                    ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
                    break;
                }
                case 2:
                {//我的统计
                    [ctl.navigationController pushViewController:statistics animated:YES];
                    ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
                    break;
                }
                case 3:
                {//我的信息
                    [UIView animateWithDuration:0.25 animations:^{
                        
                        ctl.coverView.frame = CGRectMake(-kMoreViewWidth, 0, Screen_Width, Screen_Height);
                        
                        ctl.mineView.frame = CGRectMake(Screen_Width-kMoreViewWidth, 0, kMoreViewWidth, Screen_Height);
                        
                        [ctl.view bringSubviewToFront:ctl.mineView];
                    }completion:^(BOOL finished) {
                        
                        ctl.navigationController.navigationBar.frame=CGRectMake(-kMoreViewWidth, 0, Screen_Width, 64);
                        [ctl.view addSubview:ctl.coverView];
                        // [ctl.moreView addSubview:ctl.mineView];
                        
                    }];
                    
                    break;
                }
                    
                default:
                    break;
            }
        };
        // [MJUserDefaults setBool:YES forKey:@"serviceViewHidden"];
        BOOL serviceHidden = [[MJUserDefaults objectForKey:@"serviceViewHidden"] boolValue];
        _moreView.serviceView.hidden = serviceHidden;
        
        _moreView.endService = ^{
            //  NSLog(@"结束当前服务");
            
            //将顾客设置为不存在,browseID清空
            [MJUserDefaults setObject:@"NO" forKey:@"isExist"];
           
            [MJUserDefaults synchronize];
            //保存离店信息
            NSString *browseID = [MJUserDefaults objectForKey:@"browseID"];
            NSDictionary *endDict = @{@"type" : @2, @"browseID" :browseID};
            MJNetworkTool *net = [MJNetworkTool shareInstance];
            [net getMoJingURL:MJBrowseShopSave parameters:endDict success:^(id obj) {
                NSLog(@"%@",endDict);
                //发通知客户列表界面刷新数据
                [MJNotificationCenter postNotificationName:@"customerListReloadData" object:nil];
                //将hidden置为YES
            [MJUserDefaults setObject:@"YES" forKey:@"serviceViewHidden"];
                [MJUserDefaults synchronize];
            [MJNotificationCenter postNotificationName:@"mjStartService" object:nil userInfo:@{@"mjVcHidden" : @"YES"}];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        };

    }
    return _moreView;
}

- (UIView *)chatCoverView
{
    if (_chatCoverView == nil) {
        _chatCoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        _chatCoverView.backgroundColor = [UIColor blackColor];
        _chatCoverView.alpha = 0.5;
    }
    return _chatCoverView;
}

- (MJMineView *)mineView
{
    if (_mineView == nil) {
        _mineView = [[MJMineView alloc] initWithFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        __weak MJBaseCtler *ctl = self;
        //返回
        _mineView.close = ^{
            [UIView animateWithDuration:0.25 animations:^{
                
                [ctl.mineView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth,Screen_Height)];
                ctl.navigationController.navigationBar.frame = CGRectMake(-kMoreViewWidth, 0, Screen_Width, 64);
                
            } completion:^(BOOL finished) {
            }];
            
        };
        _mineView.indexPathBlock = ^(NSInteger index){
            switch (index) {
                case 0:
                {
                    /**
                     更换头像
                     
                     - returns: 提示框
                     */
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"更换头像" message:@"请选择" delegate:ctl cancelButtonTitle:@"取消" otherButtonTitles:@"图片库", @"照相",nil];
                    [alert show];
                    
                    break;
                }
                case 1:
                {
                    NSLog(@"昵称修改");
                    MJNickNameChangeCtler *mine = [[MJNickNameChangeCtler alloc] init];
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mine];
                    nav.modalPresentationStyle =UIModalPresentationFormSheet;
                    [ctl presentViewController:nav animated:YES completion:nil];
                }
                    break;
                case 3:
                {
                    MJMineCtler *mine = [[MJMineCtler alloc] init];
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mine];
                    nav.modalPresentationStyle =UIModalPresentationFormSheet;
                    [ctl presentViewController:nav animated:YES completion:nil];
                    break;
                }
                default:
                    break;
            }
        };
        AppDelegate *myDelegate = [UIApplication sharedApplication].delegate;
        _mineView.loginOff = ^{
            [myDelegate.casePhotoArr removeAllObjects];
            
            myDelegate.shopName = @"";
            myDelegate.type = @"";
            //注销登录
            MJNetworkTool *net = [MJNetworkTool shareInstance];
            [net getMoJingURL:MJUserLogoff parameters:nil success:^(id obj) {
                
                
            } failure:^(NSError *error) {
                
            }];
            ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
            
            [APService setTags:[NSSet set]
              callbackSelector:@selector(tagsAliasCallback:tags:alias:)
                        object:ctl];

//            [MJContackModel getInstance].unreadMsgCount = 0;
            [MJChatView deleteInstance];
            [MJContackModel deleteInstance];
            [ctl.moreView removeFromSuperview];
            [ctl.mineView removeFromSuperview];
            [ctl.coverView removeFromSuperview];
            [ctl.navigationController popToRootViewControllerAnimated:YES];
        };

    }
    return _mineView;
}

//聊天界面
- (UIView *)noteView
{
    if (_noteView == nil) {
        _noteView =[MJChatView shareInstance];
        _noteView.currentIndex=0;
        _noteView.backgroundColor = [UIColor blackColor];
        _noteView.tag =200;
        
        __weak MJBaseCtler *ctl = self;
        
        _noteView.close= ^{
            [UIView animateWithDuration:kNoteViewShowTime animations:^{
                [ctl.noteView setFrame:CGRectMake(20, Screen_Height, kNoteViewWidth, kNoteViewHeight)];
            }completion:^(BOOL finished) {
//                [ctl.chatCoverView removeFromSuperview];
               [ctl viewRemove];
            }];
            
        };
        
    }
    
    return _noteView;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.frame = _replaceBarFrame;
}

- (void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated {
    UITabBarController *tabBarController = self.tabBarController;
    UITabBar *tabBar = tabBarController.tabBar;
    if (!tabBarController || (tabBar.hidden == hidden)) {
        return;
    }
    
    CGFloat tabBarHeight = tabBar.bounds.size.height;
    CGFloat adjustY = hidden ? tabBarHeight : -tabBarHeight;
    
    if (!hidden) {
        tabBar.hidden = NO;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animated ? 0.3 : 0. animations:^{
        
        // adjust TabBar
        CGRect rect = tabBar.frame;
        rect.origin.y += adjustY;
        tabBar.frame = rect;
        
        // adjust TransitionView
        for (UIView *view in tabBarController.view.subviews) {
            if ([NSStringFromClass([view class]) hasSuffix:@"TransitionView"]) {
               
                CGRect rect = view.frame;
                rect.size.height += adjustY;
                view.frame = rect;
                
                view.backgroundColor = weakSelf.view.backgroundColor;
            }
        }
    } completion:^(BOOL finished) {
        tabBar.hidden = hidden;
        
        // adjust self.view
   
            CGRect rect = weakSelf.view.frame;
            rect.size.height += adjustY;
            weakSelf.view.frame = rect;
        
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self shouldAutorotate];
    [self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationLandscapeLeft];
   // self.hidesBottomBarWhenPushed = YES;
    [self supportedInterfaceOrientations];
    // Do any additional setup after loading the view.
    NSString *name = [MJUserDefaults objectForKey:@"customerName"];
    self.moreView.customerName = name;
    BOOL serviceHidden = [[MJUserDefaults objectForKey:@"serviceViewHidden"] boolValue];
    self.moreView.serviceView.hidden = serviceHidden;
    [self initUIClass];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDidChangeAction) name:kCountDidChangeNofication object:nil];
    
    _replaceBarFrame = CGRectMake(0, 0, Screen_Width,64);
}

- (void)initUIClass
{
    
    [self.view addSubview:self.moreView];
    [self.view addSubview:self.mineView];
    //相机
    self.cameraItem = [UIBarButtonItem itemWithTarget:self action:@selector(cameraClick) image:@"item_camera" highImage:@""];
    
    //聊天
    self.chatItem = [UIBarButtonItem itemWithTarget:self action:@selector(chatClick) image:@"item_chat" highImage:@""];
    _chatItem.tag = 101;
   
    if ([MJContackModel getInstance].unreadMsgCount <= 0) {
        _chatItem.badgeValue =@"";
    }else{
        
        _chatItem.badgeValue = [NSString stringWithFormat:@"%li",[MJContackModel getInstance].unreadMsgCount];
    }
    
    //更多
    self.moreItem = [UIBarButtonItem itemWithTarget:self action:@selector(moreClick) image:@"item_more" highImage:@""];
    
    //选材搭配
    self.materialMatchItem = [UIBarButtonItem itemWithTarget:self action:@selector(materialMatchClick) image:@"material_match_item" highImage:@""];
    
    UIBarButtonItem *spaceItem = [UIBarButtonItem itemWithTarget:nil action:nil image:@"" highImage:@""];
    spaceItem.customView.width = 10;
    spaceItem.customView.height = 35;
    
    AppDelegate *myDelegate = [UIApplication sharedApplication].delegate;
    if ([myDelegate.type isEqualToString:@"0"]) {
        self.navigationItem.rightBarButtonItems = @[_moreItem,spaceItem,_chatItem,spaceItem,_cameraItem];
    }
    self.navigationItem.rightBarButtonItems = @[_moreItem,spaceItem,_chatItem,spaceItem,_cameraItem,spaceItem,_materialMatchItem];
    
    
}

#pragma mark - 昵称变换
- (void)nickNameDidChange:(NSNotification *)notification
{
    [self.mineView.tableView reloadData];
}

#pragma mark - 移除视图
- (void)viewRemove
{
    [self.noteView.chatCoverView removeFromSuperview];
    [self.noteView removeFromSuperview];
    self.noteView = nil;
   // [self.chatCoverView removeFromSuperview];
    //self.chatCoverView = nil;
}

#pragma mark - 显示服务客户
-(void)hiddenDidChange:(NSNotification *)notification
{
    NSString *userName = [MJUserDefaults objectForKey:@"customerName"];
//    NSString *nickName = [MJUserDefaults objectForKey:@"nickName"];
//    if (nickName.length  > 0) {
//        self.moreView.customerName = nickName;
//    }else
//    {
//        self.moreView.customerName = userName;
//    }
    self.moreView.customerName = userName;
    self.moreView.serviceView.hidden = [notification.userInfo[@"serviceHidden"] boolValue];
    
    [MJUserDefaults setObject:@"NO" forKey:@"serviceViewHidden"];
    [MJUserDefaults synchronize];
    
}

#pragma mark - 顶部item点击
- (void)cameraClick
{
   // NSLog(@"相机点击");
    
    self.cameraPopover = [[UIPopoverController alloc] initWithContentViewController:[[MJCameraCtler alloc] init]];
    
    CGRect rect = CGRectMake(Screen_Width -125, 55, 1, 1);
    [self.cameraPopover presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)chatClick
{
    //[self postTabBarHiddenNotification];
    NSInteger indexSelect = [MJContackModel getInstance].currentSelectIndex;
    self.noteView.currentIndex = indexSelect;
    [_noteView showWithIndexSelect:indexSelect];
    
    [MJKeyWindow  addSubview:self.noteView.chatCoverView];
    [MJKeyWindow bringSubviewToFront:self.noteView];
}

- (void)moreClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.navigationController.navigationBar.frame=CGRectMake(-kMoreViewWidth, 0, Screen_Width, 64);
        self.coverView.frame = CGRectMake(-kMoreViewWidth, 0, Screen_Width, Screen_Height);
        
        self.moreView.frame = CGRectMake(Screen_Width-kMoreViewWidth, 0, kMoreViewWidth, Screen_Height);
    }completion:^(BOOL finished) {
        
        [self.view bringSubviewToFront:self.moreView];
       _replaceBarFrame = CGRectMake(-kMoreViewWidth, 0, Screen_Width, 64);
       [self.view addSubview:self.coverView];
        self.cameraItem.enabled = NO;
        self.chatItem.enabled = NO;
        self.materialMatchItem.enabled = NO;
        self.noteView = nil;
        // [self.view bringSubviewToFront:self.coverView];
    }];
}

- (void)materialMatchClick
{
    
    MJMaterialMatchCtler *ctl = [[MJMaterialMatchCtler alloc] init];
    //UINavigationController *materialNav = [[UINavigationController alloc] initWithRootViewController:ctl];
    [self.navigationController pushViewController:ctl animated:YES];
}


#pragma mark - 遮盖点击
- (void)coverBtnClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.navigationController.navigationBar.frame =  CGRectMake(0, 0, Screen_Width, 64);
        [self.moreView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        [self.mineView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        [self.coverView removeFromSuperview];
    }completion:^(BOOL finished) {
        self.cameraItem.enabled = YES;
        self.chatItem.enabled = YES;
        self.materialMatchItem.enabled = YES;
        _replaceBarFrame = CGRectMake(0, 0, Screen_Width, 64);
        self.tabBarController.tabBar.hidden = NO;
    }];
}

#pragma mark <UIAlertViewDelegate>
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
   // [picker supportedInterfaceOrientations];
    picker.delegate = self;
    picker.allowsImageEditing = YES;
    picker.modalPresentationStyle = UIModalPresentationFormSheet;
    if (buttonIndex == 2) {//照相
        picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    }else{//图片库
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    [self presentModalViewController:picker animated:YES];
    
}

-(void)choostWithPhotoBlock:(void(^)(UIImage *image))block{
    objc_setAssociatedObject(self, &photoKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsImageEditing = YES;
    picker.modalPresentationStyle = UIModalPresentationFormSheet;
    picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    [self presentModalViewController:picker animated:YES];
}

-(void)choostWithAssetBlock:(void(^)(UIImage *image))block{
    objc_setAssociatedObject(self, &assetsKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsImageEditing = YES;
    picker.modalPresentationStyle = UIModalPresentationFormSheet;
    picker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentModalViewController:picker animated:YES];

}

#pragma mark 图片选择器的代理
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //  NSLog(@"%@",info);
    //获取修改后的图片
    UIImage *editedImg = info[UIImagePickerControllerEditedImage];
    void(^block)(UIImage *image) = objc_getAssociatedObject(self, &photoKey);
    void(^block1)(UIImage *image) = objc_getAssociatedObject(self, &assetsKey);
    if (block){
        block(editedImg);
    }
    if (block1){
        block1(editedImg);
    }
    
    //更改cell里的图片
    [self.mineView setCellImage: editedImg];
    
    //移除图片选择的控制器
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //把新的图片保存到服务器
    [self userSettingAvatar:editedImg name:@"aa.jpg"];
    
    if (block || block1){
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    
    //保存到本地
    NSData *imgData = UIImageJPEGRepresentation(editedImg, 0.7);
    [MJUserDefaults setObject:imgData forKey:@"userAvatarURL"];
    [MJUserDefaults synchronize];
}

#pragma mark - 头像上传
- (void)userSettingAvatar:(UIImage*)image name:(NSString*)name
{
    NSString *str = [MJUserDefaults objectForKey:@"userID"];
    NSDictionary *params = @{@"userId":str};
    if ([self isConnectToNet]) {
        
        [ImageUpload requestWithUrl:SETTING_AVATAR postData:params
                            imgList:@[@{@"image":image, @"name":name}] userInfo:nil
                           callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
         {
             
         }];
    }
}

#pragma mark
- (void)countDidChangeAction{

    NSInteger x= [MJContackModel getInstance].unreadMsgCount;
    
    //    [[NSNotificationCenter defaultCenter] postNotificationName:kMessageRefresh object:nil userInfo:@{@"contack" : contack}];
    
    UIBarButtonItem* chatItem =(UIBarButtonItem*)self.navigationItem.rightBarButtonItems[2];
    if (x <=0) {
        chatItem.badgeValue=@"";
    }
    else
    {
        chatItem.badgeValue=[NSString stringWithFormat:@"%li",x];
    }

}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

- (void)dealloc
{
    [MJNotificationCenter removeObserver:self];
}

- (void)tagsAliasCallback:(int)iResCode
                     tags:(NSSet *)tags
                    alias:(NSString *)alias {
    NSString *callbackString =
    [NSString stringWithFormat:@"%d, \ntags: %@, \nalias: %@\n", iResCode,
     [self logSet:tags], alias];
    
    NSLog(@"TagsAlias回调:%@", callbackString);
}

- (NSString *)logSet:(NSSet *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}





#pragma mark - Ares
- (void)hidesTabBarWhenPushed {
    [self setHidesBottomBarWhenPushed:YES];
}


@end
