//
//  MJBaseCtler.h
//  魔境家居
//
//  Created by mojing on 15/10/28.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJMoreView.h"
#import "MJMineView.h"
#import "UIBarButtonItem+Extension.h"
#import "MJChatView.h"
@interface MJBaseCtler : UIViewController

/**
 *  更多view
 */
@property (nonatomic, strong) MJMoreView *moreView;
/**
 *  遮盖view
 */
@property (nonatomic, strong)UIButton *coverView;
/**
 *  我的view
 */
@property (nonatomic, strong) MJMineView *mineView;
/**
 *  保存上一次的navBarFrame
 */
@property (nonatomic, assign) CGRect replaceBarFrame;


/** 遮盖点击*/
- (void)coverBtnClick;
/** 服务客户显示通知方法*/
-(void)hiddenDidChange:(NSNotification *)notification;
/** 移除视图*/
- (void)viewRemove;
/** 相机item*/
@property (nonatomic,strong) UIBarButtonItem *cameraItem;
/** 聊天item*/
@property (nonatomic,strong) UIBarButtonItem *chatItem;
/** 选材搭配item*/
@property (nonatomic,strong) UIBarButtonItem *materialMatchItem;
/** 聊天视图*/
@property (nonatomic,strong) MJChatView *noteView;
/** 聊天覆盖视图*/
@property (nonatomic,strong) UIView *chatCoverView;


#pragma mark - Ares
- (void)hidesTabBarWhenPushed;
+ (instancetype)sharedInstance ;
- (void)userSettingAvatar:(UIImage*)image name:(NSString*)name;

- (void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated;

-(void)choostWithPhotoBlock:(void(^)(UIImage *image))block;
-(void)choostWithAssetBlock:(void(^)(UIImage *image))block;

@end
