//
//  MJSceneCtler.m
//  魔境家居
//
//  Created by lumingliang on 15/12/21.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJSceneCtler.h"
#import "MJCategory.h"
#import "UIView+Frame.h"
#import "MJPlistTool.h"
#import "MJConmon.h"
#import "MJAllMatchCell.h"
#import "MJMatchSelectView.h"
#import "MJSearchBar.h"
#import "MJNetworkTool.h"
#import "MJSpaceTypeModel.h"
#import "MJMatchModel.h"
#import "MJMatchProductModel.h"
#import "MJExtension.h"
#import "MJRefresh.h"
#import "MJProductGroupDetailsCtler.h"
#import "GMDCircleLoader.h"
#import "MJMatchUserDataModel.h"
#import "MJUIClassTool.h"
#import "UIBarButtonItem+Extension.h"
#import "PhotoTrackController.h"
#import <CHTCollectionViewWaterfallLayout.h>
#import "UIImageView+WebCache.h"
@interface MJSceneCtler ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,CHTCollectionViewDelegateWaterfallLayout>
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) MJMatchSelectView *selectView;
/**
 *  空间类型列表数组
 */
@property (nonatomic,strong) NSMutableArray *spaceTypeArr;
/** 搭配作品列表数组 */
@property (nonatomic,strong) NSMutableArray *matchProductArr;
/** 搭配作品详情参数数组 */
@property (nonatomic,strong) NSMutableArray *matchDetailParaArr;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
/* 用户头像数组*/
@property (nonatomic, strong) NSMutableArray *userPhotoArr;

@property (nonatomic, strong) NSDictionary *sceneDic;
@end

@implementation MJSceneCtler

static NSString * const reuseIdentifier = @"Cell";

- (UIImageView *)noDataView
{
    if (_noDataView == nil) {
        
        _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
        _noDataView.image = [UIImage imageNamed:@"no_data"];
        _noDataView.center = self.view.center;
        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-10, 147, 200, 30) title:@"没有搜到相关的场景" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
        [_noDataView addSubview:lb];
        //   [self.view addSubview:_noDataView];
        
    }
    return _noDataView;
}

#pragma mark - 空间类型选择
- (MJMatchSelectView *)selectView
{
    if (_selectView == nil) {
        _selectView = [MJMatchSelectView select];
        _selectView.frame = CGRectMake(0, 64, Screen_Width, 44);
        _selectView.allMatchBtn.selected = YES;
        MJSearchBar *searchBar = [[MJSearchBar alloc] initWithFrame:CGRectMake(Screen_Width - 278, 5, 258, 34)];
        searchBar.placeholder = @"搜索场景";
        searchBar.returnKeyType = UIReturnKeySearch;
        searchBar.delegate = self;
        [_selectView addSubview:searchBar];
        //空间类型按钮点击
        __weak MJSceneCtler *ctl = self;
        _selectView.cb = ^(NSInteger index,UIButton *button){
            NSDictionary *headerDict = @{@"spaceTypeID" : [NSString stringWithFormat:@"%ld",index-10+1],@"pageNumber" : [NSString stringWithFormat:@"%ld",ctl.pageNumber],@"pageSize" : @6,@"type" : @0};
            
            switch (index-10) {
                case 0:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 1:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 2:
                {
                    [ctl addRefreshHeader:headerDict];
                    break;
                }
                case 3:
                {
                    [ctl addRefreshHeader:headerDict];
                    break;
                }
                case 4:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 5:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 6:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 7:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 8:
                {
                    NSDictionary *headerDict1 = @{@"spaceTypeID" : @0,@"pageSize" : @6,@"pageNumber" : [NSString stringWithFormat:@"%ld",ctl.pageNumber],@"type" : @0};
                    [ctl addRefreshHeader:headerDict1];
                    
                    break;
                }
                default:
                    break;
            }
            
        };
    }
    return _selectView;
}

- (NSMutableArray *)matchProductArr
{
    if (_matchProductArr == nil) {
        _matchProductArr = [NSMutableArray array];
        
    }
    return _matchProductArr;
}

- (NSMutableArray *)spaceTypeArr
{
    if (_spaceTypeArr == nil) {
        _spaceTypeArr = [NSMutableArray array];
    }
    return _spaceTypeArr;
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        //  layout.minimumLineSpacing = inset;
        layout.columnCount = 3;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 108, Screen_Width, Screen_Height-108) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.backgroundColor = RGB(220, 220, 220);
        //[_collectionView registerNib:[UINib nibWithNibName:@"MJAllMatchCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
        [_collectionView registerClass:[MJAllMatchCell class] forCellWithReuseIdentifier:reuseIdentifier];
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _sceneDic = [NSDictionary dictionary];
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    
    self.pageNumber = 1;
    self.title = @"场景列表页";
    self.view.backgroundColor = RGB(246, 246, 246);
    [self.view addSubview:self.selectView];
    // _collectionViewFrame = self.collectionView.frame;
    
    
    
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJSceneCtler *ctl = self;
    
    //获取空间类型列表
    [netTool getMoJingURL:MJSpaceTypeList parameters:nil success:^(id obj) {
        [ctl.spaceTypeArr addObjectsFromArray:obj];
        NSMutableArray *btnNameArr = [NSMutableArray array];
        for (MJSpaceTypeModel *m in ctl.spaceTypeArr) {
            NSString *name = m.name;
            [btnNameArr addObject:name];
        }
        [ctl.selectView setBtnTitleWithArray:btnNameArr];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
    NSDictionary *headerDict = @{@"spaceTypeID" : @0,@"pageSize" : @6,@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"type" : @0};
    
    [self addRefreshHeader:headerDict];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = RGB(246, 246, 246);
    [self.view addSubview:_circulateBackgroungView];
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    [self.circulateBackgroungView removeFromSuperview];
    [GMDCircleLoader hideFromView:self.view animated:YES];
}

#pragma mark - 添加上下拉加载数据
- (void)addRefreshHeader:(NSDictionary *)dict
{
    __weak MJSceneCtler *ctl = self;
    [self loadingData];
    //下拉刷新
    self.collectionView.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        [ctl loadNewData:dict];
    }];
    // 马上进入刷新状态
    [self.collectionView.mj_header beginRefreshing];
    
}

#pragma mark - 下拉加载数据
- (void)loadNewData:(NSDictionary *)dict
{
    self.pageNumber = 1;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJSceneCtler *ctl = self;
    [self.noDataView removeFromSuperview];
    [self.matchProductArr removeAllObjects];
    [self.userPhotoArr removeAllObjects];
    
    //获取场景列表
    if ([self isConnectToNet]) {
        
        [netTool getMoJingURL:MJSceneList parameters:dict success:^(id obj) {
            //  NSLog(@"%@",dict);
            MJMatchModel *model = obj;
            NSMutableArray *arr= [NSMutableArray array];
            arr = [MJMatchProductModel mj_objectArrayWithKeyValuesArray:model.data.sceneList];
            // for (int i = 0; i < 2; i++) {
            [ctl.matchProductArr addObjectsFromArray:arr];
            // }
            self.pageCount = model.data.pageCount;
            if (ctl.matchProductArr.count == 0) {
                [ctl.view addSubview:ctl.noDataView];
            }
            
            if (ctl.matchProductArr.count >= 6) {
                // 上拉刷新
                ctl.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                    // 进入刷新状态后会自动调用这个block
                    [ctl loadMoreData:dict];
                }];
                // 默认先隐藏footer
                ctl.collectionView.mj_footer.hidden = YES;
            }else{
                ctl.collectionView.mj_footer = nil;
            }
            
            [ctl.collectionView reloadData];
            //    [ctl stopDisplayLink];
            [ctl stopCircleLoader];
            [ctl.collectionView.mj_header endRefreshing];
            [ctl.view endEditing:YES];
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [ctl.collectionView.mj_header endRefreshing];
            [ctl stopCircleLoader];
        }];
        
    }else
    {
        [ctl stopCircleLoader];
        [ctl.collectionView.mj_header endRefreshing];
        
    }
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.collectionView];
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    _pageNumber++;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJSceneCtler *ctl = self;
    //[self.matchProductArr removeAllObjects];
    NSString *str = [dict objectForKey:@"spaceTypeID"];
    NSDictionary *moreDict = @{@"spaceTypeID" : str,@"pageNumber" : [NSString stringWithFormat:@"%ld",_pageNumber],@"pageSize" : @6,@"type" : @1};
    //获取搭配作品列表
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            
            [netTool getMoJingURL:MJSceneList parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                MJMatchModel *model = obj;
                NSMutableArray *arr= [NSMutableArray array];
                arr = [MJMatchProductModel mj_objectArrayWithKeyValuesArray:model.data.worksList];
                for (MJMatchProductModel *m in arr) {
                    NSString *userID = [NSString stringWithFormat:@"%ld",m.userID];
                    NSDictionary *para = @{@"ids":userID};
                    [netTool getMoJingURL:MJUsersDetailData parameters:para success:^(id obj) {
                        //用户信息
                        MJMatchUserDataModel *userModel = obj[0];
                        NSURL *url = [NSURL URLWithString:userModel.avatar];
                        [self.userPhotoArr addObject:url];
                        
                    } failure:^(NSError *error) {
                    }];
                }
                [ctl.matchProductArr addObjectsFromArray:arr];
                [ctl.collectionView reloadData];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
                //   [ctl stopDisplayLink];
                
                for (MJMatchProductModel *m in ctl.matchProductArr) {
                    NSDictionary *dict = @{@"id" : [NSString stringWithFormat:@"%ld",m.idNum]};
                    [ctl.matchDetailParaArr addObject:dict];
                    [netTool getMoJingURL:MJWorksDetail parameters:dict success:^(id obj) {
                        
                    } failure:^(NSError *error) {
                        NSLog(@"%@",error);
                        [ctl.collectionView.mj_footer endRefreshing];
                        [ctl stopCircleLoader];
                        //    [ctl stopDisplayLink];
                    }];
                }
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.collectionView.mj_footer endRefreshing];
                [ctl stopCircleLoader];
                //   [ctl stopDisplayLink];
            }];
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
        self.collectionView.mj_footer.hidden = YES;
        //   [self stopDisplayLink];
    }
    
}

#pragma mark  <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.matchProductArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MJAllMatchCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    // Configure the cell
    if (self.matchProductArr.count > 0) {
        MJMatchProductModel *model = self.matchProductArr[indexPath.row];
        NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
        [cell.bigFurnitureImageView  sd_setImageWithURL:bgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //            if (image) {
            //                if (!CGSizeEqualToSize(model.imageSize, image.size)) {
            //                    model.imageSize = image.size;
            //                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            //                }
            //            }
        }];
        cell.matchNameLabel.text = model.name;
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

#pragma mark  <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  
    MJMatchProductModel *model = self.matchProductArr[indexPath.row];
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSDictionary *para = @{@"id" : [NSString stringWithFormat:@"%ld",(long)model.idNum]};
    
    [net getMoJingURL:MJSceneDetail parameters:para success:^(id obj) {
        NSDictionary *productDic = obj[@"data"];
        if (productDic) {
            _sceneDic = productDic;
        }
        
        if (_sceneDic) {
            PhotoTrackController *photo = [[PhotoTrackController alloc] initWithItem:_sceneDic];
            [self.navigationController presentViewController:photo animated:YES completion:nil];
        }
       
        
    } failure:^(NSError *error) {
        
    }];
   
  //  NSLog(@"详情点击%ld",(long)indexPath.row);
}

#pragma mark - <UITextFieldDelegate>
//文本框开始输入的时候
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
}// became first responder

//结束输入的时候
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSDictionary *footerDict = @{@"searchString" : textField.text,@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"pageSize" : @6,@"type" : @1};
    if (textField.text.length > 0) {
        
        [self addRefreshHeader:footerDict];
        [self.selectView setBtnNotSelected];
        
    }else{
        [self.view endEditing:YES];
    }
    return YES;
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
    //    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
    //        return model.imageSize;
    //    }
    return CGSizeMake(328, 315);
}
@end
