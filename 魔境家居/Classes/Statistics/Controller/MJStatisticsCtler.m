//
//  MJStatisticsCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/28.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJStatisticsCtler.h"
#import "PNChart.h"
#import "MJGoodsDetailsCtler.h"
#import "MJdayReceptionistDetailsCtler.h"
#import "MJNetworkTool.h"
#import "MJBrandModel.h"
#import "MJBrandDataModel.h"
#import "MJExtension.h"
#import "MJGoodsTotalStatisticsModel.h"
#import "MJAllGoodsStatisticsListModel.h"
#import "MJGoodsStatisticsListModel.h"
#import "AppDelegate.h"
#import "MJStatisticsBrowseListModel.h"
#import "MJStatisticsBrowseModel.h"
#import "UIBarButtonItem+Extension.h"
#import "MJProductDetailsCtler.h"
@interface MJStatisticsCtler ()
/**
 *  日接待客户统计
 */
@property (weak, nonatomic) IBOutlet UIView *dayReceptionistView;
/**
 *  商品统计
 */
@property (weak, nonatomic) IBOutlet UIView *goodsStatisticsView;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageview;
/** 商品总量*/
@property (weak, nonatomic) IBOutlet UILabel *allGoodsLabel;
@property (weak, nonatomic) IBOutlet UILabel *allGoodsNumLabel;
/** 被搭配数*/
@property (weak, nonatomic) IBOutlet UILabel *matchedLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchedNumLabel;
/** 被收藏数*/
@property (weak, nonatomic) IBOutlet UILabel *collectedLabel;
@property (weak, nonatomic) IBOutlet UILabel *collectedNumLabel;

/** 被搭配数排行*/
@property (weak, nonatomic) IBOutlet UILabel *matchedRankLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchedFirstNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchedSecondNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchedThirdNumLabel;
/** 被收藏排行*/
@property (weak, nonatomic) IBOutlet UILabel *collectedFirstNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *collectedSecondNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *collectedThirdNumLabel;

/** 日期数组*/
@property (nonatomic, strong) NSMutableArray *dayDataArr;
/** 折线view*/
@property (nonatomic, strong) PNLineChart * lineChart;
/** 品牌数组*/
@property (nonatomic, strong) NSMutableArray *brandDataArr;
/** 统计被搭配数数组*/
@property (nonatomic, strong) NSMutableArray *matchRankArr;
/** 统计被收藏数数组*/
@property (nonatomic, strong) NSMutableArray *collectRankArr;

@property (nonatomic, strong) NSMutableArray *productListArr;
/** 获取接待客户统计列表数组*/
@property (nonatomic, strong) NSMutableArray *customerListArr;
@end

@implementation MJStatisticsCtler

- (NSMutableArray *)productListArr
{
    if (_productListArr == nil) {
        _productListArr = [NSMutableArray array];
    }
    return _productListArr;
}

- (NSMutableArray *)customerListArr
{
    if (_customerListArr == nil) {
        _customerListArr = [NSMutableArray array];
    }
    return _customerListArr;
}

- (NSMutableArray *)brandDataArr
{
    if (_brandDataArr == nil) {
        _brandDataArr = [NSMutableArray array];
    }
    return _brandDataArr;
}

- (NSMutableArray *)matchRankArr
{
    if (_matchRankArr == nil) {
        _matchRankArr = [NSMutableArray array];
    }
    return _matchRankArr;
}

- (NSMutableArray *)collectRankArr
{
    if (_collectRankArr == nil) {
        _collectRankArr = [NSMutableArray array];
    }
    return _collectRankArr;
}

- (NSMutableArray *)dayDataArr
{
    if (_dayDataArr == nil) {
        _dayDataArr = [NSMutableArray array];
    }
    return _dayDataArr;
}

- (PNLineChart *)lineChart
{
    if (_lineChart == nil) {
        _lineChart = [[PNLineChart alloc] initWithFrame:CGRectMake(0, 149, SCREEN_WIDTH, 330)];
        
        _lineChart.yLabelFormat = @"%1.1f";
        _lineChart.backgroundColor = [UIColor clearColor];
        
        _lineChart.showCoordinateAxis = YES;
        
        //Use yFixedValueMax and yFixedValueMin to Fix the Max and Min Y Value
        //Only if you needed
        
        _lineChart.yFixedValueMin = 0.0;
        
        
     //   self.lineChart.delegate = self;
        
        
        [self.view addSubview:self.lineChart];
    }
    return _lineChart;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)initUI
{
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    [self setTitle:@"我的统计"];
    [self.view bringSubviewToFront:self.dayReceptionistView];
    [self.view bringSubviewToFront:self.goodsStatisticsView];
   // [self.view addSubview:self.lineChart];
    [self getStatisticsData];
    
}

#pragma mark - 搭配、收藏
- (IBAction)statistcisRankClick:(UIButton *)button {
    NSInteger index = button.tag - 90;
    MJProductDetailsCtler *details = [[MJProductDetailsCtler alloc] init];
    if (self.productListArr.count > 5) {
        MJGoodsStatisticsListModel *model = self.productListArr[index];
        details.idNum = model.productID;
        [self.navigationController pushViewController:details animated:YES];
    }
    // NSLog(@"%ld",self.productListArr.count);
}


#pragma mark - 返回
- (void)back
{
 
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 获取统计信息
- (void)getStatisticsData
{
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];

    AppDelegate *myDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSDictionary *dict = myDelegate.brandIDParaDict;
  //   NSLog(@"%@ %@",myDelegate.hotListParaArr,dict);
            //商品总体统计
    if ([self isConnectToNet]) {
        [netTool getMoJingURL:MJProductTotalStatistics parameters:dict success:^(id obj) {
            MJGoodsTotalStatisticsModel *model1 = [MJGoodsTotalStatisticsModel mj_objectWithKeyValues:obj[@"data"]];
            self.allGoodsNumLabel.text = [NSString stringWithFormat:@"%ld",(long)model1.totalCount];
            self.matchedNumLabel.text = [NSString stringWithFormat:@"%ld",model1.totalUsedCount];
            self.collectedNumLabel.text = [NSString stringWithFormat:@"%ld",model1.totalFavoritedCount];
            
            //  NSLog(@"%ld %ld %ld",model1.totalCount,model1.totalUsedCount,model1.totalFavoritedCount);
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
        
        //商品统计列表
        //被搭配数
        NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
        NSDictionary *listDict = @{@"brandID" : brandID,@"statisticsType" : @1,@"pageSize" : @5,@"pageNumber" : [NSString stringWithFormat:@"%d",1]};
        [netTool getMoJingURL:MJProductStatisticsList parameters:listDict success:^(id obj) {
            MJAllGoodsStatisticsListModel *model2 = [MJAllGoodsStatisticsListModel mj_objectWithKeyValues:obj];
            self.matchRankArr = [MJGoodsStatisticsListModel mj_objectArrayWithKeyValuesArray:model2.data.productList];
            if (self.matchRankArr.count>2) {
                MJGoodsStatisticsListModel *m1 = self.matchRankArr[0];
                MJGoodsStatisticsListModel *m2 = self.matchRankArr[1];
                MJGoodsStatisticsListModel *m3 = self.matchRankArr[2];
                
                [self.productListArr insertObject:m1 atIndex:0];
                [self.productListArr insertObject:m2 atIndex:1];
                [self.productListArr insertObject:m3 atIndex:2];
                //被搭配数
                self.matchedFirstNumLabel.text = [NSString stringWithFormat:@"%@ %ld次",m1.productName,(long)m1.usedCount];
                self.matchedSecondNumLabel.text = [NSString stringWithFormat:@"%@ %ld次",m2.productName,(long)m2.usedCount];
                self.matchedThirdNumLabel.text = [NSString stringWithFormat:@"%@ %ld次",m3.productName,(long)m3.usedCount];
            }
            
            
            //  NSLog(@"%ld",_productListArr.count);
        } failure:^(NSError *error) {
            
        }];
        
        //收藏次数
        NSDictionary *listDict1 = @{@"brandID" : brandID,@"statisticsType" : @2,@"pageSize" : @5,@"pageNumber" : [NSString stringWithFormat:@"%d",1]};
        [netTool getMoJingURL:MJProductStatisticsList parameters:listDict1 success:^(id obj) {
            MJAllGoodsStatisticsListModel *model2 = [MJAllGoodsStatisticsListModel mj_objectWithKeyValues:obj];
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJGoodsStatisticsListModel mj_objectArrayWithKeyValuesArray:model2.data.productList];
         
            if (arr.count > 2) {
                MJGoodsStatisticsListModel *m1 = arr[0];
                MJGoodsStatisticsListModel *m2 = arr[1];
                MJGoodsStatisticsListModel *m3 = arr[2];
                [self.productListArr addObject:m1];
                [self.productListArr addObject:m2];
                [self.productListArr addObject:m3];
             

                //被收藏数
            self.collectedFirstNumLabel.text = [NSString stringWithFormat:@"%@ %ld次",m1.productName,(long)m1.favoritedCount];
            self.collectedSecondNumLabel.text = [NSString stringWithFormat:@"%@ %ld次",m2.productName,(long)m2.favoritedCount];
            self.collectedThirdNumLabel.text = [NSString stringWithFormat:@"%@ %ld次",m3.productName,(long)m3.favoritedCount];
            }
           
        } failure:^(NSError *error) {
            
        }];
        
        //日接待客户统计
        NSDictionary *dayDict = @{@"statisticsType" : @"1", @"pageSize" : @"21"};
        [netTool getMoJingURL:MJCustomerStatisticsList parameters:dayDict success:^(id obj) {
            NSDictionary *productDic = obj[@"data"];
         //       NSLog(@"%@",productDic);
            MJStatisticsBrowseModel *model = [MJStatisticsBrowseModel mj_objectWithKeyValues:productDic];
            
            NSMutableArray *modelArr = [NSMutableArray array];
            modelArr = [MJStatisticsBrowseListModel mj_objectArrayWithKeyValuesArray:model.browseList];
            
            for (MJStatisticsBrowseListModel *m in modelArr) {
                NSRange range = [m.dateString rangeOfString:@"-"];
                NSString *str = [m.dateString substringFromIndex:range.location+range.length];
                //for (int i = 0; i < 7; i++) {
                    [self.dayDataArr insertObject:str atIndex:0];
               
                    [self.customerListArr insertObject:@(m.count) atIndex:0];
                
            }
            
           
            
            NSInteger max = [[self.customerListArr valueForKeyPath:@"@max.intValue"] integerValue];
                
            if (max > 10) {
                 self.lineChart.yFixedValueMax = max;
                NSString *str1 = [NSString stringWithFormat:@"%ld ",(long)max/5];
                NSString *str2 = [NSString stringWithFormat:@"%ld ",(long)max/5*2];
                NSString *str3 = [NSString stringWithFormat:@"%ld ",(long)max/5*3];
                NSString *str4 = [NSString stringWithFormat:@"%ld ",(long)max/5*4];
                NSString *str5 = [NSString stringWithFormat:@"%ld ",(long)max/5*5];
        
            [self.lineChart setYLabels:@[@"0 ",str1,str2,str3,str4,str5]];
                self.lineChart.yFixedValueMax = max;
            }else
            {
                self.lineChart.yFixedValueMax = 10;
                [self.lineChart setYLabels:@[@"0 ",@"5 ",@"10 "]];
            }
          
            // Line Chart #1
            // NSMutableArray * data01Array = [NSMutableArray array];
            
            NSArray * data01Array = self.customerListArr;
            PNLineChartData *data01 = [PNLineChartData new];
            data01.dataTitle = @"Alpha";
            data01.color = [UIColor greenColor];
            data01.alpha = 0.5f;
            data01.itemCount = data01Array.count;
            
            data01.inflexionPointStyle = PNLineChartPointStyleTriangle;
            data01.getData = ^(NSUInteger index) {
                CGFloat yValue = [data01Array[index] floatValue];
                return [PNLineChartDataItem dataItemWithY:yValue];
            };
            
            self.lineChart.chartData = @[data01];
            [self.lineChart setXLabels:self.dayDataArr];
            [self.lineChart strokeChart];
            
        } failure:^(NSError *error) {
            
        }];

    }
    
}

#pragma mark - 日接待客户详情
- (IBAction)dayReceptionistDetails:(id)sender {
   // NSLog(@"日接待客户详情");
    MJdayReceptionistDetailsCtler *details = [[MJdayReceptionistDetailsCtler alloc] init];
    [self.navigationController pushViewController:details animated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
}

#pragma mark - 商品统计详情
- (IBAction)goodsStatisticsDetails:(id)sender {
   // NSLog(@"商品统计详情");
    MJGoodsDetailsCtler *details = [[MJGoodsDetailsCtler alloc] init];
    __weak MJStatisticsCtler *ctl = self;
    dispatch_async(dispatch_get_main_queue(), ^{
         [ctl.navigationController pushViewController:details animated:YES];
    });
}

@end
