//
//  MJdayReceptionistDetailsCtler.m
//  魔境家居
//
//  Created by mojing on 15/11/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJdayReceptionistDetailsCtler.h"
#import "UIBarButtonItem+Extension.h"
#import "MJdayReceptionistSelectView.h"
#import "MJdayReceptionistHeadView.h"
#import "MJDayReceptionistCell.h"
#import "MJNetworkTool.h"
#import "GMDCircleLoader.h"
#import "MJRefresh.h"
#import "MJExtension.h"
#import "MJStatisticsBrowseListModel.h"
#import "MJStatisticsBrowseModel.h"
#import "MJUIClassTool.h"
@interface MJdayReceptionistDetailsCtler ()<UITableViewDelegate,UITableViewDataSource>
/**
 *  选择年月日View
 */
@property (nonatomic,strong) MJdayReceptionistSelectView *selectView;
@property (nonatomic,strong) MJdayReceptionistHeadView *headView;
@property (nonatomic,strong) UITableView  *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
@end

@implementation MJdayReceptionistDetailsCtler

static NSString * const reuseIdentifier = @"Cell";

- (UIImageView *)noDataView
{
    if (_noDataView == nil) {
        
        _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
        _noDataView.image = [UIImage imageNamed:@"no_data"];
        _noDataView.center = self.view.center;
        
        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(17, 147, 150, 30) title:@"暂无客户统计!" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:20];
        [_noDataView addSubview:lb];
        
        //   [self.view addSubview:_noDataView];
        
    }
    return _noDataView;
}

- (NSMutableArray *)dataArr
{
    if (_dataArr == nil) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(20, 114,Screen_Width-40, Screen_Height-124) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = NO;
        _tableView.tableHeaderView = self.headView;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.rowHeight = 50;
        [_tableView registerNib:[UINib nibWithNibName:@"MJDayReceptionistCell" bundle:nil] forCellReuseIdentifier:reuseIdentifier];
    }
    return _tableView;
}

/**
 *  选择年月日View
 */
- (MJdayReceptionistSelectView *)selectView
{
    if (_selectView == nil) {
        _selectView = [MJdayReceptionistSelectView head];
        _selectView.frame = CGRectMake(20, 64, Screen_Width/2-20, 50);
        _selectView.dayButton.selected = YES;
        [_selectView.dayButton setBackgroundImage:[UIImage imageNamed:@"dayBack"] forState:UIControlStateSelected];
        __weak MJdayReceptionistDetailsCtler *ctl = self;
        _selectView.dateClick = ^(NSInteger index){
            
            NSDictionary *headerDict = @{@"statisticsType" : [NSString stringWithFormat:@"%ld",index-99],@"pageNumber" : [NSString stringWithFormat:@"%ld",ctl.pageNumber],@"pageSize" : @12};
        
            switch (index) {
                case 100:
                {
                    NSLog(@"按天统计");
                    [ctl addRefreshHeader:headerDict];
                    [ctl.headView setDate:@"日"];
                    break;
                }
                case 101:
                {
                    NSLog(@"按周统计");
                    [ctl addRefreshHeader:headerDict];
                    [ctl.headView setDate:@"周"];
                    break;
                }
                case 102:
                {
                    NSLog(@"按月统计");
                    [ctl addRefreshHeader:headerDict];
                    [ctl.headView setDate:@"月"];
                    break;
                }
                case 103:
                {
                    NSLog(@"按年统计");
                    [ctl addRefreshHeader:headerDict];
                    [ctl.headView setDate:@"年"];
                    break;
                }
                default:
                    break;
            }
        };
    }
    return _selectView;
}

- (MJdayReceptionistHeadView *)headView
{
    if (_headView == nil) {
        _headView = [MJdayReceptionistHeadView head];
    }
    return _headView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"接待客户统计详情";
    self.view.backgroundColor = RGB(225, 225, 225);
    self.pageNumber = 1;
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    [self.view addSubview:self.selectView];
    [self.view addSubview:self.tableView];
    NSDictionary *dict = @{@"statisticsType" : [NSString stringWithFormat:@"%d",1],@"pageNumber" :[NSString stringWithFormat:@"%ld",self.pageNumber],@"pageSize" : @12};
    
    [self addRefreshHeader:dict];
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.tableView.frame];
    self.selectView.userInteractionEnabled = NO;
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_circulateBackgroungView];
   [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    self.selectView.userInteractionEnabled = YES;
    [self.circulateBackgroungView removeFromSuperview];
    //[GMDCircleLoader hideFromView:self.view animated:YES];
}


#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 添加上下拉刷新
- (void)addRefreshHeader:(NSDictionary *)dict
{
    __weak MJdayReceptionistDetailsCtler *ctl = self;
    [self loadingData];
    //下拉刷新
    self.tableView.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        [ctl loadNewData:dict];
    }];
    // 马上进入刷新状态
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - 下拉加载数据
- (void)loadNewData:(NSDictionary *)dict
{
    _pageNumber = 1;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJdayReceptionistDetailsCtler *ctl = self;
    NSString *type = dict[@"statisticsType"];
    [self.noDataView removeFromSuperview];
    [self.dataArr removeAllObjects];
    //获取年月周日接待详情
    if ([self isConnectToNet]) {
            [netTool getMoJingURL:MJCustomerStatisticsList parameters:dict success:^(id obj) {
            //    NSLog(@"%@",dict);
                NSDictionary *productDic = obj[@"data"];
            //    NSLog(@"%@",productDic);
                MJStatisticsBrowseModel *model = [MJStatisticsBrowseModel mj_objectWithKeyValues:productDic];
                self.pageCount = model.pageCount;
               
                NSMutableArray *modelArr = [NSMutableArray array];
                modelArr = [MJStatisticsBrowseListModel mj_objectArrayWithKeyValuesArray:model.browseList];
                NSMutableArray *arr1 = [NSMutableArray array];
                for (MJStatisticsBrowseListModel *m in modelArr) {
                    m.type = type;
                    [arr1 addObject:m];
                }
                [ctl.dataArr addObjectsFromArray:arr1];
               // [ctl.dataArr addObject:model1];
                if (ctl.dataArr.count == 0) {
                    [ctl.view addSubview:ctl.noDataView];
                }
                if (ctl.dataArr.count >= 12) {
                    ctl.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        // 进入刷新状态后会自动调用这个block
                        [ctl loadMoreData:dict];
                    }];
                    // 默认先隐藏footer
                    ctl.tableView.mj_footer.hidden = YES;
                }else{
                    ctl.tableView.mj_footer = nil;
                }
                
                [ctl.tableView reloadData];
                [ctl stopCircleLoader];
                [ctl.tableView.mj_header endRefreshing];
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.tableView.mj_header endRefreshing];
                [ctl stopCircleLoader];
            }];
    }else
    {
        [ctl stopCircleLoader];
        [ctl.tableView.mj_header endRefreshing];
    }
}


#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    _pageNumber++;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJdayReceptionistDetailsCtler *ctl = self;
    NSString *str = [dict objectForKey:@"statisticsType"];
    NSDictionary *moreDict = @{@"statisticsType" : str,@"pageNumber" : [NSString stringWithFormat:@"%ld",_pageNumber],@"pageSize" : @12};
    //获取搭配作品列表
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            [netTool getMoJingURL:MJCustomerStatisticsList parameters:moreDict success:^(id obj) {
                NSDictionary *productDic = obj[@"data"];
                MJStatisticsBrowseModel *model = [MJStatisticsBrowseModel mj_objectWithKeyValues:productDic];
                
                NSMutableArray *modelArr = [NSMutableArray array];
                modelArr = [MJStatisticsBrowseListModel mj_objectArrayWithKeyValuesArray:model.browseList];
                NSMutableArray *arr1 = [NSMutableArray array];
                for (MJStatisticsBrowseListModel *m in modelArr) {
                    m.type = str;
                    [arr1 addObject:m];
                }
                [ctl.dataArr addObjectsFromArray:arr1];
                [ctl.tableView reloadData];
                [ctl stopCircleLoader];
                [ctl.tableView.mj_footer endRefreshing];
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.tableView.mj_footer endRefreshing];
                [ctl stopCircleLoader];
            }];
        }else
        {
            // 变为没有更多数据的状态
            [ctl.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        [ctl stopCircleLoader];
        [ctl.tableView.mj_footer endRefreshing];
        self.tableView.mj_footer.hidden = YES;
    }
    
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark  <UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MJDayReceptionistCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (self.dataArr.count>0) {
        MJStatisticsBrowseListModel *model = self.dataArr[indexPath.row];
       // NSLog(@"%@",model.type);
        if ([model.type isEqualToString:@"2"]) {
            [cell setDateLabel:[NSString stringWithFormat:@"%@~%@",model.firstDay,model.lastDay] dayReceptionistLabel:model.count];
        }else{
            [cell setDateLabel:model.dateString dayReceptionistLabel:model.count];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - <UITableViewDelegate>
@end
