//
//  MJdayReceptionistHeadView.m
//  魔境家居
//
//  Created by mojing on 15/11/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJdayReceptionistHeadView.h"

@implementation MJdayReceptionistHeadView
{
    __weak IBOutlet UILabel *_dateLabel;
    __weak IBOutlet UILabel *_receptionistLabel;
}

+ (instancetype)head
{
    return [[[NSBundle mainBundle] loadNibNamed:@"MJdayReceptionistHeadView" owner:nil options:nil] firstObject];
}

- (void)setDate:(NSString *)date {
    _dateLabel.text = date;
}
@end
