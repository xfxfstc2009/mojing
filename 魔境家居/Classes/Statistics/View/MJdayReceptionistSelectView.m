//
//  MJMJdayReceptionistSelectView.m
//  魔境家居
//
//  Created by mojing on 15/11/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJdayReceptionistSelectView.h"
@interface MJdayReceptionistSelectView()

@property (nonatomic,strong) UIButton *selectBtn;
@end
@implementation MJdayReceptionistSelectView

+ (instancetype)head
{
   return [[[NSBundle mainBundle] loadNibNamed:@"MJdayReceptionistSelectView" owner:nil options:nil] firstObject];
}

- (IBAction)btnClick:(UIButton *)button {
    
        self.dayButton.selected = NO;
        self.selectBtn.selected = NO;
        button.selected = YES;
        self.selectBtn = button;
    
    if (button.selected) {
       // [button setBackgroundImage:[UIImage imageNamed:@"dayBack"] forState:UIControlStateSelected];
    }
    //传tag值判断哪个按钮点击
    if (_dateClick) {
        _dateClick(button.tag);
    }
}


@end
