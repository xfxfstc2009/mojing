//
//  MJDayReceptionistCell.m
//  魔境家居
//
//  Created by mojing on 15/11/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJDayReceptionistCell.h"

@implementation MJDayReceptionistCell
{
    __weak IBOutlet UILabel *_dateLabel;
    __weak IBOutlet UILabel *_dayReceptionistLabel;
    
}


- (void)setDateLabel:(NSString *)date dayReceptionistLabel:(NSInteger)count
{
    _dateLabel.text = date;
    _dayReceptionistLabel.text = [NSString stringWithFormat:@"%ld",count];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
