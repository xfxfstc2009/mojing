//
//  MJMJdayReceptionistSelectView.h
//  魔境家居
//
//  Created by mojing on 15/11/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^selectDateClick)(NSInteger index);
@interface MJdayReceptionistSelectView : UIView

@property (nonatomic,copy) selectDateClick dateClick;
@property (weak, nonatomic) IBOutlet UIButton *dayButton;
+ (instancetype)head;
@end
