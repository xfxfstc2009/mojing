//
//  MJStatisticsDetailsCell.m
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJStatisticsDetailsCell.h"

@implementation MJStatisticsDetailsCell
{
    __weak IBOutlet UILabel *_idNumLabel;
    __weak IBOutlet UILabel *_goodsNameLabel;
    __weak IBOutlet UILabel *_matchedNumLabel;
    //__weak IBOutlet UILabel *_upNumLabel;
    __weak IBOutlet UILabel *_collectedNumLabel;
    
}

- (void)setModel:(MJGoodsStatisticsModel *)model
{
    _model = model;
    if (model.orderCount < 1000) {
        _idNumLabel.text = [NSString stringWithFormat:@"%03ld",model.orderCount];
    }else
    {
        _idNumLabel.text = [NSString stringWithFormat:@"%ld",model.orderCount];
    }
    
    _goodsNameLabel.text = model.productName;
    _matchedNumLabel.text = [NSString stringWithFormat:@"%ld",model.usedCount] ;
   // _upNumLabel.text = [NSString stringWithFormat:@"%ld",model.upCount] ;
    _collectedNumLabel.text = [NSString stringWithFormat:@"%ld",model.favoritedCount];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
