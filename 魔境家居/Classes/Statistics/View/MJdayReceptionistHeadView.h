//
//  MJdayReceptionistHeadView.h
//  魔境家居
//
//  Created by mojing on 15/11/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJdayReceptionistHeadView : UIView

+ (instancetype)head;
- (void)setDate:(NSString *)date;

@end
