//
//  MJGoodsTotalStatisticsModel.h
//  魔境家居
//
//  Created by mojing on 15/11/17.
//  Copyright © 2015年 mojing. All rights reserved.
//  商品总体统计

#import <Foundation/Foundation.h>

@interface MJGoodsTotalStatisticsModel : NSObject
/* 商品总数数*/
@property (nonatomic,assign) NSInteger totalUsedCount ;// 1,
/* 被搭配数*/
@property (nonatomic,assign) NSInteger totalCount ;// 1,
/* 被收藏数*/
@property (nonatomic,assign) NSInteger totalFavoritedCount ;// 0
/* 被点赞数*/
@property (nonatomic,assign) NSInteger totalUpCount;
@end
