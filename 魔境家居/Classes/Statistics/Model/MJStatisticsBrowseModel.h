//
//  MJStatisticsBrowseModel.h
//  魔境家居
//
//  Created by mojing on 15/12/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJStatisticsBrowseListModel.h"
@interface MJStatisticsBrowseModel : NSObject

@property (nonatomic,assign) NSInteger pageNumber ;
@property (nonatomic,strong) NSArray *browseList ;

@property (nonatomic,assign) NSInteger pageSize ;
@property (nonatomic,assign) NSInteger resourceCount ;
@property (nonatomic,assign) NSInteger pageCount ;
@end
