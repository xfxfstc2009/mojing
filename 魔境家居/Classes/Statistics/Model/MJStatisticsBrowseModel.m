//
//  MJStatisticsBrowseModel.m
//  魔境家居
//
//  Created by mojing on 15/12/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJStatisticsBrowseModel.h"
#import "MJExtension.h"
@implementation MJStatisticsBrowseModel

- (NSDictionary *)objectClassInArray
{
    return @{@"browseList":[MJStatisticsBrowseListModel class]};
}
@end
