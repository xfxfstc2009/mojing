//
//  MJAllGoodsStatisticsDataModel.m
//  魔境家居
//
//  Created by mojing on 15/11/18.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJAllGoodsStatisticsDataModel.h"
#import "MJExtension.h"
#import "MJGoodsStatisticsListModel.h"
@implementation MJAllGoodsStatisticsDataModel

- (NSDictionary *)objectClassInArray
{
    return @{@"productList":[MJGoodsStatisticsListModel class]};
}
@end
