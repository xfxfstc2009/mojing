//
//  MJGoodsStatisticsListModel.h
//  魔境家居
//
//  Created by mojing on 15/11/17.
//  Copyright © 2015年 mojing. All rights reserved.
//  商品统计列表模型

#import <Foundation/Foundation.h>

@interface MJGoodsStatisticsListModel : NSObject

/* 商品ID*/
@property (nonatomic,assign) NSInteger productID ;
/* 商品名称*/
@property (nonatomic,copy) NSString *productName ;
/* 被搭配数*/
@property (nonatomic,assign) NSInteger usedCount ;
/* 被收藏数*/
@property (nonatomic,assign) NSInteger favoritedCount;
/* 被点赞数*/
@property (nonatomic,assign) NSInteger upCount;
@end
