//
//  MJGoodsStatisticsModel.m
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJGoodsStatisticsModel.h"

@implementation MJGoodsStatisticsModel

- (instancetype)initWithProductID:(NSInteger)productID productName:(NSString *)productName usedCount:(NSInteger)usedCount upCount:(NSInteger)upCount favoritedCount:(NSInteger)favoritedCount
{
    if (self = [super init]) {
        self.productID = productID;
        self.productName = productName;
        self.usedCount = usedCount;
        self.upCount = upCount;
        self.favoritedCount = favoritedCount;
    }
    return self;
}
+ (instancetype)modelWithProductID:(NSInteger)productID productName:(NSString *)productName usedCount:(NSInteger)usedCount upCount:(NSInteger)upCount favoritedCount:(NSInteger)favoritedCount
{
    return [[self alloc] initWithProductID:productID productName:productName usedCount:usedCount upCount:upCount favoritedCount:favoritedCount];
}
@end
