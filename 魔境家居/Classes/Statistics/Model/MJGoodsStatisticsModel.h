//
//  MJGoodsStatisticsModel.h
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//  商品总体统计列表参数模型

#import <Foundation/Foundation.h>

@interface MJGoodsStatisticsModel : NSObject
/* 商品ID*/
@property (nonatomic, assign) NSInteger productID;
/* 商品名称*/
@property (nonatomic, copy) NSString *productName;
/* 被搭配数*/
@property (nonatomic, assign) NSInteger usedCount;
/* 被点赞数*/
@property (nonatomic, assign) NSInteger upCount;
/* 被收藏数*/
@property (nonatomic, assign) NSInteger favoritedCount;
/* 序列数*/
@property (nonatomic, assign) NSInteger orderCount;


- (instancetype)initWithProductID:(NSInteger)productID productName:(NSString *)productName usedCount:(NSInteger)usedCount upCount:(NSInteger)upCount favoritedCount:(NSInteger)favoritedCount;

+ (instancetype)modelWithProductID:(NSInteger)productID productName:(NSString *)productName usedCount:(NSInteger)usedCount upCount:(NSInteger)upCount favoritedCount:(NSInteger)favoritedCount;
@end
