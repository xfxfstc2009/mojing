//
//  MJStatisticsBrowseListModel.h
//  魔境家居
//
//  Created by mojing on 15/12/2.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJStatisticsBrowseListModel : NSObject

@property (nonatomic,copy) NSString * dateString  ;//  日期字符串，例如天2015-10-10、月2015-10、年2015 ,
@property (nonatomic,copy) NSString * firstDay  ;//  2015-10-10，一周的第一天，仅按周统计时有意义 ,
@property (nonatomic,copy) NSString * lastDay  ;//  2015-10-16，一周的最后一天，仅按周统计时有意义 ,
@property (nonatomic,assign) NSInteger  count  ;// 10000

@property (nonatomic,copy) NSString *type; //按年(4)月(3)周(2)日(1)统计
@end
