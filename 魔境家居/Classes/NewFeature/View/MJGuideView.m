//
//  MJGuideView.m
//  魔境家居
//
//  Created by mojing on 15/10/26.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJGuideView.h"
#import "MJLoginCtler.h"

@implementation MJGuideView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        for (int i = 0; i < 5; i++) {
            UIImageView * imgView = [[UIImageView alloc] initWithFrame:CGRectMake(Screen_Width*i, 0, Screen_Width, Screen_Height)];
            //  imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"help%zd.png",i+1]];
            imgView.backgroundColor =[UIColor colorWithRed:arc4random()%256/255.0 green:arc4random()%256/255.0 blue:arc4random()%256/255.0 alpha:1];
            
            [self addSubview:imgView];
            
            if (i == 4) {
                imgView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(click)];
                [imgView addGestureRecognizer:tap];
            }
        }
        self.contentSize = CGSizeMake(Screen_Width*5, Screen_Height);
        self.pagingEnabled = YES;
        self.showsHorizontalScrollIndicator = YES;
        self.showsVerticalScrollIndicator = NO;
        self.bounces = NO;
    }
    return self;
}

- (void)click
{
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:[[MJLoginCtler alloc] init]];
    
    MJKeyWindow.rootViewController = nav;
    
}


@end
