//
//  MJNewFeatureCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/26.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJNewFeatureCtler.h"
#import "MJGuideView.h"
@interface MJNewFeatureCtler ()

@end

@implementation MJNewFeatureCtler

- (instancetype)init
{
    if (self = [super init]) {
        MJGuideView *guide = [[MJGuideView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        
        [self.view addSubview:guide];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
