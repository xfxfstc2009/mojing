//
//  NSString+GWjson.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/22.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "NSString+GWjson.h"
#import "GWJson.h"

@implementation NSString (GWjson)
- (id)JSONFragmentValue
{
    GWJson *json = [GWJson new];
    
    NSError *error;
    id o = [json fragmentWithString:self error:&error];
    
    if (!o)
        NSLog(@"%@", error);
    return o;
}

- (id)JSONValue
{
    GWJson *json = [GWJson new];
    
    NSError *error;
    id o = [json objectWithString:self error:&error];
    
    if (!o)
        NSLog(@"%@", error);
    return o;
}

@end
