//
//  NSString+GWjson.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/22.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GWjson)
- (id)JSONFragmentValue;

- (id)JSONValue;
@end
