//
//  GWJson.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/22.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
extern NSString * SBJSONErrorDomain;

enum {
    EUNSUPPORTED = 1,
    EPARSENUM,
    EPARSE,
    EFRAGMENT,
    ECTRL,
    EUNICODE,
    EDEPTH,
    EESCAPE,
    ETRAILCOMMA,
    ETRAILGARBAGE,
    EEOF,
    EINPUT
};

@interface GWJson : NSObject{
    BOOL humanReadable;
    BOOL sortKeys;
    NSUInteger maxDepth;
@private
    // Used temporarily during scanning/generation
    NSUInteger depth;
    const char *c;
}

/// Whether we are generating human-readable (multiline) JSON
/**
 Set whether or not to generate human-readable JSON. The default is NO, which produces
 JSON without any whitespace. (Except inside strings.) If set to YES, generates human-readable
 JSON with linebreaks after each array value and dictionary key/value pair, indented two
 spaces per nesting level.
 */
@property BOOL humanReadable;

/// Whether or not to sort the dictionary keys in the output
/** The default is to not sort the keys. */
@property BOOL sortKeys;

/// The maximum depth the parser will go to
/** Defaults to 512. */
@property NSUInteger maxDepth;

/// Return JSON representation of an array  or dictionary
- (NSString*)stringWithObject:(id)value error:(NSError**)error;

/// Return JSON representation of any legal JSON value
- (NSString*)stringWithFragment:(id)value error:(NSError**)error;

/// Return the object represented by the given string
- (id)objectWithString:(NSString*)jsonrep error:(NSError**)error;

/// Return the fragment represented by the given string
- (id)fragmentWithString:(NSString*)jsonrep error:(NSError**)error;

/// Return JSON representation (or fragment) for the given object
- (NSString*)stringWithObject:(id)value
                  allowScalar:(BOOL)x
                        error:(NSError**)error;

/// Parse the string and return the represented object (or scalar)
- (id)objectWithString:(id)value
           allowScalar:(BOOL)x
                 error:(NSError**)error;


@end
