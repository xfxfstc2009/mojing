//
//  NSObject+GWJson.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/22.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "NSObject+GWJson.h"
#import "GWJson.h"

@implementation NSObject (GWJson)
- (NSString *)JSONFragment {
    GWJson *generator = [GWJson new] ;
    
    NSError *error;
    NSString *json = [generator stringWithFragment:self error:&error];
    
    if (!json)
        NSLog(@"%@", error);
    return json;
}

- (NSString *)JSONRepresentation {
    GWJson *generator = [GWJson new];
    
    NSError *error;
    NSString *json = [generator stringWithObject:self error:&error];
    
    if (!json)
        NSLog(@"%@", error);
    return json;
}

@end
