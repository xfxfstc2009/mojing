//
//  GWImageView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImageView+WebCache.h>

@interface GWImageView : UIImageView

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;
-(void)uploadImageWithURL300:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;
-(void)uploadImageWithURL50:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;
-(void)uploadImageWithURL100:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;
-(void)uploadImageWithURL200:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;
-(void)uploadImageWithURL500:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;
-(void)uploadImageWithURL:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock;
@end
