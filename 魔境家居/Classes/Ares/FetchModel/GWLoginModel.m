//
//  GWLoginModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWLoginModel.h"
@implementation GWLoginModel

+ (instancetype)shareInstance{
    
    static GWLoginModel *loginModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        loginModel = [[GWLoginModel alloc] init];
    });
    
    return loginModel;
}


- (BOOL)hasLoggedIn{
    return (self.isSessionValid && ![GWTool isEmpty:self.phone]);
}

#pragma mark 登录方法
- (void)loginWithUser:(NSString *)name andPassword:(NSString *)password completionHandler:(void(^)(BOOL isSucceeded, NSError *error))handler{
    __weak typeof(self)weakSelf = self;
    
    self.requestParams = @{@"userName": (name ?: @""), @"password": (password ?: @"")};
    [self fetchWithPath:main_login completionHandler:^(BOOL isSucceeded, NSError *error) {
        
        if (!weakSelf) {
            return;
        }
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (isSucceeded) {
            
            [GWLoginModel loginIMWithUserId:self.userId pwd:@"smartmin" backBlock:NULL];
            strongSelf.bizResult = YES;
            [strongSelf saveCurrentUserName:name];
            [strongSelf saveCurrentUserPass:@"smartmin"];
            [strongSelf setupFromLocalData:strongSelf];

        }
        //  埋点统计-业务逻辑错误
        else if ([error.domain isEqualToString:GWBizErrorDomain]) {
            
            
        }
        handler(isSucceeded, error);
    }];
}

#pragma mark 登出
- (void)logout{
    if (![GWTool isEmpty:self.phone]) {
        GWLoginModel *resetLoginModel = [[GWLoginModel alloc] init];
        [[GWLoginModel shareInstance] setupFromLocalData:resetLoginModel];
        [USERDEFAULTS removeObjectForKey:LOGINDATA];
        [self clearCookiesForBaseURL];
    }
}

- (void)setupFromLocalData:(GWLoginModel *)loginModel{
    [GWLoginModel shareInstance].userId = loginModel.userId;
    [GWLoginModel shareInstance].nick = loginModel.nick;
    [GWLoginModel shareInstance].icon = loginModel.icon;
    [GWLoginModel shareInstance].gender = loginModel.gender;
}

#pragma mark 保存信息
- (void)saveCurrentUserName:(NSString *)userName{
    [USERDEFAULTS setObject:userName forKey:CURRENT_USER_NAME_KEY];
    [USERDEFAULTS synchronize];
}

- (void)saveCurrentUserPass:(NSString *)userPass{
    [USERDEFAULTS setObject:userPass forKey:CURRENT_USER_PASS_KEY];
    [USERDEFAULTS synchronize];
}

+ (void)saveCurrentUserPass:(NSString *)userPass{
    [USERDEFAULTS setObject:userPass forKey:CURRENT_USER_PASS_KEY];
    [USERDEFAULTS synchronize];
}

#pragma mark - IM 登录
+(void)loginIMWithUserId:(NSString *)userId pwd:(NSString *)password backBlock:(void (^)(BOOL isSuccessed))callback{

}

#pragma mark - 授权登录
-(void)loginWithAuthWechatWithUserName:(NSString *)userName nickName:(NSString *)nickName gender:(NSInteger)gender icon:(NSString *)icon completionHandler:(void(^)(BOOL isSucceeded, NSError *error))handler{
    __weak typeof(self)weakSelf = self;
    self.requestParams = @{@"userName": (userName ?: @""), @"nickName": (nickName ?: @""),@"gender":@(gender),@"icon":(icon?:@"")};
    [self fetchWithPath:main_authLogin completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (isSucceeded) {
            // 1. 登录IM
            [GWLoginModel loginIMWithUserId:self.userId pwd:@"smartmin" backBlock:NULL];
            strongSelf.bizResult = YES;
            [strongSelf saveCurrentUserName:userName];
            [strongSelf saveCurrentUserPass:@"smartmin"];
            [strongSelf setupFromLocalData:strongSelf];
        } else {
   
        }
        handler(isSucceeded, error);
    }];
}
@end
