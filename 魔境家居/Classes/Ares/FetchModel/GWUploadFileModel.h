//
//  GWUploadFileModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWFetchModel.h"

@interface GWUploadFileModel : GWFetchModel

@property (nonatomic,strong)UIImage *uploadImage;
@property (nonatomic,copy)NSString *keyName;

// 获取
@property (nonatomic,copy)NSString *fileId;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *suffix;


@end
