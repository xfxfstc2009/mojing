//
//  GWUploadFileModel.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWUploadFileModel.h"

@implementation GWUploadFileModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"fileId": @"id"};
}

@end
