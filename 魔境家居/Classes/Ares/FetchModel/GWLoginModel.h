//
//  GWLoginModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWFetchModel.h"

@interface GWLoginModel : GWFetchModel

@property (nonatomic,copy)NSString *userId;           /**< userId*/
@property (nonatomic,copy)NSString *phone;            /**< tel*/
@property (nonatomic,copy)NSString *password;         /**< password*/
@property (nonatomic,copy)NSString *nick;         /**< 昵称*/
@property (nonatomic,assign)NSInteger gender;         /**< 性别*/
@property (nonatomic,copy)NSString *icon;        /**< 头像*/

+ (instancetype)shareInstance;                                  /**< 单利*/

- (void)loginWithUser:(NSString *)name andPassword:(NSString *)password completionHandler:(void(^)(BOOL isSucceeded, NSError *error))handler;

#pragma mark - 授权登录
-(void)loginWithAuthWechatWithUserName:(NSString *)userName nickName:(NSString *)nickName gender:(NSInteger)gender icon:(NSString *)icon completionHandler:(void(^)(BOOL isSucceeded, NSError *error))handler;

- (BOOL)hasLoggedIn;                                            /**< 判断是否登录*/
- (void)logout;

@end
