//
//  GWFetchModel.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <Foundation/Foundation.h>

#define GWBizErrorDomain @"GWBizErrorDomain"

typedef void (^FetchCompletionHandler) (BOOL isSucceeded, NSError *error);
typedef void (^FetchCompletionHandlerTemp) (BOOL isSucceeded, NSError *error,id data);

@interface GWFetchModel : NSObject

@property (nonatomic, assign) BOOL bizDataIsNull;
@property (nonatomic, assign) BOOL bizResult;

// 请求参数
@property (nonatomic, strong) NSDictionary *requestParams;
@property (nonatomic, strong) NSArray *requestFileDataArr;

// 自定义的UserAgent信息
+ (NSString *)customUserAgent;

// 请求接口
-(void)fetchWithPath:(NSString *)path completionHandler:(FetchCompletionHandler)handler;
-(void)fetchWithGetPath:(NSString *)path completionHandler:(FetchCompletionHandler)handler;
-(void)fetchWithGetPath1:(NSString *)path completionHandler:(FetchCompletionHandlerTemp)handler;
-(void)fetchWithImgInfoPath:(NSString *)path completionHandler:(FetchCompletionHandler)handler;

- (BOOL)isSessionValid;
- (void)clearCookiesForBaseURL;

// 仅供运行时解析JSON使用，子类不要调用此方法初始化对象
- (instancetype)initWithJSONDict:(NSDictionary *)dict;

// 子类需要覆盖此方法，提供model和JSON无法对应到的成员
- (NSDictionary *)modelKeyJSONKeyMapper;


@end
