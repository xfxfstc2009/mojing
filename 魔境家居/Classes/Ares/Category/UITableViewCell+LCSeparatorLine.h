//
//  UITableViewCell+LCSeparatorLine.h
//  LaiCai
//
//  Created by SmartMin on 15/8/12.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger,SeparatorType)
{
    SeparatorTypeHead    =   1,
    SeparatorTypeMiddle      ,
    SeparatorTypeBottom      ,
    SeparatorTypeSingle      ,
};

@interface UITableViewCell (LCSeparatorLine)
- (void)addSeparatorLineWithType:(SeparatorType)separatorType;
- (void)addSeparatorLineWithTypeWithCustomer:(SeparatorType)separatorType andUsingTag:(NSString *)usingTag;


@end
