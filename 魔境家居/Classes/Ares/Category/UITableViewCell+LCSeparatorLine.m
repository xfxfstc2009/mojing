//
//  UITableViewCell+LCSeparatorLine.m
//  LaiCai
//
//  Created by SmartMin on 15/8/12.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import "UITableViewCell+LCSeparatorLine.h"

#define kTopTag      @"topSeparatorTag"
#define kBottomTag   @"bottomSeparatorTag"
#define hltColor     @"e3e0de"


@implementation UITableViewCell (LCSeparatorLine)

- (void)addSeparatorLineWithType:(SeparatorType)separatorType
{
    UIImageView *topImageView = (UIImageView *)[self viewWithStringTag:kTopTag];
    UIImageView *bottomImageView = (UIImageView *)[self viewWithStringTag:kBottomTag];
    
    CGFloat separatorInset   = (separatorType == SeparatorTypeBottom || separatorType == SeparatorTypeSingle)?0:15;
    BOOL    showTopSeparator = (separatorType == SeparatorTypeHead   || separatorType == SeparatorTypeSingle)?YES:NO;
    
    if (showTopSeparator)
    {
        if (!topImageView)
        {
            topImageView = [[UIImageView alloc] init];
            [topImageView setStringTag:kTopTag];
            [topImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
            
            [topImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        }
        [topImageView setFrame:CGRectMake(0, 0,CGRectGetWidth([self frame]), 0.5)];
        [self addSubview:topImageView];
    }
    else
    {
        if (topImageView)
            [topImageView removeFromSuperview];
    }
    
    if (!bottomImageView)
    {
        bottomImageView = [[UIImageView alloc] init];
        [bottomImageView setStringTag:kBottomTag];
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
    }
    [bottomImageView setFrame:CGRectMake( separatorInset, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]), 0.5)];
    [self addSubview:bottomImageView];
}


#pragma mark - Customer Line
-(void)addSeparatorLineWithTypeWithCustomer:(SeparatorType)separatorType andUsingTag:(NSString *)usingTag{
    UIImageView *topImageView = (UIImageView *)[self viewWithStringTag:kTopTag];
    UIImageView *bottomImageView = (UIImageView *)[self viewWithStringTag:kBottomTag];
    
    BOOL    showTopSeparator = (separatorType == SeparatorTypeHead   || separatorType == SeparatorTypeSingle)?YES:NO;
    
    if (showTopSeparator) {
        if (!topImageView) {
            topImageView = [[UIImageView alloc] init];
            [topImageView setStringTag:kTopTag];
            
            [topImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
            [topImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        }
        [topImageView setFrame:CGRectMake(0, 0,CGRectGetWidth([self frame]), 0.5)];
        [self addSubview:topImageView];
    } else {
        if (topImageView)
            [topImageView removeFromSuperview];
    }
    
    if (!bottomImageView) {
        bottomImageView = [[UIImageView alloc] init];
        [bottomImageView setStringTag:kBottomTag];
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
    }
    if([usingTag isEqualToString:@"productTable"]){
        [bottomImageView setFrame:CGRectMake(0, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]), 0.5)];
    } else if ([usingTag isEqualToString:@"userInfo"]){
        CGSize contentOfSize = [@"出生日期" sizeWithCalcFont:[UIFont systemFontOfCustomeSize:15.] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:[UIFont systemFontOfCustomeSize:15.]])];
        [bottomImageView setFrame:CGRectMake(contentOfSize.width + 2 * LCFloat(11), CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]), 0.5)];
    } else if ([usingTag isEqualToString:@"patientDetail"]){
        [bottomImageView setFrame:CGRectMake(LCFloat(40), CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]) - LCFloat(40), 0.5)];
    } else if ([usingTag isEqualToString:@"assetLibrary"]){
        [bottomImageView setFrame:CGRectMake(0, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]), 0.5)];
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor whiteColor] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor whiteColor] renderSize:CGSizeMake(10., 10.)]];
    }
    [self addSubview:bottomImageView];
}




@end
