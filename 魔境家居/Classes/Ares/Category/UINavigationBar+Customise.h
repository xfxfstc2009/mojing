//
//  UINavigationBar+Customise.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/2/6.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Customise)
- (void)lt_setBackgroundColor:(UIColor *)backgroundColor;
- (void)lt_setElementsAlpha:(CGFloat)alpha;
- (void)lt_setTranslationY:(CGFloat)translationY;
- (void)lt_reset;
@end
