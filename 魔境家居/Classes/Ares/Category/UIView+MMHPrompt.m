//
//  UIView+MMHPrompt.m
//  MamHao
//
//  Created by SmartMin on 15/5/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 网络失败使用view

#import "UIView+MMHPrompt.h"
#import <objc/runtime.h>

#define PADDING 5.
#define  FONTSIZE [UIFont systemFontOfSize:MMHFloat(16.)];

static char promptContainerKey;
@implementation UIView (MMHPrompt)

-(void)showPrompt:(NSString *)promptString withImage:(UIImage *)promptImage andImageAlignment:(MMHPromptImageAlignment)imageAlignment{
    UIView *containerView = objc_getAssociatedObject(self, &promptContainerKey);
   // bgView
    if (containerView){
        [containerView removeFromSuperview];
    }
    
    containerView = [[UIView alloc]initWithFrame:self.bounds];
    containerView.userInteractionEnabled = NO;
    containerView.backgroundColor = self.backgroundColor;
    
    objc_setAssociatedObject(self, &promptContainerKey, containerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    // imageView
    UIImageView *promptImageView = nil;
    if (promptImage){
        promptImageView = [[UIImageView alloc]initWithImage:promptImage];
        [containerView addSubview:promptImageView];
    }
    
    // Label
    UILabel *promptLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    promptLabel.backgroundColor = [UIColor clearColor];
    promptLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    promptLabel.text = promptString;
    promptLabel.font = [UIFont systemFontOfCustomeSize:12];
    [promptLabel sizeToFit];
    [containerView addSubview:promptLabel];

    CGSize imageSize = promptImageView.bounds.size;
    CGSize labelSize = promptLabel.bounds.size;
    
    if (!promptImage){
        promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .5f);
        if ([self isKindOfClass:[UITableView class]]){
            promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .5f - ((UITableView *) self).contentInset.bottom / 2.);
        }
    } else {
        if (imageAlignment == MMHPromptImageAlignmentLeft){
            CGFloat comboWidth = imageSize.width + labelSize.width + PADDING;
            CGFloat minX = ceilf(self.bounds.size.width *.5f - comboWidth * .5f);
            promptImageView.center = CGPointMake(minX + imageSize.width * .5f, self.center.y);
            promptLabel.center = CGPointMake(minX + comboWidth - labelSize.width * .5f, self.center.y);
        } else {
            promptImageView.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .3f);
            promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .35f + imageSize.height * .5f + labelSize.height * .5f + PADDING);
        }
    }
    [self addSubview:containerView];
}

-(void)dismissPrompt{
    UIView *containerView = objc_getAssociatedObject(self, &promptContainerKey);
    if (containerView){
        [UIView animateWithDuration:.3f animations:^{
            containerView.alpha = 0.;
        } completion:^(BOOL finished) {
            [containerView removeFromSuperview];
        }];
    }
}


@end
