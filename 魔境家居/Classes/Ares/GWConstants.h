//
//  GWConstants.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#ifndef GWConstants_h
#define GWConstants_h

#define GWREQUEST_API_HOST  @"172.16.35.23:9292"
#define GWREQUEST_API_PORT  @""

#define GWREQUEST_API_VER   @"MobileAPI"
#define APP_VER        [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleShortVersionString"]
#define BUILD_VER      [[NSBundle mainBundle].infoDictionary objectForKey:(NSString *)kCFBundleVersionKey]

// 用来记录当前用户的userDefault
#define CURRENT_USER_NAME_KEY   @"CURRENT_USER_NAME_KEY"
#define CURRENT_USER_PASS_KEY   @"CURRENT_USER_PASS_KEY"
#define CURRENT_USER_ISNOTLOGIN @"CURRENT_USER_ISNOTLOGIN"              // 判断是否登录

// 用来记录当前用户是否引导图
#define  CURRENT_USER_NEWFEALTURE @"CURRENT_USER_NEWFEALTURE"



// System Default Sets
#define USERDEFAULTS     [NSUserDefaults standardUserDefaults]
#define NOTIFICENTER     [NSNotificationCenter defaultCenter]
#define DEFAULTMANAGER   [NSFileManager defaultManager]
#define LOGINDATA        @"LoginData"
#define DeviceId         @"deviceId"

// System
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)
#define IS_IOS8_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 7.99)
#define IS_IOS9_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 8.99)


// Frame
#define kScreenBounds               [[UIScreen mainScreen] bounds]


// style
#define RGB(r, g, b)    [UIColor colorWithRed:(r)/255. green:(g)/255. blue:(b)/255. alpha:1.]
#define NAVBAR_COLOR                RGB(0, 0, 0)
#define TABBAR_TEXT_NOR_COLOR       RGB(199, 199, 199)
#define TABBAR_TEXT_HLT_COLOR       RGB(0, 209, 199)
#define BACKGROUND_VIEW_COLOR       RGB(241, 241, 246)
#define SEPARATOR_NORMAL_COLOR      @"e3e0de"
#define SEPARATOR_HIGHLIGHT_COLOR   @"e3e0de"

#define ios7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)
#define ios8 ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0)
#define ios6 ([[UIDevice currentDevice].systemVersion doubleValue] >= 6.0 && [[UIDevice currentDevice].systemVersion doubleValue] < 7.0)
#define ios5 ([[UIDevice currentDevice].systemVersion doubleValue] < 6.0)
#define iphone5  ([UIScreen mainScreen].bounds.size.height == 568)
#define iphone6  ([UIScreen mainScreen].bounds.size.height == 667)
#define iphone6Plus  ([UIScreen mainScreen].bounds.size.height == 736)
#define iphone4  ([UIScreen mainScreen].bounds.size.height == 480)
#define ipadMini2  ([UIScreen mainScreen].bounds.size.height == 1024)


#import "GWTool.h"
#import <AFNetworking.h>

#import "SBJSON.h"
#import "GWJson.h"
#import "GWImageView.h"
#import "GWUploadFileModel.h"
#import "GWFetchModel.h"
// category


#import "UIView+StringTag.h"                            // tag
#import "UITableViewCell+LCSeparatorLine.h"             // tableView 线条
#import "UIView+LCGeometry.h"

#import "UIImage+RenderedImage.h"
#import "UIColor+Extended.h"
#import "NSString+LCCalcSize.h"                     // text size
#import "UIFont+Customise.h"
#import "UIImage+ImageEffects.h"
#import "NSDate+Utilities.h"
#import "UITextField+CustomShowBar.h"
#import "UIView+MMHPrompt.h"
#import "UITextView+Customise.h"
#import "SDWebImageManager+Memory.h"
#import "UINavigationBar+Customise.h"

#import "UIActionSheet+Customise.h"                     //
#import "UIAlertView+Customise.h"
#import "UIButton+Customise.h"
#import "UINavigationBar+Awesome.h"



#define main_authLogin @""
#define main_login @""

/// 商品类型
#define designerProductTypeSave @"designerProductType/save"

// 相册保存
#define albumSave @"album/save"

#endif /* GWConstants_h */
