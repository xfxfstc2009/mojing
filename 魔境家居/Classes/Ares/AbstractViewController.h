//
//  AbstractViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@class AppDelegate;
@interface AbstractViewController : UIViewController
@property (nonatomic,copy)NSString *barMainTitle;
@property (nonatomic,copy)NSString *barSubTitle;

- (void)authorizeWithCompletionHandler:(void(^)())handler;

- (void)hidesBackButton;

- (UIButton *)leftBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock;

- (UIButton *)rightBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock;

- (void)hidesTabBarWhenPushed;

- (void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated;


@end
