//
//  GWImageView.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWImageView.h"

@implementation GWImageView

#pragma mark - Normal

-(void)uploadImageWithURL300:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock{
    // 1. 修改图片
    NSString *imgString = [NSString stringWithFormat:@"http://%@/upload/%@/%@/%@_300_300.%@",GWREQUEST_API_HOST,imageSuffix,urlString,urlString,imageSuffix];
    NSURL *imgUrl = [NSURL URLWithString:imgString];
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

-(void)uploadImageWithURL50:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock{
    // 1. 修改图片
    NSString *imgString = [NSString stringWithFormat:@"%@/upload/%@/%@/%@_50_50.%@",GWREQUEST_API_HOST,imageSuffix,urlString,urlString,imageSuffix];
    NSURL *imgUrl = [NSURL URLWithString:imgString];
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

-(void)uploadImageWithURL100:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock{
    // 1. 修改图片
    NSString *imgString = [NSString stringWithFormat:@"%@/upload/%@/%@/%@_100_100.%@",GWREQUEST_API_HOST,imageSuffix,urlString,urlString,imageSuffix];
    NSURL *imgUrl = [NSURL URLWithString:imgString];
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

-(void)uploadImageWithURL200:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock{
    // 1. 修改图片
    NSString *imgString = [NSString stringWithFormat:@"%@/upload/%@/%@/%@_200_200.%@",GWREQUEST_API_HOST,imageSuffix,urlString,urlString,imageSuffix];
    NSURL *imgUrl = [NSURL URLWithString:imgString];
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

-(void)uploadImageWithURL500:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock{
    // 1. 修改图片
    NSString *imgString = [NSString stringWithFormat:@"%@/upload/%@/%@/%@_500_500.%@",GWREQUEST_API_HOST,imageSuffix,urlString,urlString,imageSuffix];
    NSURL *imgUrl = [NSURL URLWithString:imgString];
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

-(void)uploadImageWithURL:(NSString *)urlString imageSuffix:(NSString *)imageSuffix placeholder:(UIImage *)placeholder callback:(void (^)(UIImage *))callbackBlock{
    // 1. 修改图片
    NSString *imgString = [NSString stringWithFormat:@"%@/upload/%@/%@/%@_1.%@",GWREQUEST_API_HOST,imageSuffix,urlString,urlString,imageSuffix];
    NSURL *imgUrl = [NSURL URLWithString:imgString];
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}



-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL =[NSURL URLWithString:urlString];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"empty_placeholder"];
    }
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
    
}

@end
