//
//  MJNickNameChangeCtler.m
//  魔境家居
//
//  Created by lumingliang on 16/1/2.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJNickNameChangeCtler.h"
#import "UIBarButtonItem+Extension.h"
#import "MJNetworkTool.h"
#import "MJRegularExpressionTool.h"
#import "MBProgressHUD.h"
@interface MJNickNameChangeCtler ()

@property (nonatomic, strong) UIButton *modifyBtn;
@property (nonatomic, weak) UITextField *nickNameField;
@end

@implementation MJNickNameChangeCtler

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"昵称修改"];
  //  NSLog(@"%@",NSStringFromCGRect(self.view.frame));
    // Do any additional setup after loading the view.
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"close" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.rightBarButtonItem = back;
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSString *nickName = [MJUserDefaults objectForKey:@"nickName"];
    UITextField *nickNameField = [[UITextField alloc] initWithFrame:CGRectMake(30, 64, 480, 40)];
    nickNameField.placeholder = @"请输入新昵称";
    nickNameField.borderStyle =  UITextBorderStyleRoundedRect;
    nickNameField.text = nickName;
    self.nickNameField = nickNameField;
    [self.view addSubview:self.nickNameField];
    
    self.modifyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_modifyBtn setBackgroundColor:RGB(92, 166, 195)];
    [_modifyBtn setTitle:@"确认修改" forState:UIControlStateNormal];
    [_modifyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _modifyBtn.frame = CGRectMake(30, 114, 480, 40);
    [_modifyBtn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    _modifyBtn.tag = 24;
    _modifyBtn.layer.cornerRadius = 3;
    [self.view addSubview:self.modifyBtn];
    
}

- (void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 确认修改
- (void)btnClick
{
    NSLog(@"修改昵称");
    //保存到服务器
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    
   BOOL isCorrect = [MJRegularExpressionTool validateNickName:self.nickNameField.text];
    if (isCorrect) {
       para[@"nickName"] = self.nickNameField.text;
        [net getMoJingURL:MJUpdateNickName parameters:para success:^(id obj) {
            
            [MJUserDefaults setObject:para[@"nickName"] forKey:@"nickName"];
            
            [MJUserDefaults synchronize];
            
            [MJNotificationCenter postNotificationName:@"nickNameChange" object:nil userInfo:@{@"nickName" : para[@"nickName"]}];
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:MJKeyWindow animated:YES];
             [self.modifyBtn addSubview:hud];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(@"修改成功", nil);
             hud.minSize = CGSizeMake(150, 40);
            hud.layer.cornerRadius = 5;
            hud.clipsToBounds = YES;
            [hud hide:YES afterDelay:1.5];
           
            
            dispatch_queue_t queue = dispatch_queue_create("success", DISPATCH_QUEUE_CONCURRENT);
            dispatch_barrier_async(queue, ^{
               // NSLog(@"dispatch_barrier_async");
                [NSThread sleepForTimeInterval:2.0];
                //成功发通知修改
                
            });
            dispatch_async(queue, ^{
                // [NSThread sleepForTimeInterval:2];
               // NSLog(@"dispatch_async1");
                [self dismissViewControllerAnimated:YES completion:nil];
            });

        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
