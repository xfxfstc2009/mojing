//
//  MJMoreView.m
//  魔境家居
//
//  Created by mojing on 15/10/30.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJMoreView.h"
#import "MJMoreModel.h"
#import "MJUIClassTool.h"
#import "UIImageView+WebCache.h"
#import "AppDelegate.h"
@interface MJMoreView()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIImageView *bottomView;

@end
@implementation MJMoreView


/**
 *  顶部view
 */
- (UIView *)topView
{
    if (_topView == nil) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 64)];
        AppDelegate *myDelegate = [UIApplication sharedApplication].delegate;
        if ([myDelegate.type isEqualToString:@"4"]) {
            
            _topView.backgroundColor = RGB(251, 43, 8);
        }else{
           _topView.backgroundColor = RGB(112, 181, 205);
        }
        
        UILabel *label = [MJUIClassTool createLbWithFrame:CGRectMake(_topView.frame.size.width/2-20, _topView.frame.size.height/2, 40, 20) title:@"更多" aliment:NSTextAlignmentCenter color:[UIColor whiteColor] size:17];
        UIButton *closeBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(_topView.frame.size.width-50, _topView.frame.size.height-25, 40, 15) title:@"关闭" image:nil target:self action:@selector(closeView)];
        [closeBtn setFont:[UIFont systemFontOfSize:16]];
        [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_topView addSubview:label];
        [_topView addSubview:closeBtn];
    }
    return _topView;
}
/**
 *  底部View
 */
- (UIImageView *)bottomView
{
    if (_bottomView == nil) {
        _bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(33, self.frame.size.height-self.frame.size.width+33, 200, 200)];
        NSString *userID = [MJUserDefaults objectForKey:@"userID"];
       
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/png/qr/user/%@_qr_1.png",mainURL,userID]];
        NSLog(@"%@",url);
        [_bottomView sd_setImageWithURL:url placeholderImage:nil];
    }
    return _bottomView;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.frame.size.width, self.frame.size.height-64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = NO;
        //影藏多余分割线
        UIView *foot = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        _tableView.tableFooterView = foot;
    }
    return _tableView;
}

/**
 *  当前服务View
 */
- (UIView *)serviceView
{
    if (!_serviceView) {
        _serviceView = [[UIView alloc]initWithFrame:CGRectMake(0,432,266, 70)];
        [_serviceView addSubview:self.nameLb];
        [_serviceView addSubview:self.serviceBtn];
        [_serviceView addSubview:self.serviceLb];
    }
    return _serviceView;
}

- (UILabel *)nameLb
{
    if (_nameLb == nil) {
        _nameLb = [MJUIClassTool createLbWithFrame:CGRectMake(136, 5, 158, 20) title:self.customerName aliment:NSTextAlignmentLeft color:[UIColor orangeColor] size:18];
    }
    return _nameLb;
}

- (UILabel *)serviceLb
{
    if (_serviceLb == nil) {
        _serviceLb = [MJUIClassTool createLbWithFrame:CGRectMake(10, 5, 266-80-50, 20) title:@"当前服务客户：" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
    }
    return _serviceLb;
}

- (UIButton *)serviceBtn
{
    if (_serviceBtn == nil) {
        _serviceBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(75, 30, 116, 35) title:nil image:[UIImage imageNamed:@"end_service_icon"] target:self action:@selector(endCurrentService:)];
    }
    return _serviceBtn;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.tableView];
        [self addSubview:self.topView];
        [self addSubview:self.serviceView];
        [self addSubview:self.bottomView];
        [self selectTableviewDataArr];
    }
    return self;
}

/***  需要的数据 */
- (void)selectTableviewDataArr
{
    _dataArr = [NSMutableArray array];
    AppDelegate *myDelegate = [UIApplication sharedApplication].delegate;
    if ([myDelegate.type isEqualToString:@"4"]) {
        MJMoreModel *richScan = [MJMoreModel modelWithImage:[UIImage imageNamed:@"qrCode"] title:@"扫一扫" subTitle:@""];
//        MJMoreModel *customer = [MJMoreModel modelWithImage:[UIImage imageNamed:@"customer"] title:@"我的顾客" subTitle:@""];
//        MJMoreModel *statistics = [MJMoreModel modelWithImage:[UIImage imageNamed:@"statistics"] title:@"我的统计" subTitle:@""];
//        MJMoreModel *profile = [MJMoreModel modelWithImage:[UIImage imageNamed:@"information"] title:@"我的信息" subTitle:@""];
        [_dataArr addObject:richScan];
        
    }else{
        
    MJMoreModel *richScan = [MJMoreModel modelWithImage:[UIImage imageNamed:@"qrCode"] title:@"扫一扫" subTitle:@""];
    MJMoreModel *customer = [MJMoreModel modelWithImage:[UIImage imageNamed:@"customer"] title:@"我的顾客" subTitle:@""];
    MJMoreModel *statistics = [MJMoreModel modelWithImage:[UIImage imageNamed:@"statistics"] title:@"我的统计" subTitle:@""];
    MJMoreModel *profile = [MJMoreModel modelWithImage:[UIImage imageNamed:@"information"] title:@"我的信息" subTitle:@""];
        _dataArr = @[richScan,customer,statistics,profile];
    }
}

#pragma  mark - UITableViewDataSource UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellID];
    }
    cell.textLabel.text = [self.dataArr[indexPath.row] title];
    cell.imageView.image = [(UIImageView*)self.dataArr[indexPath.row] image];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_indexPathBlock) {
        _indexPathBlock(indexPath.row);
    }
}
#pragma 关闭点击
- (void)closeView
{
    if (_close) {
        _close();
    }
}

#pragma 结束当前服务
- (void)endCurrentService:(UIButton *)button;
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"确定要结束当前服务吗？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
    [alert show];
}

- (void)setCustomerName:(NSString *)customerName
{
    _customerName = customerName;
    _nameLb.text = customerName;
}

#pragma mark - <UIAlertViewDelegate>

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        self.serviceView.hidden = YES;
        
        if (_endService) {
            _endService();
        }
    }
}
@end
