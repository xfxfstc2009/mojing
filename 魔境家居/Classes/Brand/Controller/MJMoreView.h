//
//  MJMoreView.h
//  魔境家居
//
//  Created by mojing on 15/10/30.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^closeClick)();
typedef void (^indexPathClick)(NSInteger index);
typedef void (^endCurrentServiceClick)();
@interface MJMoreView : UIView

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic,copy) closeClick close;
@property (nonatomic,copy) indexPathClick indexPathBlock;
@property (nonatomic, strong) UIButton *serviceBtn;
@property (nonatomic, strong) UILabel *serviceLb;
@property (nonatomic, strong) UILabel *nameLb;
@property (nonatomic, strong) UIView *serviceView;
@property (nonatomic,copy) endCurrentServiceClick endService;
/**
 *  当前服务客户：名称
 */
@property (nonatomic,copy) NSString *customerName;

@end
