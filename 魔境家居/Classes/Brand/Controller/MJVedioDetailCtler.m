//
//  MJVedioDetailCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/27.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJVedioDetailCtler.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIBarButtonItem+Extension.h"
@interface MJVedioDetailCtler ()

/**
 *  视频播放控制器
 */
@property (nonatomic,strong) MPMoviePlayerController *moviePlayer;
@end

@implementation MJVedioDetailCtler

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"视频简介"];
    // Do any additional setup after loading the view.
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    __weak MJVedioDetailCtler *weak = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weak.moviePlayer play];
    });
    //播放
    
    //添加通知
 //   [self addNotification];
    
    //获取缩略图
    [self thumbnailImageRequest];
}

- (void)back
{
    [_moviePlayer stop];
    _moviePlayer = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc{
    //移除所有通知监控
   // [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - 私有方法
/**
 *  取得本地文件路径
 *
 *  @return 文件路径
 */
-(NSURL *)getFileUrl{
    NSString *urlStr=[[NSBundle mainBundle] pathForResource:@"wodeqipaduiyou.mp4" ofType:nil];
    NSURL *url=[NSURL fileURLWithPath:urlStr];
    return url;
}

/**
 *  取得网络文件路径
 *
 *  @return 文件路径
 */
-(NSURL *)getNetworkUrl{
    //upload/mp4/1448276901164/1448276901164_1.mp4
   
   // NSString *urlStr=@"http://www.cs.vu.nl/~eliens/assets/media/street/code.mp4"/**/;
    NSString *urlStr =[NSString stringWithFormat:@"%@upload/%@/%@/%@_1.%@",mainURL,self.videoModel.videoSuffix,self.videoModel.videoName,_videoModel.videoName,_videoModel.videoSuffix];
    urlStr=[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url=[NSURL URLWithString:urlStr];
    return url;
}

/**
 *  创建媒体播放控制器
 *
 *  @return 媒体播放控制器
 */
-(MPMoviePlayerController *)moviePlayer{
    if (!_moviePlayer) {
        NSURL *url = [[NSURL alloc] init];
        if ([self isConnectToNet]){
          url=[self getNetworkUrl];
        }
       // NSLog(@"%@",url);
        _moviePlayer=[[MPMoviePlayerController alloc]initWithContentURL:url];
        _moviePlayer.view.frame=CGRectMake(0, 64, Screen_Width, Screen_Height-64);
        _moviePlayer.view.autoresizingMask=UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_moviePlayer.view];
        }
    
    return _moviePlayer;
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

/**
 *  获取视频缩略图
 */
-(void)thumbnailImageRequest{
    //获取13.0s、21.5s的缩略图
    [self.moviePlayer requestThumbnailImagesAtTimes:@[@13.0,@21.5] timeOption:MPMovieTimeOptionNearestKeyFrame];
}

#pragma mark - 控制器通知
/**
 *  添加通知监控媒体播放控制器状态
 */
-(void)addNotification{
    NSNotificationCenter *notificationCenter=[NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(mediaPlayerPlaybackStateChange:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:self.moviePlayer];
    [notificationCenter addObserver:self selector:@selector(mediaPlayerPlaybackFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.moviePlayer];
    [notificationCenter addObserver:self selector:@selector(mediaPlayerThumbnailRequestFinished:) name:MPMoviePlayerThumbnailImageRequestDidFinishNotification object:self.moviePlayer];
    
}

/**
 *  播放状态改变，注意播放完成时的状态是暂停
 *
 *  @param notification 通知对象
 */
-(void)mediaPlayerPlaybackStateChange:(NSNotification *)notification{
    switch (self.moviePlayer.playbackState) {
        case MPMoviePlaybackStatePlaying:
            NSLog(@"正在播放...");
            break;
        case MPMoviePlaybackStatePaused:
            NSLog(@"暂停播放.");
            break;
        case MPMoviePlaybackStateStopped:
            NSLog(@"停止播放.");
            break;
        default:
            NSLog(@"播放状态:%li",self.moviePlayer.playbackState);
            break;
    }
}

/**
 *  播放完成
 *
 *  @param notification 通知对象
 */
-(void)mediaPlayerPlaybackFinished:(NSNotification *)notification{
    NSLog(@"播放完成.%li",self.moviePlayer.playbackState);
}

/**
 *  缩略图请求完成,此方法每次截图成功都会调用一次
 *
 *  @param notification 通知对象
 */
-(void)mediaPlayerThumbnailRequestFinished:(NSNotification *)notification{
    NSLog(@"视频截图完成.");
    UIImage *image=notification.userInfo[MPMoviePlayerThumbnailImageKey];
    //保存图片到相册(首次调用会请求用户获得访问相册权限)
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
}


@end
