//
//  MJViewController.m
//  魔境家居
//
//  Created by mojing on 15/10/27.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJViewController.h"
#import <UIKit/UIKitDefines.h>
#import "Masonry.h"
#import "MJStoreCtler.h"
#import "MJMatchCtler.h"
#import "MJCustomerCtler.h"
#import "MJStatisticsCtler.h"
#import "MJConmon.h"
#import "MJBrandDetailCtler.h"
#import "MJCaseDetailCtler.h"
#import "MJDiditalGalleryCtler.h"
#import "MJVedioDetailCtler.h"
#import "Masonry.h"
#import "UIBarButtonItem+Extension.h"
#import "UIView+Frame.h"
#import "MJDirect.h"
#import "MJCaseDetailCtler.h"
#import "MWPhotoBrowser.h"
#import "MJMineView.h"
#import "MJRichScanCtler.h"
#import "MJCustomerCtler.h"
#import "MJStatisticsCtler.h"
#import "MJMineCtler.h"
#import "MJChatCtler.h"
#import "MJMineCtler.h"
#import "UIImageView+WebCache.h"
#import "MJUIClassTool.h"
#import "MJNetworkTool.h"
#import "AppDelegate.h"
#import "MJBrandDetailModel.h"
#import "MJExtension.h"
#import "MJBrandDetailSpreadModel.h"
#import "MJBrandDetailCaseModel.h"
#import "PhotoTrackController.h"
#import "GMDCircleLoader.h"
#import "ImageUpload.h"


//宽度比例
#define kWidthRatio 1294/2048
//高度比例
#define kHeightRatio 415/768
//更多view宽度
#define kMoreViewWidth 266
#define kCountDidChangeNofication @"kCountDidChangeNofication"
#define kMessageRefresh @"kMessageRefresh"

@interface MJViewController () <UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,MWPhotoBrowserDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *showScrollViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *showScrollViewWidth;

@property (weak, nonatomic) IBOutlet UIImageView *vedioImageView;
@property (weak, nonatomic) IBOutlet UIImageView *digitalGalleryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *brandImageView;
@property (weak, nonatomic) IBOutlet UIImageView *caseImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *showScrollView;

/** 商城item */
@property (nonatomic, strong) UIBarButtonItem *storeItem;
/** 搭配item */
@property (nonatomic, strong) UIBarButtonItem *matchItem;

//
//#define kNoteViewWidth 984
//#define kNoteViewHeight 728
////弹出时间
//#define kNoteViewShowTime 0.5

/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/** 循环图片数组*/
@property (nonatomic, strong) NSMutableArray *albumDataArr;
/** 热销信息模型数组*/
@property (nonatomic, strong) NSMutableArray *spreadDataListArr;
/** 案例图片数组*/
@property (nonatomic, strong) NSMutableArray *casePhotoArr;
/** 完整信息模型*/
@property (nonatomic, strong) MJBrandDetailDataModel *dataModel;
/** 360全景模型*/
@property (nonatomic, strong) MJBrandDetailPanoramaModel *panoramaModel;
/** 视频信息模型*/
@property (nonatomic, strong) MJBrandDetailVideoModel *videoModel;
@property (nonatomic, copy) NSURL *videoUrl;
/** 品牌所以信息模型*/
@property (nonatomic, strong) MJBrandDetailDataModel *detailDataModel;

/** 图片下标*/
@property (nonatomic, assign) NSInteger caseImageIndex;
/**
 *  案例图片数组
 */
@property (nonatomic, strong) MWPhotoBrowser *browser;
@end

@implementation MJViewController

- (NSMutableArray *)albumDataArr
{
    if (_albumDataArr == nil) {
        _albumDataArr = [NSMutableArray array];
    }
    return _albumDataArr;
}

- (NSMutableArray *)spreadDataListArr
{
    if (_spreadDataListArr == nil) {
        _spreadDataListArr = [NSMutableArray array];
    }
    return _spreadDataListArr;
}

- (NSMutableArray *)casePhotoArr
{
    if (_casePhotoArr == nil) {
        _casePhotoArr = [NSMutableArray array];
    }
    return _casePhotoArr;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initUI];
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.view.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_circulateBackgroungView];
    
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    [self.circulateBackgroungView removeFromSuperview];
    [GMDCircleLoader hideFromView:self.view animated:YES];
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - item初始化
- (void)initUI
{
    
    [self setUpLeftItems];
    [self getDataForImageView];
  
    [MJNotificationCenter addObserver:self selector:@selector(mjVcHiddenDidChange:) name:@"mjStartService" object:nil];
    
}



#pragma mark - 隐藏服务客户
-(void)mjVcHiddenDidChange:(NSNotification *)notification
{
    BOOL hidden = [notification.userInfo[@"mjVcHidden"] boolValue];
     self.moreView.serviceView.hidden = hidden;
    
}

#pragma mark - 获取数据设置图片
- (void)getDataForImageView
{
    //获取品牌详情
    AppDelegate *myDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [self loadingData];
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    if ([self isConnectToNet]) {
        
    [net getMoJingURL:MJBrandDetail parameters:myDelegate.brandIDParaDict success:^(id obj) {
        MJBrandDetailModel *detailModel = [MJBrandDetailModel mj_objectWithKeyValues:obj];
        
        //完整信息模型
        MJBrandDetailDataModel *dataModel = [MJBrandDetailDataModel mj_objectWithKeyValues:detailModel.data];
        self.dataModel = dataModel;
        AppDelegate *myDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        //品牌logo
        NSString *brandName = [MJUserDefaults objectForKey:@"brandName"];
        NSString *shopName = myDelegate.shopName;
        NSString *type = myDelegate.type;
        if ([type isEqualToString:@"2"]||[type isEqualToString:@"3"]) {
            //经销商
            UILabel *titleLb = [MJUIClassTool createLbWithFrame:CGRectMake(0, 7, 1000, 44) title:[NSString stringWithFormat:@"%@･%@",brandName,shopName] aliment:NSTextAlignmentCenter color:[UIColor blackColor] size:25];
            CGSize size = [MJUIClassTool caculateText:brandName fontSize:25 maxSize:CGSizeMake(1000, 44)];
            titleLb.size = size;
            self.navigationItem.titleView = titleLb;
        }
        else
        {
            UILabel *titleLb = [MJUIClassTool createLbWithFrame:CGRectMake(0, 7, 1000, 44) title:[NSString stringWithFormat:@"%@",brandName] aliment:NSTextAlignmentCenter color:[UIColor blackColor] size:25];
            CGSize size = [MJUIClassTool caculateText:brandName fontSize:25 maxSize:CGSizeMake(1000, 44)];
            titleLb.size = size;
            self.navigationItem.titleView = titleLb;
        }
        
        
        
        
        //案例图片数组
        NSDictionary *caseDict = obj[@"data"];
        NSArray *caseArr = [caseDict objectForKey:@"caseData"];
        NSMutableArray *caseDataArr = [NSMutableArray array];
        caseDataArr = [MJBrandDetailCaseModel mj_objectArrayWithKeyValuesArray:caseArr];
        NSMutableArray *tempCaseArr = [NSMutableArray array];
        int i = 0;
        for (MJBrandDetailCaseModel *model in caseDataArr) {
            
            NSString *urlString =[NSString stringWithFormat:@"%@upload/%@/%@/%@_1.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix];
            NSURL *url = [NSURL URLWithString:urlString];
            MWPhoto *photo = [MWPhoto photoWithURL:url];
            
            [myDelegate.casePhotoArr addObject:photo];
            i++;
        }
        
        for (MJBrandDetailCaseModel *model in caseDataArr) {
            NSString *urlString =[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix];
            NSURL *url = [NSURL URLWithString:urlString];
            [tempCaseArr addObject:url];
        }

        if (tempCaseArr.count > 0) {
            __weak MJViewController *ctl = self;
            [self.caseImageView sd_setImageWithURL:tempCaseArr[0] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [ctl stopCircleLoader];
            }];
        }
        
        //360全景
        self.panoramaModel = [MJBrandDetailPanoramaModel mj_objectWithKeyValues:dataModel.panoramaData];
        
        //热销信息模型数组
        MJBrandDetailSpreadModel *spreadModel = [MJBrandDetailSpreadModel mj_objectWithKeyValues:dataModel.spreadData];
        self.spreadDataListArr = [MJBrandDetailSpreadDataModel mj_objectArrayWithKeyValuesArray:spreadModel.dataList];
        myDelegate.hotListParaArr = self.spreadDataListArr;
        for (MJBrandDetailSpreadDataModel *m in myDelegate.hotListParaArr) {
            NSDictionary *tmpDict = m.mj_keyValues;
            [myDelegate.hotUploadParaArr addObject:tmpDict];
        }
        
        //视频信息
        self.videoModel = [MJBrandDetailVideoModel mj_objectWithKeyValues:dataModel.videoData];
        
        //设置图片
            self.brandImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.brandImageView.clipsToBounds = YES;
            self.digitalGalleryImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.digitalGalleryImageView.clipsToBounds = YES;
            self.caseImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.caseImageView.clipsToBounds = YES;
            self.vedioImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.vedioImageView.clipsToBounds = YES;
         
            //品牌
             NSURL *brandUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,_dataModel.introduceImageSuffix,_dataModel.introduceImageName,_dataModel.introduceImageName,_dataModel.introduceImageSuffix]];
        
            //360全景数字展厅
           NSURL *digitalUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,self.panoramaModel.imageSuffix,_panoramaModel.imageName,_panoramaModel.imageName,_panoramaModel.imageSuffix]];
        
            //视频
           NSURL *videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,self.videoModel.imageSuffix,_videoModel.imageName,_videoModel.imageName,_videoModel.imageSuffix]];
        
               
            [self.brandImageView sd_setImageWithURL:brandUrl placeholderImage:[UIImage imageNamed:@"home_brand"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [self stopCircleLoader];
            }];
        
        
        UIImageView *digitalGalleryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300, 303)];
        digitalGalleryImageView.contentMode = UIViewContentModeScaleAspectFill;
        digitalGalleryImageView.clipsToBounds = YES;
       // [digitalGalleryImageView sd_setImageWithURL:digitalUrl placeholderImage:[UIImage imageNamed:@"home_digital"]];
        [digitalGalleryImageView sd_setImageWithURL:digitalUrl completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [self stopCircleLoader];
        }];
        [self.digitalGalleryImageView addSubview:digitalGalleryImageView];
        
        UIImageView *vedioImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 423, 351)];
        vedioImageView.contentMode = UIViewContentModeScaleAspectFill;
        vedioImageView.clipsToBounds = YES;
       // NSLog(@"%@",videoUrl);
      //  [vedioImageView sd_setImageWithURL:videoUrl placeholderImage:[UIImage imageNamed:@"home_video"]];
        [vedioImageView sd_setImageWithURL:videoUrl completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [self stopCircleLoader];
        }];
        [self.vedioImageView addSubview:vedioImageView];
        
        //循环相册
        NSDictionary *productDict = obj[@"data"];
        NSArray *albumArr = [productDict objectForKey:@"albumData"];
        self.albumDataArr = [MJBrandDetailAlbumModel mj_objectArrayWithKeyValuesArray:albumArr];
        
        NSMutableArray *imgArr = [NSMutableArray array];
        for (MJBrandDetailAlbumModel *model in self.albumDataArr) {
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
            NSData *imgData = [NSData dataWithContentsOfURL:url];
            
            UIImage *img =  [UIImage imageWithData:imgData];
            if (img) {
                 [imgArr addObject:img];
                [self stopCircleLoader];
            }
        }
        
        //循环滚动
        if (imgArr.count > 0) {
            MJDirect *direct = [MJDirect direcWithtFrame:CGRectMake(0, 0, _showScrollView.frame.size.width, _showScrollView.frame.size.height) ImageArr:imgArr AndImageClickBlock:^(NSInteger index) {
            }];
            [self.showScrollView addSubview:direct];
        }else
        {
            UIImage *image = [UIImage imageNamed:@"home_banner"];
            MJDirect *direct = [MJDirect direcWithtFrame:CGRectMake(0, 0, _showScrollView.frame.size.width, _showScrollView.frame.size.height) ImageArr:@[image,image] AndImageClickBlock:^(NSInteger index) {
            }];
            [self.showScrollView addSubview:direct];
        }
        
       // [self stopCircleLoader];
        
            UITapGestureRecognizer *brandTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(brandTap)];
            [self.brandImageView addGestureRecognizer:brandTap];
        
            UITapGestureRecognizer *caseTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(caseTap)];
            [self.caseImageView addGestureRecognizer:caseTap];
       
       
            UITapGestureRecognizer *digitalGalleryTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(digitalGalleryTap)];
            [self.digitalGalleryImageView addGestureRecognizer:digitalGalleryTap];
       
            UITapGestureRecognizer *vedioTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(vedioTap)];
            [self.vedioImageView addGestureRecognizer:vedioTap];
        
       
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
  }
}

- (void)setUpLeftItems
{
    self.storeItem = [UIBarButtonItem itemWithTarget:self action:@selector(storeClick) image:@"item_store" highImage:@""];
    _storeItem.customView.height = 44;
    _storeItem.customView.width = 60;
    
    UIImageView *lineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
    lineView.image = [UIImage imageNamed:@"home_head_line"];
    UIBarButtonItem *lineItem = [[UIBarButtonItem alloc] initWithCustomView:lineView];
    
    self.matchItem = [UIBarButtonItem itemWithTarget:self action:@selector(matchClick) image:@"item_match" highImage:@""];
    _matchItem.customView.height = 44;
    _matchItem.customView.width = 60;
    
    UIBarButtonItem *spaceItem = [UIBarButtonItem itemWithTarget:nil action:nil image:@"" highImage:@""];
    spaceItem.customView.width = 10;
    spaceItem.customView.height = 35;
    
    self.navigationItem.leftBarButtonItems = @[self.storeItem,spaceItem,lineItem,spaceItem,self.matchItem];
}

#pragma mark - 顶部item点击
- (void)storeClick
{
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    MJStoreCtler *store = [[MJStoreCtler alloc] init];
    [self.navigationController pushViewController:store animated:YES];
    
}

- (void)matchClick
{
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    self.replaceBarFrame = CGRectMake(0, 0, Screen_Width, 64);
    MJMatchCtler *macth = [[MJMatchCtler alloc] init];
    [self.navigationController pushViewController:macth animated:YES];
}

#pragma mark - 图片手势点击
#pragma mark - 案例详情
- (void)setUpCaseMWPhotoBrowser
{
    BOOL displayActionButton = NO;
    BOOL displaySelectionButtons = NO;
    BOOL displayNavArrows = YES;
    BOOL enableGrid = YES;
    BOOL startOnGrid = NO;
    
    // Create browser
    self.browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    _browser.displayActionButton = displayActionButton;//分享按钮,默认是
    _browser.displayNavArrows = displayNavArrows;//左右分页切换,默认否
    _browser.displaySelectionButtons = displaySelectionButtons;//是否显示选择按钮在图片上,默认否
    _browser.alwaysShowControls = displaySelectionButtons;//控制条件控件 是否显示,默认否
    _browser.zoomPhotosToFill = NO;//是否全屏,默认是
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    browser.wantsFullScreenLayout = YES;//是否全屏
#endif
    _browser.enableGrid = enableGrid;//是否允许用网格查看所有图片,默认是
    _browser.startOnGrid = startOnGrid;//是否第一张,默认否
    _browser.enableSwipeToDismiss = YES;
    [_browser showNextPhotoAnimated:YES];
    [_browser showPreviousPhotoAnimated:YES];
    [_browser setCurrentPhotoIndex:0];
    
    [self.navigationController pushViewController:self.browser animated:YES];
    
}

- (void)caseTap
{
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    //    MJCaseDetailCtler *caseVc = [[MJCaseDetailCtler alloc] init];
    //    caseVc.casePhotoArr = self.casePhotoArr;
    //    NSLog(@"%ld",caseVc.casePhotoArr.count);
    //    [self.navigationController pushViewController:caseVc animated:YES];
    
    [self setUpCaseMWPhotoBrowser];
}

- (void)brandTap
{
    // NSLog(@"brandTap");
    MJBrandDetailCtler *brandDetail = [[MJBrandDetailCtler alloc] init];
    brandDetail.dataModel = self.dataModel;
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    [self.navigationController pushViewController:brandDetail animated:YES];
}

- (void)digitalGalleryTap
{
    MJDiditalGalleryCtler *digital = [[MJDiditalGalleryCtler alloc] init];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    digital.model = self.panoramaModel;
    [self.navigationController pushViewController:digital animated:YES ];
}

- (void)vedioTap
{
    //NSLog(@"vedioTap");
    MJVedioDetailCtler *video = [[MJVedioDetailCtler alloc] init];
    video.videoModel = self.videoModel;
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    [self.navigationController pushViewController:video animated:YES];
}

#pragma mark - 遮盖点击
- (void)coverClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.navigationController.navigationBar.frame =  CGRectMake(0, 0, Screen_Width, 64);
        [self.moreView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        [self.mineView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        [self.coverView removeFromSuperview];
    }completion:^(BOOL finished) {
        self.replaceBarFrame = CGRectMake(0, 0, Screen_Width, 64);
        self.cameraItem.enabled = YES;
        self.chatItem.enabled = YES;
    }];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    
    AppDelegate *myDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
      NSLog(@"%ld",myDelegate.casePhotoArr.count);
    return myDelegate.casePhotoArr.count;
}

- (id)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    AppDelegate *myDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (index < myDelegate.casePhotoArr.count) {
        return [myDelegate.casePhotoArr objectAtIndex:index];
    }
    //    if (index < self.photos.count)
    //        return [self.photos objectAtIndex:index];
    return nil;
}

@end
