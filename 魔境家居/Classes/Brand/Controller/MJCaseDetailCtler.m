//
//  MJCaseDetailCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/27.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCaseDetailCtler.h"
#import "MJBrandDetailCaseModel.h"

@interface MJCaseDetailCtler()
#define IMAGEWIDTH 240
#define IMAGEHEIGHT 320
@property (nonatomic, strong) NSMutableArray *photos;

@end

@implementation MJCaseDetailCtler

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor blackColor];
    
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
 
}

#pragma mark - 返回
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
    ;
}
@end
