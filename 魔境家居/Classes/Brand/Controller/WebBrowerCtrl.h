//
//  WebBrowerCtrl.h
//  HuaTuo
//
//  Created by 施正士 on 15/7/11.
//  Copyright (c) 2015年 HuaTuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebBrowerCtrl : UIViewController<UIWebViewDelegate>


- (id)initWithWebURL:(NSString*)url;

@end
