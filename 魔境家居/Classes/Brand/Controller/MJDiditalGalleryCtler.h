//
//  MJDiditalGalleryCtler.h
//  魔境家居
//
//  Created by mojing on 15/10/28.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJBaseCtler.h"
#import "MJBrandDetailPanoramaModel.h"
@interface MJDiditalGalleryCtler : MJBaseCtler

@property (nonatomic,strong) MJBrandDetailPanoramaModel *model;
@end
