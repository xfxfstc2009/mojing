//
//  MJDiditalGalleryCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/28.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJDiditalGalleryCtler.h"
#import "MJNetworkTool.h"
@interface MJDiditalGalleryCtler ()

@property (nonatomic, strong) UIWebView *webView;
@end

@implementation MJDiditalGalleryCtler
- (UIWebView *)webView
{
    if (_webView == nil) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, Screen_Width, Screen_Height-64)];
        self.title = @"数字展馆";
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSURL *url = [NSURL URLWithString:self.model.panoramaURL];
          //  NSURL *url = [NSURL URLWithString:@"http://121.199.41.198/lacasapc/"];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self isConnectToNet]) {
                    //加载一个请求
                    [_webView loadRequest:request];
                }
            });
        });

    }
    return _webView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [self.view addSubview:self.webView];
    
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 返回
- (void)back
{
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.webView removeFromSuperview];
    self.webView = nil;
}
@end
