//
//  MJViewController.m
//  魔境家居
//
//  Created by mojing on 15/10/27.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJViewController.h"
#import <UIKit/UIKitDefines.h>
#import "Masonry.h"
#import "MJStoreCtler.h"
#import "MJMatchCtler.h"
#import "MJCustomerCtler.h"
#import "MJStatisticsCtler.h"
#import "MJConmon.h"
#import "MJBrandDetailCtler.h"
#import "MJCaseDetailCtler.h"
#import "MJDiditalGalleryCtler.h"
#import "MJVedioDetailCtler.h"
#import "Masonry.h"
#import "UIBarButtonItem+Extension.h"
#import "UIView+Frame.h"
#import "MJDirect.h"
#import "MJCaseDetailCtler.h"
#import "MWPhotoBrowser.h"
#import "MJMineView.h"
#import "MJRichScanCtler.h"
#import "MJCustomerCtler.h"
#import "MJStatisticsCtler.h"
#import "MJMineCtler.h"
#import "MJChatCtler.h"
#import "MJMineCtler.h"
#import "UIImageView+WebCache.h"
#import "MJUIClassTool.h"
#import "MJNetworkTool.h"
#import "AppDelegate.h"
#import "MJBrandDetailModel.h"
#import "MJExtension.h"
#import "MJBrandDetailSpreadModel.h"
#import "MJBrandDetailCaseModel.h"
#import "PhotoTrackController.h"
#import "GMDCircleLoader.h"
#import "ImageUpload.h"

#import "MJChatView.h"
#import "APService.h"
#import "MJDBManager.h"
#import "UIBarButtonItem+MJBadge.h"

#import "MJLoginCtler.h"
//宽度比例
#define kWidthRatio 1294/2048
//高度比例
#define kHeightRatio 415/768
//更多view宽度
#define kMoreViewWidth 266
#define kCountDidChangeNofication @"kCountDidChangeNofication"
#define kMessageRefresh @"kMessageRefresh"

@interface MJViewController () <MWPhotoBrowserDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *showScrollViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *showScrollViewWidth;

@property (weak, nonatomic) IBOutlet UIImageView *vedioImageView;
@property (weak, nonatomic) IBOutlet UIImageView *digitalGalleryImageView;
@property (weak, nonatomic) IBOutlet UIImageView *brandImageView;
@property (weak, nonatomic) IBOutlet UIImageView *caseImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *showScrollView;

/** 商城item */
@property (nonatomic, strong) UIBarButtonItem *storeItem;
/** 搭配item */
@property (nonatomic, strong) UIBarButtonItem *matchItem;
/**
 *  案例图片数组
 */
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) MWPhotoBrowser *browser;

/**
 *  遮盖view
 */
@property (nonatomic, strong) UIButton *coverView;
/**
 *  我的view
 */
@property (nonatomic, strong) MJMineView *mineView;

/** 聊天view*/
@property (nonatomic,strong) MJChatView *noteView;
#define kNoteViewWidth 984
#define kNoteViewHeight 728
//弹出时间
#define kNoteViewShowTime 0.5

/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/** 循环图片数组*/
@property (nonatomic, strong) NSMutableArray *albumDataArr;
/** 热销信息模型数组*/
@property (nonatomic, strong) NSMutableArray *spreadDataListArr;
/** 案例图片数组*/
@property (nonatomic, strong) NSMutableArray *casePhotoArr;
/** 完整信息模型*/
@property (nonatomic, strong) MJBrandDetailDataModel *dataModel;
/** 360全景模型*/
@property (nonatomic, strong) MJBrandDetailPanoramaModel *panoramaModel;
/** 视频信息模型*/
@property (nonatomic, strong) MJBrandDetailVideoModel *videoModel;
@property (nonatomic, copy) NSURL *videoUrl;
/** 品牌所以信息模型*/
@property (nonatomic, strong) MJBrandDetailDataModel *detailDataModel;
@end

@implementation MJViewController

{
    CGRect _replaceBarFrame;//保存上一次navgationBar的frame
}


- (NSMutableArray *)albumDataArr
{
    if (_albumDataArr == nil) {
        _albumDataArr = [NSMutableArray array];
    }
    return _albumDataArr;
}

- (NSMutableArray *)spreadDataListArr
{
    if (_spreadDataListArr == nil) {
        _spreadDataListArr = [NSMutableArray array];
    }
    return _spreadDataListArr;
}

- (NSMutableArray *)casePhotoArr
{
    if (_casePhotoArr == nil) {
        _casePhotoArr = [NSMutableArray array];
    }
    return _casePhotoArr;
}

- (MJMineView *)mineView
{
    if (_mineView == nil) {
        _mineView = [[MJMineView alloc] initWithFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
       
        __weak MJViewController *ctl = self;
        
        //返回
        _mineView.close = ^{
            [UIView animateWithDuration:0.25 animations:^{
                
                [ctl.mineView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth,Screen_Height)];
                ctl.navigationController.navigationBar.frame = CGRectMake(-kMoreViewWidth, 0, Screen_Width, 64);
                
            } completion:^(BOOL finished) {
            }];
            
        };
        _mineView.indexPathBlock = ^(NSInteger index){
            switch (index) {
                case 0:
                {
                    /**
                     更换头像                     
                     - returns: 提示框
                     */
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"更换头像" message:@"请选择" delegate:ctl cancelButtonTitle:@"取消" otherButtonTitles:@"图片库", @"照相",nil];
                    [alert show];
                    
                    break;
                }
                case 3:
                {
                    MJMineCtler *mine = [[MJMineCtler alloc] init];
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mine];
                    nav.modalPresentationStyle =UIModalPresentationFormSheet;
                    [ctl presentViewController:nav animated:YES completion:nil];
                    break;
                }
    
                default:
                    break;
            }
        };
        _mineView.loginOff = ^{
            //注销登录
            MJNetworkTool *net = [MJNetworkTool shareInstance];
            [net getMoJingURL:MJUserLogoff parameters:nil success:^(id obj) {
                
                
            } failure:^(NSError *error) {
                
            }];
            ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
            [ctl.moreView removeFromSuperview];
            [ctl.mineView removeFromSuperview];
            [ctl.coverView removeFromSuperview];
            [ctl.navigationController popToRootViewControllerAnimated:YES];
        };
    }
    return _mineView;
}

- (UIButton *)coverView
{
    if (!_coverView) {
        _coverView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Screen_Width/*-kMoreViewWidth*/, Screen_Height)];
        _coverView.backgroundColor = [UIColor grayColor];
        _coverView.alpha = 0.5;
        [_coverView addTarget:self action:@selector(coverClick) forControlEvents:UIControlEventTouchUpInside];
        // [self.view addSubview:_coverView];
    }
    return _coverView;
}

- (MJMoreView *)moreView
{
    if (!_moreView) {
        _moreView = [[MJMoreView alloc] initWithFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        __weak MJViewController *ctl = self;
        
        MJRichScanCtler *richScan = [[MJRichScanCtler alloc] init];
        UINavigationController *richScanNav = [[UINavigationController alloc] initWithRootViewController:richScan];
        
        MJCustomerCtler *customer = [[MJCustomerCtler alloc] init];
        
        MJStatisticsCtler *statistics = [[MJStatisticsCtler alloc] init];
        
        _moreView.close = ^{
            //  NSLog(@"关闭点击");
            [UIView animateWithDuration:0.25 animations:^{
                ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
                [ctl.moreView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth,Screen_Height)];
                [ctl.coverView removeFromSuperview];
            } completion:^(BOOL finished) {
                ctl.view.frame = CGRectMake(0, 0, Screen_Width, Screen_Height);
                ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
                _replaceBarFrame = CGRectMake(0, 0, Screen_Width, 64);
            }];
        };
        _moreView.indexPathBlock = ^(NSInteger index){
            switch (index) {
                case 0:
                {//扫一扫
                    _replaceBarFrame = CGRectMake(-kMoreViewWidth, 0, Screen_Width, 64);
                    [ctl presentModalViewController:richScanNav animated:YES];
                    break;
                }
                case 1:
                {//我的客户
                    [ctl.navigationController pushViewController:customer animated:YES];
                    ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
                    break;
                }
                case 2:
                {//我的统计
                    [ctl.navigationController pushViewController:statistics animated:YES];
                    ctl.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
                    break;
                }
                case 3:
                {//我的信息
                    
                    [UIView animateWithDuration:0.25 animations:^{
                        
                        ctl.coverView.frame = CGRectMake(-kMoreViewWidth, 0, Screen_Width, Screen_Height);
                        
                        ctl.mineView.frame = CGRectMake(Screen_Width-kMoreViewWidth, 0, kMoreViewWidth, Screen_Height);
                        
                        [ctl.moreView bringSubviewToFront:ctl.mineView];
                    }completion:^(BOOL finished) {
                        
                        ctl.navigationController.navigationBar.frame=CGRectMake(-kMoreViewWidth, 0, Screen_Width, 64);
                        [ctl.view addSubview:ctl.coverView];
                        // [ctl.moreView addSubview:ctl.mineView];
                        
                    }];
                    
                    break;
                }
                    
                default:
                    break;
            }
        };
       
        _moreView.serviceView.hidden = YES;
        _moreView.endService = ^{
          //  NSLog(@"结束当前服务");
            
            //保存离店信息
            NSString *browseID = [MJUserDefaults objectForKey:@"browseID"];
            NSDictionary *endDict = @{@"type" : @2, @"browseID" :browseID};
            MJNetworkTool *net = [MJNetworkTool shareInstance];
            [net getMoJingURL:MJBrowseShopSave parameters:endDict success:^(id obj) {
                NSLog(@"%@",endDict);
                //发通知客户列表界面刷新数据
                [MJNotificationCenter postNotificationName:@"customerListReloadData" object:nil];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        };
    }
    return _moreView;
}

//聊天界面
- (UIView *)noteView
{
    if (_noteView == nil) {
        _noteView =[MJChatView shareInstance];
        _noteView.currentIndex=0;
        _noteView.backgroundColor = [UIColor blackColor];
        _noteView.tag =200;
        
        __block MJViewController *ctl = self;
        
        _noteView.close= ^{
            [UIView animateWithDuration:kNoteViewShowTime animations:^{
                [ctl.noteView setFrame:CGRectMake(20, Screen_Height, kNoteViewWidth, kNoteViewHeight)];
            }completion:^(BOOL finished) {
                [ctl.noteView removeFromSuperview];

            }];
            
        };
        
    }
    
    return _noteView;
}


- (NSMutableArray *)photos
{
    if (_photos == nil) {
        _photos = [NSMutableArray array];
       
    }
    return _photos;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.photos removeAllObjects];
    self.navigationController.navigationBar.frame = _replaceBarFrame;;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(countDidChangeAction) name:kCountDidChangeNofication object:nil];
   // [self getBrandDetails];
    [self initUI];
    
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.view.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_circulateBackgroungView];
    
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    [self.circulateBackgroungView removeFromSuperview];
    [GMDCircleLoader hideFromView:self.view animated:YES];
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - item初始化
- (void)initUI
{
    NSString *brandName = [MJUserDefaults objectForKey:@"brandName"];
    UILabel *titleLb = [MJUIClassTool createLbWithFrame:CGRectMake(0, 0, 200, 44) title: brandName aliment:NSTextAlignmentCenter color:[UIColor blackColor] size:20];
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake( 0, 5, 100, 34)];
    imgV.image = [UIImage imageNamed:@"logo"];
    self.navigationItem.titleView.frame = CGRectMake(Screen_Width/2-50, 20, 100, 44);
    self.navigationItem.titleView = titleLb;
    _replaceBarFrame = CGRectMake(0, 0, Screen_Width,64);
    [self setUpLeftItems];
    [self setUpRightItems];
    [self getDataForImageView];
    
    [MJNotificationCenter addObserver:self selector:@selector(hiddenDidChange:) name:@"startService" object:nil];
    
    [self.view addSubview:self.moreView];
    [self.view addSubview:self.mineView];
    
  
}

-(void)hiddenDidChange:(NSNotification *)notification
{
    NSString *name = [MJUserDefaults objectForKey:@"customerName"];
    NSLog(@"%@",name);
    self.moreView.customerName = name;
    self.moreView.serviceView.hidden = [notification.userInfo[@"serviceHidden"] boolValue];
}

#pragma mark - 获取数据设置图片
- (void)getDataForImageView
{
    //获取品牌详情
    AppDelegate *myDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [self loadingData];
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    if ([self isConnectToNet]) {
        
    
    [net getMoJingURL:MJBrandDetail parameters:myDelegate.brandIDParaDict success:^(id obj) {
        MJBrandDetailModel *detailModel = [MJBrandDetailModel mj_objectWithKeyValues:obj];
        
        //完整信息模型
        MJBrandDetailDataModel *dataModel = [MJBrandDetailDataModel mj_objectWithKeyValues:detailModel.data];
        self.dataModel = dataModel;
        
        //案例图片数组
        NSDictionary *caseDict = obj[@"data"];
        NSArray *caseArr = [caseDict objectForKey:@"caseData"];
        NSMutableArray *caseDataArr = [NSMutableArray array];
        caseDataArr = [MJBrandDetailCaseModel mj_objectArrayWithKeyValuesArray:caseArr];
        self.casePhotoArr = caseDataArr;
        
        for (MJBrandDetailCaseModel *model in self.casePhotoArr) {
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_1.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
            NSData *imgData = [NSData dataWithContentsOfURL:url];
            
            MWPhoto *photo = [MWPhoto photoWithImage: [UIImage imageWithData:imgData]];
            
            AppDelegate *myDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
           // myDelegate.hotListParaArr = self.photos;
            
            [myDelegate.casePhotoArr addObject:photo];
        }
        
        //360全景
        self.panoramaModel = [MJBrandDetailPanoramaModel mj_objectWithKeyValues:dataModel.panoramaData];
        
        //热销信息模型数组
        MJBrandDetailSpreadModel *spreadModel = [MJBrandDetailSpreadModel mj_objectWithKeyValues:dataModel.spreadData];
        self.spreadDataListArr = [MJBrandDetailSpreadDataModel mj_objectArrayWithKeyValuesArray:spreadModel.dataList];
        myDelegate.hotListParaArr = self.spreadDataListArr;
        for (MJBrandDetailSpreadDataModel *m in myDelegate.hotListParaArr) {
            NSDictionary *tmpDict = m.mj_keyValues;
            [myDelegate.hotUploadParaArr addObject:tmpDict];
        }
        
        //视频信息
        self.videoModel = [MJBrandDetailVideoModel mj_objectWithKeyValues:dataModel.videoData];
        NSDictionary *videoDict = self.videoModel.mj_keyValues;
        NSURL *videoUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_1.%@",mainURL,_videoModel.videoSuffix,_videoModel.videoName,_videoModel.videoName,_videoModel.videoSuffix]];
        
        
        
        //设置图片
      //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            self.brandImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.brandImageView.clipsToBounds = YES;
            self.digitalGalleryImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.digitalGalleryImageView.clipsToBounds = YES;
            self.caseImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.caseImageView.clipsToBounds = YES;
            self.vedioImageView.contentMode = UIViewContentModeScaleAspectFill;
            self.vedioImageView.clipsToBounds = YES;
         
            //品牌
             NSURL *brandUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,_dataModel.introduceImageSuffix,_dataModel.introduceImageName,_dataModel.introduceImageName,_dataModel.introduceImageSuffix]];
            //案例
            NSMutableArray *tempCaseArr = [NSMutableArray array];
            for (MJBrandDetailCaseModel *model in self.casePhotoArr) {
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix] ];
                
                NSData *imgData = [NSData dataWithContentsOfURL:url];
                if (imgData) {
                    UIImage *img = [UIImage imageWithData:imgData];
                    [tempCaseArr addObject:img];
                }
            }
            //360全景数字展厅
            NSURL *digitalUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,self.panoramaModel.imageSuffix,_panoramaModel.imageName,_panoramaModel.imageName,_panoramaModel.imageSuffix]];
            //视频
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,self.videoModel.imageSuffix,_videoModel.imageName,_videoModel.imageName,_videoModel.imageSuffix]];
            
           // dispatch_async(dispatch_get_main_queue(), ^{
               
            [self.brandImageView sd_setImageWithURL:brandUrl placeholderImage:[UIImage imageNamed:@"brand"]];
          if (tempCaseArr.count > 0) {
             self.caseImageView.image = tempCaseArr[0];
           }
        
            [self.digitalGalleryImageView sd_setImageWithURL:digitalUrl placeholderImage:[UIImage imageNamed:@"digital"]];
            [self.vedioImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"video"]];
            [self stopCircleLoader];
          //  });
       // });
        //循环相册
        NSDictionary *productDict = obj[@"data"];
        NSArray *albumArr = [productDict objectForKey:@"albumData"];
        self.albumDataArr = [MJBrandDetailAlbumModel mj_objectArrayWithKeyValuesArray:albumArr];
        
        NSMutableArray *imgArr = [NSMutableArray array];
        for (MJBrandDetailAlbumModel *model in self.albumDataArr) {
            
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_500_500.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
            NSData *imgData = [NSData dataWithContentsOfURL:url];
            
            UIImage *img =  [UIImage imageWithData:imgData];
            if (img) {
                 [imgArr addObject:img];
            }
        }
        
        //循环滚动
        MJDirect *direct = [MJDirect direcWithtFrame:CGRectMake(0, 0, _showScrollView.frame.size.width, _showScrollView.frame.size.height) ImageArr:imgArr AndImageClickBlock:^(NSInteger index) {
            
        }];
        [self.showScrollView addSubview:direct];
       
        ;
       
            UITapGestureRecognizer *brandTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(brandTap)];
            [self.brandImageView addGestureRecognizer:brandTap];
        
      
            UITapGestureRecognizer *caseTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(caseTap)];
            [self.caseImageView addGestureRecognizer:caseTap];
       
       
            UITapGestureRecognizer *digitalGalleryTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(digitalGalleryTap)];
            [self.digitalGalleryImageView addGestureRecognizer:digitalGalleryTap];
       
            UITapGestureRecognizer *vedioTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(vedioTap)];
            [self.vedioImageView addGestureRecognizer:vedioTap];
        
       
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
  }
}

- (void)setUpLeftItems
{
    self.storeItem = [UIBarButtonItem itemWithTarget:self action:@selector(storeClick) image:@"item_store" highImage:@""];
    _storeItem.customView.height = 44;
    _storeItem.customView.width = 60;
    
    UIImageView *lineView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1, 20)];
    lineView.image = [UIImage imageNamed:@"home_head_line"];
    UIBarButtonItem *lineItem = [[UIBarButtonItem alloc] initWithCustomView:lineView];
    
    self.matchItem = [UIBarButtonItem itemWithTarget:self action:@selector(matchClick) image:@"item_match" highImage:@""];
    _matchItem.customView.height = 44;
    _matchItem.customView.width = 60;
    
    UIBarButtonItem *spaceItem = [UIBarButtonItem itemWithTarget:nil action:nil image:@"" highImage:@""];
    spaceItem.customView.width = 10;
    spaceItem.customView.height = 35;
    
    self.navigationItem.leftBarButtonItems = @[self.storeItem,spaceItem,lineItem,spaceItem,self.matchItem];
}

- (void)setUpRightItems
{
    //相机
    UIBarButtonItem *cameraItem = [UIBarButtonItem itemWithTarget:self action:@selector(cameraClick) image:@"item_camera" highImage:@""];
    
    //聊天
    UIBarButtonItem *chatItem = [UIBarButtonItem itemWithTarget:self action:@selector(chatClick1) image:@"item_chat" highImage:@""];
    chatItem.tag = 101;
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (delegate.unreadMsgCount == 0) {
        chatItem.badgeValue =@"";
    }else{
        
        chatItem.badgeValue = [NSString stringWithFormat:@"%li",delegate.unreadMsgCount];
    }

    
    //更多
    UIBarButtonItem *moreItem = [UIBarButtonItem itemWithTarget:self action:@selector(moreClick) image:@"item_more" highImage:@""];
    
    UIBarButtonItem *spaceItem = [UIBarButtonItem itemWithTarget:nil action:nil image:@"" highImage:@""];
    spaceItem.customView.width = 10;
    spaceItem.customView.height = 35;
    self.navigationItem.rightBarButtonItems = @[moreItem,spaceItem,spaceItem,chatItem,spaceItem,spaceItem,cameraItem];
}


#pragma mark - 顶部item点击
- (void)storeClick
{
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    MJStoreCtler *store = [[MJStoreCtler alloc] init];
    [self.navigationController pushViewController:store animated:YES];
    
}

- (void)matchClick
{
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    _replaceBarFrame = CGRectMake(0, 0, Screen_Width, 64);
    MJMatchCtler *macth = [[MJMatchCtler alloc] init];
    [self.navigationController pushViewController:macth animated:YES];
}

- (void)cameraClick
{
    
    PhotoTrackController *photo = [[PhotoTrackController alloc] init];
    [self.navigationController presentViewController:photo animated:YES completion:nil];
}

- (void)chatClick1
{
    
    NSInteger indexSelect =self.noteView.currentIndex;
    [_noteView showWithIndexSelect:indexSelect];
    
}

- (void)moreClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.navigationController.navigationBar.frame=CGRectMake(-kMoreViewWidth, 0, Screen_Width, 64);
        
        self.coverView.frame = CGRectMake(-kMoreViewWidth, 0, Screen_Width, Screen_Height);
        
        self.moreView.frame = CGRectMake(Screen_Width-266, 0, 266, Screen_Height);
    }completion:^(BOOL finished) {
        _replaceBarFrame = CGRectMake(-kMoreViewWidth, 0, Screen_Width, 64);
        [self.view addSubview:self.coverView];
    }];
    
}

#pragma mark - 图片手势点击
- (void)brandTap
{
    // NSLog(@"brandTap");
    MJBrandDetailCtler *brandDetail = [[MJBrandDetailCtler alloc] init];
    brandDetail.dataModel = self.dataModel;
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    [self.navigationController pushViewController:brandDetail animated:YES];
}

//案例详情
- (void)setUpCaseMWPhotoBrowser
{
    BOOL displayActionButton = NO;
    BOOL displaySelectionButtons = NO;
    BOOL displayNavArrows = YES;
    BOOL enableGrid = YES;
    BOOL startOnGrid = NO;
    

    // Create browser
    self.browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    _browser.displayActionButton = displayActionButton;//分享按钮,默认是
    _browser.displayNavArrows = displayNavArrows;//左右分页切换,默认否
    _browser.displaySelectionButtons = displaySelectionButtons;//是否显示选择按钮在图片上,默认否
    _browser.alwaysShowControls = displaySelectionButtons;//控制条件控件 是否显示,默认否
    _browser.zoomPhotosToFill = NO;//是否全屏,默认是
#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_7_0
    browser.wantsFullScreenLayout = YES;//是否全屏
#endif
    _browser.enableGrid = enableGrid;//是否允许用网格查看所有图片,默认是
    _browser.startOnGrid = startOnGrid;//是否第一张,默认否
    _browser.enableSwipeToDismiss = YES;
    [_browser showNextPhotoAnimated:YES];
    [_browser showPreviousPhotoAnimated:YES];
    [_browser setCurrentPhotoIndex:0];
    [self.navigationController pushViewController:self.browser animated:YES];
}

- (void)caseTap
{
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
//    MJCaseDetailCtler *caseVc = [[MJCaseDetailCtler alloc] init];
//    caseVc.casePhotoArr = self.casePhotoArr;
//    NSLog(@"%ld",caseVc.casePhotoArr.count);
//    [self.navigationController pushViewController:caseVc animated:YES];
    
    [self setUpCaseMWPhotoBrowser];
}

- (void)digitalGalleryTap
{
    MJDiditalGalleryCtler *digital = [[MJDiditalGalleryCtler alloc] init];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    digital.model = self.panoramaModel;
    [self.navigationController pushViewController:digital animated:YES ];
}

- (void)vedioTap
{
    //NSLog(@"vedioTap");
    MJVedioDetailCtler *video = [[MJVedioDetailCtler alloc] init];
    video.videoModel = self.videoModel;
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
    [self.navigationController pushViewController:video animated:YES];
    
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {

    AppDelegate *myDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
 //   NSLog(@"%ld",myDelegate.casePhotoArr.count);
    return myDelegate.casePhotoArr.count;
}

- (id)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    AppDelegate *myDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if (index < myDelegate.casePhotoArr.count) {
        return [myDelegate.casePhotoArr objectAtIndex:index];
    }
//    if (index < self.photos.count)
//        return [self.photos objectAtIndex:index];
    return nil;
}

#pragma mark - 遮盖点击
- (void)coverClick
{
    [UIView animateWithDuration:0.25 animations:^{
        self.navigationController.navigationBar.frame =  CGRectMake(0, 0, Screen_Width, 64);
        [self.moreView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        [self.mineView setFrame:CGRectMake(Screen_Width, 0, kMoreViewWidth, Screen_Height)];
        [self.coverView removeFromSuperview];
    }completion:^(BOOL finished) {
        _replaceBarFrame = CGRectMake(0, 0, Screen_Width, 64);
    }];
}

#pragma mark <UIAlertViewDelegate>
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsImageEditing = YES;
    picker.modalPresentationStyle = UIModalPresentationFormSheet;
    if (buttonIndex == 2) {//照相
        picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    }else{//图片库
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentModalViewController:picker animated:YES];
    
}

#pragma mark 图片选择器的代理
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //  NSLog(@"%@",info);
    
    //获取修改后的图片
    UIImage *editedImg = info[UIImagePickerControllerEditedImage];
    
    //更改cell里的图片
    [self.mineView setCellImage: editedImg];
   
    //移除图片选择的控制器
    [self dismissViewControllerAnimated:YES completion:nil];
    
    //把新的图片保存到服务器
    [self userSettingAvatar:editedImg name:@"aa.jpg"];

}


#pragma mark
- (void)countDidChangeAction{
    AppDelegate *delegate=(AppDelegate*)[UIApplication sharedApplication].delegate;
    NSInteger x= delegate.unreadMsgCount;
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:kMessageRefresh object:nil userInfo:@{@"contack" : contack}];
    
    UIBarButtonItem* chatItem =(UIBarButtonItem*)self.navigationItem.rightBarButtonItems[2];
    if (x ==0) {
        chatItem.badgeValue=@"";
    }
    chatItem.badgeValue=[NSString stringWithFormat:@"%li",x];
    
    
}

//头像上传
- (void)userSettingAvatar:(UIImage*)image name:(NSString*)name
{
    
    NSString *str = [MJUserDefaults objectForKey:@"userID"];
    NSDictionary *params = @{@"userId":str};
    
    [ImageUpload requestWithUrl:SETTING_AVATAR postData:params
                        imgList:@[@{@"image":image, @"name":name}] userInfo:nil
                       callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
         
     }];
}

- (void)dealloc
{
    [MJNotificationCenter removeObserver:self];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:kCountDidChangeNofication object:nil];

}

@end
