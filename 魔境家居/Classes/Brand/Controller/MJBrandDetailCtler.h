//
//  MJBrandDetailCtler.h
//  魔境家居
//
//  Created by mojing on 15/10/27.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJBaseCtler.h"
#import "MJBrandDetailDataModel.h"
@interface MJBrandDetailCtler : MJBaseCtler

@property (nonatomic,strong) MJBrandDetailDataModel *dataModel;
@end
