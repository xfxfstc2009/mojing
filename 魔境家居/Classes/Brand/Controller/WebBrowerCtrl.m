//
//  WebBrowerCtrl.m
//  HuaTuo
//
//  Created by 施正士 on 15/7/11.
//  Copyright (c) 2015年 HuaTuo. All rights reserved.
//

#import "WebBrowerCtrl.h"
#import "Global.h"
#import "NSString+md5.h"
#import "UIButton+JUtil.h"

@interface WebBrowerCtrl ()
{
    NSString *_urlString;
}
@end

@implementation WebBrowerCtrl

- (id)initWithWebURL:(NSString*)url
{
    self = [super init];
    if (self) {
        _urlString = url;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    webView.delegate = self;
    [self.view addSubview:webView];
    if ([_urlString rangeOfString:@"http"].location==0)
    {
        NSURL *url =[NSURL URLWithString:_urlString];
        NSURLRequest *request =[NSURLRequest requestWithURL:url];
        [webView loadRequest:request];
    }else{
        [webView loadHTMLString:_urlString baseURL:nil];
    }
}

- (void)backHandler
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
