//
//  MJMineCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/30.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJMineCtler.h"
#import "UIBarButtonItem+Extension.h"
#import "JenWebService.h"
#import "NSString+md5.h"
#import "MBProgressHUD.h"
#import "MJNetworkTool.h"
#import "MJRegularExpressionTool.h"
@interface MJMineCtler ()<UITextFieldDelegate,UIAlertViewDelegate>
/**
 *  旧密码
 */
@property (weak, nonatomic) IBOutlet UITextField *oldCodeField;
/**
 *  新密码
 */
@property (weak, nonatomic) IBOutlet UITextField *firstNewCodeField;
/**
 *  重新输入新密码
 */
@property (weak, nonatomic) IBOutlet UITextField *secondNewCodeField;

@end

@implementation MJMineCtler

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"密码修改"];
    // Do any additional setup after loading the view.
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"close" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.rightBarButtonItem = back;
    self.firstNewCodeField.delegate = self;
    self.secondNewCodeField.delegate = self;
}

- (void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 确认修改
- (IBAction)confirmModify:(id)sender {
    if ([self isConnectToNet]) {
       NSString *phoneNumber = [MJUserDefaults objectForKey: @"phoneNumber"];
       BOOL isCorrect =  [MJRegularExpressionTool validatePassword:self.firstNewCodeField.text];
       BOOL isSCorrect = [MJRegularExpressionTool validatePassword:self.secondNewCodeField.text];
        if (isCorrect && isSCorrect) {
            
        if (self.firstNewCodeField.text.length <6 || self.firstNewCodeField.text.length >16) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(@"请输入6到16位的新密码", nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1];
            
        }else if (![self.secondNewCodeField.text isEqualToString:self.firstNewCodeField.text]) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.removeFromSuperViewOnHide =YES;
            hud.mode = MBProgressHUDModeCustomView;
            hud.labelText = NSLocalizedString(@"两次输入的密码不一致,请重新输入", nil);
            // hud.minSize = CGSizeMake(100, 100);
            hud.layer.cornerRadius = 50;
            [hud hide:YES afterDelay:1];
            
        }else
        {
            [self regSetPwdWithPhone:phoneNumber newPwd1:self.secondNewCodeField.text];
        }
      }else
      {
          //密码格式不正确
          MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
          hud.removeFromSuperViewOnHide =YES;
          hud.mode = MBProgressHUDModeCustomView;
          hud.labelText = NSLocalizedString(@"密码格式不正确,请重新输入", nil);
          // hud.minSize = CGSizeMake(100, 100);
          hud.layer.cornerRadius = 50;
          [hud hide:YES afterDelay:1];
      }
    }
   
}

- (void)regSetPwdWithPhone:(NSString*)phone newPwd1:(NSString*)newPwd1
{
    //account=手机号&token=ios-duc
    NSString *token = [NSString stringWithFormat:@"account=%@&token=ios-duc",phone];
    NSDictionary *params = @{@"account":phone,
                             @"newPwd1":newPwd1,
                             @"newPwd2":newPwd1,
                             @"token":[token md5],
                             @"f":@"ios"};
   
    [JenWebService requestWithUrl:SETTING_UPDATE_PWD params:params method:POST
                         userInfo:nil callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
        // if (!validRespDUC(b, data)) return ;
         if(![self codeCorrectWith:b data:data]){
             MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
             hud.removeFromSuperViewOnHide =YES;
             hud.mode = MBProgressHUDModeCustomView;
             hud.labelText = NSLocalizedString(@"旧密码输入错误,请重新输入", nil);
             // hud.minSize = CGSizeMake(100, 100);
             hud.layer.cornerRadius = 50;
             [hud hide:YES afterDelay:2];
         }
             
     }];
}

- (BOOL)codeCorrectWith:(BOOL)b data:(NSDictionary *)data
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.removeFromSuperViewOnHide =YES;
    hud.mode = MBProgressHUDModeCustomView;
    // hud.minSize = CGSizeMake(100, 100);
    hud.layer.cornerRadius = 50;
    [hud hide:YES afterDelay:1.5];
    if (b) {
        if ([[data objectForKey:@"success"] boolValue]) {
            hud.labelText = NSLocalizedString(@"密码修改成功", nil);
            dispatch_queue_t queue = dispatch_queue_create("success", DISPATCH_QUEUE_CONCURRENT);
            dispatch_barrier_async(queue, ^{
                NSLog(@"dispatch_barrier_async");
                [NSThread sleepForTimeInterval:1.5];
            });
            dispatch_async(queue, ^{
               // [NSThread sleepForTimeInterval:2];
                NSLog(@"dispatch_async1");
               [self dismissViewControllerAnimated:YES completion:nil];
            });
            
            return YES;
        }else{
           hud.labelText =NSLocalizedString([data objectForKey:@"msg"], nil);
        return NO;
        }
    }else{
        hud.labelText = @"网络不给力，请重新尝试。";
        //[Alert show:@"提示" msg:@"网络不给力，请重新尝试。" delegate:nil];
        return NO;
    }
}
@end
