//
//  MJRichScanCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/30.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJRichScanCtler.h"
#import "UIBarButtonItem+Extension.h"
#import "UIView+Frame.h"
#import <AVFoundation/AVFoundation.h>
#import "MJNetworkTool.h"
#import "MJMatchUserDataModel.h"
#import "AppDelegate.h"
#import "MJViewController.h"
#import "WebBrowerCtrl.h"
#import "MJProductDetailsCtler.h"
#import "MJProductGroupDetailsCtler.h"
#import "MBProgressHUD.h"
#import "MJUserDataModel.h"
#import "MJExtension.h"
@interface MJRichScanCtler ()<AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate>
{
    UIView *_boxView;
    BOOL _isReader;
    NSString *_scanString;
    NSString *_metadateString;
    MJUserDataModel *_model;
    NSDictionary *_dictionary;
}

//捕捉会话
@property (nonatomic, strong) AVCaptureSession *captureSession;
//展示layer
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@property (strong, nonatomic) CALayer *scanLayer;
@end

@implementation MJRichScanCtler

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _isReader = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _dictionary = [NSDictionary dictionary];
    UIBarButtonItem *close = [UIBarButtonItem itemWithTarget:self action:@selector(closeView) image:@"close" highImage:@""];
    close.customView.width = 44;
    close.customView.height = 44;
    self.navigationItem.leftBarButtonItem = close;
    // [self.view setBackgroundColor:[UIColor cyanColor]];
    [self setTitle:@"扫一扫"];
    
    self.view.frame = CGRectMake(0, 0, Screen_Width, Screen_Height);
    
    
    [self startReading];
    
}


- (BOOL)startReading {
    NSError *error;
    
    //1.初始化捕捉设备（AVCaptureDevice），类型为AVMediaTypeVideo
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    //2.用captureDevice创建输入流
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    //3.创建媒体数据输出流
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    
    //4.实例化捕捉会话
    _captureSession = [[AVCaptureSession alloc] init];
    
    //4.1.将输入流添加到会话
    [_captureSession addInput:input];
    
    //4.2.将媒体输出流添加到会话中
    [_captureSession addOutput:captureMetadataOutput];
    
    //5.创建串行队列，并加媒体输出流添加到队列当中
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    //5.1.设置代理
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    
    //5.2.设置输出媒体数据类型为QRCode
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    //6.实例化预览图层
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    _videoPreviewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    //7.设置预览图层填充方式
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    //8.设置图层的frame
    [_videoPreviewLayer setFrame:self.view.bounds];
    
    //9.将图层添加到预览view的图层上
    [self.view.layer addSublayer:_videoPreviewLayer];
    
    //10.设置扫描范围
    captureMetadataOutput.rectOfInterest = CGRectMake(0.2f, 0.2f, 0.8f, 0.8f);
    
    int width = self.view.bounds.size.width * 0.5f;
    CGRect frame = CGRectMake((CGRectGetWidth(self.view.bounds)-width)/2, (CGRectGetHeight(self.view.bounds)-width)/2-40, width, width);
    
    UIView *bg = [[UIView alloc] initWithFrame:self.view.bounds];
    bg.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, Screen_Width, Screen_Height)];
    [path appendPath:[[UIBezierPath bezierPathWithRect:frame] bezierPathByReversingPath]];
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = path.CGPath;
    [bg.layer setMask:shapeLayer];
    [self.view addSubview:bg];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(frame)+20, Screen_Width, 20)];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont systemFontOfSize:12];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"将二维码放入框内，即可自动扫描";
    [self.view addSubview:label];
    
    //10.1.扫描框
    _boxView = [[UIView alloc] initWithFrame:frame];
    _boxView.layer.borderColor = [UIColor greenColor].CGColor;
    _boxView.layer.borderWidth = 1.0f;
    
    [self.view addSubview:_boxView];
    
    
    //10.2.扫描线
    _scanLayer = [[CALayer alloc] init];
    _scanLayer.frame = CGRectMake(0, 0, _boxView.bounds.size.width, 1);
    _scanLayer.backgroundColor = [UIColor brownColor].CGColor;
    
    [_boxView.layer addSublayer:_scanLayer];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:0.2f target:self selector:@selector(moveScanLayer:) userInfo:nil repeats:YES];
    
    [timer fire];
    
    //10.开始扫描
    [_captureSession startRunning];
    
    
    return YES;
}

-(void)stopReading{
    
    [self parseUrl:_scanString];
    [_captureSession stopRunning];
    _captureSession = nil;
    [_scanLayer removeFromSuperlayer];
    
    // [_videoPreviewLayer removeFromSuperlayer];
    
    // [UIManager gotoBack];
}



#pragma mark - AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    //判断是否有数据
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        _metadateString  = [metadataObj stringValue];
        [self loadMetadataObj:_metadateString];
    }
}

#pragma mark - 扫描到数据
- (void)loadMetadataObj:(NSString *)string
{
    //判断回传的数据类型
    if (_isReader) {
        // _scanString = [metadataObj stringValue];
        _scanString = string;
        _isReader = NO;
        [self performSelectorOnMainThread:@selector(stopReading) withObject:nil
                            waitUntilDone:YES];
    }
}

- (void)moveScanLayer:(NSTimer *)timer
{
    CGRect frame = _scanLayer.frame;
    if (_boxView.frame.size.height < _scanLayer.frame.origin.y) {
        frame.origin.y = 0;
        _scanLayer.frame = frame;
    }else{
        
        frame.origin.y += 5;
        
        [UIView animateWithDuration:0.1 animations:^{
            _scanLayer.frame = frame;
        }];
    }
}

- (void)parseUrl:(NSString*)url
{
    NSMutableDictionary *params ;
    NSArray *urlMain = [url componentsSeparatedByString:@"?"];
    //    NSString *urlHost = [urlMain objectAtIndex:0];
    // http://mojing.duc.cn/app/download/index.html?resourceType=supplier&userID=155590&userType=1
    if ([urlMain count]==2)
    {
        NSString *paramStr = [urlMain objectAtIndex:1];
        NSArray *paramList = [paramStr componentsSeparatedByString:@"&"];
        params = [NSMutableDictionary dictionaryWithCapacity:[paramList count]];
        for (int i=0; i<[paramList count]; i++)
        {
            NSString *item = [paramList objectAtIndex:i];
            NSArray *keyValue = [item componentsSeparatedByString:@"="];
            if ([keyValue count]==2)
            {
                [params setObject:[keyValue objectAtIndex:1] forKey:[keyValue objectAtIndex:0]];
            }
        }
    }
    
    if (params)
    {
        _dictionary = params;
        if ([params objectForKey:@"productID"]) {
            //单品
            NSString *proId = [params objectForKey:@"productID"];
            MJProductDetailsCtler *details = [[MJProductDetailsCtler alloc] init];
            details.idNum = [proId integerValue];
          
            [self.navigationController pushViewController:details animated:YES];
            self.view =nil;
            //  [UIManager gotoProductDetail:proId];
        }/*else if ([params objectForKey:@"brandID"]){
          //品牌
          // NSString *bId = [params objectForKey:@"brandID"];
          // [UIManager gotoShop:bId];
          }*/else if([params objectForKey:@"worksID"]){
              //搭配
              NSString *worksID = [params objectForKey:@"worksID"];
              MJProductGroupDetailsCtler *group = [[MJProductGroupDetailsCtler alloc] init];
              group.idNum = [worksID integerValue];

              [self.navigationController pushViewController:group animated:YES];
              self.view =nil;
          }else if ([params objectForKey:@"userID"]){
              
              NSString *isExist = [MJUserDefaults objectForKey:@"isExist"];
              if ([isExist isEqualToString:@"NO"]) {
                  
                  [self richScanWith:params];
                  
              }else if([isExist isEqualToString:@"YES"]){
                  
                  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"是否结束当前顾客服务并开始服务新的顾客？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
                  [alert show];
              }
              
          }
    }else{
        //  [UIManager gotoWebBrower:_scanString];
        WebBrowerCtrl *ctl = [[WebBrowerCtrl alloc] initWithWebURL:_scanString];
        [self.navigationController pushViewController:ctl animated:YES];
    }
}

- (void)closeView
{
    [self dismissViewControllerAnimated:YES completion:^{
        self.view = nil;
    }];
}

#pragma mark - 扫描到顾客
- (void)richScanWith:(NSDictionary *)params
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    //客户
    NSString *userID = [params objectForKey:@"userID"];
    //解析客户信息
    NSString *user =  [MJUserDefaults objectForKey:@"userID"];
    if (![user isEqualToString:userID]) {
        
        NSDictionary *userDict = @{@"userID":userID};
      //  NSLog(@"%@",userDict);
        [self getUserDetailDataWithDict:userDict];
        NSDictionary *enterDict = @{@"type" : @1,@"customerID" : userID, @"isApnsProduction" : isApnsProduction};
        NSLog(@"%@",enterDict);
        [net getMoJingURL:MJBrowseShopSave parameters:enterDict success:^(id obj) {
            
            NSInteger code = [[obj objectForKey:@"code"] integerValue];
            NSString *msgStr = [obj objectForKey:@"msg"];
            if (code == -1) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:msgStr delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alert show];
                [self dismissViewControllerAnimated:YES completion:nil];
                // return ;
            }else{
                //发通知,通知服务客户的view显示
                [MJNotificationCenter postNotificationName:@"startService" object:nil userInfo:@{@"serviceHidden" : @"NO"}];
                
                
                NSDictionary *data = [obj objectForKey:@"data"];
                NSInteger idNum = [[data objectForKey:@"id"] integerValue];
                NSString *browseID =[NSString stringWithFormat:@"%ld",(long)idNum];
                [MJUserDefaults setObject:browseID forKey:@"browseID"];
                //   NSLog(@"%@",browseID);
                [MJUserDefaults synchronize];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"扫描结果" message:[NSString stringWithFormat:@"获取顾客信息成功,开始服务"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *actoin = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    [_videoPreviewLayer removeFromSuperlayer];
                    [alert dismissViewControllerAnimated:YES completion:nil];
                    [MJUserDefaults setObject:@"YES" forKey:@"isExist"];
                    
                    [MJUserDefaults setObject:@"NO" forKey:@"serviceViewHidden"];
                    [MJUserDefaults synchronize];
                    [self dismissViewControllerAnimated:YES completion:^{
                        self.view =nil;
                    }];
                    
                }];
                NSString *isExist = [MJUserDefaults objectForKey:@"isExist"];
                if (![isExist isEqualToString:@"YES"]) {
                    
                    [alert addAction:actoin];
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];

    }else
    {
        //自己扫自己
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.removeFromSuperViewOnHide =YES;
        hud.mode = MBProgressHUDModeCustomView;
        hud.labelText = NSLocalizedString(@"当前扫描的是自己,服务失败", nil);
        // hud.minSize = CGSizeMake(100, 100);
        hud.layer.cornerRadius = 50;
        [hud hide:YES afterDelay:2];
        dispatch_queue_t queue = dispatch_queue_create("success", DISPATCH_QUEUE_CONCURRENT);
        dispatch_barrier_async(queue, ^{
            //  NSLog(@"dispatch_barrier_async");
            [NSThread sleepForTimeInterval:2];
        });
        dispatch_async(queue, ^{
            // [NSThread sleepForTimeInterval:2];
            //  NSLog(@"dispatch_async1");
            [self closeView];
        });
        
    }
}

#pragma - <UIAlertViewDelegate>
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        //确定
        //1.保存离店信息
        [MJUserDefaults setObject:@"YES" forKey:@"isExist"];
        [MJUserDefaults setObject:@"NO" forKey:@"serviceViewHidden"];
        NSString *browseID = [MJUserDefaults objectForKey:@"browseID"];
        NSDictionary *endDict = @{@"type" : @2, @"browseID" :browseID};
        MJNetworkTool *net = [MJNetworkTool shareInstance];
        [net getMoJingURL:MJBrowseShopSave parameters:endDict success:^(id obj) {
            NSLog(@"%@",endDict);
            //发通知客户列表界面刷新数据
            [MJNotificationCenter postNotificationName:@"customerListReloadData" object:nil];
            [MJNotificationCenter postNotificationName:@"mjStartService" object:nil userInfo:@{@"mjVcHidden" : @"YES"}];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
        }];
        //2.获取需要服务的客户的信息
        [self richScanWith:_dictionary];
        [self dismissViewControllerAnimated:YES completion:^{
            self.view =nil;
        }];
        //        _isReader = true;
        //        [self startReading];
        
    }else if (buttonIndex == 1){
        //取消
        //1.退出当前界面
        [self closeView];
    }
}
#pragma mark - 获取用户详情
- (void)getUserDetailDataWithDict:(NSDictionary *)dict
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    [net getMoJingURL:MJUserDetailData parameters:dict success:^(id obj) {
        NSDictionary *data = [obj objectForKey:@"data"];
        // 把字典数组转换成模型数组
        //   NSMutableArray *arr = [MJCustomerModel mj_objectArrayWithKeyValuesArray:dictArr];
        MJUserDataModel *model = [MJUserDataModel mj_objectWithKeyValues:data];
        _model = [[MJUserDataModel alloc] init];
        _model = model;
        //  NSLog(@"%@ %@",_model.userName,_model.nickName);
        //保存用户昵称
        [MJUserDefaults setObject:model.nickName forKey:@"customerName"];
        //   NSLog(@"%@ %@",model.userName,model.nickName);
        [MJUserDefaults synchronize];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];

}
@end
