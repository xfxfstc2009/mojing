//
//  MJBrandDetailCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/27.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJBrandDetailCtler.h"
#import "UIImageView+WebCache.h"
@interface MJBrandDetailCtler ()
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImgView;

@property (weak, nonatomic) IBOutlet UIView *detailsBackView;
@property (weak, nonatomic) IBOutlet UIWebView *introductWebView;

@property (strong,nonatomic) NSURL *brandDetailUrl;

@end

@implementation MJBrandDetailCtler


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    
    [self setTitle:@"品牌详情页"];
    self.brandDetailUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_1.%@",mainURL,_dataModel.introduceImageSuffix,_dataModel.introduceImageName,_dataModel.introduceImageName,_dataModel.introduceImageSuffix]];
    [self.backgroundImgView sd_setImageWithURL:self.brandDetailUrl placeholderImage:[UIImage imageNamed:@"brand_details_background"]];
    self.detailsBackView.layer.cornerRadius = 10;
    self.detailsBackView.backgroundColor = [UIColor whiteColor];
    self.introductWebView.layer.cornerRadius = 10;
    self.introductWebView.clipsToBounds = YES;
    //webView
   // UIWebView *detailWebView = [[UIWebView alloc] initWithFrame:CGRectMake(15, 713-64-40-80+60+25, Screen_Width-30, 80)];
   // self.introductWebView.scrollView.userInteractionEnabled = NO;
  //  detailWebView.delegate = self;
    _introductWebView.scalesPageToFit = YES;
    NSMutableString *htmlCode = [NSMutableString stringWithString:@"<html><head>"];
    [htmlCode appendFormat:@"<style type=\"text/css\">img{max-width:%fpx;}</style>", CGRectGetWidth(_introductWebView.frame)-15];
    [htmlCode appendString:@"<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=no' />"];
    [htmlCode appendString:@"</head><body>"];
    NSString *str = _dataModel.introduceText;
    [htmlCode appendString:str];//这个给那个content里的内容。
    [htmlCode appendString:@"</body></html>"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
      //  NSURL *url = [NSURL URLWithString:self.model.panoramaURL];
      //  NSURLRequest *request = [NSURLRequest requestWithURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            //加载一个请求
     //       [self.introductWebView loadRequest:];
            if ([self isConnectToNet]) {
             [_introductWebView loadHTMLString:htmlCode baseURL:nil];
            }
        });
    });

}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 返回
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
    ;
}

@end
