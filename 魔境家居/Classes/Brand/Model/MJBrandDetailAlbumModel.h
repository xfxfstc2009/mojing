//
//  MJBrandDetailAlbumModel.h
//  魔境家居
//
//  Created by mojing on 15/11/23.
//  Copyright © 2015年 mojing. All rights reserved.
//  品牌相册

#import <Foundation/Foundation.h>

@interface MJBrandDetailAlbumModel : NSObject

@property (nonatomic,assign) NSInteger imageID;// 1001,
@property (nonatomic,copy) NSString *imageName;// "图片名称",
@property (nonatomic,copy) NSString *imageSuffix;// "图片后缀"

@end
