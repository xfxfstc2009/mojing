//
//  MJBrandDetailPanoramaModel.h
//  魔境家居
//
//  Created by mojing on 15/11/23.
//  Copyright © 2015年 mojing. All rights reserved.
//  360全景

#import <Foundation/Foundation.h>

@interface MJBrandDetailPanoramaModel : NSObject

@property (nonatomic,copy) NSString * panoramaURL  ;//  360全景URL ,
@property (nonatomic,assign) NSInteger  imageID  ;// 2002,
@property (nonatomic,copy) NSString * imageName  ;//  视频截图名称 ,
@property (nonatomic,copy) NSString * imageSuffix  ;//  视频截图后缀 
@end
