//
//  MJBrandDetailDataModel.h
//  魔境家居
//
//  Created by mojing on 15/11/23.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJBrandDetailAlbumModel.h"//相册
#import "MJBrandDetailVideoModel.h"//视频
#import "MJBrandDetailPanoramaModel.h"//全景
#import "MJBrandDetailSpreadModel.h"//热销
@interface MJBrandDetailDataModel  : NSObject

@property (nonatomic,strong) NSArray *caseData ;
@property (nonatomic,assign) NSInteger idNum ;// 1,
@property (nonatomic,copy) NSString *introduceText ;// ,
@property (nonatomic,assign) NSInteger brandID ;// 1,
@property (nonatomic,assign) NSInteger logoID ;// 2,
@property (nonatomic,copy) NSString *introduceImageName ;// 1448276977213,
@property (nonatomic,assign) NSInteger userID ;// 155064,
@property (nonatomic,strong) MJBrandDetailPanoramaModel *panoramaData ;
@property (nonatomic,strong) MJBrandDetailSpreadModel *spreadData ;// {
@property (nonatomic,strong) MJBrandDetailVideoModel *videoData ;//
@property (nonatomic,strong) NSArray *albumData ;
@property (nonatomic,assign) NSInteger introduceImageID ;// 17,
@property (nonatomic,copy) NSString *logoName ;// 1448276099368,
@property (nonatomic,copy) NSString *logoSuffix ;// png,
@property (nonatomic,copy) NSString *introduceImageSuffix ;// png,
@property (nonatomic,copy) NSString *brandName ;// 品牌1

@end
