//
//  MJBrandDetailCaseModel.h
//  魔境家居
//
//  Created by mojing on 15/11/23.
//  Copyright © 2015年 mojing. All rights reserved.
//  品牌案例

#import <Foundation/Foundation.h>

@interface MJBrandDetailCaseModel : NSObject

@property (nonatomic,assign) NSInteger  imageID  ;// 1001,
@property (nonatomic,copy) NSString * imageName  ;//  图片名称 ,
@property (nonatomic,copy) NSString * imageSuffix  ;//  图片后缀 ,
@property (nonatomic,copy) NSString * title  ;//  图片标题 ,
@property (nonatomic,copy) NSString * desc  ;//  图标描述 
@end
