//
//  MJBrandDetailVideoModel.h
//  魔境家居
//
//  Created by mojing on 15/11/23.
//  Copyright © 2015年 mojing. All rights reserved.
//  品牌视频

#import <Foundation/Foundation.h>

@interface MJBrandDetailVideoModel : NSObject

@property (nonatomic,assign) NSInteger  videoID  ;// 1001,
@property (nonatomic,copy) NSString * videoName  ;//  视频名称 ,
@property (nonatomic,copy) NSString * videoSuffix  ;//  视频后缀 ,
@property (nonatomic,assign) NSInteger  imageID  ;// 2002,
@property (nonatomic,copy) NSString * imageName  ;//  视频截图名称 ,
@property (nonatomic,copy) NSString * imageSuffix  ;//  视频截图后缀 
@end
