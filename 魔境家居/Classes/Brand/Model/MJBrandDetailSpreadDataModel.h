//
//  MJMJBrandDetailSpreadDataModel.h
//  魔境家居
//
//  Created by mojing on 15/11/23.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJBrandDetailSpreadDataModel : NSObject

@property (nonatomic,assign) NSInteger  idNum  ;// 1001,
@property (nonatomic,copy) NSString * type  ;//  1表示搭配作品，2表示商品 ,
@property (nonatomic,assign) NSInteger  imageID  ;// 1001,
@property (nonatomic,copy) NSString * imageName  ;//  图片的名称 ,
@property (nonatomic,copy) NSString * imageSuffix  ;//  图片的后缀

@end
