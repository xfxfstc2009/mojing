//
//  MJBrandListModel.m
//  魔境家居
//
//  Created by mojing on 15/11/17.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJBrandListModel.h"
#import "MJExtension.h"
@implementation MJBrandListModel

- (NSDictionary *)objectClassInArray
{
    return @{@"brandList":[MJBrandDataModel class]};
}

@end
