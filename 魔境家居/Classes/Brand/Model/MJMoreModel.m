//
//  MJMoreModel.m
//  魔境家居
//
//  Created by mojing on 15/10/30.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJMoreModel.h"

@implementation MJMoreModel
- (UIImage *)image
{
    if (_image ==nil) {
        _image = [[UIImage alloc] init];
    }
    return _image;
}

- (instancetype)initWithImage:(UIImage *)image title:(NSString *)title subTitle:(NSString *)subTitle
{
    if (self = [super init]) {
        self.image = image;
        self.title = title;
        self.subTitle = subTitle;
    }
    return self;
}

+ (instancetype)modelWithImage:(UIImage *)image title:(NSString *)title subTitle:(NSString *)subTitle
{
    return [[self alloc] initWithImage:image title:title subTitle:subTitle];
}
@end
