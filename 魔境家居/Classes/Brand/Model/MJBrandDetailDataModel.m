//
//  MJBrandDetailDataModel.m
//  魔境家居
//
//  Created by mojing on 15/11/23.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJBrandDetailDataModel.h"
#import "MJBrandDetailCaseModel.h"
#import "MJExtension.h"
@implementation MJBrandDetailDataModel

- (NSDictionary *)objectClassInArray
{
    return @{ @"caseData":[MJBrandDetailCaseModel class]/*,@"albumData":[MJBrandDetailAlbumModel class]*/};
}

+ (NSDictionary *)replacedKeyFromPropertyName
{
    return @{@"idNum" : @"id"};
}

//- (NSDictionary *)objectClassInArray
//{
//    return @{@"productList":[MJProductModel class]};
//}
@end
