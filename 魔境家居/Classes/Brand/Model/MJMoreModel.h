//
//  MJMoreModel.h
//  魔境家居
//
//  Created by mojing on 15/10/30.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJMoreModel : NSObject

@property (nonatomic, copy, nullable) NSString *title;
@property (nonatomic, strong, nullable) UIImage *image;
@property (nonatomic, copy, nullable) NSString *subTitle;

- (instancetype)initWithImage:(UIImage *)image title:(NSString *)title subTitle:(NSString *)subTitle;

+ (instancetype)modelWithImage:(UIImage *)image title:(NSString *)title subTitle:(NSString *)subTitle;
@end
