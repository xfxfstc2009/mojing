//
//  MJBrandDetailSpreadModel.m
//  魔境家居
//
//  Created by mojing on 15/11/23.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJBrandDetailSpreadModel.h"

#import "MJExtension.h"
#import "MJBrandDataModel.h"
@implementation MJBrandDetailSpreadModel

- (NSDictionary *)objectClassInArray
{
    return @{@"dataList" : [MJBrandDetailSpreadDataModel class]};
}

+ (NSDictionary *)replacedKeyFromPropertyName
{
    return @{@"templateString" : @"template"};
}
@end
