//
//  MJHeadPortraitCell.h
//  魔境家居
//
//  Created by mojing on 15/11/9.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJMoreModel.h"
@interface MJHeadPortraitCell : UITableViewCell

@property (nonatomic,strong) MJMoreModel *model;
@property (weak, nonatomic) IBOutlet UIImageView *headImgView;

@end
