//
//  MJHeadPortraitCell.m
//  魔境家居
//
//  Created by mojing on 15/11/9.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJHeadPortraitCell.h"
#import "UIImageView+WebCache.h"
@implementation MJHeadPortraitCell
{
    __weak IBOutlet UILabel *_headLabel;
}

- (void)setModel:(MJMoreModel *)model
{
    _model = model;
    _headLabel.text = model.title;
    NSData *userAvatarURL = [MJUserDefaults objectForKey:@"userAvatarURL"];
   // [_headImgView sd_setImageWithURL:[NSURL URLWithString:userAvatarURL]];
    _headImgView.image = [UIImage imageWithData:userAvatarURL];
}

- (void)awakeFromNib {
    // Initialization code
    _headImgView.layer.cornerRadius = 25;
    _headImgView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
