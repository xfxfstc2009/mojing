//
//  MJMoreView.m
//  魔境家居
//
//  Created by mojing on 15/10/30.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJMineView.h"
#import "MJMoreModel.h"
#import "MJUIClassTool.h"

@interface MJMineView()<UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIButton *bottomView;
@property (nonatomic, strong) MJMoreModel *nickNameModel;
@end
@implementation MJMineView
/**
 *  需要的数据
 */
- (NSMutableArray *)dataArr
{
    if (_dataArr == nil) {
        _dataArr = [NSMutableArray array];
        NSString *userName = [MJUserDefaults objectForKey:@"userName"];
        NSString *phoneNumber = [MJUserDefaults objectForKey:@"phoneNumber"];
        self.nickName = [MJUserDefaults objectForKey:@"nickName"];
        MJMoreModel *richScan = [MJMoreModel modelWithImage:[UIImage imageNamed:@"mine_head"] title:@"头像" subTitle:@""];
      //  MJMoreModel *customer = [MJMoreModel modelWithImage:nil title:@"用户名" subTitle:userName];
        _nickNameModel = [MJMoreModel modelWithImage:nil title:@"昵称" subTitle:self.nickName];
        MJMoreModel *statistics = [MJMoreModel modelWithImage:nil title:@"手机号" subTitle:phoneNumber];
        MJMoreModel *profile = [MJMoreModel modelWithImage:nil title:@"登录密码" subTitle:@"******"];
        
        _dataArr = @[richScan,_nickNameModel,statistics,profile];
    }
    return _dataArr;
}

- (void)setNickName:(NSString *)nickName
{
    _nickName = nickName;
}

/**
 *  顶部view
 */
- (UIView *)topView
{
    if (_topView == nil) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 64)];
        _topView.backgroundColor = RGB(112, 181, 205);
        UILabel *label = [MJUIClassTool createLbWithFrame:CGRectMake(_topView.frame.size.width/2-40, _topView.frame.size.height/2, 80, 20) title:@"我的信息" aliment:NSTextAlignmentCenter color:[UIColor whiteColor] size:17];
        UIButton *closeBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(10, _topView.frame.size.height-25, 40, 15) title:@"返回" image:nil target:self action:@selector(closeView)];
        [closeBtn setFont:[UIFont systemFontOfSize:16]];
        [closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_topView addSubview:label];
        [_topView addSubview:closeBtn];
    }
    return _topView;
}
/**
 *  注销按钮
 */
- (UIButton *)bottomView
{
    if (_bottomView == nil) {
        _bottomView = [UIButton buttonWithType:UIButtonTypeCustom];
        _bottomView.frame = CGRectMake(20, self.frame.size.height-60, self.frame.size.width-40, 50);
        _bottomView.layer.cornerRadius = 5;
        [_bottomView addTarget:self action:@selector(loginOut) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView setTitle:@"注销登录" forState:UIControlStateNormal];
        [_bottomView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _bottomView.backgroundColor = RGB(110, 181, 206);
    }
    return _bottomView;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.frame.size.width, self.frame.size.height-64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = NO;
        //影藏分割线
        [_tableView registerNib:[UINib nibWithNibName:@"MJHeadPortraitCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        UIView *foot = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        _tableView.tableFooterView = foot;
    }
    return _tableView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.tableView];
        [self addSubview:self.topView];
        [self addSubview:self.bottomView];
    }
    return self;
}

#pragma  mark - UITableViewDataSource UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   // static NSString *cellID = @"cell";
    if (indexPath.row == 0) {
         MJHeadPortraitCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        cell.model = self.dataArr[0];
       // cell.headImgView.image = [self.dataArr[0] image];
        self.cell = cell;
        return cell;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                          reuseIdentifier:@"mycell"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 1) {
            cell.textLabel.text = @"昵称";
            NSString *name = [MJUserDefaults objectForKey:@"nickName"];
            cell.detailTextLabel.text = name;
        }else{
        cell.textLabel.text = [self.dataArr[indexPath.row] title];
            cell.detailTextLabel.text = [self.dataArr[indexPath.row] subTitle];}
        
    if (indexPath.row == 3 || indexPath.row == 1) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    return cell;
    }
}

#pragma  mark - <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (_indexPathBlock) {
        _indexPathBlock(indexPath.row);
    }
}

- (void)setCellImage:(UIImage *)image
{
    self.cell.headImgView.image = image;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 60;
    }
    return 44;
}

#pragma 关闭点击
- (void)closeView
{
    if (_close) {
        _close();
    }
}

#pragma 注销登录
- (void)loginOut
{
    if (_loginOff) {
        _loginOff();
    }
}
@end
