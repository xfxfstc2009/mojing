//
//  MJMineView.h
//  魔境家居
//
//  Created by mojing on 15/11/9.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJHeadPortraitCell.h"
typedef void (^closeClick)();
typedef void (^indexPathClick)(NSInteger index);
typedef void (^loginOffClick)();
@interface MJMineView : UIView

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic,copy) closeClick close;
@property (nonatomic,copy) indexPathClick indexPathBlock;
@property (nonatomic, strong) MJHeadPortraitCell *cell;
@property (nonatomic,copy) loginOffClick loginOff;
@property (nonatomic,copy) NSString *nickName;
- (void)setCellImage:(UIImage *)image;

@end

