//
//  MJSecondProductDetailsCtler.m
//  魔境家居
//
//  Created by lumingliang on 16/2/1.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJSecondProductDetailsCtler.h"
#import "MJDealerShopDetailsCtler.h"
#import "UIImageView+WebCache.h"
@interface MJSecondProductDetailsCtler ()

@end

@implementation MJSecondProductDetailsCtler

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self hidesTabBar:YES animated:NO];
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = RGB(249, 249, 249);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createButton
{
    CGFloat x = 627;
    CGFloat y = 460;
    //分隔线
    UIView *divideView1 = [[UIView alloc] initWithFrame:CGRectMake(x, y-15, Screen_Width-x-40, 1)];
    divideView1.backgroundColor = [UIColor grayColor];
    //divideView1.alpha = 0.5;
    UIView *divideView2 = [[UIView alloc] initWithFrame:CGRectMake(x, y-15+65, Screen_Width-x-40, 1)];
    divideView2.backgroundColor = [UIColor grayColor];
    //divideView2.alpha = 0.5;
    //品牌logo
    UIImageView *logoView = [[UIImageView alloc] initWithFrame:CGRectMake(x, y, 40, 40)];
    logoView.layer.cornerRadius = 20;
    logoView.clipsToBounds = YES;
    NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_100_100.%@",mainURL,self.model.brandData.logoSuffix,self.model.brandData.logoName,self.model.brandData.logoName,self.model.brandData.logoSuffix]];
    [logoView sd_setImageWithURL:bgUrl placeholderImage:nil];
    
    //品牌名称
    UILabel *brandNameLb = [MJUIClassTool createLbWithFrame:CGRectMake(x+50, y+10, 150, 25) title:self.model.brandData.name aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:20];
    //brandNameLb.backgroundColor = [UIColor greenColor];
    
    //进入店铺
    UIButton *enterShopBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(Screen_Width-180, y, 40, 40) title:nil image:[UIImage imageNamed:@"product_enter_shop"] target:self action:@selector(detailBtnClick:)];
    UIButton *enterLbBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(Screen_Width-140, y, 100, 40) title:@"进入店铺" image:nil target:self action:@selector(detailBtnClick:)];
    enterLbBtn.titleLabel.font = [UIFont systemFontOfSize:21];
    enterLbBtn.tag = 1;
    enterShopBtn.tag = 1;
    //查看经销商
    UIButton *lookjxsBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(x, y+62, 148, 40) title:nil image:[UIImage imageNamed:@"product_look_jxs"] target:self action:@selector(detailBtnClick:)];
    lookjxsBtn.tag = 2;
    //实景搭配
    UIButton *startMatchBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(x+158, y+62, 148, 40) title:@"实景搭配" image:nil target:self action:@selector(startMatch)];
    [startMatchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    startMatchBtn.backgroundColor = RGB(251, 43, 8);
    startMatchBtn.layer.cornerRadius = 20;
    
    //收藏
    UIButton *collectBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(x+158*2, y+62, 40, 40) title:nil image:[UIImage imageNamed:@"product_detail_collect"] target:self action:@selector(detailBtnClick:)];
    [collectBtn setImage:[UIImage imageNamed:@"product_has_collected"] forState:UIControlStateSelected];
    if ([self.model.isFavorited isEqualToString:@"1"]) {
        collectBtn.selected = YES;
    }else
    {
        collectBtn.selected = NO;
    }
    collectBtn.tag = 3;
    [self.scrollView addSubview:divideView1];
    [self.scrollView addSubview:divideView2];
    [self.scrollView addSubview:lookjxsBtn];
    [self.scrollView addSubview:logoView];
    [self.scrollView addSubview:brandNameLb];
    [self.scrollView addSubview:enterShopBtn];
    [self.scrollView addSubview:enterLbBtn];
    [self.scrollView addSubview:startMatchBtn];
    [self.scrollView addSubview:collectBtn];
}

#pragma mark - 按钮点击
- (void)detailBtnClick:(UIButton *)button
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    switch (button.tag) {
        case 1:
        {//进入店铺
            NSLog(@"进入店铺");
            MJDealerShopDetailsCtler *shop = [[MJDealerShopDetailsCtler alloc] init];
            self.model.pushIndex = 1;
            shop.model = self.model;
            [self.navigationController pushViewController:shop animated:YES];
        }
            break;
         case 2:
        {//查看经销商
            NSLog(@"查看经销商");
            MJDealerShopDetailsCtler *shop = [[MJDealerShopDetailsCtler alloc] init];
            self.model.pushIndex = 2;
            shop.model = self.model;
            [self.navigationController pushViewController:shop animated:YES];
            
        }
            break;
        case 3:
        {//取消收藏
            NSLog(@"%@",self.model.isFavorited);
           
            button.selected = !button.isSelected;
            if (!button.selected) {
    
                NSDictionary *dict = @{@"resourceID" : [NSString stringWithFormat:@"%ld",self.model.idNum],@"resourceType" : @"2",@"favoriteType" : @"2"};
                [net getMoJingURL:MJResourceFavorite parameters:dict success:^(id obj) {
                    
                } failure:^(NSError *error) {
                    NSLog(@"%@",error);
                }];
                
                NSLog(@"取消收藏");
            }else
            {
                NSLog(@"收藏");
                NSDictionary *dict = @{@"resourceID" : [NSString stringWithFormat:@"%ld",self.model.idNum],@"resourceType" : @"2",@"favoriteType" : @"1"};
                [net getMoJingURL:MJResourceFavorite parameters:dict success:^(id obj) {
                    
                } failure:^(NSError *error) {
                    NSLog(@"%@",error);
                }];
            }
        }
            break;
        default:
            break;
    }
}



@end
