//
//  MJViewController.m
//  魔境家居选材搭配
//
//  Created by lumingliang on 16/1/12.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJMaterialMatchCtler.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "MJNetworkTool.h"
#import "MJMaterialMatchModel.h"
#import "MJMaterialMatchCell.h"
#import "MJMaterialCategoryModel.h"
#import <QuartzCore/QuartzCore.h>
#import "CHTCollectionViewWaterfallLayout.h"
#import "MJRefresh.h"
#import "MJUIClassTool.h"
#import "MJMaterialScrollView.h"
#import "MJProductListModel.h"
#import "MJProductListDataModel.h"
#import "MJProductModel.h"
#import "GMDCircleLoader.h"
#import "MJMaterialMatchFileView.h"
#import "MJMatchProductModel.h"
#import "MJSecondProductDetailsCtler.h"
#import "ImageUpload.h"
#define imageViewX 0
#define pageSize 18
#define collectionWidth 308
#define newImageViewWitdh 200
static NSString * const reuseIdentifier = @"Cell";
@interface MJMaterialMatchCtler ()<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UIGestureRecognizerDelegate,CHTCollectionViewDelegateWaterfallLayout,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    UIScrollView *_scrollView;
    //拖动图片原始x的值
    CGFloat imgViewX;
    //拖动后图片的偏移量
    CGFloat imgViewContentOffset;
    //指引index
    NSInteger _newImageViewIndex;
    //选中的商品分类
    UIButton *_selectBtn;
}

/***  选中view */
@property (nonatomic ,strong) UIImageView *insteadView;
/***  存放新增view */
@property (nonatomic ,strong) NSMutableArray *viewArr;
/***  提示文字label */
@property (nonatomic ,strong) UILabel *noteLabel;
/***  点击后的数据数组 */
@property (nonatomic ,strong) NSMutableArray *tapDataArr;
/***  数据数组 */
@property (nonatomic ,strong) NSMutableArray *matchDataArr;
/***  分类信息数组 */
@property (nonatomic ,strong) NSMutableArray *categoryDataArr;
/***  第二级分类信息数组 */
@property (nonatomic ,strong) NSMutableArray *subCategoryDataArr;
/***  第三级分类信息数组 */
@property (nonatomic ,strong) NSMutableArray *thirdCategoryDataArr;
/***  分类品牌数组 */
@property (nonatomic ,strong) NSMutableArray *brandListArr;
/***  分类品牌系列数组 */
@property (nonatomic ,strong) NSMutableArray *brandSeriesListArr;
/***  全局点击手势*/
@property (nonatomic ,strong) UITapGestureRecognizer *tap;
/***  全局双击手势*/
@property (nonatomic ,strong)UITapGestureRecognizer *doubleRecognizer;
/***  全局拖动手势*/
@property (nonatomic ,strong) UIPanGestureRecognizer *secondPan;
/***  全局旋转手势*/
@property (nonatomic ,strong)UIRotationGestureRecognizer *rotation;
/***  全局缩放手势*/
@property (nonatomic ,strong)UIPinchGestureRecognizer *pinch;

/***  底部编辑视图缩放手势*/
@property (nonatomic ,strong)UIPinchGestureRecognizer *editViewPinch;
/***  底部编辑视图移动手势*/
@property (nonatomic ,strong)UIPanGestureRecognizer *editViewPan;
/***  底部编辑视图点击手势*/
@property (nonatomic ,strong)UITapGestureRecognizer *editViewTap;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
@property (nonatomic, strong) UICollectionView *collectionView;
/* 选材底部视图*/
@property (nonatomic, strong) UIView *bottomView;
/* 选材编辑视图*/
@property (nonatomic, strong) UIImageView *materialEditView;
/* 分类选择下的滚动视图*/
@property (nonatomic, strong) MJMaterialScrollView *categoryScrollView;
/* 第二个分类选择下的滚动视图*/
@property (nonatomic, strong) MJMaterialScrollView *secondCategoryScrollView;
/* 商城商品按钮*/
@property (nonatomic, strong) UIButton *storeProductBtn;
/* 我的商品按钮*/
@property (nonatomic, strong) UIButton *myProductBtn;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 锁定View数组*/
@property (nonatomic,strong) NSMutableArray *lockViewArr;
/** 搭配清单的view*/
@property (nonatomic, strong) MJMaterialMatchFileView *materialMatchFileView;
/** 遮盖*/
@property (nonatomic, strong) UIButton *coverButton;
/** 删除*/
@property (nonatomic, strong) UIButton *deleteButton;
/** 背景图片ID*/
@property (nonatomic, assign) NSInteger editViewImageID;
/** 背景图片的URL*/
@property (nonatomic, strong) NSURL *editViewImageUrl;
@property (nonatomic, assign) CGFloat lastRotation;
@property (nonatomic, assign) CGFloat lastScale;
@end

@implementation MJMaterialMatchCtler

- (UIButton *)coverButton
{
    if (!_coverButton) {
        _coverButton = [MJUIClassTool createBtnWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height) title:nil image:nil target:self action:@selector(buttonClick:)];
        _coverButton.alpha = 0.1;
        _coverButton.tag = 13;
    }
    return _coverButton;
}

- (MJMaterialMatchFileView *)materialMatchFileView
{
    if (!_materialMatchFileView) {
        _materialMatchFileView = [[MJMaterialMatchFileView alloc] initWithFrame:CGRectMake(0, 0, 360, 100)];
//        _materialMatchFileView.layer.cornerRadius = 5;
//        _materialMatchFileView.clipsToBounds = YES;
        __weak MJMaterialMatchCtler *ctl = self;
        _materialMatchFileView.indexPathClick = ^(NSInteger index){
            MJSecondProductDetailsCtler *detail = [[MJSecondProductDetailsCtler alloc] init];
            detail.idNum = index;
           // UINavigationController *detailNav = [[UINavigationController alloc] initWithRootViewController:detail];
            [ctl.navigationController pushViewController:detail animated:YES];
        };
        [self.view addSubview:_materialMatchFileView];
    }
    return _materialMatchFileView;
}

- (MJMaterialScrollView *)secondCategoryScrollView
{
    if (!_secondCategoryScrollView) {
        _secondCategoryScrollView = [[MJMaterialScrollView alloc] initWithFrame:CGRectMake(Screen_Width-collectionWidth, 66, collectionWidth,46)];
        _secondCategoryScrollView.backgroundColor = [UIColor whiteColor];
       
        _secondCategoryScrollView.titleArr = @[@"我的商品"];
        _secondCategoryScrollView.select = ^(NSInteger index){
           // NSLog(@"%ld",index);
            switch (index) {
                case 0:
                {
                    
                }
                    break;
                case 1:
                {
                    
                }
                    break;
                case 2:
                {
                    
                }
                    break;
                case 3:
                {
                    
                }
                    break;
                case 4:
                {
                    
                }
                    break;
                case 5:
                {
                    
                }
                    break;
                case 6:
                {
                    
                }
                    break;
                case 7:
                {
                    
                }
                    break;
                default:
                    break;
            }
        };
    }
    return _secondCategoryScrollView;
}

- (MJMaterialScrollView *)categoryScrollView
{
    if (!_categoryScrollView) {
        _categoryScrollView = [[MJMaterialScrollView alloc] initWithFrame:CGRectMake(Screen_Width-collectionWidth, 66, collectionWidth,46)];
        
        _categoryScrollView.titleArr = @[@"全部",@"架子类",@"椅/凳/榻",@"柜子",@"沙发",@"桌子",@"茶几",@"床",@"其它"];
        _categoryScrollView.backgroundColor = [UIColor whiteColor];
        __weak MJMaterialMatchCtler *ctl = self;
        _categoryScrollView.select = ^(NSInteger index){
            NSDictionary *dict = @{@"productTypeID" : [NSString stringWithFormat:@"%ld",index],@"pageNumber" : [NSString stringWithFormat:@"%ld",ctl.pageNumber],@"pageSize" :@(pageSize)};
            [ctl addRefreshHeader:dict];
          
        };
        
    }
    return _categoryScrollView;
}

- (UIImageView *)materialEditView
{
    if (_materialEditView == nil) {
        _materialEditView = [[UIImageView alloc] initWithFrame:CGRectMake(13, 74, Screen_Width-collectionWidth-30, Screen_Height-150)];
        _materialEditView.backgroundColor = [UIColor whiteColor];
       
        //缩放
       self.editViewPinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(materialEditViewPinch:)];
        [_materialEditView addGestureRecognizer:_editViewPinch];
        
        //移动
        self.editViewPan  = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(materialEditViewPan:)];
        [_materialEditView addGestureRecognizer:_editViewPan];
        _editViewPan.enabled = NO;
        _materialEditView.userInteractionEnabled = YES;
        
        self.editViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(editViewTap:)];
        [_materialEditView addGestureRecognizer:_editViewTap];
        
//        _editViewTap.delegate = self;
//        _editViewPinch.delegate = self;
//        _editViewPan.delegate = self;
    }
    return _materialEditView;
}

- (UIView *)bottomView
{
    if (_bottomView == nil) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(Screen_Width-collectionWidth, 114, collectionWidth, Screen_Height-114)];
        _bottomView.userInteractionEnabled = YES;
        [self.view addSubview:_bottomView];
    }
    return _bottomView;
}

#pragma mark - collectionView
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        CGFloat inset = 2;
        layout.sectionInset = UIEdgeInsetsMake(inset, inset, inset, inset);
        layout.minimumInteritemSpacing = inset;
        layout.minimumColumnSpacing = inset;
        //  layout.minimumLineSpacing = inset;
        layout.columnCount = 3;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, collectionWidth, Screen_Height-114) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor =[UIColor colorWithRed:239 green:239 blue:239 alpha:1];
        [_collectionView registerClass:[MJMaterialMatchCell class] forCellWithReuseIdentifier:reuseIdentifier];
        [self.bottomView addSubview:_collectionView];
    }
    return _collectionView;
}

- (NSMutableArray *)brandSeriesListArr
{
    if (_brandSeriesListArr == nil) {
        _brandSeriesListArr = [NSMutableArray array];
    }
    return _brandSeriesListArr;
}

- (NSMutableArray *)brandListArr
{
    if (_brandListArr == nil) {
        _brandListArr = [NSMutableArray array];
    }
    return _brandListArr;
}

- (NSMutableArray *)categoryDataArr
{
    if (_categoryDataArr == nil) {
        _categoryDataArr = [NSMutableArray array];
    }
    return _categoryDataArr;
}

- (NSMutableArray *)subCategoryDataArr
{
    if (_subCategoryDataArr == nil) {
        _subCategoryDataArr = [NSMutableArray array];
    }
    return _subCategoryDataArr;
}

- (NSMutableArray *)thirdCategoryDataArr
{
    if (_thirdCategoryDataArr == nil) {
        _thirdCategoryDataArr = [NSMutableArray array];
    }
    return _thirdCategoryDataArr;
}

- (NSMutableArray *)matchDataArr
{
    if (_matchDataArr == nil) {
        _matchDataArr = [NSMutableArray array];
    }
    return _matchDataArr;
}

- (NSMutableArray *)viewArr
{
    if (!_viewArr) {
        _viewArr = [NSMutableArray array];
    }
    return _viewArr;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    [self initUI];
    [self initModel];
}

- (void)initUI
{
    self.title = @"魔境家居选材搭配";
    self.view.backgroundColor = RGB(226, 226, 226);
    self.insteadView.contentMode = UIViewContentModeScaleAspectFit;
    self.insteadView.backgroundColor = [UIColor cyanColor];
   // [self.materialEditView addSubview:self.insteadView];
    [self.view addSubview:self.materialEditView];
    //NSRegularExpression
    [self.view addSubview:self.categoryScrollView];
     [self createButton];
    [self createTopButton];
    [self createBottomButton];
  
    //改变场景选中
    [MJNotificationCenter addObserver:self selector:@selector(materialMatchChangeScene:) name:@"materialMatchChangeScene" object:nil];
    
   //搭配作品保存
    [MJNotificationCenter addObserver:self selector:@selector(planeWorksSave:) name:@"planeWorksSave" object:nil];
}

#pragma mark - 保存搭配
- (void)planeWorksSave:(NSNotification *)notification
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSString *planeWorksName = notification.userInfo[@"planeWorksName"];
    NSString *planeWorksTypeIndex = notification.userInfo[@"planeWorksTypeIndex"];
    
    NSMutableDictionary *para = [NSMutableDictionary dictionary];
    para[@"name"] = planeWorksName;
    para[@"spaceTypeID"] = planeWorksTypeIndex;
   
    NSMutableArray *productList = [NSMutableArray array];
    NSMutableArray *productIdArr = [NSMutableArray array];
   // [SVProgressHUD showWithStatus:@"作品保存中"];
    
    
    int i = 0;
    for (MJProductModel *model in self.tapDataArr) {
        
        NSDictionary *imageDict = @{@"imageID" : [NSString stringWithFormat:@"%ld",model.imageID],@"imageName" :model.imageName,@"imageSuffix" :model.imageSuffix};
        NSDictionary *locationDict = [NSDictionary dictionary];
        if ((model.location.x == 0) && (model.location.y == 0)) {
            UIImageView *imageView = self.viewArr[i];
            locationDict = @{@"x" : [NSString stringWithFormat:@"%f",imageView.frame.origin.x],@"y" : [NSString stringWithFormat:@"%f",imageView.frame.origin.y]};
            
        }else{
            locationDict = @{@"x" : [NSString stringWithFormat:@"%f",model.location.x],@"y" : [NSString stringWithFormat:@"%f",model.location.y]};
        }
        
        NSMutableDictionary *modelData = [NSMutableDictionary dictionary];
        modelData[@"productID"] = [NSString stringWithFormat:@"%ld",model.idNum];
        modelData[@"image"] = imageDict;
        modelData[@"location"] = locationDict;
        if (model.scale == 0) {
            model.scale = 1.00;
        }
        
        modelData[@"scale"] = [NSString stringWithFormat:@"%.2f",model.scale];
        modelData[@"rotation"] = [NSString stringWithFormat:@"%.2f",model.rotation];
        
        modelData[@"layer"] = [NSString stringWithFormat:@"%d",model.layer];
        
        [productList addObject:modelData];
        [productIdArr addObject:[NSString stringWithFormat:@"%ld",model.idNum]];
        i++;
    }
    
    NSDictionary *productListDict = @{@"productList" : productList};
    NSError *error1;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:productListDict options:NSJSONWritingPrettyPrinted error:&error1];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
   // NSDictionary *uploadPara = @{@"spreadData":jsonString};
    
    para[@"collocationData"] = jsonString;
    
   
    NSString *productStr = [productIdArr componentsJoinedByString:@","];
    
    para[@"productIDs"] = productStr;

    NSLog(@"%@",para);
    
    if (_editViewImageID>0) {
        
         NSDictionary *img = @{@"image":_materialEditView.image, @"name":@"aa.jpg"};
      //  [SVProgressHUD showWithStatus:@"作品保存中"];
        __weak MJMaterialMatchCtler *ctl = self;
         [ImageUpload requestWithUrl:fileUploadURL postData:nil imgList:@[img] userInfo:nil callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo) {
             
             //NSLog(@"%@ %@",data,userInfo);
             NSDictionary *dataDict = [data objectForKey:@"data"];
             
             NSString *bgImageID = [[dataDict objectForKey:@"id"] stringValue];
             para[@"bgImageID"] = bgImageID;
             
            // NSLog(@"dataDict%@ userInfo%@ %@",dataDict,userInfo,bgImageID);
             if (bgImageID) {
                 NSDictionary *collocationImage = @{@"image":[ctl getImageWithView:ctl.materialEditView], @"name":@"bb.jpg"};
                 [ImageUpload requestWithUrl:fileUploadURL postData:nil imgList:@[collocationImage] userInfo:nil callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo) {
                     //NSLog(@"%@ %@",data,userInfo);
                     NSDictionary *dataDict = [data objectForKey:@"data"];
                     
                     NSString *collocationImageID = [[dataDict objectForKey:@"id"] stringValue];
                     para[@"collocationImageID"] = collocationImageID;
                    // NSLog(@"%@ %@",dataDict,collocationImageID);
                     
                     if (collocationImage) {
                         //[SVProgressHUD dismiss];
                         NSLog(@"%@",para);
                        // [SVProgressHUD showSuccessWithStatus:@"保存成功"];

                         [net getMoJingURL:MJPlaneWorksSave parameters:para success:^(id obj) {
                             
                         } failure:^(NSError *error) {
                             
                         }];
                     }
                 }];
             }
         }];
    }else
    {
        //没背景
        NSLog(@"没有选择背景！");
        
        NSDictionary *collocationImage = @{@"image":[self getImageWithView:self.materialEditView], @"name":@"bb.jpg"};
        [ImageUpload requestWithUrl:fileUploadURL postData:nil imgList:@[collocationImage] userInfo:nil callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo) {
            //NSLog(@"%@ %@",data,userInfo);
            NSDictionary *dataDict = [data objectForKey:@"data"];
            
            NSString *collocationImageID = [[dataDict objectForKey:@"id"] stringValue];
            para[@"collocationImageID"] = collocationImageID;
            // NSLog(@"%@ %@",dataDict,collocationImageID);
            
            if (collocationImage) {
                //[SVProgressHUD dismiss];
                NSLog(@"%@",para);
                [SVProgressHUD showSuccessWithStatus:@"保存成功"];
                [net getMoJingURL:MJPlaneWorksSave parameters:para success:^(id obj) {
                    
                } failure:^(NSError *error) {
                    
                }];
            }
        }];
        
    }
    
    
}

//得到截取图片的方法
- (UIImage *)getImageWithView:(UIView *)view
{
   // NSLog(@"%@",NSStringFromCGSize(self.materialEditView.size));
//
    UIGraphicsBeginImageContext(self.materialEditView.size);
    //将view绘制到图形上下文中
    
    [self.materialEditView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // 生成后的image
    UIImage *newImage=UIGraphicsGetImageFromCurrentImageContext();
    
    return newImage;
}

#pragma mark - 背景更换
- (void)materialMatchChangeScene:(NSNotification *)notification
{
    MJMatchProductModel *model = notification.userInfo[@"matchProductModel"];
    _editViewImageID = model.idNum;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_1.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
    //[_materialEditView sd_setImageWithURL:url];
    //[SVProgressHUD showWithStatus:@"背景更换中"];
    _editViewImageUrl = url;
        [_materialEditView sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            // [SVProgressHUD dismiss];
            _materialEditView.contentMode = UIViewContentModeScaleAspectFit;
            _materialEditView.backgroundColor = nil;
        }];
    
    if (model.showWhiteColor) {
        _editViewImageUrl = nil;
        _editViewImageID = 0;
        _materialEditView.contentMode = UIViewContentModeScaleToFill;
        _materialEditView.backgroundColor = [UIColor whiteColor];
    }
    
}

- (void)initModel
{
    _newImageViewIndex = -1;
    self.pageNumber = 1;
    self.lockViewArr = [NSMutableArray array];
    self.tapDataArr = [NSMutableArray array];
    NSDictionary *dict = @{@"productTypeID" : [NSString stringWithFormat:@"%d",0],@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber],@"pageSize" :@(pageSize)};
    [self addRefreshHeader:dict];
}
#pragma mark - 按钮创建
- (void)createButton
{
    //商城商品按钮
    _storeProductBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(self.bottomView.x, 20,collectionWidth/2-1 , 44) title:@"商城商品" image:nil target:self action:@selector(selectCategory:)];
   //  storeProductBtn.titleLabel.font = [UIFont systemFontOfSize:20];
    [_storeProductBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _storeProductBtn.tag = 11;
    _selectBtn = _storeProductBtn;
    _storeProductBtn.backgroundColor = [UIColor whiteColor];
    _storeProductBtn.selected = YES;
    [_storeProductBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
    [self.view addSubview:_storeProductBtn];
    //中间分隔view
    
    //我的商品按钮
    self.myProductBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_myProductBtn setTitle:@"我的商品" forState:UIControlStateNormal];
    _myProductBtn.frame = CGRectMake(collectionWidth/2+1+self.bottomView.x, 20, collectionWidth/2-1, 44);
    // storeProductBtn.titleLabel.font = [UIFont systemFontOfSize:30];
    [_myProductBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _myProductBtn.tag = 12;
    [_myProductBtn addTarget:self action:@selector(selectCategory:) forControlEvents:UIControlEventTouchUpInside];
    _myProductBtn.backgroundColor = [UIColor whiteColor];
    [_myProductBtn setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
    [self.view addSubview:_myProductBtn];
    
    UIButton *allDeleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [allDeleteBtn setTitle:@"全部删除" forState:UIControlStateNormal];
    [allDeleteBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    allDeleteBtn.frame = CGRectMake(20, 64, 190, 100);
    allDeleteBtn.titleLabel.font = [UIFont systemFontOfSize:28];
   // allDeleteBtn.tag = 1;
    [allDeleteBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
   // [self.view addSubview:allDeleteBtn];

   //显示与影藏collectionView
    UIButton *showOrHiddenBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(Screen_Width-collectionWidth-37, Screen_Height/2-24, 32, 48) title:nil image:[UIImage imageNamed:@"select_side_popup"] target:self action:@selector(buttonClick:)];
    showOrHiddenBtn.tag = 10;
    [self.view addSubview:showOrHiddenBtn];
   
    //分隔view
    UIView *divideView = [[UIView alloc] initWithFrame:CGRectMake(-5, -94, 5, Screen_Height-20)];
    divideView.backgroundColor = RGB(135, 135, 135);
    [self.bottomView addSubview:divideView];

}

#pragma mark - 顶部按钮
- (void)createTopButton
{
    NSMutableArray *imgArr = [NSMutableArray array];
    //查看清单按钮
    UIImage *materialFileImage = [UIImage imageNamed:@"seletc_material_file"];
    //场景选择按钮
    UIImage *sceneImage = [UIImage imageNamed:@"seletc_material_scene"];
    //保存按钮
    UIImage *saveImage = [UIImage imageNamed:@"select_save_works"];
    //返回按钮
    UIImage *backImage = [UIImage imageNamed:@"seletc_material_back"];
    //相机按钮
    UIImage *cameraImage = [UIImage imageNamed:@"seletc_material_camera"];
    UIButton *backBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(20, 20, 40, 40) title:nil image:backImage target:self action:@selector(buttonClick:)];
    backBtn.tag = 4;
    [self.view addSubview:backBtn];
    
    [imgArr addObject:materialFileImage];
    [imgArr addObject:sceneImage];
    [imgArr addObject:saveImage];
    [imgArr addObject:cameraImage];
    
    CGFloat btnW = 40;
    CGFloat padding = 10;

    for (int i=0; i<4; i++) {
      //  (padding+imgW*i, 0, imgW-padding, 210-60)
        UIButton *btn = [MJUIClassTool createBtnWithFrame:CGRectMake(300+btnW*i+padding*i, 20, btnW, btnW) title:nil image:imgArr[i] target:self action:@selector(buttonClick:)];

        btn.tag = i+1;
        if (i == 3) {
            btn.tag = i+11;
        }
        [self.view addSubview:btn];
    }
}

#pragma mark - 底部按钮
- (void)createBottomButton
{
    //图层向前
    UIImage *toFrontImage = [UIImage imageNamed:@"seletc_material_up"];
    //图层向后
    UIImage *toBackImage = [UIImage imageNamed:@"seletc_material_down"];
    //锁住与解锁
    UIImage *lockImage = [UIImage imageNamed:@"seletc_material_unlock"];
    //角度切换
    UIImage *chageAngleImage = [UIImage imageNamed:@"seletc_change_angle"];
    //删除
    UIImage *deleteImage = [UIImage imageNamed:@"seletc_material_delete"];
    NSArray *imgArr = @[toFrontImage,toBackImage,lockImage,chageAngleImage,deleteImage];
    CGFloat btnW = 40;
    CGFloat padding = 2;
    
    for (int i=0 ; i<5; i++) {
        UIButton *btn = [MJUIClassTool createBtnWithFrame:CGRectMake(13+btnW*i+padding*i, Screen_Height-60, btnW, btnW) title:nil image:imgArr[i] target:self action:@selector(buttonClick:)];
        btn.tag = i+5;
        if (i == 4) {
            self.deleteButton = btn;
        }
        [self.view addSubview:btn];
    }
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.bottomView addSubview:_circulateBackgroungView];
    self.categoryScrollView.userInteractionEnabled = NO;
    self.secondCategoryScrollView.userInteractionEnabled = NO;
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    self.categoryScrollView.userInteractionEnabled = YES;
    self.secondCategoryScrollView.userInteractionEnabled = YES;
    [self.circulateBackgroungView removeFromSuperview];
    //[GMDCircleLoader hideFromView:self.view animated:YES];
    
}


#pragma mark - 添加上下拉加载数据
- (void)addRefreshHeader:(NSDictionary *)dict
{
    __weak MJMaterialMatchCtler *ctl = self;
    [self loadingData];
    //下拉刷新
    self.collectionView.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        [ctl loadNewData:dict];
    }];
    // 马上进入刷新状态
    [self.collectionView.mj_header beginRefreshing];
    
}

#pragma mark - 下拉加载数据
- (void)loadNewData:(NSDictionary *)dict
{
    self.pageNumber = 1;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJMaterialMatchCtler *ctl = self;
   // [self.noDataView removeFromSuperview];
    [self.matchDataArr removeAllObjects];
   // [self.userPhotoArr removeAllObjects];
    
    //获取搭配作品列表
    if ([self isConnectToNet]) {
        [netTool getMoJingURL:MJProductList parameters:dict success:^(id obj) {
            //     NSLog(@"%@",dict);
            MJProductListModel *listModel = obj;
            MJProductListDataModel *model = listModel.data;
            self.pageCount = model.pageCount;
            // 把字典数组转换成模型数组
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
            
            
            [ctl.matchDataArr addObjectsFromArray:arr];
            
            //  NSLog(@"%ld",ctl.productListArr.count);
            [ctl stopCircleLoader];
            
            if (ctl.matchDataArr.count >= pageSize) {
                ctl.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                    // 进入刷新状态后会自动调用这个block
                    [ctl loadMoreData:dict];
                }];
                // 默认先隐藏footer
                ctl.collectionView.mj_footer.hidden = YES;
            }else{
                ctl.collectionView.mj_footer = nil;
            }
            
            [ctl.collectionView reloadData];
            [ctl.collectionView.mj_header endRefreshing];
            [ctl.view endEditing:YES];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
           [ctl stopCircleLoader];
            [ctl.collectionView.mj_header endRefreshing];
        }];
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_header endRefreshing];
    }
    
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    _pageNumber++;
    //  NSLog(@"%ld",_pageNumber);
    NSString *str = [dict objectForKey:@"productTypeID"];
   
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSDictionary *moreDict = @{@"productTypeID" : str,@"pageNumber" : [NSString stringWithFormat:@"%ld",_pageNumber],@"pageSize" : @(pageSize)};
    __weak MJMaterialMatchCtler *ctl = self;
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            [net getMoJingURL:MJProductList parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                MJProductListModel *listModel = obj;
                NSLog(@"%@",listModel.mj_keyValues);
                MJProductListDataModel *model = listModel.data;
                // 把字典数组转换成模型数组
                NSMutableArray *arr = [NSMutableArray array];
                arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
                
                //                for (MJProductModel *m in ctl.productListArr) {
                //                    MJBrandDataModel *brand = m.brandData;
                //                    MJCreatedDateModel *create = m.createdDate;
                //                    MJModifiedDateModel *modify = m.modifiedDate;
                //                    //  NSLog(@"%ld %@ %@ %@",brand.idNum,m.userID,create.time,modify.day);
                //                }
                [ctl.matchDataArr addObjectsFromArray:arr];
                //   NSLog(@"%ld",ctl.productListArr.count);
                [UIView animateWithDuration:0.1 animations:^{
                    [ctl.collectionView reloadData];
                }];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            }];
            
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
    }

    
}


#pragma mark - 获取我的商品
- (void)getMyProductWithParameters:(NSDictionary *)parameters
{
   
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 滚动视图上的view点击
- (void)tap:(UITapGestureRecognizer *)recognizer
{
    self.deleteButton.enabled = YES;
    _newImageViewIndex++;
    [self.tapDataArr insertObject:self.matchDataArr[recognizer.view.tag] atIndex:_newImageViewIndex];
   // NSLog(@"%ld",_newImageViewIndex);
   // NSLog(@"%ld",recognizer.view.tag);
   // NSLog(@"%ld",self.tapDataArr.count);
    NSMutableArray *arr = [NSMutableArray array];
    arr = (NSMutableArray *)self.insteadView.subviews;
    
    //原来view上的边框移除掉
    self.insteadView.layer.borderWidth = 0;
    //原来view的交互开启
    self.insteadView.userInteractionEnabled = YES;
    
    if (arr.count > 0) {
        //将新增的view移除掉
        [arr removeAllObjects];
        
    }
    
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, newImageViewWitdh, newImageViewWitdh)];
    imgV.tag = self.tapDataArr.count-1;
   // imgV.center = self.materialEditView.center;
    imgV.center = CGPointMake(self.materialEditView.frame.size.width/2, self.materialEditView.frame.size.height/2);
   
    UIImageView *imgV1 = [[UIImageView alloc] init];
    if ([recognizer.view isKindOfClass:[UIImageView class]]) {
        imgV1 = (UIImageView *)recognizer.view;
        imgV.image = imgV1.image;
    }
    //拖动手势
    _secondPan= [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(secondPan:)];
    _secondPan.delegate = self;
    imgV.userInteractionEnabled = YES;
    imgV.multipleTouchEnabled = YES;
   // imgV.contentMode = UIViewContentModeScaleAspectFit;
   // imgV.clipsToBounds = YES;
    [imgV addGestureRecognizer:_secondPan];
    
    //普通点击
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(secondTap:)];
   // _tap.delegate = self;
    [imgV addGestureRecognizer:_tap];
    
    //双击
    _doubleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dubleTap:)];
    
    _doubleRecognizer.delegate=self;
    
    _doubleRecognizer.numberOfTapsRequired = 2; // 双击
    
    [imgV addGestureRecognizer:_doubleRecognizer];
    
    //旋转手势
    _rotation = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotation:)];
    _rotation.delegate = self;
    [imgV addGestureRecognizer:_rotation];
    
    //放大手势
    _pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
    _pinch.delegate = self;
    [imgV addGestureRecognizer:_pinch];
    
    
    [self.viewArr insertObject:imgV atIndex:_newImageViewIndex];
   // [self.view addSubview:_newSuperView];
    [self.materialEditView addSubview:imgV];
    
    //添加边框
    imgV.layer.borderWidth = 1;
    imgV.layer.borderColor = [UIColor redColor].CGColor;
    self.insteadView = imgV;
    
//    int j = 0;
//    int total = 0;
//    for (UIView *v in self.materialEditView.subviews) {
//        for (MJProductModel *m in self.tapDataArr) {
//            total+=m.layer;
//        }
//        NSLog(@"%d",total);
//        if (total == 0) {
//            MJProductModel *model = self.tapDataArr[j];
//            model.layer = j;
//            NSLog(@"j_%d layer%d",j,model.layer);
//        }
//        j++;
//    }
    
}

#pragma mark - 新生成图片的移动手势
- (void)secondPan:(UIPanGestureRecognizer *)recognizer
{
    self.deleteButton.enabled = YES;
    _editViewPinch.enabled = NO;
    _editViewPan.enabled = NO;
    NSMutableArray *arr = [NSMutableArray array];
    arr = (NSMutableArray *)self.insteadView.subviews;
    
    //原来view上的边框移除掉
    self.insteadView.layer.borderWidth = 0;
    //原来view的交互开启
    self.insteadView.userInteractionEnabled = YES;
    if (arr.count > 0) {
        //将新增的view移除掉
        [arr removeAllObjects];
    }
    UIView *recognizerSuperView = [recognizer.view superview];
    CGFloat superViewHeight = recognizerSuperView.frame.size.height;
    CGFloat superViewWidth = recognizerSuperView.frame.size.width;
    CGPoint translation = [recognizer translationInView:self.materialEditView];
//    CGPoint location = [recognizer locationInView:self.materialEditView];
//    recognizer.view.center = CGPointMake(location.x+translation.x,location.y+ translation.y);
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,recognizer.view.center.y + translation.y);
//    CGFloat tapVW = recognizer.view.frame.size.width;
//    CGFloat tapVH = recognizer.view.frame.size.height;
    CGFloat centerX = recognizer.view.center.x;
    CGFloat centerY = recognizer.view.center.y;
    NSLog(@"centerX%f_centerY%f",centerX,centerY);
    if (centerX<0 && centerY>0 && centerY<superViewHeight) {
        if (translation.x ==0 && translation.y ==0) {
            
             recognizer.view.center = CGPointMake(0 + translation.x,recognizer.view.center.y + translation.y);
        }
    }else if (centerX>superViewWidth && centerY>0 && centerY<superViewHeight)
    {
        if (translation.x ==0 && translation.y ==0) {
            
            recognizer.view.center = CGPointMake(superViewWidth + translation.x,recognizer.view.center.y + translation.y);
        }
    }else if (centerY<0 && centerX>0 && centerX<superViewWidth)
    {
        if (translation.x ==0 && translation.y ==0) {
            
            recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,0 + translation.y);
        }
    }else if (centerY>superViewHeight && centerX>0 && centerX<superViewWidth)
    {
        if (translation.x ==0 && translation.y ==0) {
            
            recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,superViewHeight + translation.y);
        }
    }else if (centerX<0 && centerY<0)
    {
        if (translation.x ==0 && translation.y ==0) {
            
            recognizer.view.center = CGPointMake(0 + translation.x,0 + translation.y);
        }
    }else if (centerX<0 && centerY>superViewHeight)
    {
        if (translation.x ==0 && translation.y ==0) {
            
            recognizer.view.center = CGPointMake(0 + translation.x,superViewHeight + translation.y);
        }
    }else if (centerX>superViewWidth && centerY<0)
    {
        if (translation.x ==0 && translation.y ==0) {
            
            recognizer.view.center = CGPointMake(superViewWidth + translation.x,0 + translation.y);
        }
    }else if (centerX>superViewWidth && centerY>superViewHeight)
    {
        if (translation.x ==0 && translation.y ==0) {
            
            recognizer.view.center = CGPointMake(superViewWidth + translation.x,superViewHeight + translation.y);
        }
    }
    
    [recognizer setTranslation:CGPointZero inView:self.materialEditView];
   // [self.view bringSubviewToFront:recognizer.view];
    for (UIImageView *view in self.viewArr) {
        if ([recognizer.view isEqual:view]) {
            self.insteadView = view;
            //添加边框
            view.layer.borderWidth = 1;
            view.layer.borderColor = [UIColor redColor].CGColor;
        }
    }
    int i = 0;
    for (MJProductModel *model in self.tapDataArr) {
        if (recognizer.view.tag == i) {
            model.location = recognizer.view.frame.origin;
            
            int j = 0;
            for (UIView *v in self.materialEditView.subviews) {
                if (v.tag == recognizer.view.tag) {
                    model.layer = j;
                    NSLog(@"j_%d layer%d",j,model.layer);
                }
                j++;
            }
        }
        i++;
    }
    NSLog(@"%@",NSStringFromCGPoint(recognizer.view.frame.origin));
}

#pragma mark - 新生成图片的点击手势
- (void)secondTap:(UITapGestureRecognizer *)recognizer
{
    _editViewPan.enabled = NO;
    _editViewPinch.enabled = NO;
    self.deleteButton.enabled = YES;
    NSMutableArray *arr = [NSMutableArray array];
    arr = (NSMutableArray *)self.insteadView.subviews;
   
    //原来view上的边框移除掉
    self.insteadView.layer.borderWidth = 0;
    //原来view的交互开启
    self.insteadView.userInteractionEnabled = YES;
    if (arr.count > 0) {
        //将新增的view移除掉
        [arr removeAllObjects];
        
    }
    int i = 0;
    for (MJProductModel *model in self.tapDataArr) {
        if (recognizer.view.tag == i) {
            int j = 0;
            for (UIView *v in self.materialEditView.subviews) {
                if ([v isEqual:recognizer.view]) {
                    model.layer = j;
                    NSLog(@"j_%d layer%d",j,model.layer);
                }
                j++;
            }
        }
        i++;
    }
    for (UIImageView *view in self.viewArr) {
        if ([recognizer.view isEqual:view]) {
            self.insteadView = view;

            view.layer.borderWidth = 1;
            view.layer.borderColor = [UIColor redColor].CGColor;
        }
    }
}

#pragma mark - 新增图片的dubleTap
- (void)dubleTap:(UITapGestureRecognizer *)recognizer
{
    int i = 0;
    for (MJProductModel *model in self.tapDataArr) {
        if (recognizer.view.tag == i) {
            int j = 0;
            for (UIView *v in self.materialEditView.subviews) {
                if ([v isEqual:recognizer.view]) {
                    model.layer = j;
                    NSLog(@"j_%d layer%d",j,model.layer);
                }
                j++;
            }
        }
        i++;
    }
    
    self.deleteButton.enabled = YES;
    _editViewPinch.enabled = NO;
    _editViewPan.enabled = NO;
    [self.materialEditView bringSubviewToFront:recognizer.view];
}

#pragma mark - 新生成图片的旋转手势
- (void)rotation:(UIRotationGestureRecognizer *)recognizer
{
    self.deleteButton.enabled = YES;
    _editViewPinch.enabled = NO;
    _editViewPan.enabled = NO;
    NSMutableArray *arr = [NSMutableArray array];
    arr = (NSMutableArray *)self.insteadView.subviews;

    //原来view上的边框移除掉
    self.insteadView.layer.borderWidth = 0;
    //原来view的交互开启
    self.insteadView.userInteractionEnabled = YES;
    if (arr.count > 0) {
        //将新增的view移除掉
        [arr removeAllObjects];
        
    }
   // [self.view bringSubviewToFront:tap.view];
        for (UIImageView *view in self.viewArr) {
        if ([recognizer.view isEqual:view]) {
            self.insteadView = view;
            view.layer.borderWidth = 1;
            view.layer.borderColor = [UIColor redColor].CGColor;
            
        }
    }
    if ([recognizer state] == UIGestureRecognizerStateEnded) {
        self.lastRotation = 0;
        return;
    }
    
    //CGAffineTransform currentTransform = self.imageView2.transform;
    CGFloat rotation = 0.0 - (self.lastRotation - recognizer.rotation);
    recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, rotation);
    int i = 0;
    for (MJProductModel *model in self.tapDataArr) {
        if (recognizer.view.tag == i) {
            NSLog(@"%.2f",recognizer.rotation);
            model.rotation = recognizer.rotation+model.rotation;
            int j = 0;
            for (UIView *v in self.materialEditView.subviews) {
                if ([v isEqual:recognizer.view]) {
                    model.layer = j;
                    NSLog(@"j_%d layer%d",j,model.layer);
                }
                j++;
            }
        }
        i++;
    }
    self.lastRotation = recognizer.rotation;
    
}

#pragma mark - 新生产图片缩放手势
- (void)pinch:(UIPinchGestureRecognizer *)recognizer
{
    self.deleteButton.enabled = YES;
    _editViewPinch.enabled = NO;
    _editViewPan.enabled = NO;
    NSMutableArray *arr = [NSMutableArray array];
    arr = (NSMutableArray *)self.insteadView.subviews;

    //原来view上的边框移除掉
    self.insteadView.layer.borderWidth = 0;
    //原来view的交互开启
    self.insteadView.userInteractionEnabled = YES;
    if (arr.count > 0) {
        //将新增的view移除掉
        [arr removeAllObjects];
    }

   // [self.materialEditView bringSubviewToFront:recognizer.view];
    NSLog(@"%@",NSStringFromCGSize(recognizer.view.size));
    int i = 0;
    for (MJProductModel *model in self.tapDataArr) {
        if (recognizer.view.tag == i) {
            model.scale = [[NSString stringWithFormat:@"%.2f",recognizer.view.size.width/newImageViewWitdh] floatValue];
            NSLog(@"i_%d scale%.2f",i,model.scale);
            
            int j = 0;
            for (UIView *v in self.materialEditView.subviews) {
                if ([v isEqual:recognizer.view]) {
                    model.layer = j;
                    NSLog(@"j_%d layer%d",j,model.layer);
                }
                j++;
            }
        }
        i++;
    }
    
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
   
    recognizer.scale = 1;
    for (UIImageView *view in self.viewArr) {
        if ([recognizer.view isEqual:view]) {
            self.insteadView = view;
            //添加边框
            view.layer.borderWidth = 2.;
            view.layer.borderColor = [UIColor redColor].CGColor;
        }
    }
    
//    if( CGRectContainsPoint(recognizer.view.superview.frame, [recognizer locationInView:recognizer.view])) {
//        _editViewPan.enabled = NO;
//    } else{
//        _editViewPan.enabled = YES;
//    };
//
//    if([recognizer state] == UIGestureRecognizerStateEnded)
//    {
//        _editViewPan.enabled = YES;
//    }
}

- (void)addImageView:(CGRect)frame toView:(UIView *)view
{
    UIImageView *v1 = [[UIImageView alloc] initWithFrame:frame];
    v1.image = [UIImage imageNamed:@"select_material_board"];
    v1.contentMode = UIViewContentModeScaleAspectFit;
    v1.userInteractionEnabled = YES;
    [view addSubview:v1];
}

#pragma mark - 选材编辑视图缩放手势
- (void)materialEditViewPinch:(UIPinchGestureRecognizer *)tap
{
    tap.view.transform = CGAffineTransformScale(tap.view.transform, tap.scale, tap.scale);
    
    tap.scale = 1;
}

#pragma mark - 选材编辑视图移动手势
- (void)materialEditViewPan:(UIPanGestureRecognizer *)recognizer
{
    // [self addImageView:CGRectMake(2, 2, recognizer.view.width-4, recognizer.view.height-4) toView:recognizer.view];
//     if( CGRectContainsPoint(recognizer.view.superview.frame, [recognizer locationInView:recognizer.view])) {
//        
//     } else{
         CGPoint translation = [recognizer translationInView:self.view];
         recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,recognizer.view.center.y + translation.y);
         [recognizer setTranslation:CGPointZero inView:self.view];
   //  };
}

#pragma mark - 选材编辑视图点击手势
- (void)editViewTap:(UITapGestureRecognizer *)recognizer
{
    //原来view上的边框移除掉
    self.insteadView.layer.borderWidth = 0;
    //原来view的交互开启
    self.insteadView.userInteractionEnabled = YES;
   
    _editViewPan.enabled = YES;
    _editViewPinch.enabled = YES;
}

#pragma mark - 按钮点击
- (void)buttonClick:(UIButton *)button
{
    switch (button.tag) {
        case 1:
        {//查看清单
            self.materialMatchFileView.hidden = NO;
            [self.view addSubview:self.coverButton];
            [self.view bringSubviewToFront:self.materialMatchFileView];
            [self.materialMatchFileView setFrame:CGRectMake(140, 66, 360, 516)];

            //发通知,传模型数组
            [MJNotificationCenter postNotificationName:@"lookMaterialFileList" object:nil userInfo:@{@"materialFileList" : self.tapDataArr}];
            
            NSLog(@"查看清单");
        }
            break;
        case 2:
        {//场景选择
            self.materialMatchFileView.hidden = NO;
            [self.view addSubview:self.coverButton];
            [self.materialMatchFileView setFrame:CGRectMake(190, 66, 360, 516)];
            [self.view bringSubviewToFront:self.materialMatchFileView];
          //  [self.materialFilePopover presentPopoverFromRect:rect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            [MJNotificationCenter postNotificationName:@"selectMaterialScene" object:nil];
            NSLog(@"场景选择");
            
        }
            break;
        case 3:
        {//保存
            self.materialMatchFileView.hidden = NO;
            [self.view addSubview:self.coverButton];
            [self.view bringSubviewToFront:self.materialMatchFileView];
            [self.materialMatchFileView setFrame:CGRectMake(240, 66, 360, 331)];
            
            [MJNotificationCenter postNotificationName:@"selectMaterialSave" object:nil userInfo:@{@"selectMaterialView" : self.materialEditView}];
            
            NSLog(@"保存");
      
        }
            break;
        case 4:
        {//返回
            
            NSLog(@"返回");
            [self.navigationController popViewControllerAnimated:YES];
            self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
            
        }
            break;
        case 5:
        {//图层向前
            NSLog(@"图层向前");
            int i = 0;
            for (UIView *view in self.materialEditView.subviews) {
                if ([view isEqual:self.insteadView]) {
                    
                    if (i<self.materialEditView.subviews.count-1) {
                        
                        int j = 0;
                        for (UIView *v in self.viewArr) {
                            if ([v isEqual:self.insteadView]) {
                                
                                MJProductModel *model1 = self.tapDataArr[j];
                                model1.layer = i+1;
                                NSLog(@"j_%d model1layer%d",i,model1.layer);
                                //将图层大的减1
                                if ((j+1) < self.tapDataArr.count-1) {
                                    MJProductModel *model2 = self.tapDataArr[j+1];
                                    model2.layer = i;
                                     NSLog(@"j_%d model2layer%d",i,model2.layer);
                                }
                            }
                            j++;
                        }
                       [self.materialEditView exchangeSubviewAtIndex:i withSubviewAtIndex:i+1];
                    }
                    
                }
                i++;
            }
        }
            break;
        case 6:
        {//图层向后
            NSLog(@"图层向后");
            int i = 0;
            for (UIView *view in self.materialEditView.subviews) {
                if ([view isEqual:self.insteadView]) {
                    if (i>0) {
                        int j = 0;
                        for (UIView *v in self.viewArr) {
                            if ([v isEqual:self.insteadView]) {
                                
                                MJProductModel *model = self.tapDataArr[j];
                                model.layer = i-1;
                                NSLog(@"j_%d layer%d",i,model.layer);
                                //将图层小的+1
                                if ((j-1) >= 0) {
                                    MJProductModel *model2 = self.tapDataArr[j-1];
                                    model2.layer = i;
                                }
                            }
                            j++;
                        }
                        [self.materialEditView exchangeSubviewAtIndex:i withSubviewAtIndex:i-1];
                    }
                }
                i++;
            }
        }
            break;
        case 7:
        {//解锁
            
            button.selected = !button.isSelected;
            if (button.isSelected) {
                [button setImage:[UIImage imageNamed:@"seletc_material_lock"] forState:UIControlStateSelected];
                self.insteadView.userInteractionEnabled = NO;
                NSLog(@"锁定");
            }else
            {
                self.insteadView.userInteractionEnabled = YES;
                NSLog(@"解锁");
            }
        }
            break;
        case 8:
        {//角度更换
            NSLog(@"角度更换");
        }
            break;
        case 9:
        {//删除
            NSLog(@"删除");
            button.enabled = NO;
           // NSLog(@"%ld",self.insteadView.tag);
            if (_newImageViewIndex>=0) {
                _newImageViewIndex--;
            }
            int i=0;
            for (UIView *v in self.materialEditView.subviews) {
                if ([v isEqual:self.insteadView]) {
                    //将图层大的减1
                    if ((i+1) < self.tapDataArr.count-1) {
                        MJProductModel *model2 = self.tapDataArr[i+1];
                        model2.layer = i;
                        NSLog(@"j_%d model2layer%d",i,model2.layer);
                    }
                }
                i++;
            }
            if (self.viewArr.count>0) {
                 [self.viewArr removeObjectAtIndex:self.insteadView.tag];
            }
            
            if (self.tapDataArr.count>0) {
               
                [self.tapDataArr removeObjectAtIndex:self.insteadView.tag];
                
                for (UIView *view in self.viewArr) {
                    if (view.tag > self.insteadView.tag) {
                        //NSLog(@"%ld",self.insteadView.tag);
                        view.tag = view.tag-1;
                    }
                }
  
            }
            
            [self.insteadView removeFromSuperview];
            
        }
            break;
        case 10:
        {//显示与隐藏
            NSLog(@"显示与隐藏");
            button.selected = !button.isSelected;
            if (button.selected) {
                NSLog(@"隐藏");
                //  [self.view addSubview:self.bottomView];
                
                [UIView animateWithDuration:0.5 animations:^{
                    [self.bottomView setFrame:CGRectMake(Screen_Width, 114, collectionWidth, Screen_Height-114)];
                    [button setFrame:CGRectMake(Screen_Width-37, Screen_Height/2-24, 32, 48)];
                    [self.materialEditView setFrame:CGRectMake(150, 74+15, Screen_Width-collectionWidth-30, Screen_Height-150)];
                    [_storeProductBtn setFrame:CGRectMake(Screen_Width, 20, collectionWidth/2-1, 44)];
                    [_myProductBtn setFrame:CGRectMake(Screen_Width+collectionWidth/2+1, 20, collectionWidth/2-1, 44)];
                    [self.categoryScrollView setFrame:CGRectMake(Screen_Width, 66, collectionWidth,46)];
                    [self.secondCategoryScrollView setFrame:CGRectMake(Screen_Width, 66, collectionWidth,46)];
                }];
            }else
            {
                NSLog(@"显示");
                [UIView animateWithDuration:0.5 animations:^{
                    //改变底部view的frame
                    [self.bottomView setFrame:CGRectMake(Screen_Width-collectionWidth, 114, collectionWidth, Screen_Height-114)];
                    //改变button本身的frame
                    [button setFrame:CGRectMake(Screen_Width-collectionWidth-37 , Screen_Height/2-24, 32, 48)];
                    [_storeProductBtn setFrame:CGRectMake(self.bottomView.x, 20,collectionWidth/2-1 , 44)];
                    [_myProductBtn setFrame:CGRectMake(collectionWidth/2+1+self.bottomView.x, 20, collectionWidth/2-1, 44)];
                    [_categoryScrollView setFrame:CGRectMake(Screen_Width-collectionWidth, 66, collectionWidth,46)];
                    [_secondCategoryScrollView setFrame:CGRectMake(Screen_Width-collectionWidth, 66, collectionWidth,46)];
                    //改变选材编辑视图的frame
                    //[self.materialEditView setFrame:CGRectMake(10, 200, 800, Screen_Height-320)];
                }completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.25 animations:^{
                        [self.materialEditView setFrame:CGRectMake(13, 74, Screen_Width-collectionWidth-30, Screen_Height-150)];
                    }];
                    
                }];
                
            }

        }
            break;
        case 13:
        {//遮盖点击
            
            NSLog(@"遮盖点击");
            [self.coverButton removeFromSuperview];
            self.materialMatchFileView.hidden = YES;
        }
            break;
        case 14:
        {//相机点击
            NSLog(@"相机点击");
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            
            // [picker supportedInterfaceOrientations];
            picker.delegate = self;
            picker.allowsImageEditing = YES;
            picker.modalPresentationStyle = UIModalPresentationFormSheet;
            picker.sourceType =  UIImagePickerControllerSourceTypeCamera;
            [self presentModalViewController:picker animated:YES];
        }
        default:
            break;
    }
}

#pragma mark - 商城商品-我的商品
- (void)selectCategory:(UIButton *)button
{
    _storeProductBtn.selected = NO;
    _selectBtn.selected = NO;
    button.selected = YES;
    _selectBtn = button;
    switch (button.tag) {
        case 11:
        {
            //商城商品
            NSLog(@"商城商品");
            [self.secondCategoryScrollView removeFromSuperview];
            [self.view addSubview:self.categoryScrollView];
            _myProductBtn.selected = NO;
        
        }
            break;
        case 12:
        {
            //我的商品
            NSLog(@"我的商品");
            [self.categoryScrollView removeFromSuperview];
            [self.view addSubview:self.secondCategoryScrollView];
            _storeProductBtn.selected = NO;
           
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.matchDataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MJMaterialMatchCell *cell = (MJMaterialMatchCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (self.matchDataArr.count > 0) {
        MJProductModel *model = self.matchDataArr[indexPath.row];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_200_200.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        cell.imgView.tag = indexPath.row;
        [cell.imgView addGestureRecognizer:tap];
        cell.imgView.contentMode = UIViewContentModeScaleAspectFill;
        cell.imgView.userInteractionEnabled = YES;
        [cell.imgView sd_setImageWithURL:url placeholderImage:nil];
    }
    return cell;
}

#pragma mark  <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
    //    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
    //        return model.imageSize;
    //    }
    return CGSizeMake(100, 110);
}

#pragma mark 图片选择器的代理
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    //  NSLog(@"%@",info);
    //获取修改后的图片
    UIImage *editedImg = info[UIImagePickerControllerEditedImage];
    
    //更改cell里的图片
    [_materialEditView setImage:editedImg];
    _editViewImageID = 100;
    //移除图片选择的控制器
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)dealloc
{
    [MJNotificationCenter removeObserver:self];
}

#pragma mark - 手势代理
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
//    if (gestureRecognizer == _editViewPan || gestureRecognizer ==_editViewPinch) {
//       return  NO;
//    }
    return YES;
}

@end
