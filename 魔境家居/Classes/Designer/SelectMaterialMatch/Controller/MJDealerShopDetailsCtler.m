//
//  MJDealerShopDetailsCtler.m
//  魔境家居
//
//  Created by lumingliang on 16/2/2.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDealerShopDetailsCtler.h"
#import "MJUIClassTool.h"
#import "GMDCircleLoader.h"
#import "UIImageView+WebCache.h"
#import "MJNetworkTool.h"
#import "MJRefresh.h"
#import "MJExtension.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "MJNewProductsCell.h"
#import "MJGoodsDropdownView.h"
#import "MJProductListModel.h"
#import "MJSecondProductDetailsCtler.h"
#import "MJBrandDetailModel.h"
#import "MJDealerListModel.h"
#import "MJDealerListCell.h"
#import "MJProductGroupDetailsCtler.h"
#import "MJSecondProductDetailsCtler.h"
#import "MJMatchUserDataModel.h"
#import "BaiduMapCtrller.h"
#import "AppDelegate.h"
#import "MJSetHotProductView.h"
#import "MJDBManager.h"

#define kNoteViewWidth 984
#define kNoteViewHeight 728
//弹出时间
#define kNoteViewShowTime 0.5

@interface MJDealerShopDetailsCtler ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>

@property (nonatomic, strong) UIScrollView *categoryScrollView;
/* collectionView*/
@property (nonatomic,strong) UICollectionView *collectionView;
/* 经销商所属collectionView*/
@property (nonatomic,strong) UICollectionView *dealerCollectionView;

@property (nonatomic, strong) NSMutableArray *productListArr;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
@property (nonatomic, weak) UIView *categoryView;
/* 弹出视图*/
@property (nonatomic,strong) MJGoodsDropdownView *dropdownView;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
/* 热销按钮*/
@property (nonatomic, strong) UIButton *hotProductsBtn;
/* 最新上市按钮*/
@property (nonatomic, strong) UIButton *newestProductsBtn;
/* 全部商品按钮*/
@property (nonatomic, strong) UIButton *allProductsBtn;
/* 经销商按钮*/
@property (nonatomic, strong) UIButton *dealerProductsBtn;
/* 选中按钮*/
@property (nonatomic, strong) UIButton *selectBtn;
/* 热销推荐界面*/
@property (nonatomic, strong) MJSetHotProductView *setHotProductView;
@property (nonatomic, strong) UIScrollView *topScrollView;
@property (nonatomic, strong) MJChatView *noteView;
@property (nonatomic, strong) UIView *coverView;
@end

@implementation MJDealerShopDetailsCtler
static NSString * const reuseIdentifier = @"collectionCell";
static NSString * const dealerReuseIdentifier = @"dealerCollectionCell";

#pragma mark - 在线联系客服
- (MJChatView *)noteView
{
    if (_noteView == nil) {
        _noteView =[MJChatView shareInstance];
        _noteView.backgroundColor = [UIColor blackColor];
        _noteView.tag =200;
        
        __block MJDealerShopDetailsCtler *ctl = self;
        
        _noteView.close= ^{
            [UIView animateWithDuration:0.5 animations:^{
                [ctl.noteView setFrame:CGRectMake(20, Screen_Height, kNoteViewWidth, kNoteViewHeight)];
            }completion:^(BOOL finished) {
                [ctl.noteView removeFromSuperview];
                [ctl.coverView removeFromSuperview];
                ctl.coverView = nil;
            }];
            
        };
        
    }
    
    return _noteView;
}

- (UIView *)coverView
{
    if (!_coverView) {
        _coverView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Screen_Width/*-kMoreViewWidth*/, Screen_Height)];
        _coverView.backgroundColor = [UIColor blackColor];
        _coverView.alpha = 0.5;
        
        // [self.view addSubview:_coverView];
    }
    return _coverView;
}

#pragma mark - 热销滑动底部视图
-(UIScrollView *)topScrollView
{
    if (!_topScrollView) {
        _topScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 304, Screen_Width, Screen_Height-304)];
        _topScrollView.contentSize = CGSizeMake(0, Screen_Height-114);
        _topScrollView.showsVerticalScrollIndicator = NO;
//        _topScrollView.delegate = self;
//        _topScrollView.pagingEnabled = YES;
        [self.view addSubview:_topScrollView];
    }
    return _topScrollView;
}
#pragma mark - 热销推荐商品设置
- (MJSetHotProductView *)setHotProductView
{
    if (_setHotProductView == nil) {
        _setHotProductView = [MJSetHotProductView hotView];
        _setHotProductView.backgroundColor = RGB(200, 200, 200);
        _setHotProductView.frame = CGRectMake(0, 0, Screen_Width, Screen_Height-114);
        [_setHotProductView setButtonHidden:YES];
        [_setHotProductView setCoverHidden:NO];
        //_setHotProductView.hidden = YES;
        __weak MJDealerShopDetailsCtler *ctl = self;
        _setHotProductView.editImage = ^(NSInteger index){
           // ctl.index = index;
            
            //  NSLog(@"%ld点击",ctl.index);
            switch (index) {
                case 7:
                {//组合跳转
                    MJProductGroupDetailsCtler *group = [[MJProductGroupDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJMatchProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        group.idNum = model.idNum;
                        
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:group animated:YES];
                        }
                        
                    }
                    
                }
                    break;
                case 8:
                {
                    MJProductGroupDetailsCtler *group = [[MJProductGroupDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJMatchProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        group.idNum = model.idNum;
                        
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:group animated:YES];
                        }
                        
                    }
                }
                    break;
                case 9:
                {//单品跳转
                    MJSecondProductDetailsCtler *detail = [[MJSecondProductDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        detail.idNum = model.idNum;
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:detail animated:YES];
                        }
                    }
                }
                    break;
                case 10:
                {
                    MJSecondProductDetailsCtler *detail = [[MJSecondProductDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        detail.idNum = model.idNum;
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:detail animated:YES];
                        }
                    }
                }
                    break;
                case 11:
                {
                    MJSecondProductDetailsCtler *detail = [[MJSecondProductDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        detail.idNum = model.idNum;
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:detail animated:YES];
                        }
                    }
                }
                    break;
                case 12:
                {
                    MJProductDetailsCtler *detail = [[MJSecondProductDetailsCtler alloc] init];
                    
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        
                        detail.idNum = model.idNum;
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:detail animated:YES];
                        }
                    }
                    
                }
                    break;
                case 13:
                {
                    MJProductDetailsCtler *detail = [[MJSecondProductDetailsCtler alloc] init];
                    if (ctl.setHotProductView.spreadDataArr.count > 0) {
                        MJProductModel *model = ctl.setHotProductView.spreadDataArr[index - 7];
                        detail.idNum = model.idNum;
                        if (model.idNum > 0) {
                            [ctl.navigationController pushViewController:detail animated:YES];
                        }
                    }
                }
                    break;
                default:
                    break;
            }
            
        };
        //_setHotProductView.hidden = YES;
        [self.topScrollView addSubview:_setHotProductView];
    }
    return _setHotProductView;
}

- (NSMutableArray *)productListArr
{
    if (_productListArr == nil) {
        _productListArr = [NSMutableArray array];
    }
    return _productListArr;
}

#pragma mark - collectionView
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        CGFloat inset = 2;
        layout.sectionInset = UIEdgeInsetsMake(inset, inset, inset, inset);
          layout.minimumColumnSpacing = inset;
        layout.minimumInteritemSpacing = inset;
        layout.columnCount = 4;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 304, Screen_Width, Screen_Height-294) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = RGB(225, 225, 225);
        [_collectionView registerClass:[MJNewProductsCell class] forCellWithReuseIdentifier:reuseIdentifier];
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

#pragma mark - dealerCollectionView
- (UICollectionView *)dealerCollectionView
{
    if (!_dealerCollectionView) {
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        CGFloat inset = 10;
        layout.sectionInset = UIEdgeInsetsMake(inset, inset, inset, inset);
        layout.minimumColumnSpacing = inset;
        layout.minimumInteritemSpacing = inset;
        layout.columnCount = 3;
        _dealerCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 304, Screen_Width, Screen_Height-294) collectionViewLayout:layout];
      
        _dealerCollectionView.delegate = self;
        _dealerCollectionView.dataSource = self;
        _dealerCollectionView.showsVerticalScrollIndicator = YES;
        _dealerCollectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _dealerCollectionView.alwaysBounceVertical = YES;
        _dealerCollectionView.backgroundColor = RGB(255, 255, 255);
        [_dealerCollectionView registerClass:[MJDealerListCell class] forCellWithReuseIdentifier:dealerReuseIdentifier];
        [self.view addSubview:_dealerCollectionView];
    }
    return _dealerCollectionView;
}


#pragma mark - 全部商品下拉弹窗
- (MJGoodsDropdownView *)dropdownView
{
    if (_dropdownView == nil) {
        _dropdownView = [MJGoodsDropdownView dropdown];
        _dropdownView.frame = CGRectMake(Screen_Width/2-200, 294, Screen_Width/2+200, 50);
        _dropdownView.allGoodsBtn.selected = YES;
        _dropdownView.alpha = 0.5;
        __weak MJDealerShopDetailsCtler *ctl = self;
        
        //商品类型列表点击
        _dropdownView.click = ^(NSInteger index){
            NSString *brandID = [NSString stringWithFormat:@"%ld",ctl.model.brandData.idNum];
            NSDictionary *dict = @{@"productTypeID" : [NSString stringWithFormat:@"%ld",index+1-30],@"pageNumber" : [NSString stringWithFormat:@"%ld",ctl.pageNumber],@"pageSize" :@8,@"brandID" : brandID,@"type" : @"0"};
         //   ctl.dropdownIndex = index+1-30;
            //NSLog(@"%@",dict);
            switch (index-30) {
                case 0:
                {
                    
                    [ctl dropdownClickIndex:0 dictionary:dict];
                    // NSLog(@"%@",dict);
                    break;
                }
                case 1:
                {
                    
                    [ctl dropdownClickIndex:1 dictionary:dict];
                    break;
                }
                case 2:
                {
                    [ctl dropdownClickIndex:2 dictionary:dict];
                    break;
                }
                case 3:
                {
                    [ctl dropdownClickIndex:3 dictionary:dict];
                    break;
                }
                case 4:
                {
                    [ctl dropdownClickIndex:4 dictionary:dict];
                    break;
                }
                case 5:
                {
                    [ctl dropdownClickIndex:5 dictionary:dict];
                    break;
                }
                case 6:
                {
                    [ctl dropdownClickIndex:6 dictionary:dict];
                    break;
                }
                case 7:
                {
                    [ctl dropdownClickIndex:7 dictionary:dict];
                    break;
                }
                case 8:
                {
                    NSDictionary *dict1 = @{@"productTypeID" : @"0",@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)ctl.pageNumber],@"brandID" : brandID,@"pageSize" :@8,@"type" : @"0"};
                    [ctl dropdownClickIndex:8 dictionary:dict1];
                    break;
                }
                default:
                    break;
            }
        };
    }
    return _dropdownView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
}

- (void)initUI
{
    NSString *brandName = self.model.brandData.name;
    UILabel *titleLb = [MJUIClassTool createLbWithFrame:CGRectMake(0, 7, 1000, 44) title:[NSString stringWithFormat:@"%@",brandName] aliment:NSTextAlignmentCenter color:[UIColor blackColor] size:25];
    CGSize size = [MJUIClassTool caculateText:brandName fontSize:25 maxSize:CGSizeMake(1000, 44)];
    titleLb.size = size;
    self.navigationItem.titleView = titleLb;
    
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *topView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, Screen_Width, 190)];
    topView.image = [UIImage imageNamed:@"product_shop_topbackground"];
    [self.view addSubview:topView];
    
    //logoView
    UIImageView *logoView = [MJUIClassTool createImgViewWithFrame:CGRectMake(Screen_Width/2-48, 30, 96, 96) imageName:nil];
    logoView.layer.cornerRadius = 48;
    logoView.clipsToBounds = YES;
    NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_200_200.%@",mainURL,self.model.brandData.logoSuffix,self.model.brandData.logoName,self.model.brandData.logoName,self.model.brandData.logoSuffix]];
    [logoView sd_setImageWithURL:bgUrl placeholderImage:nil];
    [topView addSubview:logoView];
    
    //brandname
    UILabel *nameLb = [MJUIClassTool createLbWithFrame:CGRectMake(0, 215,Screen_Width , 30) title:self.model.brandData.name aliment:NSTextAlignmentCenter color:[UIColor whiteColor] size:17];
    CGSize size1 = [MJUIClassTool caculateText:brandName fontSize:25 maxSize:CGSizeMake(1000, 44)];
    nameLb.center = CGPointMake(self.view.center.x,215) ;
    NSLog(@"%@",NSStringFromCGSize(size1));
   // nameLb.size = size1;
    [self.view addSubview:nameLb];
    
    //分类选择
    CGFloat btnW = 130;
    UIView *categoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 254, Screen_Width, 50)];
    categoryView.backgroundColor = RGB(251, 251, 251);
    _categoryView = categoryView;
    [self.view addSubview:categoryView];
    
    NSArray *btnNameArr = @[@"热销商品", @"最新上市", @"全部商品", @"经销商"];
    NSMutableArray *btnArr = [NSMutableArray array];
    for (int i=0; i<4; i++) {
        UIButton *btn = [MJUIClassTool createBtnWithFrame:CGRectMake(Screen_Width/2-btnW*2+btnW*i, 10, btnW, 30) title:btnNameArr[i] image:nil target:self action:@selector(categoryBtnClick:)];
        [btn setTitleColor:RGB(251, 43, 8) forState:UIControlStateSelected];
        [btnArr addObject:btn];
        btn.tag = i+1;
        [categoryView addSubview:btn];
    }
    self.hotProductsBtn = btnArr[0];
    self.newestProductsBtn = btnArr[1];
    self.allProductsBtn = btnArr[2];
    self.dealerProductsBtn = btnArr[3];
    
    for (int i=0; i<3; i++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(Screen_Width/2-btnW+btnW*i, 12, 1, 26)];
        view.backgroundColor = [UIColor blackColor];
        [categoryView addSubview:view];
    }
    
    _categoryScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(Screen_Width/2-2*btnW+28+btnW*3, 294, btnW-56, 2)];
    _categoryScrollView.backgroundColor = RGB(251, 43, 8);
   
    [self.view addSubview:_categoryScrollView];
    
    
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSString *brandID = [NSString stringWithFormat:@"%ld",(long)self.model.brandData.idNum];
    NSDictionary *dict = @{@"brandID" : brandID};
    if (self.model.pushIndex == 2) {
        self.dealerProductsBtn.selected = YES;
        [self.dropdownView removeFromSuperview];
        [self.noDataView removeFromSuperview];
        self.allProductsBtn.selected = NO;
        self.collectionView.hidden = YES;
        self.dealerCollectionView.hidden = NO;
        if ([self isConnectToNet]) {
            [net getMoJingURL:MJBrandDealerList parameters:dict success:^(id obj) {
                [self.productListArr removeAllObjects];
                NSDictionary *dict = obj[@"data"];
                MJDealerListModel *model = [MJDealerListModel mj_objectWithKeyValues:dict];
                self.productListArr = [MJDealerListDataModel mj_objectArrayWithKeyValuesArray:model.dealerList];
                if (self.productListArr.count == 0) {
                    UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-40, 147, 260, 30) title:@"对应品牌还没有设置经销商" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                    [self addNoDataViewWith:nil];
                    [_noDataView addSubview:lb];
                }
                [self.dealerCollectionView reloadData];
                //   NSLog(@"%ld",_productListArr.count);
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        }
    }else if (self.model.pushIndex == 1)
    {
        self.allProductsBtn.selected = YES;
        self.dealerProductsBtn.selected = NO;
        [self.noDataView removeFromSuperview];
        self.collectionView.hidden = NO;
        self.dealerCollectionView.hidden = YES;
        [self.productListArr removeAllObjects];
       [_categoryScrollView setFrame:CGRectMake(Screen_Width/2-2*130+28+130*(3-1), 294, 130-56, 2)];
        [self.view addSubview:self.dropdownView];
        NSDictionary *dict = @{@"productTypeID" : [NSString stringWithFormat:@"%d",0],@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"brandID" : brandID,@"pageSize" :@8,@"type" : @"0"};
        [self dropdownClickIndex:0 dictionary:dict];
        //热销商品
        NSLog(@"热销商品");
    }
    // NSLog(@"%@",dict);
    //没有热销数据的通知
    [MJNotificationCenter addObserver:self selector:@selector(noSpreadData) name:@"noSpreadData" object:nil];
}

- (void)noSpreadData
{
    UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-38, 147, 260, 30) title:@"对应品牌没有设置热销商品" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
    [self addNoDataViewWith:nil];
    [_noDataView addSubview:lb];
    _setHotProductView.hidden = YES;
}

#pragma mark - 总体分类选择
- (void)categoryBtnClick:(UIButton *)button
{
    self.dealerProductsBtn.selected = NO;
    self.selectBtn.selected = NO;
    button.selected = YES;
    self.selectBtn = button;
    [UIView animateWithDuration:0.25 animations:^{
         [_categoryScrollView setFrame:CGRectMake(Screen_Width/2-2*130+28+130*(button.tag-1), 294, 130-56, 2)];
    }];
   
    __weak MJDealerShopDetailsCtler *ctl = self;
    self.pageNumber = 1;
    NSString *brandID = [NSString stringWithFormat:@"%ld",(long)ctl.model.brandData.idNum];
    switch (button.tag) {
        case 1:
        {//热销商品
           NSLog(@"热销商品");
           [self.dropdownView removeFromSuperview];
            self.collectionView.mj_footer = nil;
            self.collectionView.mj_header = nil;
            self.allProductsBtn.selected = NO;
            self.collectionView.hidden = YES;
            self.setHotProductView.hidden = NO;
            self.topScrollView.hidden = NO;
            self.dealerCollectionView.hidden = YES;
           // self.dealerCollectionView.contentSize = CGSizeMake(0, Screen_Height-114);
            [self.productListArr removeAllObjects];
            [_noDataView removeFromSuperview];
            NSDictionary *dict = @{@"brandID" : brandID};
           // MJNetworkTool *net = [MJNetworkTool shareInstance];
  
            [self.setHotProductView setImageWithDict:dict];
            
        }
            break;
        case 2:
        {//最新上市
            NSLog(@"最新上市");
            [self.dropdownView removeFromSuperview];
            [_noDataView removeFromSuperview];
            self.allProductsBtn.selected = NO;
            self.setHotProductView.hidden = YES;
            self.topScrollView.hidden = YES;
            self.collectionView.hidden = NO;
            self.dealerCollectionView.hidden = YES;
            //  if (button.isSelected) {
            NSDictionary *footerDict = @{@"productTypeID":[NSString stringWithFormat:@"%d",0],@"type" : @"3",@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"pageSize" :@8,@"brandID" : brandID};
            self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
                [ctl loadNewDataWithPara:footerDict];
            }];
            
            // 马上进入刷新状态
            [self loadingData];
            [self.collectionView.mj_header beginRefreshing];
        }
            break;
        case 3:
        {//全部商品
            NSLog(@"全部商品");
            self.collectionView.hidden = NO;
            self.dealerCollectionView.hidden = YES;
            self.setHotProductView.hidden = YES;
            self.topScrollView.hidden = YES;
            [_noDataView removeFromSuperview];
            [self.productListArr removeAllObjects];
           // button.selected = !button.isSelected;
            //if (button.isSelected == YES) {
                [self.view addSubview:self.dropdownView];
                // self.dropdownView.allGoodsBtn.selected = YES;
                NSDictionary *dict = @{@"productTypeID" : [NSString stringWithFormat:@"%d",0],@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)ctl.pageNumber],@"brandID" : brandID,@"pageSize" :@8,@"type" : @"0"};
                [self dropdownClickIndex:0 dictionary:dict];
               // [self.collectionView reloadData];
//            }else
//            {
//                [self.dropdownView removeFromSuperview];
//            }

        }
            break;
        case 4:
        {//经销商
            NSLog(@"经销商");
           [self.dropdownView removeFromSuperview];
            [self.noDataView removeFromSuperview];
            self.allProductsBtn.selected = NO;
            self.setHotProductView.hidden = YES;
            self.collectionView.hidden = YES;
            self.topScrollView.hidden = YES;
            self.dealerCollectionView.hidden = NO;
            MJNetworkTool *net = [MJNetworkTool shareInstance];
            NSDictionary *dict = @{@"brandID" : brandID};
           // NSLog(@"%@",dict);
            if ([self isConnectToNet]) {
                [net getMoJingURL:MJBrandDealerList parameters:dict success:^(id obj) {
                    [self.productListArr removeAllObjects];
                    NSDictionary *dict = obj[@"data"];
                    MJDealerListModel *model = [MJDealerListModel mj_objectWithKeyValues:dict];
                    self.productListArr = [MJDealerListDataModel mj_objectArrayWithKeyValuesArray:model.dealerList];
                    if (self.productListArr.count == 0) {
                        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-40, 147, 260, 30) title:@"对应品牌还没有设置经销商" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                        [ctl addNoDataViewWith:nil];
                        [_noDataView addSubview:lb];
                    }
                    [self.dealerCollectionView reloadData];
                 //   NSLog(@"%ld",_productListArr.count);
                } failure:^(NSError *error) {
                    NSLog(@"%@",error);
                }];
            }
        }
            break;
        default:
            break;
    }
}


#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_circulateBackgroungView];
    self.categoryView.userInteractionEnabled = NO;
    self.dropdownView.userInteractionEnabled = NO;
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    self.categoryView.userInteractionEnabled = YES;
    self.dropdownView.userInteractionEnabled = YES;
    [self.circulateBackgroungView removeFromSuperview];
    //[GMDCircleLoader hideFromView:self.view animated:YES];
    
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 下拉弹窗点击封装
- (void)dropdownClickIndex:(NSInteger)index dictionary:(NSDictionary *)dict
{
    //self.setHotProductView.hidden = YES;
    __weak MJDealerShopDetailsCtler *ctl = self;
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [ctl loadNewDataWithPara:dict];
    }];
    [self loadingData];
    [self.collectionView.mj_header beginRefreshing];
    //[self.dropdownView removeFromSuperview];
    [self.collectionView reloadData];
//    self.allGoodsButton.selected = YES;
//    self.collectionView.hidden = NO;
//    self.bottomBigView.hidden = YES;
    [self.view addSubview:self.dropdownView];
}

- (void)addRefreshWithDict:(NSDictionary *)dict
{
    //self.setHotProductView.hidden = YES;
    __weak MJDealerShopDetailsCtler *ctl = self;
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [ctl loadNewDataWithPara:dict];
    }];
    [self loadingData];
    [self.collectionView.mj_header beginRefreshing];
    //[self.dropdownView removeFromSuperview];
    [self.collectionView reloadData];
//    self.allGoodsButton.selected = NO;
//    self.collectionView.hidden = NO;
//    self.bottomBigView.hidden = YES;
    [self.dropdownView removeFromSuperview];
}

#pragma mark - 下拉加载新数据
- (void)loadNewDataWithPara:(NSDictionary *)dict
{
    self.pageNumber = 1;
    
    NSString *type = [dict objectForKey:@"type"];
    //获取商品列表数据
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    __weak MJDealerShopDetailsCtler *ctl = self;
    [self.noDataView removeFromSuperview];
    [self.productListArr removeAllObjects];
    if ([self isConnectToNet]) {
        
        [net getMoJingURL:MJProductList parameters:dict success:^(id obj) {
            //     NSLog(@"%@",dict);
            MJProductListModel *listModel = obj;
            MJProductListDataModel *model = listModel.data;
            self.pageCount = model.pageCount;
            // 把字典数组转换成模型数组
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
            
            // for (MJProductModel *m in ctl.productListArr) {
            //                MJBrandDataModel *brand = m.brandData;
            //                MJCreatedDateModel *create = m.createdDate;
            //                MJModifiedDateModel *modify = m.modifiedDate;
            //   NSLog(@"%ld %@ %@ %@",brand.idNum,m.userID,create.time,modify.day);
            //  }
            
            [ctl.productListArr addObjectsFromArray:arr];
            
            if (ctl.productListArr.count == 0) {
                if ([type isEqualToString:@"3"]) {
                    UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-38, 147, 260, 30) title:@"对应品牌没有设置新品上市" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                    [ctl addNoDataViewWith:nil];
                    [_noDataView addSubview:lb];
                }else{
                    UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-5, 147, 180, 30) title:@"没有搜到相关的商品" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                    [ctl addNoDataViewWith:nil];
                    [_noDataView addSubview:lb];
                }
            }
            
         //   NSLog(@"%ld",ctl.productListArr.count);
            [ctl stopCircleLoader];
            
            if (ctl.productListArr.count >= 8) {
                ctl.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                    // 进入刷新状态后会自动调用这个block
                    [ctl loadMoreData:dict];
                }];
                // 默认先隐藏footer
                ctl.collectionView.mj_footer.hidden = YES;
            }else{
                ctl.collectionView.mj_footer = nil;
            }
            
            [ctl.collectionView reloadData];
            [ctl.collectionView.mj_header endRefreshing];
            [ctl.view endEditing:YES];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [ctl stopCircleLoader];
            [ctl.collectionView.mj_header endRefreshing];
        }];
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_header endRefreshing];
    }
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    // [self.productListArr removeAllObjects];
    //获取商品列表数据
    _pageNumber++;
    //  NSLog(@"%ld",_pageNumber);
    NSString *str = [dict objectForKey:@"productTypeID"];
    NSString *brandID = [dict objectForKey:@"brandID"];
    NSString *type = [dict objectForKey:@"type"];
    //NSLog(@"%@ %@ %@",str,brandID,type);
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSDictionary *moreDict = @{@"productTypeID" : str,@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)_pageNumber],@"pageSize" : @8,@"brandID" : brandID,@"type" : type};
    __weak MJDealerShopDetailsCtler *ctl = self;
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            [net getMoJingURL:MJProductList parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                MJProductListModel *listModel = obj;
                NSLog(@"%@",listModel.mj_keyValues);
                MJProductListDataModel *model = listModel.data;
                // 把字典数组转换成模型数组
                NSMutableArray *arr = [NSMutableArray array];
                arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
                
                //                for (MJProductModel *m in ctl.productListArr) {
                //                    MJBrandDataModel *brand = m.brandData;
                //                    MJCreatedDateModel *create = m.createdDate;
                //                    MJModifiedDateModel *modify = m.modifiedDate;
                //                    //  NSLog(@"%ld %@ %@ %@",brand.idNum,m.userID,create.time,modify.day);
                //                }
                [ctl.productListArr addObjectsFromArray:arr];
                //   NSLog(@"%ld",ctl.productListArr.count);
                [UIView animateWithDuration:0.1 animations:^{
                    [ctl.collectionView reloadData];
                }];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            }];
            
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
    }
}


#pragma mark - 返回
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
     self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
}

- (NSArray*)timeSort:(NSArray* )obj
{
    
    
    NSComparator cmptr = ^(id obj1, id obj2){
        Contack* contack1 = (Contack*)obj1;
        Contack* contack2 = (Contack*)obj2;
        if ([contack1.lasttime doubleValue]> [contack2.lasttime doubleValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        if ([contack1.lasttime doubleValue] < [contack2.lasttime doubleValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    NSArray *array = [[NSArray alloc]init];
    array =[obj sortedArrayUsingComparator:cmptr];
    
    
    
    return array;
}

#pragma mark  <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.productListArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
   
    if ([collectionView isEqual:self.dealerCollectionView]) {
        MJDealerListCell *cell1 = [collectionView dequeueReusableCellWithReuseIdentifier:dealerReuseIdentifier forIndexPath:indexPath];
        if (self.productListArr.count > 0) {
            MJDealerListDataModel *model = self.productListArr[indexPath.row];
           
            MJNetworkTool *net = [MJNetworkTool shareInstance];
            
            NSDictionary *dict = @{@"ids":[NSString stringWithFormat:@"%ld",model.userID]};
            
           
            [net getMoJingURL:MJUsersDetailData parameters:nil success:^(id obj) {
                MJMatchUserDataModel *usersDataModel = obj[0];
                
                [cell1.photoImageView sd_setImageWithURL:[NSURL URLWithString:usersDataModel.avatarPic] placeholderImage:nil];
                
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
            

            cell1.dealerShopNameLabel.text = model.shopName;
            cell1.dealerPhoneNumLabel.text = model.phoneNumber;
            cell1.dealerAddressLabel.text = model.address;
            cell1.contentView.backgroundColor = [UIColor whiteColor];
            cell1.dealerDataModel = model;
             __weak MJDealerShopDetailsCtler *ctl = self;
            cell1.cb = ^(NSInteger index){
                switch (index) {
                    case 100:
                    {//在线联系客服
                        NSLog(@"在线联系客服");
                        [ctl.view addSubview:self.coverView];
                        NSString *myID = [MJUserDefaults objectForKey:@"userID"];
                        MJDBManager* managerData = [MJDBManager shareInstanceWithCoreDataID:myID];
                        
                        NSString* useId =[NSString stringWithFormat:@"%li",model.idNum];
                        NSArray *objX = [[NSMutableArray alloc]initWithArray:[managerData query:@"Contack" predicate:nil]];
                        NSArray *obj = [ctl timeSort:objX];
                        
                        NSMutableArray *localContact = [[NSMutableArray alloc]init];
                        
                        for (Contack* contack in obj) {
                            [localContact addObject:[contack.contack_id stringValue]];
                        }
                        
                        
                        if ([localContact indexOfObject:useId]== NSNotFound)
                        {
                            AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
                            session.responseSerializer = [AFHTTPResponseSerializer serializer];
                            NSMutableDictionary *contackDic1 = [[NSMutableDictionary alloc]init];
                            
                            [contackDic1 setObject:useId forKey:@"ids"];
                            
                            [session GET:CHAT_CONTACK parameters:contackDic1 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                                
                                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                                NSArray *contackInfo =[dict objectForKey:@"data"];
                                
                                for (NSDictionary* dic in contackInfo) {
                                    
                                    
                                    
                                    NSString *headImgUrl = [dic objectForKey:@"avatarPic"];
                                    NSString *name = model.name;
                                    NSNumber *contack_id = [dic objectForKey:@"id"];
                                    
                                    
                                    Contack *contack1 = (Contack *)[managerData createMO:@"Contack"];
                                    contack1.image =headImgUrl;
                                    contack1.nickname = name;
                                    contack1.contack_id = contack_id;
                                    
                                    [managerData saveManagerObj:contack1];
                                    
                                }
                                NSArray *obj = [ctl timeSort:[[NSMutableArray alloc]initWithArray:[managerData query:@"Contack" predicate:nil]]];
                                [self.noteView showWithIndexSelect:[obj count]-1];
                                
                                
                            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                NSLog(@"添加联系人信息失败%@",error);
                                
                            }];
                        }else//联系人已经存在
                        {
                            
                            NSUInteger indexSelect = [localContact indexOfObject:useId];
                            
                            [self.noteView showWithIndexSelect:indexSelect];
                            
                            
                        }
                    }
                        break;
                    case 101:
                    {
                        NSLog(@"跳转地图");
                        BaiduMapCtrller *map = [[BaiduMapCtrller alloc]init];
                        map.mapDict = model.mj_keyValues;
                        NSLog(@"%@",model.mj_keyValues);
                        [ctl.navigationController pushViewController:map animated:YES];
                    }
                        break;
                    default:
                        break;
                }

            };
        }
        return cell1;
    }
    MJNewProductsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (self.productListArr.count > 0) {
        MJProductModel *model = self.productListArr[indexPath.row];
        
        NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
        [cell.singleProductImgView  sd_setImageWithURL:bgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //            if (image) {
            //                if (!CGSizeEqualToSize(model.imageSize, image.size)) {
            //                    model.imageSize = image.size;
            //                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            //                }
            //            }
        }];
        if (model.type.length) {
            cell.singleProductName.text = nil;
            cell.singleProductPrice.text = nil;
        }else{
            cell.singleProductName.text = model.name;
            cell.singleProductPrice.text = [NSString stringWithFormat:@"￥%ld",model.price];
        }
        cell.contentView.backgroundColor = [UIColor whiteColor];
        
    }
    
    return cell;
}

#pragma mark  <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.collectionView) {
        MJProductModel *model = self.productListArr[indexPath.row];
        if ([model.type isEqualToString:@"1"]) {
            MJProductGroupDetailsCtler *group = [[MJProductGroupDetailsCtler alloc]init];
            group.idNum = model.idNum;
            [self.navigationController pushViewController:group animated:YES];
        }else
        {
        MJSecondProductDetailsCtler *ctl = [[MJSecondProductDetailsCtler alloc] init];
        
        ctl.idNum = model.idNum;
        
        [self.navigationController pushViewController:ctl animated:YES];
        }
    }else if (collectionView == self.dealerCollectionView)
    {
        
    }
    //NSLog(@"详情点击%ld",indexPath.row);
}


#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
    //    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
    //        return model.imageSize;
    //    }
    if (collectionView == self.collectionView) {
       return CGSizeMake(256, 275);
    }
    return CGSizeMake(315, 188);
}

#pragma mark - noDataView
- (void)addNoDataViewWith:(NSString *)string
{
    _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
    _noDataView.image = [UIImage imageNamed:@"no_data"];
    _noDataView.center = CGPointMake(self.view.center.x, self.view.center.y+50) ;
    
    
    [self.view addSubview:_noDataView];
}

/* 移除通知*/
- (void)dealloc
{
    [MJNotificationCenter removeObserver:self];
}

@end
