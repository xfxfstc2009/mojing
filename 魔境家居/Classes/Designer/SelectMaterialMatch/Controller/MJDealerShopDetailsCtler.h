//
//  MJDealerShopDetailsCtler.h
//  魔境家居
//
//  Created by lumingliang on 16/2/2.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJBaseCtler.h"
#import "MJProductModel.h"
@interface MJDealerShopDetailsCtler : UIViewController

@property (nonatomic, strong) MJProductModel *model;
@end
