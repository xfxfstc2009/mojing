//
//  MJDealerListModel.m
//  魔境家居
//
//  Created by lumingliang on 16/2/15.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDealerListModel.h"

@implementation MJDealerListModel

- (NSDictionary *)objectClassInArray{
    return @{@"dealerList" : [MJDealerListDataModel class]};
}

@end
