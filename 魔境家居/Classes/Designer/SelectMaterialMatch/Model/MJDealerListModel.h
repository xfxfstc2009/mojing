//
//  MJDealerListModel.h
//  魔境家居
//
//  Created by lumingliang on 16/2/15.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJDealerListDataModel.h"
@interface MJDealerListModel : NSObject

@property (nonatomic, strong) NSArray<MJDealerListDataModel *> *dealerList;
@end
