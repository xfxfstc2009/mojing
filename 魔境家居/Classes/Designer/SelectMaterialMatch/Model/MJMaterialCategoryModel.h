//
//  MJMaterialCategoryModel.h
//  魔境家居选材搭配
//
//  Created by lumingliang on 16/1/22.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@class MJMaterialCategoryDataModel,MJMaterialCategoryDataSubModel,MJMaterialCategoryDataThirdModel,MJMaterialBrandListModel,MJMaterialSerieslistModel;
@interface MJMaterialCategoryModel : NSObject

@property (nonatomic, copy) NSString *code;

@property (nonatomic, copy) NSString *errMsg;

@property (nonatomic, assign) BOOL success;

@property (nonatomic, strong) NSArray<MJMaterialCategoryDataModel *> *data;

@property (nonatomic, strong) NSArray<MJMaterialBrandListModel *> *brandList;

@end

@interface MJMaterialCategoryDataModel : NSObject

@property (nonatomic, assign) NSInteger idNum;

@property (nonatomic, assign) NSInteger level;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) NSArray<MJMaterialCategoryDataSubModel *> *sub;

@end

@interface MJMaterialCategoryDataSubModel : NSObject

@property (nonatomic, assign) NSInteger idNum;

@property (nonatomic, assign) NSInteger level;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) NSArray<MJMaterialCategoryDataThirdModel *> *sub;

@end

@interface MJMaterialCategoryDataThirdModel : NSObject

@property (nonatomic, assign) NSInteger idNum;

@property (nonatomic, assign) NSInteger level;

@property (nonatomic, copy) NSString *name;

@end

@interface MJMaterialBrandListModel : NSObject

@property (nonatomic, assign) NSInteger brandId;

@property (nonatomic, strong) NSArray<MJMaterialSerieslistModel *> *seriesList;

@property (nonatomic, copy) NSString *brandName;

@end

@interface MJMaterialSerieslistModel : NSObject

@property (nonatomic, copy) NSString *seriesName;

@property (nonatomic, assign) NSInteger seriesId;

@end

