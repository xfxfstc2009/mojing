//
//  MJDealerListDataModel.m
//  魔境家居
//
//  Created by lumingliang on 16/2/15.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDealerListDataModel.h"

@implementation MJDealerListDataModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"idNum" : @"id",@"desc" : @"description"};
}
@end
