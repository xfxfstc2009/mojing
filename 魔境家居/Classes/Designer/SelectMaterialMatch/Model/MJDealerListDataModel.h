//
//  MJDealerListDataModel.h
//  魔境家居
//
//  Created by lumingliang on 16/2/15.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@interface MJDealerListDataModel : NSObject

@property (nonatomic,copy) NSString *isDeleted ;// 0,
@property (nonatomic,assign) NSInteger guideCount ;// 5,
@property (nonatomic,copy) NSString *nickName ;// 好风景杭州,
@property (nonatomic,copy) NSString *userAvatarURL ;// ,
@property (nonatomic,copy) NSString *userName ;// haofengjinghz,
@property (nonatomic,assign) NSInteger existGuideCount ;// 0,
@property (nonatomic,assign) NSInteger supplierID ;// 157685,
@property (nonatomic,assign) NSInteger userID ;// 158033,
@property (nonatomic,copy) NSString *oldPassword ;// ,
@property (nonatomic,copy) NSString *permission ;// ,
@property (nonatomic,copy) NSString *shopName ;// 杭州新时代家居广场店,
@property (nonatomic,copy) NSString *isNickNameUpdate ;// ,
@property (nonatomic,copy) NSString *type ;// 2,
@property (nonatomic,assign) NSInteger idNum ;// 14,
@property (nonatomic,copy) NSString *provinceName ;// 浙江,
@property (nonatomic,copy) NSString *isTest ;// ,
@property (nonatomic,copy) NSString *cityName ;// 杭州,
@property (nonatomic,copy) NSString *password ;// ,
@property (nonatomic,copy) NSString *address ;// 古墩路808号,
@property (nonatomic,copy) NSString *phoneNumber ;// 13588545856

/*后面增加的属性*/
@property (nonatomic, copy) NSString *imageSuffix;
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, copy) NSString *name;
@end
