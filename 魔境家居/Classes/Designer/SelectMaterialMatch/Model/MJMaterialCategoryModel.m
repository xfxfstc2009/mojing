//
//  MJMaterialCategoryModel.m
//  魔境家居选材搭配
//
//  Created by lumingliang on 16/1/22.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJMaterialCategoryModel.h"

@implementation MJMaterialCategoryModel

- (NSDictionary *)objectClassInArray{
    return @{@"data" : [MJMaterialCategoryDataModel class], @"brandList" : [MJMaterialBrandListModel class]};
}
@end


@implementation MJMaterialCategoryDataModel

- (NSDictionary *)objectClassInArray{
    return @{@"sub" : [MJMaterialCategoryDataSubModel class]};
}

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"idNum" : @"id",@"desc" : @"description"};
}
@end


@implementation MJMaterialCategoryDataSubModel

- (NSDictionary *)objectClassInArray{
    return @{@"sub" : [MJMaterialCategoryDataThirdModel class]};
}

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"idNum" : @"id",@"desc" : @"description"};
}
@end


@implementation MJMaterialCategoryDataThirdModel

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"idNum" : @"id",@"desc" : @"description"};
}
@end


@implementation MJMaterialBrandListModel

- (NSDictionary *)objectClassInArray{
    return @{@"seriesList" : [MJMaterialSerieslistModel class]};
}

@end


@implementation MJMaterialSerieslistModel

@end


