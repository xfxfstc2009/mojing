

//
//  MJMaterialMatchModel.h
//  魔境家居选材搭配
//
//  Created by lumingliang on 16/1/18.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"
@class MJMaterialMatchDataModel;
@interface MJMaterialMatchModel : NSObject

@property (nonatomic, assign) NSInteger totalNum;

@property (nonatomic, copy) NSString *code;

@property (nonatomic, copy) NSString *errMsg;

@property (nonatomic, assign) BOOL success;

@property (nonatomic, assign) NSInteger totalpage;

@property (nonatomic, strong) NSArray<MJMaterialMatchDataModel *> *data;

@end

@interface MJMaterialMatchDataModel : NSObject

@property (nonatomic, copy) NSString *thumbnail;

@property (nonatomic, strong) NSArray<NSString *> *thumbnails;

@property (nonatomic, assign) NSInteger cate3Id;

@property (nonatomic, assign) NSInteger idNum;

@property (nonatomic, assign) NSInteger seriesId;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *buyUrl;

@property (nonatomic, assign) NSInteger cate1Id;

@property (nonatomic, assign) NSInteger price;

@property (nonatomic, assign) NSInteger cate2Id;

@property (nonatomic, copy) NSString *seriesName;

@end

