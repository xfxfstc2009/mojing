//
//  MJMaterialMatchModel.m
//  魔境家居选材搭配
//
//  Created by lumingliang on 16/1/18.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJMaterialMatchModel.h"

@implementation MJMaterialMatchModel

- (NSDictionary *)objectClassInArray{
    return @{@"data" : [MJMaterialMatchDataModel class]};
}

+ (NSDictionary *)replacedKeyFromPropertyName{
    return @{@"idNum" : @"id",@"desc" : @"description"};
}
@end


@implementation MJMaterialMatchDataModel

@end





