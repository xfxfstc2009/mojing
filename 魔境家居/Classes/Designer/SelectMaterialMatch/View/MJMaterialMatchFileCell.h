//
//  MJMaterialMatchFileCell.h
//  魔境家居
//
//  Created by lumingliang on 16/1/30.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJProductModel.h"
@interface MJMaterialMatchFileCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *materialImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic, strong) MJProductModel *model;
@end
