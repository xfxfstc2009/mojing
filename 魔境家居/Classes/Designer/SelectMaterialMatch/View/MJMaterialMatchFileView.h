//
//  MJMaterialMatchFileView.h
//  魔境家居
//
//  Created by lumingliang on 16/1/30.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJProductModel.h"
#import "CHTCollectionViewWaterfallLayout.h"
typedef void (^materialMatchFileIndexPathClick)(NSInteger index);
@interface MJMaterialMatchFileView : UIView<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *sceneArr;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton *firstBtn;
@property (nonatomic, copy) materialMatchFileIndexPathClick indexPathClick;
@end
