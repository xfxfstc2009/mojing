//
//  MJDealerListCell.h
//  魔境家居
//
//  Created by lumingliang on 16/2/15.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJDealerListModel.h"

typedef void(^addressClick)(NSInteger index);

@interface MJDealerListCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *photoImageView;
@property (nonatomic,strong) UILabel *dealerShopNameLabel;
@property (nonatomic,strong) UILabel *dealerPhoneNumLabel;
@property (nonatomic,strong) UILabel *dealerAddressLabel;
@property (nonatomic,strong) MJDealerListDataModel *dealerDataModel;
@property (nonatomic, copy) addressClick cb;

@end
