//
//  MJMaterialScrollView.m
//  魔境家居选材搭配
//
//  Created by lumingliang on 16/1/21.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJMaterialScrollView.h"
#import "MJUIClassTool.h"
@implementation MJMaterialScrollView

//- (instancetype)initWithFrame:(CGRect)frame
//{
//    if (self = [super initWithFrame:frame]) {
//   //     [self createButtonWithArray:self.titleArr];
//        self.contentSize = CGSizeMake(self.width/4*self.titleArr.count, 0);
//    }
//    return self;
//}

- (void)setTitleArr:(NSArray *)titleArr
{
    _titleArr = titleArr;
    CGFloat btnW = 303/4;
    CGFloat btnH = 30;
    int i = 0;
    for (NSString *str in titleArr) {
        UIButton *btn = [MJUIClassTool createBtnWithFrame:CGRectMake(i*btnW, 8, btnW, btnH) title:str image:nil target:self action:@selector(btnClick:)];
        [btn setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        if (i==0) {
            self.firstBtn = btn;
            btn.selected = YES;
        }
        
        btn.tag = i;
        i++;
        [self addSubview:btn];
    }

    self.contentSize = CGSizeMake(303/4*self.titleArr.count, 0);
}


- (void)btnClick:(UIButton *)button
{
    self.firstBtn.selected = NO;
    self.selectBtn.selected = NO;
    button.selected = YES;
    self.selectBtn = button;
    
    if (_select) {
        _select(button.tag);
    }
}

@end
