//
//  MJMaterialScrollView.h
//  魔境家居选材搭配
//
//  Created by lumingliang on 16/1/21.
//  Copyright © 2016年 mojing. All rights reserved.
//  分类滚动视图

#import <UIKit/UIKit.h>
typedef void (^categorySelect)(NSInteger index);
@interface MJMaterialScrollView : UIScrollView

@property (nonatomic, copy) categorySelect select;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) UIButton *selectBtn;
@property (nonatomic, strong) UIButton *firstBtn;;
@end
