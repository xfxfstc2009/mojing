//
//  MJMaterialMatchFileCell.m
//  魔境家居
//
//  Created by lumingliang on 16/1/30.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJMaterialMatchFileCell.h"
#import "UIImageView+WebCache.h"
@implementation MJMaterialMatchFileCell

- (void)setModel:(MJProductModel *)model
{
    _model = model;
    NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
    [self.materialImageView sd_setImageWithURL:bgUrl placeholderImage:nil];
    
    _nameLabel.text = model.name;
    _priceLabel.text = [NSString stringWithFormat:@"￥%ld",model.price];
}

- (void)awakeFromNib {
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
