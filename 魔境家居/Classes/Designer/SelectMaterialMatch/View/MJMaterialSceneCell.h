//
//  MJMaterialSceneCell.h
//  魔境家居
//
//  Created by lumingliang on 16/2/1.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJMatchProductModel.h"

@interface MJMaterialSceneCell : UICollectionViewCell

@property (nonatomic ,strong) UIImageView *imgView;
@property (nonatomic ,strong) UIImageView *selectImgView;
@property (nonatomic ,strong) MJMatchProductModel *model;
@end
