//
//  MJMaterialMatchFileView.m
//  魔境家居
//
//  Created by lumingliang on 16/1/30.
//  Copyright © 2016年 mojing. All rights reserved.
//  搭配清单

#import "MJMaterialMatchFileView.h"
#import "MJUIClassTool.h"
#import "MJMaterialMatchFileCell.h"
#import "MJSecondProductDetailsCtler.h"
#import "GMDCircleLoader.h"
#import "UIImageView+WebCache.h"
#import "MJNetworkTool.h"
#import "MJRefresh.h"
#import "MJMatchModel.h"
#import "MJMatchProductModel.h"
#import "MJExtension.h"
#import "MJMaterialSceneCell.h"
#define selfWidth 360
#define selfHeight 516
@interface MJMaterialMatchFileView ()

@property (nonatomic, strong) UIImageView *topImageView;
@property (nonatomic, strong) UILabel *topLabel;
@property (nonatomic, strong) UIView *saveWorksView;
@property (nonatomic, strong) UILabel *saveWorksLabel;
@property (nonatomic, strong) UITextField *saveWorksField;
@property (nonatomic, strong) UIButton *saveWorksButton;
@property (nonatomic, strong) UILabel *spaceLabel;

@property (nonatomic, strong) UIButton *selectBtn;
/* 其他按钮*/
@property (nonatomic, strong) UIButton *otherCategoryBtn;
/** 保存的空间类型*/
@property (nonatomic, assign) NSInteger spaceIndex;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 顶部箭头*/
@property (nonatomic,strong) UIImageView *arrowsView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
@end
@implementation MJMaterialMatchFileView
static NSString * const reuseIdentifier = @"collectionCell";

- (UIImageView *)arrowsView
{
    if (!_arrowsView) {
        _arrowsView = [[UIImageView alloc] initWithFrame:CGRectMake(selfWidth/2-6, -5, 12, 5)];
        _arrowsView.image = [UIImage imageNamed:@"select_material_arrows"];
    }
    return _arrowsView;
}

- (UIView *)saveWorksView
{
    if (!_saveWorksView) {
        _saveWorksView = [[UIView alloc] initWithFrame:CGRectMake(1, 50, selfWidth-2, 280)];
        _saveWorksView.backgroundColor = RGB(235, 235, 235);
        
        _saveWorksLabel = [MJUIClassTool createLbWithFrame:CGRectMake(30, 30, 200, 20) title:@"输入名称：" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:15];
        [_saveWorksView addSubview:_saveWorksLabel];
        
        _saveWorksField = [[UITextField alloc] initWithFrame:CGRectMake(30, 60, selfWidth-60, 35)];
        _saveWorksField.borderStyle = UITextBorderStyleRoundedRect;
        _saveWorksField.placeholder = @"请输入搭配作品名称";
        _saveWorksField.backgroundColor = [UIColor whiteColor];
        [_saveWorksView addSubview:_saveWorksField];
        
        _spaceLabel = [MJUIClassTool createLbWithFrame:CGRectMake(30, 110, 200, 20) title:@"空间类型：" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:15];
        [_saveWorksView addSubview:_spaceLabel];
        
        CGFloat btnW = (selfWidth-90)/4;
        CGFloat padding = 10;
        NSArray *btnTitleArr = @[@"客厅",@"餐厅",@"卧室",@"卫生间",@"厨房",@"书房",@"儿童房",@"其他"];
        
        for (int i=0; i<4; i++) {
            UIButton *btn = [MJUIClassTool createBtnWithFrame:CGRectMake(30+btnW*i+padding*i, 140, btnW, 30) title:btnTitleArr[i] image:nil target:self action:@selector(saveWorksBtnClick:)];
            btn.backgroundColor = [UIColor whiteColor];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn setTitleColor:RGB(255, 40, 0) forState:UIControlStateSelected];
            btn.tag = i;
            [_saveWorksView addSubview:btn];
        }
        for (int i=0; i<4; i++) {
            UIButton *btn = [MJUIClassTool createBtnWithFrame:CGRectMake(30+btnW*i+padding*i, 180, btnW, 30) title:btnTitleArr[i+4] image:nil target:self action:@selector(saveWorksBtnClick:)];
            btn.backgroundColor = [UIColor whiteColor];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [btn setTitleColor:RGB(255, 40, 0) forState:UIControlStateSelected];
            btn.tag = i+4;
            if (i == 4) {
                self.otherCategoryBtn = btn;
            }
            [_saveWorksView addSubview:btn];
            self.otherCategoryBtn.selected = YES;
            UIButton *corfirmSaveBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(selfWidth/2-40, 230, 80, 30) title:nil image:[UIImage imageNamed:@"seletc_material_save"] target:self action:@selector(buttonClick:)];
            corfirmSaveBtn.tag = 10;
            [_saveWorksView addSubview:corfirmSaveBtn];
        }
        
    }
    return _saveWorksView;
}

#pragma mark - 顶部图
- (UIView *)topView
{
    if (!_topView) {
        _topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, selfWidth, 50)];
        _topView.backgroundColor = RGB(81, 81, 81);
       
    }
    return _topView;
}
#pragma mark - 搭配清单底部视图
- (UIView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(1, selfHeight-50, selfWidth-2, 49)];
       // NSLog(@"%f",selfWidth);
        _bottomView.backgroundColor = [UIColor whiteColor];
        
        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(selfWidth/2-50, 10, 60, 30) title:@"总价：" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:17];
        
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(selfWidth/2, 10, 80, 30)];
        _priceLabel.textAlignment = NSTextAlignmentLeft;
      //  _priceLabel.backgroundColor = [UIColor greenColor];
        _priceLabel.textColor = RGB(249, 50, 18);
       
        _priceLabel.font = [UIFont systemFontOfSize:17];
        
//        UIButton *saveMaterialFileBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(selfWidth/2+40, 10, 80, 30) title:nil image:[UIImage imageNamed:@"select_save_file"] target:self action:@selector(buttonClick:)];
//        saveMaterialFileBtn.tag = 2;
        
        [_bottomView addSubview:lb];
        [_bottomView addSubview:_priceLabel];
    //    [_bottomView addSubview:saveMaterialFileBtn];
    }
    return _bottomView;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(1, 50, selfWidth-2, selfHeight-50-50) style:UITableViewStylePlain];
        _tableView.backgroundColor = RGB(239, 239, 239);
        _tableView.delegate = self;
        _tableView.rowHeight = 100;
        _tableView.dataSource = self;
        //影藏分割线
        [_tableView registerNib:[UINib nibWithNibName:@"MJMaterialMatchFileCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        UIView *foot = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        _tableView.tableFooterView = foot;
    }
    return _tableView;
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        
        //  layout.minimumLineSpacing = inset;
        layout.columnCount = 2;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(1, 50, selfWidth-2, selfHeight-51) collectionViewLayout:layout];
        CGFloat inset = 21;
        layout.sectionInset = UIEdgeInsetsMake(inset, inset, inset, inset);
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor =[UIColor colorWithRed:239 green:239 blue:239 alpha:1];
        [_collectionView registerClass:[MJMaterialSceneCell class] forCellWithReuseIdentifier:reuseIdentifier];
        [self addSubview:_collectionView];
    }
    return _collectionView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.dataArr = [NSMutableArray array];
        self.sceneArr = [NSMutableArray array];
        [MJNotificationCenter addObserver:self selector:@selector(materialFileListChange:) name:@"lookMaterialFileList" object:nil];
        [MJNotificationCenter addObserver:self selector:@selector(selectMaterialScene) name:@"selectMaterialScene" object:nil];
        [MJNotificationCenter addObserver:self selector:@selector(selectMaterialSave:) name:@"selectMaterialSave" object:nil];
        _spaceIndex = 7;
        [self initUI];
    }
    return self;
}

- (void)initUI
{
    [self addSubview:self.arrowsView];
    
    self.backgroundColor = RGB(81, 81, 81);
    
    //改变场景选中个数
    [MJNotificationCenter addObserver:self selector:@selector(materialChangeSceneSelected:) name:@"materialChangeSceneSelected" object:nil];
    
}

#pragma mark - 场景更换
- (void)materialChangeSceneSelected:(NSNotification *)notification
{
    MJMatchProductModel *dataModel = notification.userInfo[@"matchProductModel"];
    for (MJMatchProductModel *model in self.sceneArr) {
        if (![model isEqual:dataModel]) {
            if (model.isSelected) {
                model.selected = NO;
                [self.collectionView reloadData];
            }
        }
    }
    //更换选材搭配背景背景
    //发通知
    [MJNotificationCenter postNotificationName:@"materialMatchChangeScene" object:nil userInfo:@{@"matchProductModel" :dataModel}];
    
}


#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = RGB(246, 246, 246);
    [self addSubview:_circulateBackgroungView];
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    [self.circulateBackgroungView removeFromSuperview];
    [GMDCircleLoader hideFromView:self animated:YES];
}

#pragma mark - 添加上下拉加载数据
- (void)addRefreshHeader:(NSDictionary *)dict
{
    __weak MJMaterialMatchFileView *ctl = self;
    [self loadingData];
    //下拉刷新
    self.collectionView.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        [ctl loadNewData:dict];
    }];
    // 马上进入刷新状态
    [self.collectionView.mj_header beginRefreshing];
    
}

#pragma mark - 下拉加载数据
- (void)loadNewData:(NSDictionary *)dict
{
    self.pageNumber = 1;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJMaterialMatchFileView *ctl = self;
   // [self.noDataView removeFromSuperview];
    [self.sceneArr removeAllObjects];
   // [self.userPhotoArr removeAllObjects];
    
    //获取场景列表
    if ([self isConnectToNet]) {
        
        [netTool getMoJingURL:MJSceneList parameters:dict success:^(id obj) {
            //  NSLog(@"%@",dict);
            MJMatchModel *model = obj;
            NSMutableArray *arr= [NSMutableArray array];
            arr = [MJMatchProductModel mj_objectArrayWithKeyValuesArray:model.data.sceneList];
            MJMatchProductModel *tempModel = [[MJMatchProductModel alloc] init];
            tempModel.selected = YES;
            tempModel.showWhiteColor = YES;
            [arr insertObject:tempModel atIndex:0];
            // for (int i = 0; i < 2; i++) {
            [ctl.sceneArr addObjectsFromArray:arr];
            // }
            self.pageCount = model.data.pageCount;
            if (ctl.sceneArr.count == 0) {
               // [ctl.view addSubview:ctl.noDataView];
            }
            
            if (ctl.sceneArr.count >= 6) {
                // 上拉刷新
                ctl.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                    // 进入刷新状态后会自动调用这个block
                    [ctl loadMoreData:dict];
                }];
                // 默认先隐藏footer
                ctl.collectionView.mj_footer.hidden = YES;
            }else{
                ctl.collectionView.mj_footer = nil;
            }
            
            [ctl.collectionView reloadData];
            //    [ctl stopDisplayLink];
            [ctl stopCircleLoader];
            [ctl.collectionView.mj_header endRefreshing];
            [ctl endEditing:YES];
            
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [ctl.collectionView.mj_header endRefreshing];
            [ctl stopCircleLoader];
        }];
        
    }else
    {
        [ctl stopCircleLoader];
        [ctl.collectionView.mj_header endRefreshing];
        
    }
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.collectionView];
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    _pageNumber++;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJMaterialMatchFileView *ctl = self;
    //[self.matchProductArr removeAllObjects];
    NSString *str = [dict objectForKey:@"spaceTypeID"];
    NSDictionary *moreDict = @{@"spaceTypeID" : str,@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)_pageNumber],@"pageSize" : @6,@"type" : @1};
    //获取场景列表
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            
            [netTool getMoJingURL:MJSceneList parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                MJMatchModel *model = obj;
                NSMutableArray *arr= [NSMutableArray array];
                arr = [MJMatchProductModel mj_objectArrayWithKeyValuesArray:model.data.worksList];
                [ctl.sceneArr addObjectsFromArray:arr];
                [ctl.collectionView reloadData];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
                //   [ctl stopDisplayLink];
                
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.collectionView.mj_footer endRefreshing];
                [ctl stopCircleLoader];
                //   [ctl stopDisplayLink];
            }];
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
        self.collectionView.mj_footer.hidden = YES;
        //   [self stopDisplayLink];
    }
    
}

#pragma mark - 添加顶部视图子视图
- (void)addTopViewWithImage:(UIImage *)image title:(NSString *)title
{
    self.topImageView = [[UIImageView alloc] initWithFrame:CGRectMake(130, 5, 40, 40)];
    _topImageView.image = image;
    [_topView addSubview:_topImageView];
    
    _topLabel = [[UILabel alloc] initWithFrame:CGRectMake(175, 5, 150, 40)];
    _topLabel.textAlignment = NSTextAlignmentLeft;
    _topLabel.font = [UIFont systemFontOfSize:20];
    _topLabel.textColor = [UIColor whiteColor];
    _topLabel.text = title;
    [_topView addSubview:_topLabel];
}

#pragma mark - 通知tableView数据刷新
- (void)materialFileListChange:(NSNotification *)notification
{
    [self.topImageView removeFromSuperview];
    [self.topLabel removeFromSuperview];
    [self.saveWorksView removeFromSuperview];
    [self.collectionView removeFromSuperview];
    NSMutableArray *arr = (NSMutableArray *)_topView.subviews;
    if (arr.count>0) {
        [arr removeAllObjects];
        [_topView removeFromSuperview];
    }
    [self addSubview:self.topView];
    UIImage *img = [UIImage imageNamed:@"seletc_material_file"];
    [self addTopViewWithImage:img title:@"搭配清单"];
    [self addSubview:self.tableView];
    [self addSubview:self.bottomView];
    //[self addSubview:self.arrowsView];
    self.dataArr = notification.userInfo[@"materialFileList"];
    NSInteger totalPrice = 0;
    for (MJProductModel * model in self.dataArr) {
        totalPrice  += model.price;
    }
    _priceLabel.text = [NSString stringWithFormat:@"￥%ld",(long)totalPrice];
    
    [self.tableView reloadData];
    //self.preferredContentSize = CGSizeMake(360, 516);
   
   // NSLog(@"%ld",self.dataArr.count);
}

#pragma mark - 场景选择通知
- (void)selectMaterialScene
{
    [self.topImageView removeFromSuperview];
    [self.topLabel removeFromSuperview];
    NSMutableArray *arr = (NSMutableArray *)_topView.subviews;
    if (arr.count>0) {
        [arr removeAllObjects];
        [self.topView removeFromSuperview];
    }
    [self.bottomView removeFromSuperview];
    [self.tableView removeFromSuperview];
    [self.saveWorksView removeFromSuperview];
    UIImage *img = [UIImage imageNamed:@"seletc_material_scene"];
    [self addSubview:self.topView];
    [self addTopViewWithImage:img title:@"场景选择"];
    [self addSubview:self.collectionView];
    NSDictionary *headerDict = @{@"spaceTypeID" : @0,@"pageSize" : @6,@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"type" : @0};
    [self addRefreshHeader:headerDict];
    //self.preferredContentSize = CGSizeMake(360, 516);
}

#pragma mark - 保存搭配通知
- (void)selectMaterialSave:(NSNotification *)notification
{
    NSMutableArray *topViewarr = (NSMutableArray *)_topView.subviews;
    if (topViewarr.count>0) {
        [topViewarr removeAllObjects];
        [self.topView removeFromSuperview];
    }
    [self.topImageView removeFromSuperview];
    [self.topLabel removeFromSuperview];
    [self.topView removeFromSuperview];
    [self.collectionView removeFromSuperview];
    [self.tableView removeFromSuperview];
    [self.bottomView removeFromSuperview];
    NSMutableArray *arr = (NSMutableArray *)_topView.subviews;
    if (arr.count>0) {
        [arr removeAllObjects];
        [self.topView removeFromSuperview];
    }
    UIImage *img = [UIImage imageNamed:@"select_save_works"];
    [self addSubview:self.topView];
 
    [self addSubview:self.saveWorksView];
    [self addTopViewWithImage:img title:@"保存搭配"];
   
}



#pragma mark - <UITableViewDataSource>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MJMaterialMatchFileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.contentView.backgroundColor = RGB(239, 239, 239);
    cell.model = self.dataArr[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MJProductModel *model = self.dataArr[indexPath.row];
   
   //发通知跳转
    if (_indexPathClick) {
        _indexPathClick(model.idNum);
    }
}


#pragma mark - 按钮点击
- (void)buttonClick:(UIButton *)button
{
    switch (button.tag) {
        case 10:
        {//保存搭配
            NSLog(@"保存%ld%@",(long)_spaceIndex,_saveWorksField.text);
           [MJNotificationCenter postNotificationName:@"planeWorksSave" object:nil userInfo:@{@"planeWorksTypeIndex" : [NSString stringWithFormat:@"%ld",_spaceIndex],@"planeWorksName": _saveWorksField.text}];
        }
        default:
            break;
    }
}

#pragma mark  <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.sceneArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MJMaterialSceneCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    // Configure the cell
    if (self.sceneArr.count > 0) {
        MJMatchProductModel *model = self.sceneArr[indexPath.row];
        
        if (indexPath.row == 0) {
            cell.imgView.backgroundColor = [UIColor whiteColor];
            
            cell.imgView.image = nil;
        }else{
        
        NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
        NSLog(@"%@",bgUrl);
        [cell.imgView  sd_setImageWithURL:bgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        }
        cell.model = model;
        // 根据模型属性来控制打钩的显示和隐藏
        cell.selectImgView.hidden = !model.isSelected;
        
       
    }
    
    return cell;
}


#pragma mark  <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
    //    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
    //        return model.imageSize;
    //    }
    return CGSizeMake(150, 150);
}

#pragma mark - 保存类型选择
- (void)saveWorksBtnClick:(UIButton *)button
{
    button.backgroundColor = [UIColor whiteColor];
    self.otherCategoryBtn.selected = NO;
    self.selectBtn.selected = NO;
    button.selected = YES;
    self.selectBtn = button;
    if (button.isSelected) {
       
        _spaceIndex = button.tag;
        
    }
}

- (void)dealloc
{
    [MJNotificationCenter removeObserver:self];
}


@end
