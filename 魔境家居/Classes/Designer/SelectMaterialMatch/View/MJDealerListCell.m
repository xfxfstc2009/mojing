//
//  MJDealerListCell.m
//  魔境家居
//
//  Created by lumingliang on 16/2/15.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDealerListCell.h"
#import "MJUIClassTool.h"
#import "MJNetworkTool.h"
#import "UIImageView+WebCache.h"
#import "MJMatchUserDataModel.h"
@implementation MJDealerListCell
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self uiConfig];
    }
    return self;
}

-(void)uiConfig
{
    CGFloat width = 315;
    CGFloat height = 188;
    CGFloat x = 10;
    //外框
    UIView *v1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 1)];
    UIView *v2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, height)];
    UIView *v3 = [[UIView alloc] initWithFrame:CGRectMake(width, 0, 1, height)];
    UIView *v4 = [[UIView alloc] initWithFrame:CGRectMake(0, height, width, 1)];
    
    CGFloat photoW = 70;
    _photoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, 10, photoW, photoW)];
       //_photoImageView.backgroundColor = [UIColor cyanColor];
    _photoImageView.layer.cornerRadius = 35;
    _photoImageView.clipsToBounds = YES;

    _dealerShopNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(x+photoW+5, 20, 170, 70)];
    // _dealerShopNameLabel.backgroundColor = [UIColor redColor];
    _dealerShopNameLabel.textAlignment = NSTextAlignmentLeft;
    _dealerShopNameLabel.font = [UIFont systemFontOfSize:20];
    _dealerShopNameLabel.numberOfLines = 0;
    //_dealerShopNameLabel.text = @"顾家家居杭州店xxxx";
    
    UIButton *serviceBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(width-50, 25, 40, 40) title:nil image:[UIImage imageNamed:@"product_jxs_service"] target:self action:@selector(addressClick:)];
    serviceBtn.tag = 100;
    
    //分割线
    UIView *v5 = [[UIView alloc] initWithFrame:CGRectMake(x, photoW+20, width-20, 1)];
    
    //电话
    UIImageView *phoneImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, photoW+30, 15, 15)];
    phoneImageView.image = [UIImage imageNamed:@"product_jxs_phonenumber"];
    _dealerPhoneNumLabel = [[UILabel alloc] initWithFrame:CGRectMake(x+25, photoW+28, 200, 20)];
    _dealerPhoneNumLabel.textAlignment = NSTextAlignmentLeft;
    _dealerPhoneNumLabel.font = [UIFont systemFontOfSize:17];
   // _dealerPhoneNumLabel.text = @"13888888888";
    
    //分割线
    UIView *v6 = [[UIView alloc] initWithFrame:CGRectMake(x, photoW+60, width-20, 1)];
    
    //地址
    UIImageView *addressImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, photoW+85, 11, 15)];
    addressImageView.image = [UIImage imageNamed:@"product_jxs_address"];
    
    _dealerAddressLabel = [[UILabel alloc] initWithFrame:CGRectMake(x+25, photoW+70, width-2*x-35, 40)];
    _dealerAddressLabel.textAlignment = NSTextAlignmentLeft;
    _dealerAddressLabel.font = [UIFont systemFontOfSize:15];
    _dealerAddressLabel.numberOfLines = 0;
    //_dealerAddressLabel.text = @"萧山区萧山经济技术开发区启迪路198号B1-1001室";
    //指示
    UIImageView *indicateImageView = [[UIImageView alloc] initWithFrame:CGRectMake(width-23, photoW+73, 13, 24)];
    indicateImageView.image = [UIImage imageNamed:@"product_jxs_cellindicater"];
    
    //透明按钮
    UIButton *addressBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(x, photoW+60, width-20, 60) title:nil image:nil target:self action:@selector(addressClick:)];
    addressBtn.alpha = 0.1;
    addressBtn.tag = 101;
  //  addressBtn.backgroundColor = [UIColor redColor];
    
    v1.backgroundColor = RGB(223, 223, 223);
    v2.backgroundColor = RGB(223, 223, 223);
    v3.backgroundColor = RGB(223, 223, 223);
    v4.backgroundColor = RGB(223, 223, 223);
    v5.backgroundColor = RGB(223, 223, 223);
    v6.backgroundColor = RGB(223, 223, 223);
    
    [self.contentView addSubview:serviceBtn];
    [self.contentView addSubview:phoneImageView];
    [self.contentView addSubview:addressImageView];
    [self.contentView addSubview:indicateImageView];
    [self.contentView addSubview:v1];
    [self.contentView addSubview:v2];
    [self.contentView addSubview:v3];
    [self.contentView addSubview:v4];
    [self.contentView addSubview:v5];
    [self.contentView addSubview:v6];
    [self.contentView addSubview:_photoImageView];
    [self.contentView addSubview:_dealerShopNameLabel];
    [self.contentView addSubview:_dealerPhoneNumLabel];
    [self.contentView addSubview:_dealerAddressLabel];
    [self.contentView addSubview:addressBtn];
}

- (void)addressClick:(UIButton *)button
{
    if (_cb) {
        _cb(button.tag);
    }
}
@end
