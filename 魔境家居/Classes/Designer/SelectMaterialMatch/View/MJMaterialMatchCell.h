//
//  MJMaterialMatchCell.h
//  魔境家居选材搭配
//
//  Created by lumingliang on 16/1/27.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJMaterialMatchCell : UICollectionViewCell

@property (nonatomic,strong) UIImageView *imgView;
@end
