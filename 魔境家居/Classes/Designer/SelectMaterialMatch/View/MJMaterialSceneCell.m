//
//  MJMaterialSceneCell.m
//  魔境家居
//
//  Created by lumingliang on 16/2/1.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJMaterialSceneCell.h"

@implementation MJMaterialSceneCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self uiConfig];
    }
    return self;
}

- (void)uiConfig
{
    CGFloat width = 150;
    CGFloat height = 150;
    
    _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
   // _imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_imgView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.backgroundColor = [UIColor whiteColor];
    btn.frame = CGRectMake(0, 0, width, height);
    btn.alpha = 0.1;
    [btn addTarget:self action:@selector(coverClick) forControlEvents:UIControlEventTouchUpInside];
    
    _selectImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    _selectImgView.image = [UIImage imageNamed:@"seletc_scene_selected"];
    
    //  [self.contentView addSubview:_singleProductPrice];
    [self.contentView addSubview:btn];
    [self.contentView addSubview:_selectImgView];
}

- (void)coverClick
{
    // 设置模型选中状态
    self.model.selected = !self.model.isSelected;
    //self.model.selected = YES;
    // 直接修改状态
    _selectImgView.hidden = !_selectImgView.isHidden;
    //发通知
    [MJNotificationCenter postNotificationName:@"materialChangeSceneSelected" object:nil userInfo:@{@"matchProductModel" :self.model}];
    
}

@end
