//
//  MJMaterialMatchCell.m
//  魔境家居选材搭配
//
//  Created by lumingliang on 16/1/27.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJMaterialMatchCell.h"

@implementation MJMaterialMatchCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self uiConfig];
    }
    return self;
}

- (void)uiConfig
{
    CGFloat width = 100;
    CGFloat height = 110;
    
    _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [self.contentView addSubview:_imgView];
}
@end
