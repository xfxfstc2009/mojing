//
//  MJDesignerDiscoverCtler.m
//  魔境家居
//
//  Created by lumingliang on 16/2/21.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerDiscoverCtler.h"
#import "MJUIClassTool.h"
#import "MJProductGroupDetailsCtler.h"
#import "MJSecondProductDetailsCtler.h"
#import "PhotoTrackController.h"

@interface MJDesignerDiscoverCtler ()<UIWebViewDelegate>
{
    NSDictionary *_sceneDic;
}
@end

@implementation MJDesignerDiscoverCtler

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //tabBar显示通知
    self.tabBarController.tabBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *brandName = @"发现";
    UILabel *titleLb = [MJUIClassTool createLbWithFrame:CGRectMake(0, 7, 1000, 44) title:[NSString stringWithFormat:@"%@",brandName] aliment:NSTextAlignmentCenter color:[UIColor blackColor] size:25];
    CGSize size = [MJUIClassTool caculateText:brandName fontSize:25 maxSize:CGSizeMake(1000, 44)];
    titleLb.size = size;
    self.navigationItem.titleView = titleLb;
    self.view.backgroundColor = [UIColor cyanColor];
    
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 64, Screen_Width, Screen_Height-64-49)];
    
    
   // [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.baidu.com"]]];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://192.168.191.1:9292/padFind/padFind.html"]]];
    webView.delegate = self;
    [self.view addSubview:webView];
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString * url = request.URL.absoluteString ;
    NSLog(@"%@",url);
    NSArray *urlMain = [url componentsSeparatedByString:@"?"];
    //http://mj.duc.cn/?3,241
    //    NSString *urlHost = [urlMain objectAtIndex:0];
    // http://mojing.duc.cn/app/download/index.html?resourceType=supplier&userID=155590&userType=1
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        if ([urlMain count]==2)
        {
            NSString *paramStr = [urlMain objectAtIndex:1];
            NSArray *urlMain1 = [paramStr componentsSeparatedByString:@","];
            if (urlMain1.count == 2) {
                NSString *type = [urlMain1 objectAtIndex:0];
                NSString *idNum = [urlMain1 objectAtIndex:1];
                NSLog(@"%@ %@",type,idNum);
                NSDictionary *para = @{@"id" : idNum};
                if ([type isEqualToString:@"1"]) {
                    //场景
                    MJNetworkTool *net = [MJNetworkTool shareInstance];
                    
                    [net getMoJingURL:MJSceneDetail parameters:para success:^(id obj) {
                        
                        NSDictionary *productDic = obj[@"data"];
                        if (productDic) {
                            _sceneDic = productDic;
                        }
                        
                        if (_sceneDic) {
                            PhotoTrackController *photo = [[PhotoTrackController alloc] initWithItem:_sceneDic];
                            [self.navigationController presentViewController:photo animated:YES completion:nil];
                        }
                        
                        
                    } failure:^(NSError *error) {
                        NSLog(@"%@",error);
                    }];
                    
                }else if ([type isEqualToString:@"2"]){
                    //商品
                    MJSecondProductDetailsCtler *detail = [[MJSecondProductDetailsCtler alloc] init];
                    detail.idNum = [idNum integerValue];
                    [self.navigationController pushViewController:detail animated:YES];
                    
                }else if ([type isEqualToString:@"3"])
                {
                    //搭配作品
                    MJProductGroupDetailsCtler *group = [[MJProductGroupDetailsCtler alloc] init];
                    group.idNum = [idNum integerValue];
                    [self.navigationController pushViewController:group animated:YES];
                }
            }
        }
        return NO;
    }
    
    
    return YES;
}

@end
