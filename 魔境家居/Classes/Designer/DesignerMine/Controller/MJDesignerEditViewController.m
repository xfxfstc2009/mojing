//
//  MJDesignerEditViewController.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/26.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerEditViewController.h"
#import "GWFetchModel.h"
#import "MJDesignMineEditViewController.h"
@interface MJDesignerEditViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *changeTableView;
@property (nonatomic,strong)NSArray *tableViewArr;

@end

@implementation MJDesignerEditViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.transferSingleAblum){
        self.barMainTitle = @"修改方案分类";
    } else if (self.transferProductClassSingleModel){
        self.barMainTitle = @"修改商品分类";
    }
    
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:@"取消" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.tableViewArr = @[@[@"分类名称"],@[@"删除此分类"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.changeTableView){
        self.changeTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.changeTableView.delegate = self;
        self.changeTableView.dataSource = self;
        self.changeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.changeTableView.showsVerticalScrollIndicator = NO;
        self.changeTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.changeTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.changeTableView];
    }
}
#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tableViewArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.tableViewArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdengifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        // 1. 创建fixedLabel =
        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.font = [UIFont systemFontOfCustomeSize:15.];
        fixedLabel.stringTag = @"fixedLabel";
        [cellWithRowOne addSubview:fixedLabel];

        // 3. 创建arrowImageView;
        UIImageView *imageView = [[UIImageView alloc]init];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.image = [UIImage imageNamed:@"product_jxs_cellindicater"];
        imageView.frame = CGRectMake(kScreenBounds.size.width - MJFloat(11) - 13, (cellHeight - 24) / 2., 13, 24);
        imageView.stringTag = @"imageView";
        [cellWithRowOne addSubview:imageView];
        
        // 2. 创建dymicLabel
        UILabel *dymicLabel = [[UILabel alloc]init];
        dymicLabel.backgroundColor = [UIColor clearColor];
        dymicLabel.font = [UIFont systemFontOfCustomeSize:15.];
        dymicLabel.textColor = [UIColor lightGrayColor];
        dymicLabel.textAlignment = NSTextAlignmentRight;
        dymicLabel.frame = CGRectMake(imageView.orgin_x - MJFloat(11) - 100, 0, 100, cellHeight);
        dymicLabel.stringTag = @"dymicLabel";
        [cellWithRowOne addSubview:dymicLabel];
    }
    // 1. fixedLabel
    UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
    UILabel *dymicLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"dymicLabel"];
    
    if (self.transferProductClassSingleModel){
        dymicLabel.text = self.transferProductClassSingleModel.name;
    } else if (self.transferSingleAblum){
        dymicLabel.text = self.transferSingleAblum.name;
    }
    
    UIImageView *imageView = (UIImageView *)[cellWithRowOne viewWithStringTag:@"imageView"];
    fixedLabel.text = [[self.tableViewArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if (indexPath.section == 0){
        fixedLabel.textColor = [UIColor blackColor];
        fixedLabel.frame = CGRectMake(MJFloat(11), 0, MJFloat(100), cellHeight);
        fixedLabel.textAlignment = NSTextAlignmentLeft;
        dymicLabel.hidden = NO;
        imageView.hidden = NO;
        
    } else {
        fixedLabel.textColor = [UIColor redColor];
        fixedLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, cellHeight);
        fixedLabel.textAlignment = NSTextAlignmentCenter;
        dymicLabel.hidden = YES;
        imageView.hidden = YES;
    }
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return MJFloat(44);
}



#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    __weak typeof(self)weakSelf = self;
    if (indexPath.section == 0){                    // 修改分类
        MJDesignMineEditViewController *designerMineEditViewController = [[MJDesignMineEditViewController alloc]init];
        if (self.transferProductClassSingleModel){
            designerMineEditViewController.type = uploadInfoTypeClass;
            designerMineEditViewController.transferProductClassSingleModel = self.transferProductClassSingleModel;
            
            [designerMineEditViewController uploadInfo:weakSelf.transferProductClassSingleModel.name typeInfo:uploadInfoTypeClass andBlockManager:^(NSString *info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf.barMainTitle = info;
                [strongSelf.changeTableView reloadData];
            }];
        } else if (self.transferSingleAblum){       // 修改相册
            designerMineEditViewController.type = uploadInfoTypeCase;
            designerMineEditViewController.transferDesignSingleAblumModel = self.transferSingleAblum;
            
            [designerMineEditViewController uploadInfo:weakSelf.transferSingleAblum.name typeInfo:uploadInfoTypeCase andBlockManager:^(NSString *info) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf.changeTableView reloadData];
            }];
        }
        [self.navigationController pushViewController:designerMineEditViewController animated:YES];
    } else if (indexPath.section == 1){                 // 删除分类
        if (self.transferProductClassSingleModel){      // 删除商品分类
            [weakSelf sendRequestToDeleteClassWithId:weakSelf.transferProductClassSingleModel.productClassId];
        } else if (self.transferSingleAblum){           // 删除相册
            [weakSelf sendRequestToDeleteAblumId:weakSelf.transferSingleAblum.ablumId];
        } else if (self.transferMaterialSingleModel){       // 删除选材搭配
            [weakSelf sendRequestToDeleteMaterialId:weakSelf.transferMaterialSingleModel.materialId];
        }
    }
}

#pragma mark - sendRequestToDeleteClass
-(void)sendRequestToDeleteClassWithId:(NSString *)classId{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    fetchModel.requestParams = @{@"id":classId};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:@"designerProductType/delete" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (strongSelf.block){
                strongSelf.block(strongSelf.transferProductClassSingleModel);
                [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            }
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


#pragma mark 删除相册
-(void)sendRequestToDeleteAblumId:(NSString *)ablumId{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    fetchModel.requestParams = @{@"id":ablumId};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:@"album/delete" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (strongSelf.deleteCaseBlock){
                strongSelf.deleteCaseBlock(strongSelf.transferSingleAblum);
                [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            }
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma mark - 删除选材搭配
-(void)sendRequestToDeleteMaterialId:(NSString *)materialId{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    fetchModel.requestParams = @{@"id":materialId};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:@"planeWorks/delete" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (strongSelf.materialDeleteBlock){
                strongSelf.materialDeleteBlock(strongSelf.transferMaterialSingleModel);
                [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            }
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


@end
