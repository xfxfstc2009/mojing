//
//  MJDesignMineProductDetailViewController.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,productDetailViewControllerType) {
    productDetailViewControllerTypeProduct,                 /**< 商品分类*/
    productDetailViewControllerTypeCase                    /**< 其他分类*/
};

@interface MJDesignMineProductDetailViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferProductName;
@property (nonatomic,copy)NSString *transferAblumId;            /**< 传递相册的id*/
@property (nonatomic,assign)productDetailViewControllerType productDetailViewControllerType;            /**< 传递过来的类型*/

@end
