//
//  MJDesignerAddDrawingBoard.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//
// 添加画板
#import "AbstractViewController.h"
#import "MJDesignerProductClassSingleModel.h"
#import "MJDesignAblumList.h"

// 向相册里增加一张图片
typedef void(^addImgManagerWithBlock)(MJDesignSingleAblum *ablumSingleModel);
typedef void(^addImgManagerInProductWithBlock)(MJDesignerProductClassSingleModel *productClassSingleModel);

typedef NS_ENUM(NSInteger,drawingBoardType) {
    drawingBoardTypeProduct,            /**< 上传商品*/
    drawingBoardTypeCase,               /**< 上传案例*/
};

@interface MJDesignerAddDrawingBoard : UIViewController

@property (nonatomic,strong) UIImage *backGroundImage;                      // 背景图片

@property (nonatomic,assign) BOOL isShareProduct;                           // 判断是否分享
@property (nonatomic,assign) BOOL isHasCancelButton;                        // 判断是否需要有取消按钮
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势
@property (nonatomic,assign)drawingBoardType drawingBoardType;              // 上传类型

-(void)hideParentViewControllerTabbar:(UIViewController *)viewController;
-(void)showInView:(UIViewController *)viewController type:(drawingBoardType)type andBlock:(void(^)())block;                           // 显示show
-(void)dismissFromView:(UIViewController *)viewController;                      // 隐藏dismiss


// 增加图片到相册方法
-(void)addImgManagerWithBlock:(addImgManagerWithBlock)block;
-(void)addProManagerBlock:(addImgManagerInProductWithBlock)block;

@end

