//
//  MJDesignerAddDrawingBoard.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerAddDrawingBoard.h"
#import "MJDesignerProductClassListModel.h"

#import <objc/runtime.h>

static char viewShowBlockKey;
static char addImgManagerWithBlockKey;
static char addproManagerWithBlockKey;

@interface MJDesignerAddDrawingBoard()<UITextFieldDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>{
    UIView          *backgrondView;
}
@property (nonatomic,   weak) UIViewController  *showViewController;
@property (nonatomic, strong) UIImageView       *shareView;

@property (nonatomic,strong)UIButton *closeButton;          /**< 创建关闭按钮*/
@property (nonatomic,strong)UIButton *checkButton;          /**< 创建确定按钮*/
@property (nonatomic,strong)UILabel *contenLabel;           /**< 内容label*/
@property (nonatomic,strong)UITextField *inputTextField;    /**< 输入画板*/
@property (nonatomic,strong)UIView *lineView;


@property (nonatomic,strong)UIScrollView *mainScrollView;

// page one
@property (nonatomic,strong)UITableView *mainTableView;
@property (nonatomic,strong)NSMutableArray *mainMutableArr;
@property (nonatomic,assign)BOOL isEdit;
@end

#define kSheetHeight MJFloat(400)
@implementation MJDesignerAddDrawingBoard

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self createSheetView];                      // 加载Sheetview
}

-(void)arrayWithInit{
    self.mainMutableArr = [NSMutableArray array];
}

#pragma mark - createView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.3f];
    [self.view addSubview:backgrondView];
    
    // 背景色渐入
    [self backGroundColorFadeInOrOutFromValue:0.0f toValue:1.0f];
    
    CGFloat viewHeight = kSheetHeight;
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    
    
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.clipsToBounds = YES;
//    _shareView.image = _backGroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    [self.view addSubview:_shareView];
    
    [self createView];
    
    
    // Gesture
    if (_isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gestureRecognizerHandle:)];
        [self.view addGestureRecognizer:tapGestureRecognizer];
    }
}

// 背景颜色渐入渐出
- (void)backGroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = .4f;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}


#pragma mark - ActionClick
// 手势进行隐藏
- (void)gestureRecognizerHandle:(UITapGestureRecognizer *)sender {
    [self dismissFromView:_showViewController];
}

// 显示view
-(void)showInView:(UIViewController *)viewController type:(drawingBoardType)type andBlock:(void(^)())block{
    _drawingBoardType = type;
    objc_setAssociatedObject(self, &viewShowBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    __weak MJDesignerAddDrawingBoard *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.3f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    }];
    
    __weak typeof(self)weakSelf = self;
    if (type == drawingBoardTypeCase){                      // 上传案例
        [weakSelf sendRequestToGetAblumList];
    } else if (type == drawingBoardTypeProduct) {           // 上传商品
        [weakSelf sendRequestToGetProductList];
    }
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak MJDesignerAddDrawingBoard *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    //背景色渐出
    [self backGroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 隐藏tabBar
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backGroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}













#pragma mark - createView
-(void)createView{
    // 0 . 创建view
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    bgView.frame = CGRectMake(0, 0 , kScreenBounds.size.width, MJFloat(40));
    [self.shareView addSubview:bgView];
    
    // 1. 创建按钮
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.closeButton.frame = CGRectMake(MJFloat(10),(bgView.size_height - MJFloat(21)) /2., MJFloat(21), MJFloat(21));
    __weak typeof(self)weakSelf = self;
    [weakSelf.closeButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf dismissFromView:strongSelf.showViewController];
    }];
    [self.closeButton setBackgroundImage:[UIImage imageNamed:@"post_case_fail"] forState:UIControlStateNormal];
    [bgView addSubview:self.closeButton];
    
    // 2. 创建右侧按钮
    self.checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.checkButton.frame = CGRectMake(kScreenBounds.size.width - MJFloat(10) - MJFloat(21), self.closeButton.orgin_y, self.closeButton.size_width, self.closeButton.size_height);
    [self.checkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.isEdit){
            if (strongSelf.drawingBoardType == drawingBoardTypeProduct){            // 保存商品
                [strongSelf sendRequestToSaveProductInfo];
            } else if (strongSelf.drawingBoardType == drawingBoardTypeCase){        // 保存方案
                [strongSelf sendRequestToSaveAblum];
            }
        } else {
            [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width, 0) animated:YES];
            [strongSelf.checkButton setBackgroundImage:[UIImage imageNamed:@"post_case_gray"] forState:UIControlStateNormal];
            [strongSelf.inputTextField becomeFirstResponder];
            
        }
        
    }];
    [self.checkButton setBackgroundImage:[UIImage imageNamed:@"post_case_highimage"] forState:UIControlStateNormal];
    self.checkButton.userInteractionEnabled = YES;
    [bgView addSubview:self.checkButton];
    
    // 3. 创建label
    self.contenLabel = [[UILabel alloc]init];
    self.contenLabel.backgroundColor = [UIColor clearColor];
    self.contenLabel.frame = CGRectMake(CGRectGetMaxX(self.closeButton.frame) + MJFloat(20), 0, bgView.size_width - 2 * (CGRectGetMaxX(self.closeButton.frame) + MJFloat(20)), bgView.size_height);
    self.contenLabel.text = @"添加画板";
    self.contenLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:self.contenLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor lightGrayColor];
    self.lineView.frame = CGRectMake(0, CGRectGetMaxY(bgView.frame) + MJFloat(1), kScreenBounds.size.width, 1);
    [self.shareView addSubview:self.lineView];
    
    // 2. 创建scrollView
    [self createScrollView];
}

#pragma mark - createScrollView
-(void)createScrollView{
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.delegate = self;
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.scrollEnabled = NO;
        self.mainScrollView.backgroundColor = [UIColor whiteColor];
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.lineView.frame), kScreenBounds.size.width, MJFloat(400)- CGRectGetMaxY(self.lineView.frame));
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2, self.mainScrollView.size_height);
        [self.shareView addSubview:self.mainScrollView];
    }
    
    // 1.创建tableView
    [self createTableView];
    
    // 2. 创建textField
    [self createPageTwo];
    
    
}

#pragma mark  createpage2
-(void)createPageTwo{
    // 创建textField
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.textAlignment = NSTextAlignmentLeft;
    self.inputTextField.textColor = [UIColor grayColor];
    self.inputTextField.font = [UIFont systemFontOfSize:15.];
    self.inputTextField.frame =  CGRectMake(kScreenBounds.size.width + MJFloat(25),CGRectGetMaxY(self.lineView.frame) + MJFloat(15),(kScreenBounds.size.width - 2 * MJFloat(25)),MJFloat(40));
    self.inputTextField.borderStyle = UITextBorderStyleRoundedRect;
    self.inputTextField.placeholder = @"请输入画板名称";
    self.inputTextField.delegate = self;
    self.inputTextField.backgroundColor = [UIColor colorWithRed:236/255. green:236/255. blue:236/255. alpha:1];
    [self.mainScrollView addSubview:self.inputTextField];
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if([string isEqualToString:@"\n"]) {
        return YES;
    }
    NSString *toBeString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if(toBeString.length)  {
        [self.checkButton setBackgroundImage:[UIImage imageNamed:@"post_case_highimage"] forState:UIControlStateNormal];
    } else {
        [self.checkButton setBackgroundImage:[UIImage imageNamed:@"post_case_gray"] forState:UIControlStateNormal];
    }
     return YES;

}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mainTableView){
        self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.mainTableView.delegate = self;
        self.mainTableView.dataSource = self;
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.mainTableView.showsVerticalScrollIndicator = NO;
        self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.mainTableView.backgroundColor = [UIColor clearColor];
        [self.mainScrollView addSubview:self.mainTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.mainMutableArr.count + 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return 1;
    } else {
        NSArray *sectionOfArr = [self.mainMutableArr objectAtIndex:section - 1];
        return sectionOfArr.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIImageView *headerImageView = [[UIImageView alloc]init];
            headerImageView.image = [UIImage imageNamed:@"product_brand_logo"];
            headerImageView.backgroundColor = [UIColor clearColor];
            headerImageView.stringTag = @"headerImageView";
            headerImageView.frame = CGRectMake(MJFloat(11), MJFloat(10), cellHeight - 2 * MJFloat(10), cellHeight - 2 * MJFloat(10));
            [cellWithRowOne addSubview:headerImageView];
        }
        UIImageView *headerImageView = (UIImageView *)[cellWithRowOne viewWithStringTag:@"headerImageView"];
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if(!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = [UIFont systemFontOfCustomeSize:14.];
            fixedLabel.textAlignment = NSTextAlignmentLeft;
            fixedLabel.textColor = [UIColor blackColor];
            fixedLabel.frame = CGRectMake(MJFloat(11), 0, 150, cellHeight);
            fixedLabel.stringTag = @"fixedLabel";
            [cellWithRowTwo addSubview:fixedLabel];
            
            UILabel *dymicLabel = [[UILabel alloc]init];
            dymicLabel.backgroundColor = [UIColor clearColor];
            dymicLabel.font = [UIFont systemFontOfCustomeSize:12.];
            dymicLabel.stringTag = @"dymicLabel";
            dymicLabel.textColor = [UIColor lightGrayColor];
            dymicLabel.textAlignment = NSTextAlignmentRight;
            dymicLabel.frame = CGRectMake(kScreenBounds.size.width - MJFloat(11) - 150, 0, 150, cellHeight);
            [cellWithRowTwo addSubview:dymicLabel];
        }
        UILabel *fixedLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"fixedLabel"];
        UILabel *dymicLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"dymicLabel"];
        if (self.drawingBoardType == drawingBoardTypeProduct){
            MJDesignerProductClassSingleModel *productSingleModel = [[self.mainMutableArr objectAtIndex:indexPath.section - 1] objectAtIndex:indexPath.row];
            fixedLabel.text = productSingleModel.name;
            dymicLabel.text = [NSString stringWithFormat:@"%li",(long)productSingleModel.productCount];
        } else if (self.drawingBoardType == drawingBoardTypeCase){
            MJDesignSingleAblum *ablumSingleModel = [[self.mainMutableArr objectAtIndex:indexPath.section - 1] objectAtIndex:indexPath.row];
            fixedLabel.text = ablumSingleModel.name;
            dymicLabel.text = [NSString stringWithFormat:@"%li",(long)ablumSingleModel.imageCount];
        }
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section != 0){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        // 增加到信息
        __weak typeof(self)weakSelf = self;
        if (self.drawingBoardType == drawingBoardTypeCase){     // 方案
            MJDesignSingleAblum *productSingleModel = [[self.mainMutableArr objectAtIndex:indexPath.section - 1] objectAtIndex:indexPath.row];
            [weakSelf sendRequestToAblumWithAblum:productSingleModel image:nil];
        } else if(self.drawingBoardType == drawingBoardTypeProduct){
            MJDesignerProductClassSingleModel *productSingleModel = [[self.mainMutableArr objectAtIndex:indexPath.section - 1] objectAtIndex:indexPath.row];
            [weakSelf sendRequestToAddParoductInClass:productSingleModel img:nil];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor lightGrayColor];
    headerView.frame = CGRectMake(0, 0, kScreenBounds.size.width, MJFloat(44));
    UILabel *contentOfLabel = [[UILabel alloc]init];
    contentOfLabel.backgroundColor = [UIColor clearColor];
    contentOfLabel.font = [UIFont systemFontOfCustomeSize:14.];
    contentOfLabel.frame = CGRectMake(MJFloat(11), 0, kScreenBounds.size.width, MJFloat(44));
    if (self.drawingBoardType == drawingBoardTypeCase){
        contentOfLabel.text =  @"选择案例分类";
    } else if (self.drawingBoardType == drawingBoardTypeProduct){
        contentOfLabel.text = @"选择空间分类";
    }
    [headerView addSubview:contentOfLabel];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_mainTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [self.mainMutableArr count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([self.mainMutableArr count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return MJFloat(40);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return MJFloat(100);
    } else {
        return MJFloat(44);
    }
}


#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView){
        CGPoint point = scrollView.contentOffset;
        if (point.x == kScreenBounds.size.width){
            self.isEdit = YES;
            self.mainScrollView.scrollEnabled = YES;

        } else {
            self.isEdit = NO;
            self.mainScrollView.scrollEnabled = NO;
        }
    }
}

#pragma mark - sendRequestToGetInfo
// 1. 获取我的商品列表
-(void)sendRequestToGetProductList{
    MJDesignerProductClassListModel *productMainModel = [[MJDesignerProductClassListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [productMainModel fetchWithGetPath:@"designerProductType/getListByDesignerID" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.mainMutableArr addObject:productMainModel.productTypeList];
            NSMutableIndexSet *dateSourceSectionSet = [[NSMutableIndexSet alloc]initWithIndex:1];
            [strongSelf.mainTableView insertSections:dateSourceSectionSet withRowAnimation:UITableViewRowAnimationNone];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma mark 获取相册列表
-(void)sendRequestToGetAblumList{
    __weak typeof(self)weakSelf = self;
    MJDesignAblumList *designAblumList = [[MJDesignAblumList alloc]init];
    designAblumList.requestParams = @{@"referenceID":[GWTool userDefaultGetWithKey:@"userID"]};
    [designAblumList fetchWithPath:@"album/getList" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.mainMutableArr addObject:designAblumList.albumList];
            NSMutableIndexSet *dateSourceSectionSet = [[NSMutableIndexSet alloc]initWithIndex:1];
            [strongSelf.mainTableView insertSections:dateSourceSectionSet withRowAnimation:UITableViewRowAnimationNone];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

// 2. 商品类型保存
-(void)sendRequestToSaveProductInfo{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    fetchModel.requestParams = @{@"name":self.inputTextField.text.length?self.inputTextField.text:@""};
    [fetchModel fetchWithPath:designerProductTypeSave completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"保存成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                // 1. 消失
                [strongSelf sheetViewDismiss];
                void(^callBack)()  = objc_getAssociatedObject(strongSelf, &viewShowBlockKey);
                if (callBack){
                    callBack();
                }
            }]show];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


// 增加图片到相册
-(void)sendRequestToAblumWithAblum:(MJDesignSingleAblum *)ablumSingleModel image:(UIImage *)image{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    
    GWUploadFileModel *fileModel = [[GWUploadFileModel alloc]init];
    fileModel.uploadImage = [UIImage imageNamed:@"product_brand_logo"];
    fileModel.keyName = @"123";
    
    fetchModel.requestFileDataArr = @[fileModel];
    fetchModel.requestParams = @{@"albumID":ablumSingleModel.ablumId};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithImgInfoPath:@"album/addImage" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            void(^addImgManagerWithBlock)(MJDesignSingleAblum *ablumSingleModel) = objc_getAssociatedObject(strongSelf, &addImgManagerWithBlockKey);
            if (addImgManagerWithBlock){
                addImgManagerWithBlock(ablumSingleModel);
            }
            [strongSelf sheetViewDismiss];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


#pragma mark - 相册保存
-(void)sendRequestToSaveAblum{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    fetchModel.requestParams = @{@"name":self.inputTextField.text.length?self.inputTextField.text:@"",@"referenceID":[GWTool userDefaultGetWithKey:@"userID"]};
    [fetchModel fetchWithPath:albumSave completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"保存成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                // 1. 消失
                [strongSelf sheetViewDismiss];
                void(^callBack)()  = objc_getAssociatedObject(strongSelf, &viewShowBlockKey);
                if (callBack){
                    callBack();
                }
            }]show];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


// 增加图片到相册方法
-(void)addImgManagerWithBlock:(addImgManagerWithBlock)block{
    objc_setAssociatedObject(self, &addImgManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)addProManagerBlock:(addImgManagerInProductWithBlock)block{
    objc_setAssociatedObject(self, &addproManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 增加商品到我的类型
-(void)sendRequestToAddParoductInClass:(MJDesignerProductClassSingleModel *)productSingleModel img:(UIImage *)image{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:@"123" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            void(^addImgManagerInProductWithBlock)(MJDesignerProductClassSingleModel *productClassSingleModel) = objc_getAssociatedObject(strongSelf, &addproManagerWithBlockKey);
            if (addImgManagerInProductWithBlock){
                addImgManagerInProductWithBlock(productSingleModel);
            }
            [strongSelf sheetViewDismiss];

        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


@end
