//
//  MJDesignerSettingViewController.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerSettingViewController.h"
#import "MJDesignMineEditViewController.h"
#import "MJMineCtler.h"                     // 修改密码

#import "MJAvatorTempModel.h"
@class MJDesignMineEditViewController;
@interface MJDesignerSettingViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *settingTableView;
@property (nonatomic,strong)NSArray *settingArr;            /**< 设置页面*/

@property (nonatomic,strong)GWImageView *headerImageView;
@end

@implementation MJDesignerSettingViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
//    [weakSelf sendRequestGetDesignerDetail];
    [weakSelf sendRequestToGetAvatar];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"设置";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.settingArr = @[@[@"头像"],@[@"昵称",@"手机号",@"登录密码",@"设计专长",@"工作年限",@"所属机构",@"关于自己"],@[@"清理缓存",@"退出登录"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.settingTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.settingTableView.delegate = self;
        self.settingTableView.dataSource = self;;
        self.settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.settingTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
        [self.view addSubview:self.settingTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.settingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.settingArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
            self.headerImageView = [[GWImageView alloc]init];
            self.headerImageView.backgroundColor = [UIColor clearColor];
            self.headerImageView.frame = CGRectMake(MJFloat(16), MJFloat(14), (cellHeight - 2 * MJFloat(14)) , (cellHeight - 2 * MJFloat(14)) );
            self.headerImageView.clipsToBounds = YES;
            self.headerImageView.layer.cornerRadius = self.headerImageView.size_height / 2;
            [cellWithRowOne addSubview:self.headerImageView];
            
            // 2. 创建label
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = [UIFont systemFontOfCustomeSize:17];
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + MJFloat(10), 0, kScreenBounds.size.width, cellHeight);
            [cellWithRowOne addSubview:fixedLabel];
        }
        
        //2.
        UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [[self.settingArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        return cellWithRowOne;
    } else if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 1. fixedLabel
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.font = [UIFont systemFontOfCustomeSize:15.];
            fixedLabel.frame = CGRectMake(MJFloat(45), 0, MJFloat(200), cellHeight);
            fixedLabel.textColor = [UIColor blackColor];
            fixedLabel.font = [UIFont systemFontOfCustomeSize:18];
            [fixedLabel.font boldFont];
            fixedLabel.stringTag = @"fixedLabel";
            [cellWithRowTwo addSubview:fixedLabel];
            
            UILabel *dymicLabel = [[UILabel alloc]init];
            dymicLabel.font = [UIFont systemFontOfCustomeSize:14.];
            dymicLabel.textAlignment = NSTextAlignmentRight;
            dymicLabel.textColor = [UIColor lightGrayColor];
            dymicLabel.stringTag = @"dymicLabel";
            dymicLabel.frame = CGRectMake(kScreenBounds.size.width - MJFloat(45) - MJFloat(200), 0, MJFloat(200), cellHeight);
            [cellWithRowTwo addSubview:dymicLabel];
            
            //
            UIImageView *arrowImageView = [[UIImageView alloc]init];
            arrowImageView.backgroundColor = [UIColor clearColor];
            arrowImageView.frame = CGRectMake(kScreenBounds.size.width - 13 - MJFloat(45), (cellHeight - 24) / 2., 13, 24);
            arrowImageView.image = [UIImage imageNamed:@"product_jxs_cellindicater"];
            arrowImageView.stringTag = @"arrowImageView";
            [cellWithRowTwo addSubview:arrowImageView];
        }
        UILabel *fixedLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [[self.settingArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        // arrowImageView
        UIImageView *imageView = (UIImageView *)[cellWithRowTwo viewWithStringTag:@"arrowImageView"];
        
        // dymicLabel
        UILabel *dymicLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"dymicLabel"];
        if (indexPath.row == 0){    // 昵称
            dymicLabel.text = self.designerDetailInfoModel.nickName;
        } else if (indexPath.row == 1){ // 手机号码
            dymicLabel.text = @"手机号码";
        } else if (indexPath.row == 2){// 密码
            dymicLabel.text = @"******";
        } else if (indexPath.row == 3){     // 设计专长
            dymicLabel.text = self.designerDetailInfoModel.specialty;
        } else if (indexPath.row == 4){     // 工作年限
            dymicLabel.text = self.designerDetailInfoModel.workingLife;
        } else if (indexPath.row == 5){     // 所属机构
            dymicLabel.text = self.designerDetailInfoModel.organization;
        } else if (indexPath.row == 6){ // 关于自己
            dymicLabel.text = self.designerDetailInfoModel.introduce;
        }
        CGSize dymicOfSize = [dymicLabel.text sizeWithCalcFont:dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)];
        dymicLabel.frame = CGRectMake(imageView.orgin_x - MJFloat(11) - dymicOfSize.width, 0, dymicOfSize.width, cellHeight);
        
        return cellWithRowTwo;
    } else if (indexPath.section == 2){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = [UIFont systemFontOfCustomeSize:15.];
            fixedLabel.textAlignment = NSTextAlignmentCenter;
            fixedLabel.text = [[self.settingArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            fixedLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, cellHeight);
            fixedLabel.stringTag = @"fixedLabel";
            [cellWithRowThr addSubview:fixedLabel];
        }
        UILabel *fixedLabel = (UILabel *)[cellWithRowThr viewWithStringTag:@"fixedLabel"];
        if (indexPath.row == 1){
            fixedLabel.textColor = [UIColor colorWithRed:242/255. green:108/255. blue:85/255. alpha:1];
        } else {
            
        }
        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    uploadInfoType type = -1;
    if (indexPath.section == 0){            // 修改头像
        [self sendRequestToChangeAvatar];
    } else if (indexPath.section == 1){
        if (indexPath.row == 0){            // 昵称
            type = uploadInfoTypeNickName;
        } else if (indexPath.row == 1){     // 手机号
            type = uploadInfoTypeNone;
        } else if (indexPath.row == 2){     // 登录密码
            type = uploadInfoTypeNone;
        } else if (indexPath.row == 3){     // 设计专长
            type = uploadInfoTypeExpertise;
        } else if (indexPath.row == 4){     // 工作年限
            type = uploadInfoTypeWorkTime;
        } else if (indexPath.row == 5){     // 所属机构
            type = uploadInfoTypeAffiliation;
        } else if (indexPath.row == 6){     // 关于自己
            type = uploadInfoTypeAbout;
        }
        
        if (type == uploadInfoTypeNone){        // 不操作
            if (indexPath.row == 2){
                MJMineCtler *updatePassword = [[MJMineCtler alloc]init];
                [self.navigationController pushViewController:updatePassword animated:YES];
            }
        } else {
            MJDesignMineEditViewController *designMineEditViewController =[[MJDesignMineEditViewController alloc]init];
            designMineEditViewController.transferDesignerDetailInfoModel = self.designerDetailInfoModel;
            [designMineEditViewController uploadInfo:@"123" typeInfo:type andBlockManager:^(NSString *info) {
                NSLog(@"%@",info);
            }];
            [self.navigationController pushViewController:designMineEditViewController animated:YES];
        }
        
    } else if (indexPath.section == 2){
        if (indexPath.row == 0){            // 清流缓存
#warning Todo 清除缓存
        } else if (indexPath.row == 1){     // 退出登录
#warning Todo 退出登录
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_settingTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.settingArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.settingArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){            // 更改头像
        return MJFloat(88);
    } else if (indexPath.row == 1){         // 昵称
        return 44;
    } else if (indexPath.row == 2){         // 退出登录
        return 44;
    } else {
        return 44;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30;
}


#pragma mark - sendRequestToUploadHeaderImg
-(void)sendRequestToUpload{
    
}

-(void)sendRequestGetDesignerDetail{
    self.designerDetailInfoModel = [[MJDesignerDetailInfoModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [self.designerDetailInfoModel fetchWithPath:@"designer/getDetail" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSrlf = self;
        if (isSucceeded){
            [strongSrlf.settingTableView reloadData];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

-(void)sendRequestToChangeAvatar{
    [[MJBaseCtler sharedInstance] userSettingAvatar:[UIImage imageNamed:@"mine_head_photo"] name:@"123.jpg"];
}

-(void)sendRequestToGetAvatar{
    MJAvatorTempModel *fetchModel = [[MJAvatorTempModel alloc]init];
    fetchModel.requestParams = @{@"ids":[MJUserDefaults objectForKey:@"userID"]};
    __weak typeof(self)weakSelf = self;

    [fetchModel fetchWithGetPath1:@"http://api.duc.cn/user/getUserByQuery" completionHandler:^(BOOL isSucceeded, NSError *error, id data) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            NSDictionary *dic = [(NSArray *)data lastObject];
            NSString *string = [dic objectForKey:@"avatar"];
            [strongSelf.headerImageView uploadImageWithURL:string placeholder:nil callback:NULL];
        }
    }];
}


@end
