//
//  MJDesignMineEditViewController.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//
// 信息修改
#import "AbstractViewController.h"
#import "MJDesignerProductClassSingleModel.h"           // 商品类型
#import "MJDesignerDetailInfoModel.h"                   // 设计师详情
#import "MJDesignSingleAblum.h"

typedef NS_ENUM(NSInteger,uploadInfoType) {
    uploadInfoTypeNickName,                 /**< 昵称*/
    uploadInfoTypePhoneNumber,              /**< 手机号码*/
    uploadInfoTypeExpertise,                /**< 设计专长*/
    uploadInfoTypeWorkTime,                 /**< 工作年限*/
    uploadInfoTypeAffiliation,              /**< 所属机构*/
    uploadInfoTypeAbout,                    /**< 关于自己*/
    uploadInfoTypeClass,                    /**< 修改分类*/
    uploadInfoTypeCase,                     /**< 修改方案*/
    uploadInfoTypeMaterial,                 /**< 修改选材搭配*/
    uploadInfoTypeNone = -1,                /**< 其他*/
};


@interface MJDesignMineEditViewController : AbstractViewController
@property (nonatomic,assign)uploadInfoType type;
@property (nonatomic,strong)MJDesignerProductClassSingleModel *transferProductClassSingleModel;     /**< 修改商品类型*/
@property (nonatomic,strong)MJDesignerDetailInfoModel *transferDesignerDetailInfoModel;             /**< 传递过来的详情*/
@property (nonatomic,strong)MJDesignSingleAblum *transferDesignSingleAblumModel;                    /**< 传递过来的相册*/

-(void)uploadInfo:(NSString *)info typeInfo:(uploadInfoType)type andBlockManager:(void(^)(NSString *info))block;

@end
