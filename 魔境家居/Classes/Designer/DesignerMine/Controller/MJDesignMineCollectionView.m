 //
//  MJDesignMineCollectionView.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignMineCollectionView.h"
#import "HTHorizontalSelectionList.h"

// cell
#import "MJDesignerMineOneCollectionViewCell.h"
#import "MJDesignerMineHeaderView.h"

// model
#import "MJDesignerDetailInfoModel.h"
#import "MJDesignAblumList.h"
#import "MJDesignerAblumAddModel.h"                      // 增加一个相册的方法
#import "MJDesignerProductClassListModel.h"                           // 商品
#import "MJDesignerMaterialMainModel.h"                  // 选材搭配
#import "MJDesignerMainModel.h"                          // 搭配作品
#import "GWUploadFileModel.h"
#import "MJAvatorTempModel.h"
// view

#import "MJMenuItem.h"

// controller
#import "MJDesignerSettingViewController.h"             // 设置页面
#import "MJDesignMineProductDetailViewController.h"     // 详情页面
#import "MJCameraCtler.h"                               // 照片控制器
#import "MJDesignerAddDrawingBoard.h"                   // 画板
#import "MJDesignMineEditViewController.h"              // 修改当前的内容
#import "MJDesignerEditViewController.h"
#define kMargin_designMine_header (MJFloat(40) + MJFloat(150))

@interface MJDesignMineCollectionView()<UIScrollViewDelegate,HTHorizontalSelectionListDelegate, HTHorizontalSelectionListDataSource,UICollectionViewDataSource,UICollectionViewDelegate>
// segment
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSArray *segmentArr;

// header
@property (nonatomic,strong)MJDesignerMineHeaderView *designerMineHeaderView;

// scrollView
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UICollectionView *collectionViewOne;
@property (nonatomic,strong)UICollectionView *collectionViewTwo;
@property (nonatomic,strong)UICollectionView *collectionViewThr;
@property (nonatomic,strong)UICollectionView *collectionViewFour;

@property (nonatomic,strong)NSMutableArray *mutableArrOne;
@property (nonatomic,strong)NSMutableArray *mutableArrTwo;
@property (nonatomic,strong)NSMutableArray *mutableArrThr;
@property (nonatomic,strong)NSMutableArray *mutableArrFour;
@property (nonatomic,strong)NSMutableArray *collectionViewMutableArr;

@property (nonatomic,strong) MJDesignerDetailInfoModel *designerDetailInfoModel;

@end

@implementation MJDesignMineCollectionView


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetAvatar];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createdesignHeaderView];
    [self createSegment];
    [self createMainScrollView];
    [self navigationBarMenu];                         // 创建menu
    
    // 请求数据
    __weak typeof(self)weakSelf = self;
//    [weakSelf sendRequestToGetDesignerDetailInfo];          // 请求个人信息数据
    [weakSelf sendRequestToGetMyCase];                      // 请求我的案例
    [weakSelf sendRequestProductList];                      // 请求我的商品
    [weakSelf sendRequestGetPlaneWorksList];                // 获取选材搭配
    [weakSelf sendRequestRealCollocation];                  // 获取当前实景搭配
    [weakSelf sendRequestGetDesignerDetail];                // 获取关于我

}

#pragma mark - [1]pageSetting
-(void)pageSetting{
    self.title = @"我的主页";
    // 1. 创建左侧按钮
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setBackgroundColor:[UIColor clearColor]];
    leftButton.frame = CGRectMake(0, 20 + (44 - 22) /2., 22, 22);
    [leftButton setImage:[UIImage imageNamed:@"designer_mine_post"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
        CGRect rect = [self.navigationController.navigationBar convertRect:leftButton.frame toView:window];
        
        [strongSelf showMenu:leftButton WithRect:rect];
    }];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

-(void)arrayWithInit{
    self.segmentArr = @[@"我的案例",@"我的商品",@"我的实景搭配",@"我的选材搭配"];
    self.collectionViewMutableArr = [NSMutableArray array];
    self.mutableArrOne = [NSMutableArray array];
    self.mutableArrTwo = [NSMutableArray array];
    self.mutableArrThr = [NSMutableArray array];
    self.mutableArrFour = [NSMutableArray array];
}

#pragma mark - createDesignHeaderView
-(void)createdesignHeaderView{
    self.designerMineHeaderView = [[MJDesignerMineHeaderView alloc]initWithFrame:CGRectMake(0, 64, kScreenBounds.size.width, MJFloat(150))];
    self.designerMineHeaderView.userInteractionEnabled = YES;
    self.designerMineHeaderView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.designerMineHeaderView];
    // 1. 创建齿轮button
    UIButton *settingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [settingButton setImage:[UIImage imageNamed:@"designer_mine_setting"] forState:UIControlStateNormal];
    settingButton.backgroundColor = [UIColor clearColor];
    settingButton.frame = CGRectMake(kScreenBounds.size.width - MJFloat(15) - MJFloat(24), (self.designerMineHeaderView.size_height - MJFloat(24)) / 2., MJFloat(24), MJFloat(24));
    [self.designerMineHeaderView addSubview:settingButton];
    __weak typeof(self)weakSelf = self;
    [settingButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        MJDesignerSettingViewController *designerViewController = [[MJDesignerSettingViewController alloc]init];
        designerViewController.designerDetailInfoModel = self.designerDetailInfoModel;
        [strongSelf.navigationController pushViewController:designerViewController animated:YES];
    }];
    
    // 2. 头像点击
    [self.designerMineHeaderView headerImageViewTapWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
        CGRect rect = [strongSelf.designerMineHeaderView convertRect:strongSelf.designerMineHeaderView.headerImageView.frame toView:window];
        
        [strongSelf showHeaderImageView:strongSelf.designerMineHeaderView.headerImageView rect:rect];
    }];
}


#pragma mark - HTHorizontalSelectionList
-(void)createSegment{
    self.segmentList = [[HTHorizontalSelectionList alloc]init];
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentList.font = [UIFont systemFontOfCustomeSize:16.];
    self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
    self.segmentList.bottomTrimColor = [UIColor colorWithCustomerName:@"分割线"];
    self.segmentList.frame = CGRectMake(0, CGRectGetMaxY(self.designerMineHeaderView.frame), kScreenBounds.size.width, MJFloat(40));
    self.segmentList.isNotScroll = YES;
    [self.view addSubview:self.segmentList];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.mainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, self.mainScrollView.contentOffset.y) animated:YES];
}


#pragma mark - createCollectionView
-(void)createMainScrollView{
    self.mainScrollView = [[UIScrollView alloc]init];
    self.mainScrollView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainScrollView.delegate = self;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.segmentList.frame) - 44 );
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 4, self.mainScrollView.size_height);
    [self.view addSubview:self.mainScrollView];
    
    if (!self.collectionViewOne){
        self.collectionViewOne = [self createCustomerCollectionView];
        self.collectionViewOne.orgin_x = 0;
        [self.mainScrollView addSubview:self.collectionViewOne];
        [self.collectionViewOne registerClass:[MJDesignerMineOneCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
        [self.collectionViewMutableArr addObject:self.collectionViewOne];
    }
    if (!self.collectionViewTwo){
        self.collectionViewTwo = [self createCustomerCollectionView];
        self.collectionViewTwo.orgin_x = kScreenBounds.size.width;
        [self.mainScrollView addSubview:self.collectionViewTwo];
        [self.collectionViewTwo registerClass:[MJDesignerMineOneCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
        [self.collectionViewMutableArr addObject:self.collectionViewTwo];
    }
    if (!self.collectionViewThr){
        self.collectionViewThr = [self createCustomerCollectionView];
        self.collectionViewThr.orgin_x = kScreenBounds.size.width * 2;
        [self.mainScrollView addSubview:self.collectionViewThr];
        [self.collectionViewThr registerClass:[MJDesignerMineOneCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
        [self.collectionViewMutableArr addObject:self.collectionViewThr];
    }
    if (!self.collectionViewFour){
        self.collectionViewFour = [self createCustomerCollectionView];
        self.collectionViewFour.orgin_x = kScreenBounds.size.width * 3;
        [self.mainScrollView addSubview:self.collectionViewFour];
        [self.collectionViewFour registerClass:[MJDesignerMineOneCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
        [self.collectionViewMutableArr addObject:self.collectionViewFour];
    }
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.collectionViewOne){      // 1.
        return self.mutableArrOne.count;
    } else if (collectionView == self.collectionViewTwo){   // 2.
        return self.mutableArrTwo.count;
    } else if (collectionView == self.collectionViewThr){   // 3.
        return self.mutableArrThr.count;
    } else if (collectionView == self.collectionViewFour){  // 4.
        return self.mutableArrFour.count;
    }
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MJDesignerMineOneCollectionViewCell *cell = (MJDesignerMineOneCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    if (collectionView == self.collectionViewTwo){  // 商品
        MJDesignerProductClassSingleModel *productSingleModel = [self.mutableArrTwo objectAtIndex:indexPath.row];
        cell.transferproductClassSingleModel = productSingleModel;
    } else if (collectionView == self.collectionViewOne){   // 案例
        MJDesignSingleAblum *singleAblumModel = [self.mutableArrOne objectAtIndex:indexPath.row];
        cell.transferSingleAblum = singleAblumModel;
    } else if (collectionView == self.collectionViewThr){       // 我的实景搭配
        MJDesignerWorksSingleModel *singleModel = [self.mutableArrThr objectAtIndex:indexPath.row];
        cell.transferWorksSingleModel = singleModel;
    } else if (collectionView == self.collectionViewFour){      // 我的选材搭配
        MJDesignerMaterialSingleModel *materialSingleModel = [self.mutableArrFour objectAtIndex:indexPath.row];
        cell.transferMaterialSingleModel = materialSingleModel;
    }
    
    
    
    __weak typeof(self)weakSelf = self;
    if (collectionView == self.collectionViewTwo){      // 我的商品
        cell.cellType = cellTypeProduct;
        [cell productEditBlock:^(MJDesignerProductClassSingleModel *transferproductClassSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            MJDesignerEditViewController *editViewController = [[MJDesignerEditViewController alloc]init];
            editViewController.transferProductClassSingleModel = transferproductClassSingleModel;
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:editViewController];

            [strongSelf.navigationController presentViewController:nav animated:YES completion:NULL];
        }];
    } else if (collectionView == self.collectionViewOne){                                            // 其他        // 修改方案
        cell.cellType = cellTypeCase;
        [cell caseEditBlock:^(MJDesignSingleAblum *transferSingleAblum) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            MJDesignerEditViewController *editViewController = [[MJDesignerEditViewController alloc]init];
            editViewController.transferSingleAblum = transferSingleAblum;
            editViewController.deleteCaseBlock = ^(MJDesignSingleAblum *transferSingleAblum){
                if ([strongSelf.mutableArrOne containsObject:transferSingleAblum]){
                    NSInteger index = [strongSelf.mutableArrOne indexOfObject:transferSingleAblum];
                    [strongSelf.mutableArrOne removeObject:transferSingleAblum];
                    NSIndexPath *deleteIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
                    [strongSelf.collectionViewOne deleteItemsAtIndexPaths:@[deleteIndexPath]];
                }
                
            };
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:editViewController];
            
            [strongSelf.navigationController presentViewController:nav animated:YES completion:NULL];
        }];
    } else if (collectionView == self.collectionViewThr){       // 我的实景搭配
        cell.cellType = cellTypeReal;
        [cell realEditClickBlock:^(MJDesignerWorksSingleModel *transferWorksSingleModel) {
            if (!weakSelf){
                return ;
            }
#warning todo实景搭配Edit
        }];
    } else if (collectionView == self.collectionViewFour){      // 我的选材搭配
        cell.cellType = cellTypeMaterial;
        [cell materialEditBlock:^(MJDesignerMaterialSingleModel *transferMaterialSingleModel) {
            if (!weakSelf){
                return ;
            }
#warning todo选材搭配Edit
        }];
    }



    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.collectionViewTwo){
        return [MJDesignerMineOneCollectionViewCell calculationSizeWithType:cellTypeProduct];
    } else {
        return [MJDesignerMineOneCollectionViewCell calculationSizeWithType:cellTypeCase];
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, MJFloat(10), 5, MJFloat(10));
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MJDesignMineProductDetailViewController *designMineProiductDetailViewController = [[MJDesignMineProductDetailViewController alloc]init];
    
    if (collectionView == self.collectionViewOne){          // 【方案】
        designMineProiductDetailViewController.productDetailViewControllerType = productDetailViewControllerTypeCase;
        MJDesignSingleAblum *ablumSingleModel = [self.mutableArrOne objectAtIndex:indexPath.row];
        designMineProiductDetailViewController.transferProductName = ablumSingleModel.name;
        designMineProiductDetailViewController.transferAblumId = ablumSingleModel.ablumId;
        [self.navigationController pushViewController:designMineProiductDetailViewController animated:YES];
    } else if (collectionView == self.collectionViewTwo){       // 【商品】
        designMineProiductDetailViewController.productDetailViewControllerType = productDetailViewControllerTypeProduct;
        MJDesignerProductClassSingleModel *productSingleModel = [self.mutableArrTwo objectAtIndex:indexPath.row];
        designMineProiductDetailViewController.transferProductName = productSingleModel.name;
        designMineProiductDetailViewController.transferAblumId = productSingleModel.productClassId;
        [self.navigationController pushViewController:designMineProiductDetailViewController animated:YES];
    } else if (collectionView == self.collectionViewThr){           // 我的实景搭配
#warning todo实景搭配
    } else if (collectionView == self.collectionViewFour){          // 我的选材搭配
#warning todo选材搭配
    }

}




-(UICollectionView *)createCustomerCollectionView{
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.mainScrollView.bounds collectionViewLayout:flowLayout];
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.showsVerticalScrollIndicator = NO;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.scrollsToTop = YES;
    return collectionView;
}

#pragma mark - createMenu
-(void)navigationBarMenu{
    [MJMenu setTintColor:[UIColor whiteColor]];
    [MJMenu setTitleFont:[UIFont systemFontOfSize:14]];
}

- (void)showMenu:(UIButton *)sender WithRect:(CGRect)rect{
    NSArray *menuItems = @[[MJMenuItem menuItem:@"上传商品" image:nil target:self action:@selector(uploadProduct)],
                           [MJMenuItem menuItem:@"上传案例" image:nil target:self action:@selector(uploadCase)]];
    
    MJMenuItem *first = menuItems[0];
    first.foreColor =  UURed;
    
    MJMenuItem *two = menuItems[1];
    two.foreColor =  UURed;
    
    [MJMenu showMenuInView:self.view.window fromRect:rect menuItems:menuItems];
}

-(void)showHeaderImageView:(UIImageView *)sender rect:(CGRect)rect{
    NSArray *menuItems = @[[MJMenuItem menuItem:@"     相册     " image:nil target:self action:@selector(choosePhotoInAssetsLibrary)],
                           [MJMenuItem menuItem:@"     拍照     " image:nil target:self action:@selector(takePhoto)]];
    MJMenuItem *first = menuItems[0];
    first.foreColor =  UURed;
    
    MJMenuItem *two = menuItems[1];
    two.foreColor =  UURed;
    
    [MJMenu showMenuInView:self.view.window fromRect:rect menuItems:menuItems];

}


#pragma mark - Action
-(void)choosePhotoInAssetsLibrary{          // 从相册寻找照片
    [self choostWithAssetBlock:^(UIImage *image) {
        NSLog(@"%@",image);
    }];
}

-(void)takePhoto{
    [self choostWithPhotoBlock:^(UIImage *image) {
        
    }];
}

-(void)uploadProduct{           // 上传商品
    MJDesignerAddDrawingBoard *shareVC = [[MJDesignerAddDrawingBoard alloc] init];
    [shareVC hideParentViewControllerTabbar:self];
    shareVC.isShareProduct = YES;
    shareVC.isHasGesture = NO;
    // 修改商品列表
    __weak typeof(self)weakSelf = self;
    [shareVC showInView:self type:drawingBoardTypeProduct andBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.mutableArrTwo.count){
            [strongSelf.mutableArrTwo removeAllObjects];
        }
        [strongSelf.segmentList setSelectedButtonIndex:1 animated:YES];
        [strongSelf sendRequestProductList];
    }];
    
    [shareVC addProManagerBlock:^(MJDesignerProductClassSingleModel *productClassSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"上传成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            for (MJDesignerProductClassSingleModel *ablumTempModel in strongSelf.mutableArrTwo){
                if ([ablumTempModel.productClassId isEqualToString:productClassSingleModel.productClassId]){
                    ablumTempModel.productCount++;
                    NSInteger index = [strongSelf.mutableArrTwo indexOfObject:ablumTempModel];
                    [strongSelf.collectionViewTwo reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]];
                }
            }
        }]show];
    }];
}

-(void)uploadCase{              // 上传案例
    MJDesignerAddDrawingBoard *shareVC = [[MJDesignerAddDrawingBoard alloc] init];
    [shareVC hideParentViewControllerTabbar:self];
    shareVC.isShareProduct = YES;
    shareVC.isHasGesture = NO;
    // 修改案例列表
    __weak typeof(self)weakSelf = self;
    [shareVC showInView:self type:drawingBoardTypeCase andBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.mutableArrOne.count){
            [strongSelf.mutableArrOne removeAllObjects];
        }
        [strongSelf.segmentList setSelectedButtonIndex:0 animated:YES];
        [strongSelf sendRequestToGetMyCase];
    }];
    [shareVC addImgManagerWithBlock:^(MJDesignSingleAblum *ablumSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"上传成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            for (MJDesignSingleAblum *ablumTempModel in strongSelf.mutableArrOne){
                if ([ablumTempModel.ablumId isEqualToString:ablumSingleModel.ablumId]){
                    ablumTempModel.imageCount ++;
                    NSInteger index = [strongSelf.mutableArrOne indexOfObject:ablumTempModel];
                    [strongSelf.collectionViewOne reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]];
                }
            }
        }]show];
    }];
}



#pragma mark - Interface
#pragma mark 获取当前设计师的信息
-(void)sendRequestToGetDesignerDetailInfo{
    MJDesignerDetailInfoModel *designerDetailInfoModel = [[MJDesignerDetailInfoModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [designerDetailInfoModel fetchWithPath:@"designer/getDetail" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            // 1. 头像
//            [self.designerMineHeaderView.headerImageView uploadImageWithURL:designerDetailInfoModel.userAvatarURL placeholder:[UIImage imageNamed:@"designer_mine_head"] callback:NULL];
//            [self.designerMineHeaderView.headerImageView uploadImageWithURL200:designerDetailInfoModel. imageSuffix:<#(NSString *)#> placeholder:<#(UIImage *)#> callback:<#^(UIImage *)callbackBlock#>]
            
            // 2. label
            strongSelf.designerMineHeaderView.authorNameLabel.text = designerDetailInfoModel.nickName;
            
            // 3. 创建
        }
    }];
}

#pragma mark  1.获取相册列表接口
-(void)sendRequestToGetMyCase{
    __weak typeof(self)weakSelf = self;
    MJDesignAblumList *designAblumList = [[MJDesignAblumList alloc]init];
    designAblumList.requestParams = @{@"referenceID":[GWTool userDefaultGetWithKey:@"userID"]};
    [designAblumList fetchWithPath:@"album/getList" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.mutableArrOne addObjectsFromArray:designAblumList.albumList];
            [strongSelf.collectionViewOne reloadData];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma mark 2.获取当前商品数量
-(void)sendRequestProductList{
    MJDesignerProductClassListModel *productMainModel = [[MJDesignerProductClassListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [productMainModel fetchWithGetPath:@"designerProductType/getListByDesignerID" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.mutableArrTwo addObjectsFromArray:productMainModel.productTypeList];
            [strongSelf.collectionViewTwo reloadData];
            
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma mark - 获取我的实景搭配
-(void)sendRequestRealCollocation{
    MJDesignerMainModel *designerMainModel = [[MJDesignerMainModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [designerMainModel fetchWithPath:@"works/getList" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.mutableArrThr addObjectsFromArray:designerMainModel.worksList];
            [strongSelf.collectionViewThr reloadData];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma mark - 获取选材搭配
-(void)sendRequestGetPlaneWorksList{
    MJDesignerMaterialMainModel *materialModel = [[MJDesignerMaterialMainModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [materialModel fetchWithPath:@"planeWorks/getList" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.mutableArrFour addObjectsFromArray:materialModel.planeWorksList];
            [strongSelf.collectionViewFour reloadData];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


#pragma mark - 上传头像
-(void)fetchAvatar{
    
}

#pragma mark - ScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView){
        CGPoint point = scrollView.contentOffset;
        NSInteger page = point.x / kScreenBounds.size.width;
        [self.segmentList setSelectedButtonIndex:page animated:YES];
    }
}

-(void)sendRequestToGetAvatar{
    MJAvatorTempModel *fetchModel = [[MJAvatorTempModel alloc]init];
    fetchModel.requestParams = @{@"ids":[MJUserDefaults objectForKey:@"userID"]};
    __weak typeof(self)weakSelf = self;
    
    [fetchModel fetchWithGetPath1:@"http://api.duc.cn/user/getUserByQuery" completionHandler:^(BOOL isSucceeded, NSError *error, id data) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            NSDictionary *dic = [(NSArray *)data lastObject];
            NSString *string = [dic objectForKey:@"avatar"];
            [strongSelf.designerMineHeaderView.headerImageView uploadImageWithURL:string placeholder:nil callback:NULL];
        }
    }];
}

-(void)sendRequestGetDesignerDetail{
    self.designerDetailInfoModel = [[MJDesignerDetailInfoModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [self.designerDetailInfoModel fetchWithPath:@"designer/getDetail" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSrlf = self;
        if (isSucceeded){
            strongSrlf.designerMineHeaderView.authorNameLabel.text = self.designerDetailInfoModel.nickName;
            strongSrlf.designerMineHeaderView.aboutDymicLabel.text = self.designerDetailInfoModel.introduce;
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

@end
