//
//  MJDesignerEditViewController.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/26.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "AbstractViewController.h"
#import "MJDesignerProductClassSingleModel.h"
#import "MJDesignSingleAblum.h"
#import "MJDesignerMaterialSingleModel.h"

typedef void(^designerClassDeleteBlock)(MJDesignerProductClassSingleModel *transferProductClassSingleModel);          /**< 删除block*/
typedef void (^designerCaseDeleteBlock)(MJDesignSingleAblum *transferSingleAblum);           /**< 删除相册block*/

typedef void (^materialDeleteBlock)(MJDesignerMaterialSingleModel *transferMaterialSingleModel);           /**< 删除相册block*/

@interface MJDesignerEditViewController : AbstractViewController

@property (nonatomic,copy)designerClassDeleteBlock block;
@property (nonatomic,copy)designerCaseDeleteBlock deleteCaseBlock;
@property (nonatomic,copy)materialDeleteBlock materialDeleteBlock;

@property (nonatomic,strong)MJDesignerProductClassSingleModel *transferProductClassSingleModel;
@property (nonatomic,strong)MJDesignSingleAblum *transferSingleAblum;
@property (nonatomic,strong)MJDesignerMaterialSingleModel *transferMaterialSingleModel;

@end
