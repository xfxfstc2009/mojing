//
//  MJDesignMineProductDetailViewController.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignMineProductDetailViewController.h"
#import "MJDesignImageListModel.h"
#import "MJProductDetailCollectionViewCell.h"
#import "MJProductMainModel.h"

@interface MJDesignMineProductDetailViewController()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic,strong)UICollectionView *collectionView;
@property (nonatomic,strong)NSMutableArray *collectionMutableArr;


@end

@implementation MJDesignMineProductDetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createCollectionView];
    __weak typeof(self)weakSelf = self;
    if (self.productDetailViewControllerType == productDetailViewControllerTypeProduct){         //商品
        [weakSelf sendRequestToGetproductList];
    } else if (self.productDetailViewControllerType == productDetailViewControllerTypeCase){        // 方案
        [weakSelf sendRequestToGetInfo];                // 获取当前的图片列表
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = self.transferProductName;               // 设置当前的title
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.collectionMutableArr = [NSMutableArray array];
}

#pragma mark createCollectionView
-(void)createCollectionView{
    if (!self.collectionView){
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        self.collectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        self.collectionView.backgroundColor = [UIColor clearColor];
        self.collectionView.showsVerticalScrollIndicator = NO;
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        self.collectionView.showsHorizontalScrollIndicator = NO;
        self.collectionView.scrollsToTop = YES;
        self.collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.collectionView registerClass:[MJProductDetailCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
        [self.view addSubview:self.collectionView];
    }
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.collectionMutableArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MJProductDetailCollectionViewCell *cell = (MJProductDetailCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
    
    if (self.productDetailViewControllerType == productDetailViewControllerTypeCase){      // 其他类型
        MJDesignImageListSingleModel *imageSingleModel = [self.collectionMutableArr objectAtIndex:indexPath.row];
        cell.transferDesignImageListSingleModel = imageSingleModel;
    } else if (self.productDetailViewControllerType == productDetailViewControllerTypeProduct){ // 商品类型
        MJProductSingleModel *productSingleModel = [self.collectionMutableArr objectAtIndex:indexPath.row];
        cell.transferProductSingleModel = productSingleModel;
    }
    return cell;
}

#pragma mark -UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.productDetailViewControllerType == productDetailViewControllerTypeCase){      // 其他类型
        return [MJProductDetailCollectionViewCell calculationSizeWithType:MJProductDetailCollectionViewCellTypeCase];
    } else if (self.productDetailViewControllerType == productDetailViewControllerTypeProduct){ // 商品类型
        return [MJProductDetailCollectionViewCell calculationSizeWithType:MJProductDetailCollectionViewCellTypeProduct];
    }
    return [MJProductDetailCollectionViewCell calculationSizeWithType:MJProductDetailCollectionViewCellTypeProduct];
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, MJFloat(10), 5, MJFloat(10));
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
   
}









#pragma mark - interface
-(void)sendRequestToGetInfo{
    MJDesignImageListModel *designImageListModel = [[MJDesignImageListModel alloc]init];
    designImageListModel.requestParams = @{@"albumID":self.transferAblumId};
    __weak typeof(self)weakSelf = self;
    [designImageListModel fetchWithPath:@"album/getImageList" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.collectionMutableArr addObjectsFromArray:designImageListModel.albumImageList];
            [strongSelf.collectionView reloadData];
        } else {
            [[UIAlertView alertViewWithTitle:@"失败" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL] show];
        }
    }];
}


// 获取商品列表
-(void)sendRequestToGetproductList{
    MJProductMainModel *productListModel = [[MJProductMainModel alloc]init];
    __weak typeof(self)weakSelf = self;
    productListModel.requestParams = @{@"designerProductTypeID":self.transferAblumId};
    [productListModel fetchWithPath:@"product/getDesignerProductList" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.collectionMutableArr addObjectsFromArray:productListModel.productList];
            [strongSelf.collectionView reloadData];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}



@end
