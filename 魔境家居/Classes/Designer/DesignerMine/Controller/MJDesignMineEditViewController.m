//
//  MJDesignMineEditViewController.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignMineEditViewController.h"
#import <objc/runtime.h>
#import "GWFetchModel.h"
static char designMineEditBlockKey;
@interface MJDesignMineEditViewController()
@property (nonatomic,strong)UITextView *inputTextView;


@end

@implementation MJDesignMineEditViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"完成" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (strongSelf.type == uploadInfoTypeClass){            // 修改分类
            [strongSelf updateDesignerProductType];
        } else if (strongSelf.type == uploadInfoTypeNickName){       // 修改昵称
            [strongSelf sendRequestWithNickName];
        } else if (strongSelf.type == uploadInfoTypeCase){          // 修改方案
            [strongSelf sendRequestToUpdateCaseName];
        } else{
            [strongSelf sendRequestWithChangeDesignerInfoWithType:strongSelf.type];
        }
    }];
    
    if (self.type == uploadInfoTypeNickName){
        self.barMainTitle = @"修改昵称";
    } else if (self.type == uploadInfoTypePhoneNumber){
        self.barMainTitle = @"手机号码";
    } else if (self.type == uploadInfoTypeExpertise){
        self.barMainTitle = @"设计专长";
    } else if (self.type == uploadInfoTypeWorkTime){
        self.barMainTitle = @"工作年限";
    } else if (self.type == uploadInfoTypeAffiliation){
        self.barMainTitle = @"所属机构";
    } else if (self.type == uploadInfoTypeAbout){
        self.barMainTitle = @"关于自己";
    } else if (self.type == uploadInfoTypeClass){
        self.barMainTitle = @"修改分类";
    } else if (self.type == uploadInfoTypeCase){
        self.barMainTitle = @"修改方案";
    } else if (self.type == uploadInfoTypeMaterial){
        self.barMainTitle = @"修改选材搭配";
    } else {
        self.barMainTitle = @"其他";
    }
}

-(void)createView{
    self.inputTextView = [[UITextView alloc]init];
    self.inputTextView.backgroundColor = [UIColor clearColor];
    self.inputTextView.font = [UIFont systemFontOfSize:18];
    self.inputTextView.textColor = [UIColor blackColor];
    self.inputTextView.limitMax = 1000;
    self.inputTextView.frame = CGRectMake(MJFloat(11), MJFloat(10), kScreenBounds.size.width - 2 * MJFloat(11), kScreenBounds.size.height - 2 * MJFloat(10));
    self.inputTextView.layer.borderColor = [UIColor clearColor].CGColor;
    NSString *placeholder = @"";
    if (self.type == uploadInfoTypeAbout){
        placeholder = @"请输入自己的介绍";
    } else if (self.type == uploadInfoTypeAffiliation){
        placeholder = @"请输入所属机构";
    } else if (self.type == uploadInfoTypeExpertise){
        placeholder = @"请输入自己的设计专长";
    } else if (self.type == uploadInfoTypeNickName){
        placeholder = @"请输入自己的昵称";
    } else if (self.type == uploadInfoTypeWorkTime){
        placeholder = @"请输入自己的工作年限";
    } else if (self.type == uploadInfoTypeCase){
        placeholder = @"请输入方案名称";
    } else if (self.type == uploadInfoTypeMaterial){
        placeholder = @"修改选材搭配名称";
    }
    self.inputTextView.placeholder = placeholder;
    [self.view addSubview:self.inputTextView];
}


-(void)uploadInfo:(NSString *)info typeInfo:(uploadInfoType)type andBlockManager:(void(^)(NSString *info))block{
    self.type = type;
    objc_setAssociatedObject(self, &designMineEditBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 修改用户信息
-(void)updateDesignerProductType{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    fetchModel.requestParams = @{@"id":self.transferProductClassSingleModel.productClassId,@"name":self.inputTextView.text};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:@"designerProductType/save" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"修改成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                strongSelf.transferProductClassSingleModel.name = strongSelf.inputTextView.text.length?strongSelf.inputTextView.text:strongSelf.transferProductClassSingleModel.name;
                void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &designMineEditBlockKey);
                if (block){
                    block(strongSelf.inputTextView.text);
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }] show];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

// 修改用户昵称
-(void)sendRequestWithNickName{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    __weak typeof(self)weakSelf =  self;
    fetchModel.requestParams = @{@"nickName":self.inputTextView.text};
    [fetchModel fetchWithPath:@"user/updateNickName" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"修改成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                strongSelf.transferDesignerDetailInfoModel.nickName = strongSelf.inputTextView.text.length?strongSelf.inputTextView.text:strongSelf.transferDesignerDetailInfoModel.nickName;
                void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &designMineEditBlockKey);
                if (block){
                    block(strongSelf.inputTextView.text);
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }] show];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}


-(void)sendRequestWithChangeDesignerInfoWithType:(uploadInfoType)type{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    NSDictionary *dic = [NSDictionary dictionary];
    if (type == uploadInfoTypeExpertise){       // 设计专场
        dic = @{@"specialty":self.inputTextView.text};
    } else if (type == uploadInfoTypeWorkTime){ // 工作年限
        dic = @{@"workingLife":self.inputTextView.text};
    } else if (type == uploadInfoTypeAffiliation){  // 所属机构
        dic = @{@"organization":self.inputTextView.text};
    } else if (type == uploadInfoTypeAbout){        // 关于自己
        dic = @{@"introduce":self.inputTextView.text};
    }
    fetchModel.requestParams = dic;
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:@"designer/update" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"修改成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (type == uploadInfoTypeExpertise){           // 设计专场
                    strongSelf.transferDesignerDetailInfoModel.specialty = strongSelf.inputTextView.text;
                } else if (type == uploadInfoTypeWorkTime){     // 工作年限
                    strongSelf.transferDesignerDetailInfoModel.workingLife = strongSelf.inputTextView.text;
                } else if (type == uploadInfoTypeAffiliation){  // 所属机构
                    strongSelf.transferDesignerDetailInfoModel.organization = strongSelf.inputTextView.text;
                } else if (type == uploadInfoTypeAbout){        // 关于自己
                    strongSelf.transferDesignerDetailInfoModel.introduce = strongSelf.inputTextView.text;
                }
                
                void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &designMineEditBlockKey);
                if (block){
                    block(strongSelf.inputTextView.text);
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }] show];
            
        } else {
              [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma mark 修改方案名称
-(void)sendRequestToUpdateCaseName{
    GWFetchModel *fetchModel = [[GWFetchModel alloc]init];
    fetchModel.requestParams = @{@"id":self.transferDesignSingleAblumModel.ablumId,@"name":self.inputTextView.text.length?self.inputTextView.text:self.transferDesignSingleAblumModel.name};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:@"album/save" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"修改成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                
                strongSelf.transferDesignSingleAblumModel.name = strongSelf.inputTextView.text.length?strongSelf.inputTextView.text:strongSelf.transferDesignSingleAblumModel.name;
                void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &designMineEditBlockKey);
                if (block){
                    block(strongSelf.inputTextView.text);
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }] show];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

@end
