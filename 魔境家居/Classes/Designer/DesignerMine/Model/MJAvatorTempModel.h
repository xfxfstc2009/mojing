//
//  MJAvatorTempModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/27.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"

@protocol MJAvatorTempModel <NSObject>

@end

@interface MJAvatorTempModel : GWFetchModel

@property (nonatomic,copy)NSString *avatarPic;
@end
