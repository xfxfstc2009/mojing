//
//  MJDesignerWorksSingleModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/25.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"

@protocol MJDesignerWorksSingleModel <NSObject>

@end

@interface MJDesignerWorksSingleModel : GWFetchModel

@property (nonatomic,copy)NSString* worksId;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *spaceTypeID;                    /**< 空间id*/
@property (nonatomic,copy)NSString *spaceTypeName;                  /**< 空间名称*/
@property (nonatomic,assign)NSTimeInterval createdDateTime;        /**< 创建时间*/
@property (nonatomic,assign)NSTimeInterval modifiedDateTime;       /**< 修改时间*/
@property (nonatomic,copy)NSString *desc;                           /**< 描述*/
@property (nonatomic,copy)NSString *collocationData;                /**< 渲染的json*/
@property (nonatomic,copy)NSString *bgImageID;                      /**< 背景图片*/
@property (nonatomic,copy)NSString *bgImageName;                    /**< 背景图片名称*/
@property (nonatomic,copy)NSString *bgImageSuffix;                  /**< 背景图片后缀*/
@property (nonatomic,copy)NSString *collocationImageID;             /**< 效果图片id*/
@property (nonatomic,copy)NSString *collocationImageName;           /**< 效果图片的名称*/
@property (nonatomic,copy)NSString *collocationImageSuffix;         /**< 效果图片的后缀*/
@property (nonatomic,assign)BOOL isFavorited;                       /**< 是否收藏*/
@property (nonatomic,copy)NSString *userID;                         /**< 用户ID*/
@property (nonatomic,copy)NSString *userName;                       /**< 上传者用户名*/
@property (nonatomic,copy)NSString *userNickName;                   /**< 上传者的昵称*/
@property (nonatomic,copy)NSString *userAvatarURL;                  /**< 用户头像url*/
@property (nonatomic,copy)NSString *favoriteCount;                  /**< 喜欢总数*/
@property (nonatomic,assign)BOOL isDeleted;                         /**< 是否删除*/

@end
