//
//  MJDesigner_DetailInfoModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"

@interface MJDesignerDetailInfoModel : GWFetchModel

@property (nonatomic,copy) NSString *provinceName;          /**< 省份*/
@property (nonatomic,copy) NSString *cityName;              /**< 城市*/
@property (nonatomic,copy) NSString *workingLife;           /**< 工作年限*/
@property (nonatomic,copy) NSString *organization;          /**< 所属机构*/
@property (nonatomic,copy) NSString *introduce;             /**< 个人简介*/
@property (nonatomic,copy) NSString *specialty;             /**< 设计专长*/
@property (nonatomic,assign)NSInteger score;                /**< 分数*/
@property (nonatomic,assign)NSInteger modelCount;           /**< 模型数量*/
@property (nonatomic,assign)NSInteger existModelCount;      /**< 存在模型计数*/

@property (nonatomic,assign)NSInteger existProductCount;    /**< 我的商品*/
@property (nonatomic,assign)NSInteger existCaseCount;       /**< 我的方案*/
@property (nonatomic,assign)NSInteger existWorksCount;      /**< 我的实景搭配*/
@property (nonatomic,assign)NSInteger existPlaneWorksCount; /**< 我的选材搭配*/


@property (nonatomic,copy)NSString *userName;               /**< 用户名称*/
@property (nonatomic,copy)NSString *userAvatarURL;          /**< 用户头像*/
@property (nonatomic,copy)NSString *nickName;               /**< 用户昵称*/


@end
