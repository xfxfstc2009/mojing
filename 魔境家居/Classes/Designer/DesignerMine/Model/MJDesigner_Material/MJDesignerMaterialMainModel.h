//
//  MJDesignerMaterialMainModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/25.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"
#import "MJDesignerMaterialSingleModel.h"
@interface MJDesignerMaterialMainModel : GWFetchModel

@property (nonatomic,copy)NSString *pageCount;
@property (nonatomic,copy)NSString *pageNumber;
@property (nonatomic,copy)NSString *pageSize;
@property (nonatomic,copy)NSString *resourceCount;
@property (nonatomic,strong)NSArray<MJDesignerMaterialSingleModel> *planeWorksList; /**< list*/

@end
