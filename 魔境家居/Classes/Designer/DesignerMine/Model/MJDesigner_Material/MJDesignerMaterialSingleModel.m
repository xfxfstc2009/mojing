//
//  MJDesignerMaterialSingleModel.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/25.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerMaterialSingleModel.h"

@implementation MJDesignerMaterialSingleModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"materialId": @"id",@"desc":@"description"};
}

@end
