//
//  MJDesignerMaterialSingleModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/25.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"

@protocol  MJDesignerMaterialSingleModel<NSObject>

@end

@interface MJDesignerMaterialSingleModel : GWFetchModel

@property (nonatomic,copy)NSString *materialId;     /**< id*/
@property (nonatomic,copy)NSString *name;           /**< 名称*/
@property (nonatomic,copy)NSString *spaceTypeID;    /**< 空间类型id*/
@property (nonatomic,copy)NSString *spaceTypeName;  /**< 所属空间类型的名称*/
@property (nonatomic,assign)NSTimeInterval createdDateTime; /**< 创建时间*/
@property (nonatomic,assign)NSTimeInterval modifiedDateTime;/**< 修改日期*/
@property (nonatomic,copy)NSString *desc;
@property (nonatomic,copy)NSString *collocationData;        /**< 搭配作品的渲染数据json*/
@property (nonatomic,copy)NSString *bgImageID;              /**< 背景图片id*/
@property (nonatomic,copy)NSString *bgImageName;            /**< 背景图片的名称*/
@property (nonatomic,copy)NSString *bgImageSuffix;          /**< 图片背景的url*/
@property (nonatomic,copy)NSString *collocationImageID;     /**< 效果图片id*/
@property (nonatomic,copy)NSString *collocationImageName;   /**< 效果图片名称*/
@property (nonatomic,copy)NSString *collocationImageSuffix; /**< 效果图片url*/
@property (nonatomic,assign)BOOL isFavorited;               /**< 是否收藏*/
@property (nonatomic,copy)NSString *userID;                 /**< 用户id*/
@property (nonatomic,copy)NSString *userName;               /**< 上传人的名字*/
@property (nonatomic,copy)NSString *userNickName;           /**< 用户昵称*/
@property (nonatomic,copy)NSString *userAvatarURL;          /**< 客户头像url*/
@property (nonatomic,assign)NSInteger favoriteCount;          /**< 喜欢数量*/
@property (nonatomic,assign)BOOL isDeleted;                 /**< 判断是否删除*/

@end
