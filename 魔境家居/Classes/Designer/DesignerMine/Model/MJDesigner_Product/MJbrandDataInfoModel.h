//
//  MJbrandDataModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/25.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"

@interface MJbrandDataInfoModel : GWFetchModel

@property (nonatomic,copy)NSString *brandId;            /**< 品牌id*/
@property (nonatomic,copy)NSString *name;               /**< 所属品牌名称*/
@property (nonatomic,copy)NSString *logoID;             /**< logo的编号*/
@property (nonatomic,copy)NSString *logoName;           /**< logo 名称*/
@property (nonatomic,copy)NSString *logoSuffix;         /**< 图片的名称*/

@end
