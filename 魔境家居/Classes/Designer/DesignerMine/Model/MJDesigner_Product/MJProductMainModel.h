//
//  MJProductMainModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/25.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"
#import "MJProductSingleModel.h"
@interface MJProductMainModel : GWFetchModel

@property (nonatomic,copy)NSString *pageCount;          /**< 页面总数*/
@property (nonatomic,copy)NSString *pageNumber;         /**< 当前页码*/
@property (nonatomic,copy)NSString *pageSize;           /**< 页面大小*/
@property (nonatomic,copy)NSString *resourceCount;      /**< 总收藏数*/
@property (nonatomic,strong)NSArray<MJProductSingleModel> *productList;


@end
