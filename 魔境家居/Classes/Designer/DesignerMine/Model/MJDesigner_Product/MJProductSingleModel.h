//
//  MJProductSingleModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/25.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"
#import "MJbrandDataInfoModel.h"

@protocol MJProductSingleModel <NSObject>

@end

@interface MJProductSingleModel : GWFetchModel

@property (nonatomic,copy)NSString *productId;                  /**< 商品id */
@property (nonatomic,copy)NSString *name;                       /**< 商品名称*/
@property (nonatomic,copy)NSString *productTypeID;              /**< 商品类型id*/
@property (nonatomic,copy)NSString *productTypeName;            /**< 商品类型名称*/
@property (nonatomic,copy)NSString *brandProductTypeName;       /**< 所属品牌商品类型名称*/
@property (nonatomic,strong)MJbrandDataInfoModel *brandData;        /**< 品牌信息*/
@property (nonatomic,assign)NSTimeInterval createdDateTime;     /**< 创建时间*/
@property (nonatomic,assign)NSTimeInterval modifiedDateTime;    /**< 修改时间*/
@property (nonatomic,copy)NSString *desc;                       /**< 商品描述*/
@property (nonatomic,assign)CGFloat price;                      /**< 价格*/
@property (nonatomic,copy)NSString *imageID;                    /**< 图片编号*/
@property (nonatomic,copy)NSString *imageName;                  /**< 图片名称*/
@property (nonatomic,copy)NSString *imageSuffix;                /**< 图片后缀*/
@property (nonatomic,assign)BOOL isFavorited;                   /**< 是否被收藏*/
@property (nonatomic,assign)BOOL isNew;                         /**< 是否是新品*/
@property (nonatomic,assign)NSInteger usedCount;                /**< usedCount*/
@property (nonatomic,copy)NSString *userID;                     /**< 用户编号*/
@property (nonatomic,copy)NSString *userName;                   /**< 用户名称*/
@property (nonatomic,assign)NSInteger favoriteCount;            /**< 喜欢数量*/
@property (nonatomic,copy)NSString *modelID;                    /**< 模型编号*/
@property (nonatomic,copy)NSString *modelName;                  /**< 模型zip名称*/
@property (nonatomic,copy)NSString *modelSuffix;                /**< 模型url*/
@property (nonatomic,assign)BOOL isDeleted;                     /**< 是否删除*/
@end
