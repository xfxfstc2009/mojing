//
//  MJDesignerProductClassSingleModel.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/26.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerProductClassSingleModel.h"

@implementation MJDesignerProductClassSingleModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"productClassId": @"id"};
}


@end
