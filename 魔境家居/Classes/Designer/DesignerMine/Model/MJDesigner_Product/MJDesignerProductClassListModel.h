//
//  MJDesignerProductClassListModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/26.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"
#import "MJDesignerProductClassSingleModel.h"
@interface MJDesignerProductClassListModel : GWFetchModel

@property (nonatomic,strong)NSArray<MJDesignerProductClassSingleModel> *productTypeList;
@end
