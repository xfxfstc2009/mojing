//
//  MJProductSingleModel.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/25.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJProductSingleModel.h"

@implementation MJProductSingleModel
- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"productId": @"id",@"desc":@"description"};
}
@end
