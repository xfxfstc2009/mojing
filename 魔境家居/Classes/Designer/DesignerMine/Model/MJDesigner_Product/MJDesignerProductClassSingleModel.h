//
//  MJDesignerProductClassSingleModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/26.
//  Copyright © 2016年 mojing. All rights reserved.
//
// 获取设计商品类型列表
#import "GWFetchModel.h"

@protocol MJDesignerProductClassSingleModel <NSObject>

@end

@interface MJDesignerProductClassSingleModel : GWFetchModel

@property (nonatomic,copy)NSString *productClassId;             /**< 商品类型id*/
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *parentID;                   /**< 父类id*/
@property (nonatomic,copy)NSString *designerID;                 /**< 设计师id*/
@property (nonatomic,assign)BOOL isDeleted;                     /**< 是否已经删除*/
@property (nonatomic,assign)NSInteger productCount;             /**< 商品数量*/
@property (nonatomic,copy)NSString *firstProductImageName;      /**< 图片名称*/
@property (nonatomic,copy)NSString *firstProductImageID;        /**< 图片id*/
@property (nonatomic,copy)NSString *firstProductImageSuffix;    /**< 图片后缀*/


@end
