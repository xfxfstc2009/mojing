//
//  MJDesignAblumList.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"
#import "MJDesignSingleAblum.h"

@interface MJDesignAblumList : GWFetchModel

@property (nonatomic,strong)NSArray<MJDesignSingleAblum > *albumList;

@end
