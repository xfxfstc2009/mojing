//
//  MJDesignerAblumAddModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"

@interface MJDesignerAblumAddModel : GWFetchModel

@property (nonatomic,copy)NSString *ablumId;        /**< 相册id*/

@end
