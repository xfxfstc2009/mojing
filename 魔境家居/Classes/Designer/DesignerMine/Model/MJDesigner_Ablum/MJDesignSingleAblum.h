//
//  MJDesignSingleAblum.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"

@protocol MJDesignSingleAblum <NSObject>

@end

@interface MJDesignSingleAblum : GWFetchModel
@property (nonatomic,copy)NSString *ablumId;            /**< 相册ID*/
@property (nonatomic,copy)NSString *name;               /**< 相册名称*/
@property (nonatomic,copy)NSString *referenceID;        /**< 参考标志*/
@property (nonatomic,assign)NSInteger imageCount;         /**< 图片总数*/
@property (nonatomic,copy)NSString *firstImageSuffix;   /**< 图片后缀*/
@property (nonatomic,copy)NSString *firstImageID;       /**< 图片ID*/
@property (nonatomic,copy)NSString *firstImageName;         /**< 图片总数*/






@end
