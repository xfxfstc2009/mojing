//
//  MJDesignSingleAblum.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignSingleAblum.h"

@implementation MJDesignSingleAblum

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ablumId": @"id"};
}

@end
