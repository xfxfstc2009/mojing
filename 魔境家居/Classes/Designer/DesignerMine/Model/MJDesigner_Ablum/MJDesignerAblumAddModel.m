//
//  MJDesignerAblumAddModel.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerAblumAddModel.h"

@implementation MJDesignerAblumAddModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ablumId": @"id"};
}

@end
