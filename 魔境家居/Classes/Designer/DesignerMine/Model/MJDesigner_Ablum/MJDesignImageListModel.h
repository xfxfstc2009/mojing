//
//  MJDesignImageListModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"
#import "MJDesignImageListSingleModel.h"


@interface MJDesignImageListModel : GWFetchModel

@property (nonatomic,assign)NSInteger pageCount;              /**< 总页数*/
@property (nonatomic,assign)NSInteger pageNumber;             /**< 当前页面*/
@property (nonatomic,assign)NSInteger pageSize;               /**< 一页大小*/
@property (nonatomic,assign)NSInteger resourceCount;          /**< 总共数量*/
@property (nonatomic,strong)NSArray <MJDesignImageListSingleModel> *albumImageList;

@end
