//
//  MJDesignImageListSingleModel.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "GWFetchModel.h"

@protocol MJDesignImageListSingleModel <NSObject>

@end

@interface MJDesignImageListSingleModel : GWFetchModel

@property (nonatomic,copy)NSString *smartId;                /**< id*/
@property (nonatomic,copy)NSString *albumID;                /**< 相册的id*/
@property (nonatomic,copy)NSString *imageID;                /**< 图片id*/
@property (nonatomic,copy)NSString *imageName;              /**< 图片名称*/
@property (nonatomic,copy)NSString *imageSuffix;            /**< 图片后缀*/
@property (nonatomic,copy)NSString *sequence;               /**< 序列*/

@end
