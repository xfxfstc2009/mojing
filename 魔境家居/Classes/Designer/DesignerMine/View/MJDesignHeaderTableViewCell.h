//
//  MJDesignHeaderTableViewCell.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJDesignHeaderTableViewCell : UITableViewCell

+(CGFloat)calculationCellHeight;

@end
