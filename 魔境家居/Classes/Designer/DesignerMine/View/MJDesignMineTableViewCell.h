//
//  MJDesignMineTableViewCell.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJDesignMineTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeiht;

+(CGFloat)calculationCellheight;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@end
