//
//  MJDesignHeaderTableViewCell.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignHeaderTableViewCell.h"

@interface MJDesignHeaderTableViewCell()<UIScrollViewDelegate>
@property (nonatomic,strong)UIScrollView *mainHeaderScrollView;
@property (nonatomic,strong)GWImageView *headerImageView;
@property (nonatomic,strong)UIView *headerPageViewOne;
@property (nonatomic,strong)UILabel *authorNameLabel;

@end

@implementation MJDesignHeaderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建背景
    self.headerImageView = [[GWImageView alloc]init];
    self.headerImageView.backgroundColor = [UIColor redColor];
    self.headerImageView.image = [UIImage imageNamed:@"mine_head_bottom"];
    self.headerImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width, MJFloat(150));
    [self addSubview:self.headerImageView];
    
    self.mainHeaderScrollView = [[UIScrollView alloc]init];
    self.mainHeaderScrollView.backgroundColor = [UIColor clearColor];
    self.mainHeaderScrollView.delegate = self;
    self.mainHeaderScrollView.showsHorizontalScrollIndicator = NO;
    self.mainHeaderScrollView.showsVerticalScrollIndicator = NO;
    self.mainHeaderScrollView.backgroundColor = [UIColor clearColor];
    self.mainHeaderScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 3, self.headerImageView.size_height);
    self.mainHeaderScrollView.pagingEnabled = YES;
    [self addSubview:self.mainHeaderScrollView];
    
    // 创建第一页
    self.headerPageViewOne = [[UIView alloc]init];
    self.headerPageViewOne.backgroundColor = [UIColor yellowColor];
    self.headerPageViewOne.frame = self.mainHeaderScrollView.bounds;
    [self.mainHeaderScrollView addSubview:self.headerPageViewOne];
    
    self.headerImageView = [[GWImageView alloc]init];
    self.headerImageView.frame = CGRectMake((kScreenBounds.size.width - MJFloat(60)) / 2., MJFloat(40), MJFloat(50), MJFloat(50));
    self.headerImageView.backgroundColor = [UIColor clearColor];;
    self.headerImageView.clipsToBounds = YES;
    self.headerImageView.layer.cornerRadius = self.headerImageView.size_height / 2.;
    [self.headerPageViewOne addSubview:self.headerImageView];
    
    // 2. 创建name
    self.authorNameLabel = [[UILabel alloc]init];
    self.authorNameLabel.backgroundColor = [UIColor clearColor];
    self.authorNameLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.authorNameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.headerImageView.frame) + LCFloat(10), kScreenBounds.size.width, [NSString contentofHeight:self.authorNameLabel.font]);
    self.authorNameLabel.text = @"采花狂魔";
    [self.headerPageViewOne addSubview:self.authorNameLabel];
}

+(CGFloat)calculationCellHeight{
    return MJFloat(150);
}
@end
