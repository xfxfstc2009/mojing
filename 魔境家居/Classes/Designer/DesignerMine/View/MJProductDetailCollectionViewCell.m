//
//  MJProductDetailCollectionViewCell.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJProductDetailCollectionViewCell.h"

@interface MJProductDetailCollectionViewCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)GWImageView *headerImageView;
@property (nonatomic,strong)UILabel *contentOfLabel;
@property (nonatomic,strong)UILabel *priceLabel;

@end

@implementation MJProductDetailCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.clipsToBounds = YES;
    self.bgView.layer.cornerRadius = LCFloat(5);
    [self addSubview:self.bgView];
    self.bgView.frame = self.bounds;
    
    // 2. 创建imageView
    self.headerImageView = [[GWImageView alloc]init];
    self.headerImageView.backgroundColor = [UIColor clearColor];
    self.headerImageView.frame = CGRectMake(0, 0, self.bounds.size.width, MJFloat(249));
    [self.bgView addSubview:self.headerImageView];
    
    // 3. 创建label
    self.contentOfLabel = [[UILabel alloc]init];
    self.contentOfLabel.backgroundColor = [UIColor clearColor];
    self.contentOfLabel.font = [UIFont systemFontOfCustomeSize:15.];
    self.contentOfLabel.frame = CGRectMake(MJFloat(10), CGRectGetMaxY(self.headerImageView.frame), self.bounds.size.width - 2 * MJFloat(10), [NSString contentofHeight:self.contentOfLabel.font]);
    [self.bgView addSubview:self.contentOfLabel];
    
    // 4. 创建label
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [UIFont systemFontOfCustomeSize:15.];
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    self.priceLabel.textColor = [UIColor colorWithRed:249/255. green:140/255. blue:98/255. alpha:1];
    [self.bgView addSubview:self.priceLabel];
}

-(void)setTransferDesignImageListSingleModel:(MJDesignImageListSingleModel *)transferDesignImageListSingleModel{
    _transferDesignImageListSingleModel = transferDesignImageListSingleModel;
    
    // 1. 图片内容
    [self.headerImageView uploadImageWithURL300:transferDesignImageListSingleModel.imageName imageSuffix:transferDesignImageListSingleModel.imageSuffix placeholder:[UIImage imageNamed:@""] callback:NULL];
    
    // 2. 图片名字
//    self.contentOfLabel.text = transferDesignImageListSingleModel.imageName;
}

// 传递过来的商品信息
-(void)setTransferProductSingleModel:(MJProductSingleModel *)transferProductSingleModel{
    [self.headerImageView uploadImageWithURL300:transferProductSingleModel.imageName imageSuffix:transferProductSingleModel.imageSuffix placeholder:[UIImage imageNamed:@""] callback:NULL];
    
    // 2. 商品价格
    self.priceLabel.text = [NSString stringWithFormat:@"￥%.2f",transferProductSingleModel.price];
    CGSize priceOfSize = [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.priceLabel.font])];
    self.priceLabel.frame = CGRectMake(self.bounds.size.width - MJFloat(10) - priceOfSize.width, CGRectGetMaxY(self.headerImageView.frame), priceOfSize.width, (self.bounds.size.height - CGRectGetMaxY(self.headerImageView.frame)));
    
    // 3. 商品名称
    self.contentOfLabel.text = transferProductSingleModel.name;
    self.contentOfLabel.frame = CGRectMake(MJFloat(10), self.priceLabel.orgin_y, self.priceLabel.orgin_x - 2 * MJFloat(10), self.priceLabel.size_height);
    
}

+(CGSize)calculationSizeWithType:(MJProductDetailCollectionViewCellType)type{
    if (type == MJProductDetailCollectionViewCellTypeProduct){
        return CGSizeMake((kScreenBounds.size.width - 5 * MJFloat(10)) / 4., MJFloat(280));
    } else if (type == MJProductDetailCollectionViewCellTypeCase){
        return CGSizeMake((kScreenBounds.size.width - 4 * MJFloat(10)) / 3., MJFloat(249));
    } else {
        return CGSizeMake(0, 0);
    }
}

@end
