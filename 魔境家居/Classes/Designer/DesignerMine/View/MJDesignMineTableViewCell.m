//
//  MJDesignMineTableViewCell.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignMineTableViewCell.h"
#import "MJDesignerMineOneCollectionViewCell.h"

@interface MJDesignMineTableViewCell()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic,strong)UICollectionView *collectionViewOne;
@property (nonatomic,strong)UICollectionView *collectionViewTwo;
@property (nonatomic,strong)UICollectionView *collectionViewThr;
@property (nonatomic,strong)UICollectionView *collectionViewFour;

@property (nonatomic,strong)NSMutableArray *mutableArrOne;
@property (nonatomic,strong)NSMutableArray *mutableArrTwo;
@property (nonatomic,strong)NSMutableArray *mutableArrThr;
@property (nonatomic,strong)NSMutableArray *mutableArrFour;


@end

@implementation MJDesignMineTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self arrayWithInit];
        [self createMainScrollView];
    }
    return self;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.mutableArrOne = [NSMutableArray array];
    self.mutableArrTwo = [NSMutableArray array];
    self.mutableArrThr = [NSMutableArray array];
    self.mutableArrFour = [NSMutableArray array];
}

#pragma mark - createMainScrollView
-(void)createMainScrollView{
    self.mainScrollView = [[UIScrollView alloc]init];
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.delegate = self;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.pagingEnabled = YES;
    [self addSubview:self.mainScrollView];
    
    if (!self.collectionViewOne){
        self.collectionViewOne = [self createCustomerCollectionView];
        self.collectionViewOne.backgroundColor = [UIColor yellowColor];
        [self.mainScrollView addSubview:self.collectionViewOne];
        [self.collectionViewOne registerClass:[MJDesignerMineOneCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
    }
    if (!self.collectionViewTwo){
        self.collectionViewTwo = [self createCustomerCollectionView];
        self.collectionViewTwo.backgroundColor = [UIColor redColor];
        [self.mainScrollView addSubview:self.collectionViewTwo];
        [self.collectionViewTwo registerClass:[MJDesignerMineOneCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
    }
    if (!self.collectionViewThr){
        self.collectionViewThr = [self createCustomerCollectionView];
        self.collectionViewThr.backgroundColor = [UIColor blackColor];
        [self.mainScrollView addSubview:self.collectionViewThr];
        [self.collectionViewThr registerClass:[MJDesignerMineOneCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
    }
    if (!self.collectionViewFour){
        self.collectionViewFour = [self createCustomerCollectionView];
        self.collectionViewFour.backgroundColor = [UIColor orangeColor];
        [self.mainScrollView addSubview:self.collectionViewFour];
        [self.collectionViewFour registerClass:[MJDesignerMineOneCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
    }
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == self.collectionViewOne){      // 1.
        return 50;
    } else if (collectionView == self.collectionViewTwo){   // 2.
        return 50;
    } else if (collectionView == self.collectionViewThr){   // 3.
        return 50;
    } else if (collectionView == self.collectionViewFour){  // 4.
            return 50;
    }
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MJDesignerMineOneCollectionViewCell *cell = (MJDesignerMineOneCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor whiteColor];
    
//    // 赋值
//    MMHSingleProductModel *singleProductModel = [self.productList itemAtIndex:indexPath.row];
//    cell.productModel = singleProductModel;
    
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.collectionViewTwo){
        return [MJDesignerMineOneCollectionViewCell calculationSizeWithType:cellTypeProduct];
    } else {
        return [MJDesignerMineOneCollectionViewCell calculationSizeWithType:cellTypeCase];
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, MJFloat(10), 5, MJFloat(10));
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"123");
}























-(void)setTransferCellHeiht:(CGFloat)transferCellHeiht{
    _transferCellHeiht = transferCellHeiht;
    self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, transferCellHeiht);
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 4 , transferCellHeiht);
    
    self.collectionViewOne.frame = CGRectMake(0, 0, self.mainScrollView.size_width, self.mainScrollView.size_height);
    self.collectionViewTwo.frame = CGRectMake(self.mainScrollView.size_width, 0, self.mainScrollView.size_width, self.mainScrollView.size_height);
    self.collectionViewThr.frame = CGRectMake(self.mainScrollView.size_width * 2, 0, self.mainScrollView.size_width, self.mainScrollView.size_height);
    self.collectionViewFour.frame = CGRectMake(self.mainScrollView.size_width * 3, 0, self.mainScrollView.size_width, self.mainScrollView.size_height);
}



-(UICollectionView *)createCustomerCollectionView{
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    UICollectionView *collectionView = [[UICollectionView alloc]initWithFrame:self.mainScrollView.bounds collectionViewLayout:flowLayout];
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.showsVerticalScrollIndicator = NO;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.scrollsToTop = YES;
    collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    return collectionView;
}

+(CGFloat)calculationCellheight{
    return kScreenBounds.size.height - MJFloat(150);
}
@end
