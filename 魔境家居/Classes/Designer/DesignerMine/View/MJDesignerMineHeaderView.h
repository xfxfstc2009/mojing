//
//  MJDesignerMineHeaderView.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJDesignerMineHeaderView : UIView
+(CGFloat)calculationCellHeight;
@property (nonatomic,strong)UIScrollView *mainHeaderScrollView;
@property (nonatomic,strong)UIView *headerPageViewOne;
@property (nonatomic,strong)GWImageView *headerImageView;
@property (nonatomic,strong)UILabel *authorNameLabel;

@property (nonatomic,strong)UILabel *aboutFixedLabel;
@property (nonatomic,strong)UILabel *aboutDymicLabel;

-(void)headerImageViewTapWithBlock:(void(^)())block;

@end
