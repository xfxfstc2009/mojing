//
//  MJProductDetailCollectionViewCell.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/24.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJDesignImageListSingleModel.h"
#import "MJProductSingleModel.h"

typedef NS_ENUM(NSInteger,MJProductDetailCollectionViewCellType) {
    MJProductDetailCollectionViewCellTypeProduct,               /**< 商品*/
    MJProductDetailCollectionViewCellTypeCase                   /**< 方案*/
};

@interface MJProductDetailCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)MJDesignImageListSingleModel *transferDesignImageListSingleModel;
@property (nonatomic,strong)MJProductSingleModel *transferProductSingleModel;           /**< 传递过来的商品model*/
+(CGSize)calculationSizeWithType:(MJProductDetailCollectionViewCellType)type;
@end
