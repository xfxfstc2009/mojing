//
//  MJDesignerMineHeaderView.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerMineHeaderView.h"
#import <objc/runtime.h>
@interface MJDesignerMineHeaderView()


@property (nonatomic,strong)UIView *headerPageViewTwo;

@end

static char headerImageViewTapKey;
@implementation MJDesignerMineHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.clipsToBounds = YES;
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建背景
    self.headerImageView = [[GWImageView alloc]init];
    self.headerImageView.image = [UIImage imageNamed:@"mine_head_bottom"];
    self.headerImageView.userInteractionEnabled = YES;
    self.headerImageView.backgroundColor = [UIColor clearColor];
    self.headerImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width, MJFloat(150));
    [self addSubview:self.headerImageView];
    
    self.mainHeaderScrollView = [[UIScrollView alloc]init];
    self.mainHeaderScrollView.userInteractionEnabled = YES;
    self.mainHeaderScrollView.backgroundColor = [UIColor clearColor];
    self.mainHeaderScrollView.showsHorizontalScrollIndicator = NO;
    self.mainHeaderScrollView.showsVerticalScrollIndicator = NO;
    self.mainHeaderScrollView.backgroundColor = [UIColor clearColor];
    self.mainHeaderScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2, self.headerImageView.size_height);
    self.mainHeaderScrollView.pagingEnabled = YES;
    self.mainHeaderScrollView.frame = self.headerImageView.bounds;
    self.mainHeaderScrollView.userInteractionEnabled = YES;
    [self.headerImageView addSubview:self.mainHeaderScrollView];
    
    // 创建第一页
    self.headerPageViewOne = [[UIView alloc]init];
    self.headerPageViewOne.backgroundColor = [UIColor clearColor];
    self.headerPageViewOne.frame = self.mainHeaderScrollView.bounds;
    [self.mainHeaderScrollView addSubview:self.headerPageViewOne];
    
    self.headerImageView = [[GWImageView alloc]init];
    self.headerImageView.frame = CGRectMake((kScreenBounds.size.width - MJFloat(50)) / 2., MJFloat(40), MJFloat(50), MJFloat(50));
    self.headerImageView.backgroundColor = [UIColor clearColor];
    self.headerImageView.clipsToBounds = YES;
    self.headerImageView.userInteractionEnabled = YES;
    self.headerImageView.layer.cornerRadius = self.headerImageView.size_height / 2.;
    [self.headerPageViewOne addSubview:self.headerImageView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerImageViewTap)];
    [self.headerImageView addGestureRecognizer:tapGesture];
    
    // 2. 创建name
    self.authorNameLabel = [[UILabel alloc]init];
    self.authorNameLabel.backgroundColor = [UIColor clearColor];
    self.authorNameLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.authorNameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.headerImageView.frame) + LCFloat(10), kScreenBounds.size.width, [NSString contentofHeight:self.authorNameLabel.font]);
    self.authorNameLabel.textAlignment = NSTextAlignmentCenter;
    self.authorNameLabel.textColor = [UIColor whiteColor];
    [self.headerPageViewOne addSubview:self.authorNameLabel];
    
    //
    self.headerPageViewTwo = [[UIView alloc]init];
    self.headerPageViewTwo.backgroundColor = [UIColor clearColor];
    self.headerPageViewTwo.frame = CGRectMake(CGRectGetMaxX(self.headerPageViewOne.frame), 0, self.mainHeaderScrollView.size_width, self.mainHeaderScrollView.size_height);
    [self.mainHeaderScrollView addSubview:self.headerPageViewTwo];
    
//    @property (nonatomic,strong)UILabel *aboutFixedLabel;
//    @property (nonatomic,strong)UILabel *aboutDymicLabel;
    self.aboutFixedLabel = [[UILabel alloc]init];
    self.aboutFixedLabel.backgroundColor = [UIColor clearColor];
    self.aboutFixedLabel.font = [UIFont systemFontOfCustomeSize:15.];
    [self.aboutFixedLabel.font boldFont];
    self.aboutFixedLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, 100);
    self.aboutFixedLabel.textAlignment = NSTextAlignmentCenter;
    self.aboutFixedLabel.textColor = [UIColor whiteColor];
    self.aboutFixedLabel.text = @"关于我";
    [self.headerPageViewTwo addSubview:self.aboutFixedLabel];
    
    self.aboutDymicLabel = [[UILabel alloc]init];
    self.aboutDymicLabel.backgroundColor = [UIColor clearColor];
    self.aboutDymicLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.aboutDymicLabel.frame = CGRectMake(LCFloat(10), CGRectGetMaxY(self.aboutFixedLabel.frame), kScreenBounds.size.width - 2 * MJFloat(100), self.headerPageViewTwo.size_height - 100);
    self.aboutDymicLabel.textAlignment = NSTextAlignmentCenter;
    self.aboutDymicLabel.textColor = [UIColor whiteColor];
    [self.headerPageViewTwo addSubview:self.aboutDymicLabel];
    
}

-(void)headerImageViewTap{
   void(^block)() = objc_getAssociatedObject(self, &headerImageViewTapKey);
    if (block){
        block();
    }
}

-(void)headerImageViewTapWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &headerImageViewTapKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeight{
    return MJFloat(150);
}
@end
