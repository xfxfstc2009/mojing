//
//  MJDesignerMineOneCollectionViewCell.m
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/22.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerMineOneCollectionViewCell.h"
#import <objc/runtime.h>
#define kmargin_width_one MJFloat(10)


static char productEditButtonKey;
static char caseEditButtonKey;
static char materialClickKey;
static char realClickKey;

@interface MJDesignerMineOneCollectionViewCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)GWImageView *imageView;
@property (nonatomic,strong)GWImageView *alphaView;
@property (nonatomic,strong)UILabel *headerLabel;
@property (nonatomic,strong)UILabel *countLabel;
@property (nonatomic,strong)UIButton *editButton;

@end

@implementation MJDesignerMineOneCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.layer.cornerRadius = MJFloat(3);
    self.bgView.clipsToBounds = YES;
    self.bgView.frame = self.bounds;
    [self.contentView addSubview:self.bgView];
    
    // 1. 创建view
    self.imageView = [[GWImageView alloc]init];
    self.imageView.backgroundColor = [UIColor clearColor];
    self.imageView.frame = CGRectMake(MJFloat(6), MJFloat(6), self.bgView.size_width - 2 * MJFloat(6), MJFloat(240));
    [self.bgView addSubview:self.imageView];

    // 2. 创建label
    self.alphaView = [[GWImageView alloc]init];
    self.alphaView.frame = self.imageView.bounds;
    self.alphaView.image = [UIImage imageNamed:@"mine_top_blackBackground"];
    [self.imageView addSubview:self.alphaView];

    // 3. 创建label
    self.headerLabel = [[UILabel alloc]init];
    self.headerLabel.backgroundColor = [UIColor clearColor];
    self.headerLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.headerLabel.textColor = [UIColor whiteColor];
    self.headerLabel.textAlignment = NSTextAlignmentLeft;
    self.headerLabel.frame = CGRectMake(MJFloat(10), MJFloat(8), self.bounds.size.width - 2 * MJFloat(10), [NSString contentofHeight:self.headerLabel.font]);
    [self.bgView addSubview:self.headerLabel];

    // 4. 创建label
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor lightGrayColor];
    self.countLabel.alpha = .7f;
    self.countLabel.layer.cornerRadius = MJFloat(3);
    self.countLabel.textColor = [UIColor whiteColor];
    self.countLabel.font = [UIFont systemFontOfCustomeSize:13.];
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    [self.imageView addSubview:self.countLabel];

    // 5. 创建编辑按钮
    self.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.editButton.backgroundColor = [UIColor whiteColor];
    [self.editButton setTitle:@"编辑" forState:UIControlStateNormal];
    self.editButton.titleLabel.font = [UIFont systemFontOfCustomeSize:MJFloat(15)];
    [self.editButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.editButton.frame = CGRectMake(0, CGRectGetMaxY(self.imageView.frame), self.bounds.size.width, self.bounds.size.height - CGRectGetMaxY(self.imageView.frame));
    
    __weak typeof(self)weakSelf = self;
    [weakSelf.editButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.cellType == cellTypeProduct){
            void(^productEditClickBlock)(MJDesignerProductClassSingleModel *transferproductClassSingleModel) = objc_getAssociatedObject(strongSelf, &productEditButtonKey);
            if (productEditClickBlock){
                productEditClickBlock(strongSelf.transferproductClassSingleModel);
            }
        } else if (strongSelf.cellType == cellTypeCase){
            void(^caseEditClickBlock)(MJDesignSingleAblum *transferSingleAblum) = objc_getAssociatedObject(strongSelf, &caseEditButtonKey);
            if (caseEditClickBlock){
                caseEditClickBlock(strongSelf.transferSingleAblum);
            }
        } else if (strongSelf.cellType == cellTypeMaterial){        // 选材搭配
            void(^materialClickBlock)(MJDesignerMaterialSingleModel *transferMaterialSingleModel) = objc_getAssociatedObject(strongSelf, &materialClickKey);
            if (materialClickBlock){
                materialClickBlock(strongSelf.transferMaterialSingleModel);
            }
        } else if (strongSelf.cellType == cellTypeReal){
            void(^realEditClickBlock)(MJDesignerWorksSingleModel *transferWorksSingleModel) = objc_getAssociatedObject(strongSelf, &realClickKey);
            if (realEditClickBlock){
                realEditClickBlock(strongSelf.transferWorksSingleModel);
            }
        }
    }];
    
    [self addSubview:self.editButton];
}

-(void)setCellType:(cellType)cellType{
    _cellType = cellType;
}

// 传入商品类型
-(void)setTransferproductClassSingleModel:(MJDesignerProductClassSingleModel *)transferproductClassSingleModel{
    _transferproductClassSingleModel = transferproductClassSingleModel;
    
    // 1. 相册
    [self.imageView uploadImageWithURL300:transferproductClassSingleModel.firstProductImageName imageSuffix:transferproductClassSingleModel.firstProductImageSuffix placeholder:[UIImage imageNamed:@""] callback:NULL];
    
    // 2. header
    self.headerLabel.text = transferproductClassSingleModel.name;
    
    // 3.
    self.countLabel.text = [NSString stringWithFormat:@"%li",(long)transferproductClassSingleModel.productCount];
    CGSize productCountSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(MJFloat(8), CGRectGetMaxY(self.imageView.frame) - MJFloat(10) - [NSString contentofHeight:self.countLabel.font], productCountSize.width + 2 * MJFloat(5), [NSString contentofHeight:self.countLabel.font]);
}

// 传入相册
-(void)setTransferSingleAblum:(MJDesignSingleAblum *)transferSingleAblum{
    _transferSingleAblum = transferSingleAblum;
    // 1. 相册
    [self.imageView uploadImageWithURL300:transferSingleAblum.firstImageName imageSuffix:transferSingleAblum.firstImageSuffix placeholder:[UIImage imageNamed:@""] callback:NULL];
    
    // 2. 头部名称
    self.headerLabel.text = transferSingleAblum.name;
    
    // 3. 创建count
    self.countLabel.text = [NSString stringWithFormat:@"%li",(long)transferSingleAblum.imageCount];
    CGSize productCountSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(MJFloat(8), CGRectGetMaxY(self.imageView.frame) - MJFloat(10) - [NSString contentofHeight:self.countLabel.font], productCountSize.width + 2 * MJFloat(5), [NSString contentofHeight:self.countLabel.font]);
}

// 传入我的选材
-(void)setTransferMaterialSingleModel:(MJDesignerMaterialSingleModel *)transferMaterialSingleModel{
    _transferMaterialSingleModel = transferMaterialSingleModel;
    
    // 1. 相册
    [self.imageView uploadImageWithURL300:transferMaterialSingleModel.collocationImageName imageSuffix:transferMaterialSingleModel.collocationImageSuffix placeholder:[UIImage imageNamed:@""] callback:NULL];
    
    // 2. 头部名称
    self.headerLabel.text = transferMaterialSingleModel.name;
    
    // 3. 创建count
    self.countLabel.text = [NSString stringWithFormat:@"%li",(long)transferMaterialSingleModel.favoriteCount];
    CGSize productCountSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(MJFloat(8), CGRectGetMaxY(self.imageView.frame) - MJFloat(10) - [NSString contentofHeight:self.countLabel.font], productCountSize.width + 2 * MJFloat(5), [NSString contentofHeight:self.countLabel.font]);
    
}

-(void)setTransferWorksSingleModel:(MJDesignerWorksSingleModel *)transferWorksSingleModel{
    _transferWorksSingleModel = transferWorksSingleModel;
    
    // 1. 相册
    [self.imageView uploadImageWithURL300:transferWorksSingleModel.collocationImageName imageSuffix:transferWorksSingleModel.collocationImageSuffix placeholder:[UIImage imageNamed:@""] callback:NULL];
    
    // 2. 头部名称
    self.headerLabel.text = transferWorksSingleModel.name;
    
    // 3. 创建count
    self.countLabel.text = [NSString stringWithFormat:@"%li",(long)transferWorksSingleModel.favoriteCount];
    CGSize productCountSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeight:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(MJFloat(8), CGRectGetMaxY(self.imageView.frame) - MJFloat(10) - [NSString contentofHeight:self.countLabel.font], productCountSize.width + 2 * MJFloat(5), [NSString contentofHeight:self.countLabel.font]);
}



+(CGSize)calculationSizeWithType:(cellType)cellType{
    if (cellType == cellTypeCase){
       return CGSizeMake((kScreenBounds.size.width - 4 * kmargin_width_one) / 3., MJFloat(536 / 2.));
    } else {
       return CGSizeMake((kScreenBounds.size.width - 5 * kmargin_width_one) / 4., MJFloat(536 / 2.));
    }
}

-(void)productEditBlock:(productEditClickBlock)block{
    objc_setAssociatedObject(self, &productEditButtonKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)caseEditBlock:(caseEditClickBlock)block{
    objc_setAssociatedObject(self, &caseEditButtonKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)materialEditBlock:(materialClickBlock)block{
    objc_setAssociatedObject(self, &materialClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)realEditClickBlock:(realEditClickBlock)block{
    objc_setAssociatedObject(self, &realClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
