//
//  MJDesignerMineOneCollectionViewCell.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/22.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJDesignerProductClassSingleModel.h"
#import "MJDesignSingleAblum.h"
#import "MJDesignerMaterialSingleModel.h"           // 选材
#import "MJDesignerWorksSingleModel.h"              // 实景搭配

typedef void(^productEditClickBlock)(MJDesignerProductClassSingleModel *transferproductClassSingleModel);
typedef void(^caseEditClickBlock)(MJDesignSingleAblum *transferSingleAblum);
typedef void(^materialClickBlock)(MJDesignerMaterialSingleModel *transferMaterialSingleModel);
typedef void(^realEditClickBlock)(MJDesignerWorksSingleModel *transferWorksSingleModel);

typedef NS_ENUM(NSInteger,cellType) {
    cellTypeProduct,                    /**< 商品类型*/
    cellTypeCase,                        /**< 其他类型*/
    cellTypeReal,                       /**< 实景搭配*/
    cellTypeMaterial                    /**< 选材搭配*/
};

@interface MJDesignerMineOneCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)MJDesignerProductClassSingleModel *transferproductClassSingleModel;
@property (nonatomic,strong)MJDesignSingleAblum *transferSingleAblum;
@property (nonatomic,strong)MJDesignerMaterialSingleModel *transferMaterialSingleModel;         /**< 选材model */
@property (nonatomic,strong)MJDesignerWorksSingleModel *transferWorksSingleModel;               /**< 实景搭配*/


@property (nonatomic,assign)cellType cellType;

+(CGSize)calculationSizeWithType:(cellType)cellType;


-(void)productEditBlock:(productEditClickBlock)block;
-(void)caseEditBlock:(caseEditClickBlock)block;
-(void)materialEditBlock:(materialClickBlock)block;
-(void)realEditClickBlock:(realEditClickBlock)block;


@end
