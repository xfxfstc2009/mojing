//
//  MJBrandScreenView.h
//  魔境家居
//
//  Created by lumingliang on 16/2/16.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJSearchBar.h"
#import "MJUIClassTool.h"
#import "GMDCircleLoader.h"
#import "UIImageView+WebCache.h"
#import "MJNetworkTool.h"
#import "MJRefresh.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "MJExtension.h"
#import "MJBrandScreenCell.h"
#import "MJBrandListModel.h"

typedef void(^brandScreenClick)(NSInteger index);
@interface MJBrandScreenView : UIView<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout,UITextFieldDelegate>

@property (nonatomic, strong) MJSearchBar *searchBar;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton *confirmBtn;
@property (nonatomic, strong) UIButton *cancelBtn;
@property (nonatomic, copy) brandScreenClick cb;

@property (nonatomic, strong) NSMutableArray *productListArr;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
@property (nonatomic, strong) UIImageView *smallBrandView;

- (void)addSmallBrandViewWithUrl:(NSURL *)url;

@end
