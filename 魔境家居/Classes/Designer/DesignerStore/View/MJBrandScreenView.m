//
//  MJBrandScreenView.m
//  魔境家居
//
//  Created by lumingliang on 16/2/16.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJBrandScreenView.h"
#define x 30
@implementation MJBrandScreenView
static NSString * const reuseIdentifier = @"collectionCell";

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        MJSearchBar *searchBar = [[MJSearchBar alloc] initWithFrame:CGRectMake(x, 15, 258, 30)];
        searchBar.placeholder = @"搜品牌名称";
        searchBar.delegate = self;
        searchBar.returnKeyType = UIReturnKeySearch;
        [self addSubview:searchBar];
        [self addSubview:self.collectionView];
        [self addSubview:self.confirmBtn];
        [self addSubview:self.cancelBtn];
        self.productListArr = [NSMutableArray array];
        
      
        NSDictionary *footerDict = @{@"searchString" :@"",@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"pageSize" : @27};
        
        [self addRefreshWithDict:footerDict];
    }
    return self;
}

- (void)addSmallBrandViewWithUrl:(NSURL *)url
{
     self.smallBrandView  = [[UIImageView alloc] initWithFrame:CGRectMake(298, 10, 79, 40)];
    [_smallBrandView sd_setImageWithURL:url];
    [self addSubview:_smallBrandView];
}

- (UIButton *)confirmBtn
{
    if (_confirmBtn == nil) {
        _confirmBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(Screen_Width/2-82, 232, 77, 32) title:nil image:[UIImage imageNamed:@"product_select_corfirm"] target:self action:@selector(buttonClick:)];
        _confirmBtn.tag = 1;
    }
    return _confirmBtn;
}

- (UIButton *)cancelBtn
{
    if (_cancelBtn == nil) {
        _cancelBtn = [MJUIClassTool createBtnWithFrame:CGRectMake(Screen_Width/2+5, 232, 77, 32) title:nil image:[UIImage imageNamed:@"product_select_cancel"] target:self action:@selector(buttonClick:)];
        _cancelBtn.tag = 2;
    }
    return _cancelBtn;
}

#pragma mark - 确定 取消
- (void)buttonClick:(UIButton *)button
{
    if (_cb) {
        _cb(button.tag);
    }
}

#pragma mark - collectionView
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        CGFloat inset = 2;
        layout.sectionInset = UIEdgeInsetsMake(inset, inset, inset, inset);
        layout.minimumColumnSpacing = inset;
        layout.minimumInteritemSpacing = inset;
        layout.columnCount = 9;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(x, 60, Screen_Width-2*x-3, 162+5) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = RGB(225, 225, 225);
        [_collectionView registerClass:[MJBrandScreenCell class] forCellWithReuseIdentifier:reuseIdentifier];
       // [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_circulateBackgroungView];
    
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    //self.dropdownView.userInteractionEnabled = YES;
    [self.circulateBackgroungView removeFromSuperview];
    //[GMDCircleLoader hideFromView:self.view animated:YES];
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self];
}

- (void)addRefreshWithDict:(NSDictionary *)dict
{
    //self.setHotProductView.hidden = YES;
    __weak MJBrandScreenView *ctl = self;
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [ctl loadNewDataWithPara:dict];
    }];
    [self loadingData];
    [self.collectionView.mj_header beginRefreshing];
    //[self.dropdownView removeFromSuperview];
    [self.collectionView reloadData];
}

#pragma mark - 下拉加载新数据
- (void)loadNewDataWithPara:(NSDictionary *)dict
{
    self.pageNumber = 1;
   
    NSString *type = [dict objectForKey:@"type"];
    //获取商品列表数据
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    __weak MJBrandScreenView *ctl = self;
    [self.noDataView removeFromSuperview];
    [self.productListArr removeAllObjects];
    if ([self isConnectToNet]) {
        
        [net getMoJingURL:MJBrandList parameters:dict success:^(id obj) {
        //    NSLog(@"%@",obj);
            NSDictionary *dataDict = [obj objectForKey:@"data"];
            MJBrandListModel *model = [MJBrandListModel mj_objectWithKeyValues:dataDict];
         //   NSLog(@"%@",model.mj_keyValues);
            self.pageCount = model.pageCount;
            // 把字典数组转换成模型数组
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJBrandDataModel mj_objectArrayWithKeyValuesArray:model.brandList];
            
            [ctl.productListArr addObjectsFromArray:arr];
            
            if (ctl.productListArr.count == 0) {
                if ([type isEqualToString:@"3"]) {
                    UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-40, 147, 260, 30) title:@"对应品牌没有设置新品上市" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                    [ctl addNoDataViewWith:nil];
                    [_noDataView addSubview:lb];
                }else{
                    UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-40, 74, 180, 30) title:@"没有搜到相关的品牌" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                    [ctl addNoDataViewWith:nil];
                    [_noDataView addSubview:lb];
                }
            }
            
            //   NSLog(@"%ld",ctl.productListArr.count);
            [ctl stopCircleLoader];
            
            if (ctl.productListArr.count >= 27) {
                ctl.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                    // 进入刷新状态后会自动调用这个block
                    [ctl loadMoreData:dict];
                }];
                // 默认先隐藏footer
                ctl.collectionView.mj_footer.hidden = YES;
            }else{
                ctl.collectionView.mj_footer = nil;
            }
            
            [ctl.collectionView reloadData];
            [ctl.collectionView.mj_header endRefreshing];
            [ctl endEditing:YES];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [ctl stopCircleLoader];
            [ctl.collectionView.mj_header endRefreshing];
        }];
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_header endRefreshing];
    }
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    // [self.productListArr removeAllObjects];
    //获取商品列表数据
    _pageNumber++;
    //  NSLog(@"%ld",_pageNumber);
    //    NSString *str = [dict objectForKey:@"productTypeID"];
    //   // NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
    //    NSString *type = [dict objectForKey:@"type"];
    
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSDictionary *moreDict = @{@"pageNumber" : [NSString stringWithFormat:@"%ld",_pageNumber],@"pageSize" : @27};
    __weak MJBrandScreenView *ctl = self;
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            [net getMoJingURL:MJBrandList parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                MJBrandListModel *model = [MJBrandListModel mj_objectWithKeyValues:obj];
               
                // 把字典数组转换成模型数组
                NSMutableArray *arr = [NSMutableArray array];
                arr = [MJBrandDataModel mj_objectArrayWithKeyValuesArray:model.brandList];
                
                [ctl.productListArr addObjectsFromArray:arr];
                //   NSLog(@"%ld",ctl.productListArr.count);
                [UIView animateWithDuration:0.1 animations:^{
                    [ctl.collectionView reloadData];
                }];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            }];
            
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
    }
}



#pragma mark  <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.productListArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MJBrandScreenCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (self.productListArr.count > 0) {
        MJBrandDataModel *model = self.productListArr[indexPath.row];
//        if (indexPath.row == 0) {
//            model.selected = YES;
//        }
        NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_100_100.%@",mainURL,model.logoSuffix,model.logoName,model.logoName,model.logoSuffix]];
        [cell.logoImageView  sd_setImageWithURL:bgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //            if (image) {
            //                if (!CGSizeEqualToSize(model.imageSize, image.size)) {
            //                    model.imageSize = image.size;
            //                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            //                }
            //            }
        }];
        cell.model = model;
        // 根据模型属性来控制打钩的显示和隐藏
        cell.selectImgView.hidden = !model.isSelected;
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

#pragma mark  <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    //NSLog(@"详情点击%ld",indexPath.row);
}


#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
    //    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
    //        return model.imageSize;
    //    }
    
    return CGSizeMake(106, 54);
}

#pragma mark - noDataView
- (void)addNoDataViewWith:(NSString *)string
{
    _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 74, 74)];
    _noDataView.image = [UIImage imageNamed:@"no_data"];
    _noDataView.center = self.collectionView.center ;
    
    [self addSubview:_noDataView];
}


#pragma mark - <UITextFieldDelegate>
//文本框开始输入的时候
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
    
}// became first responder

//结束输入的时候
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    NSDictionary *footerDict = @{@"searchString" : textField.text,@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"pageSize" : @12};
    if (textField.text.length == 0) {
        [self endEditing:YES];
    }else{
        [self addRefreshWithDict:footerDict];
    }
    return YES;
}

@end
