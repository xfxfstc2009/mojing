//
//  MJBrandScreenCell.m
//  魔境家居
//
//  Created by lumingliang on 16/2/16.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJBrandScreenCell.h"

@implementation MJBrandScreenCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self uiConfig];
    }
    return self;
}

-(void)uiConfig
{
    CGFloat width = 106;
    CGFloat height = 54;
    _logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
  
    [self.contentView addSubview:_logoImageView];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.backgroundColor = [UIColor whiteColor];
    btn.frame = CGRectMake(0, 0, width, height);
    btn.alpha = 0.1;
    [btn addTarget:self action:@selector(coverClick) forControlEvents:UIControlEventTouchUpInside];
    
    _selectImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    _selectImgView.image = [UIImage imageNamed:@"product_brand_selected"];
   
    //  [self.contentView addSubview:_singleProductPrice];
    [self.contentView addSubview:btn];
    [self.contentView addSubview:_selectImgView];
}
- (void)coverClick
{
    // 设置模型选中状态
    self.model.selected = !self.model.isSelected;
    // 直接修改状态
    _selectImgView.hidden = !_selectImgView.isHidden;
    //发通知
    [MJNotificationCenter postNotificationName:@"changeBrandSelected" object:nil userInfo:@{@"brandDataModel" :self.model}];
    
}
@end
