//
//  MJBrandScreenCell.h
//  魔境家居
//
//  Created by lumingliang on 16/2/16.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJBrandDataModel.h"
@interface MJBrandScreenCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic, strong) UIImageView *selectImgView;
@property (nonatomic, strong) MJBrandDataModel *model;
@end
