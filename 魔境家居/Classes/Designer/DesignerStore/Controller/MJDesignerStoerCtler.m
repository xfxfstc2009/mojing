//
//  MJDesignerStoerCtler.m
//  魔境家居
//
//  Created by lumingliang on 16/2/16.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerStoerCtler.h"
#import "MJUIClassTool.h"
#import "GMDCircleLoader.h"
#import "UIImageView+WebCache.h"
#import "MJNetworkTool.h"
#import "MJRefresh.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "MJExtension.h"
#import "MJGoodsDropdownView.h"
#import "MJNewProductsCell.h"
#import "MJProductListModel.h"
#import "MJSecondProductDetailsCtler.h"
#import "MJSearchBar.h"
#import "MJBrandScreenView.h"
#import "MJSecondProductDetailsCtler.h"

@interface MJDesignerStoerCtler ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *brandScreenBtn;
@property (weak, nonatomic) IBOutlet UIButton *categoryScreenBtn;

/* collectionView*/
@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *productListArr;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
/* 分类筛选*/
@property (nonatomic,strong) MJGoodsDropdownView *dropdownView;
/* 品牌筛选*/
@property (nonatomic,strong) MJBrandScreenView *screenView;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
/* 遮盖view*/
@property (nonatomic, strong) UIView *storeCoverView;
@property (nonatomic ,strong) NSMutableDictionary *para;
@end

@implementation MJDesignerStoerCtler

static NSString * const reuseIdentifier = @"collectionCell";
- (UIView *)storeCoverView
{
    if (_storeCoverView == nil) {
        _storeCoverView = [[UIView alloc] initWithFrame:CGRectMake(0, 118, Screen_Width, Screen_Height-118)];
        _storeCoverView.backgroundColor = [UIColor blackColor];
        _storeCoverView.alpha = 0.3;
    }
    return _storeCoverView;
}

- (NSMutableArray *)productListArr
{
    if (_productListArr == nil) {
        _productListArr = [NSMutableArray array];
    }
    return _productListArr;
}

#pragma mark - collectionView
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        CGFloat inset = 2;
        layout.sectionInset = UIEdgeInsetsMake(inset, inset, inset, inset);
        layout.minimumColumnSpacing = inset;
        layout.minimumInteritemSpacing = inset;
        layout.columnCount = 4;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 115, Screen_Width, Screen_Height-115) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.backgroundColor = RGB(225, 225, 225);
        [_collectionView registerClass:[MJNewProductsCell class] forCellWithReuseIdentifier:reuseIdentifier];
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

#pragma mark - 分类筛选
- (MJGoodsDropdownView *)dropdownView
{
    if (_dropdownView == nil) {
        _dropdownView = [MJGoodsDropdownView dropdown];
        _dropdownView.frame = CGRectMake(120, 116, Screen_Width/2+200, 50);
        //_dropdownView.allGoodsBtn.selected = YES;
        _dropdownView.alpha = 0.8;
        __weak MJDesignerStoerCtler *ctl = self;
        
        //商品类型列表点击
        _dropdownView.click = ^(NSInteger index){
            ctl.dropdownView.hidden = YES;
            ctl.categoryScreenBtn.selected = NO;
            CGAffineTransform transform= CGAffineTransformMakeRotation(M_PI);
            ctl.categoryScreenBtn.imageView.transform = transform;
            NSString *brandID = ctl.para[@"brandID"];
            NSDictionary *dict = @{@"productTypeID" : [NSString stringWithFormat:@"%ld",index+1-30],@"pageNumber" : [NSString stringWithFormat:@"%ld",ctl.pageNumber],@"pageSize" :@12,@"brandID":brandID};
            //   ctl.dropdownIndex = index+1-30;
            //NSLog(@"%@",dict);
            switch (index-30) {
                case 0:
                {
                    
                    [ctl dropdownClickIndex:0 dictionary:dict];
                    // NSLog(@"%@",dict);
                    break;
                }
                case 1:
                {
                    
                    [ctl dropdownClickIndex:1 dictionary:dict];
                    break;
                }
                case 2:
                {
                    [ctl dropdownClickIndex:2 dictionary:dict];
                    break;
                }
                case 3:
                {
                    [ctl dropdownClickIndex:3 dictionary:dict];
                    break;
                }
                case 4:
                {
                    [ctl dropdownClickIndex:4 dictionary:dict];
                    break;
                }
                case 5:
                {
                    [ctl dropdownClickIndex:5 dictionary:dict];
                    break;
                }
                case 6:
                {
                    [ctl dropdownClickIndex:6 dictionary:dict];
                    break;
                }
                case 7:
                {
                    [ctl dropdownClickIndex:7 dictionary:dict];
                    break;
                }
                case 8:
                {
                    NSDictionary *dict1 = @{@"productTypeID" : [NSString stringWithFormat:@"%d",0],@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)ctl.pageNumber],@"pageSize" :@12,@"brandID":brandID};
                    [ctl dropdownClickIndex:8 dictionary:dict1];
                    break;
                }
                default:
                    break;
            }
        };
    }
    return _dropdownView;
}

#pragma mark - 品牌筛选
- (MJBrandScreenView *)screenView
{
    if (_screenView == nil) {
        _screenView = [[MJBrandScreenView alloc] initWithFrame:CGRectMake(0, 116, Screen_Width, 275)];
        _screenView.backgroundColor = [UIColor whiteColor];
        __weak MJDesignerStoerCtler *ctl = self;
        _screenView.cb = ^(NSInteger index){
            ctl.brandScreenBtn.selected = NO;
            switch (index) {
                case 1:
                {//确定
                    NSLog(@"确定");
                    CGAffineTransform transform= CGAffineTransformMakeRotation(M_PI);
                    ctl.brandScreenBtn.imageView.transform = transform;
                    NSMutableArray *tempArr = [NSMutableArray array];
                    int i = 0;
                    for (MJBrandDataModel *model in ctl.screenView.productListArr) {
                        if (model.isSelected) {
                           [tempArr addObject:model];
                        }
                        i++;
                    }
                    
                    
                    ctl.para[@"pageNumber"] = [NSString stringWithFormat:@"%ld",(long)ctl.pageNumber];
                    ctl.para[@"pageSize"] = @12;
                    ctl.para[@"productTypeID"] = [NSString stringWithFormat:@"%d",0];
                    if (tempArr.count>0) {
                        for (MJBrandDataModel *model in tempArr) {
                            ctl.para[@"brandID"] = [NSString stringWithFormat:@"%ld",(long)model.idNum];
                        }
                    }else
                    {
                        ctl.para[@"brandID"] = [NSString stringWithFormat:@"%d",0];
                    }
                    
                    NSLog(@"%@",ctl.para);
                    [ctl.productListArr removeAllObjects];
                    [ctl addRefreshWithDict:ctl.para];
                    [ctl.collectionView reloadData];
                    [ctl.storeCoverView removeFromSuperview];
                    [ctl.screenView removeFromSuperview];
                }
                    break;
                case 2:
                {//取消
                    NSLog(@"取消");
                    CGAffineTransform transform= CGAffineTransformMakeRotation(M_PI);
                    ctl.brandScreenBtn.imageView.transform = transform;
                    [ctl.storeCoverView removeFromSuperview];
                    [ctl.screenView removeFromSuperview];
                }
                    break;
                    
                default:
                    break;
            }
        };
    }
    return _screenView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //tabBar显示通知
    self.tabBarController.tabBar.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
}

- (void)initUI
{
    self.view.backgroundColor = RGB(225, 225, 225);
    
    //添加搜索框
    MJSearchBar *searchBar = [[MJSearchBar alloc] initWithFrame:CGRectMake(Screen_Width - 268, 10, 258, 30)];
    searchBar.placeholder = @"搜商品";
    searchBar.delegate = self;
    searchBar.returnKeyType = UIReturnKeySearch;
    [self.topView addSubview:searchBar];
    
    NSString *brandName = @"商城主页";
    UILabel *titleLb = [MJUIClassTool createLbWithFrame:CGRectMake(0, 7, 1000, 44) title:[NSString stringWithFormat:@"%@",brandName] aliment:NSTextAlignmentCenter color:[UIColor blackColor] size:25];
    CGSize size = [MJUIClassTool caculateText:brandName fontSize:25 maxSize:CGSizeMake(1000, 44)];
    titleLb.size = size;
    self.navigationItem.titleView = titleLb;
    
    self.para = [NSMutableDictionary dictionary];
    _para[@"brandID"] = @"0";
    NSString *brandID = _para[@"brandID"];
    // self.dropdownView.allGoodsBtn.selected = YES;
    NSDictionary *dict = @{@"productTypeID":[NSString stringWithFormat:@"%d",0],@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"pageSize" :@12,@"brandID":brandID};
    [self addRefreshWithDict:dict];
    //改变品牌选中个数
    [MJNotificationCenter addObserver:self selector:@selector(changeBrandSelected:) name:@"changeBrandSelected" object:nil];
    [self.brandScreenBtn setImage:[UIImage imageNamed:@"product_select_down"] forState:UIControlStateNormal];
    CGAffineTransform transform= CGAffineTransformMakeRotation(M_PI);
    self.brandScreenBtn.imageView.transform = transform;
    [self.categoryScreenBtn setImage:[UIImage imageNamed:@"product_select_down"] forState:UIControlStateNormal];
    self.categoryScreenBtn.imageView.transform = transform;
    
    
}

- (void)changeBrandSelected:(NSNotification *)notification
{
    [self.screenView.smallBrandView removeFromSuperview];
    MJBrandDataModel *model = notification.userInfo[@"brandDataModel"];
    for (MJBrandDataModel *dataModel in self.screenView.productListArr) {
        if (![model isEqual:dataModel]) {
            if (dataModel.isSelected) {
                dataModel.selected = NO;
                [self.screenView.collectionView reloadData];
            }
        }
    }
    NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_100_100.%@",mainURL,model.logoSuffix,model.logoName,model.logoName,model.logoSuffix]];
    
    [self.screenView addSmallBrandViewWithUrl:bgUrl];
}



#pragma mark - 筛选点击
- (IBAction)selectClick:(UIButton *)button {
    self.pageNumber = 1;
    button.selected = !button.isSelected;
    [_noDataView removeFromSuperview];
    [self.productListArr removeAllObjects];

    switch (button.tag) {
        case 10:
        {
            NSLog(@"品牌筛选");
            self.dropdownView.hidden = YES;
            self.screenView.hidden = NO;
            self.categoryScreenBtn.selected = NO;
            self.storeCoverView.hidden = NO;
            if (button.isSelected == YES) {
                [self.view addSubview:self.screenView];
              
                CGAffineTransform transform= CGAffineTransformMakeRotation(0);
                button.imageView.transform = transform;
                [self.screenView.collectionView reloadData];
                [self.view addSubview:self.storeCoverView];
                [self.view addSubview:self.screenView];
                
            }else
            {
                CGAffineTransform transform= CGAffineTransformMakeRotation(M_PI);
                button.imageView.transform = transform;
                [self.screenView removeFromSuperview];
                
                [self.storeCoverView removeFromSuperview];
                self.storeCoverView = nil;
               // [self.view bringSubviewToFront:self.collectionView];
            }

        }
            break;
        case 11:
        {
            NSLog(@"分类筛选");
           // self.collectionView.hidden = NO;
           // self.dealerCollectionView.hidden = YES;
            self.brandScreenBtn.selected = NO;
            self.dropdownView.hidden = NO;
            self.screenView.hidden = YES;
            //self.screenView = nil;
            self.storeCoverView.hidden = YES;
            if (button.isSelected == YES) {
                [self.view addSubview:self.dropdownView];
                CGAffineTransform transform= CGAffineTransformMakeRotation(0);
                button.imageView.transform = transform;
                [self.view bringSubviewToFront:self.dropdownView];
                
//                NSString *brandID = self.para[@"brandID"];
//               
//                // self.dropdownView.allGoodsBtn.selected = YES;
//                NSDictionary *dict = @{@"productTypeID":[NSString stringWithFormat:@"%d",0],@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"pageSize" :@12,@"brandID":brandID};
//                [self dropdownClickIndex:0 dictionary:dict];
                // [self.collectionView reloadData];
            }else
            {
                CGAffineTransform transform= CGAffineTransformMakeRotation(M_PI);
                button.imageView.transform = transform;
                [self.dropdownView removeFromSuperview];
                [self.storeCoverView removeFromSuperview];
            }

        }
            break;
            
        default:
            break;
    }
}


#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_circulateBackgroungView];
    //self.categoryView.userInteractionEnabled = NO;
    self.dropdownView.userInteractionEnabled = NO;
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    self.dropdownView.userInteractionEnabled = YES;
    [self.circulateBackgroungView removeFromSuperview];
    //[GMDCircleLoader hideFromView:self.view animated:YES];
    
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 下拉弹窗点击封装
- (void)dropdownClickIndex:(NSInteger)index dictionary:(NSDictionary *)dict
{
    //self.setHotProductView.hidden = YES;
    __weak MJDesignerStoerCtler *ctl = self;
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [ctl loadNewDataWithPara:dict];
    }];
    [self loadingData];
    [self.collectionView.mj_header beginRefreshing];
    //[self.dropdownView removeFromSuperview];
    [self.collectionView reloadData];
    //    self.allGoodsButton.selected = YES;
    //    self.collectionView.hidden = NO;
    //    self.bottomBigView.hidden = YES;
    [self.view addSubview:self.dropdownView];
}

- (void)addRefreshWithDict:(NSDictionary *)dict
{
    //self.setHotProductView.hidden = YES;
    __weak MJDesignerStoerCtler *ctl = self;
    
    self.collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [ctl loadNewDataWithPara:dict];
    }];
    [self loadingData];
    [self.collectionView.mj_header beginRefreshing];
    //[self.dropdownView removeFromSuperview];
    [self.collectionView reloadData];
    //    self.allGoodsButton.selected = NO;
    //    self.collectionView.hidden = NO;
    //    self.bottomBigView.hidden = YES;
    [self.dropdownView removeFromSuperview];
}

#pragma mark - 下拉加载新数据
- (void)loadNewDataWithPara:(NSDictionary *)dict
{
    self.pageNumber = 1;
    
    //获取商品列表数据
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    __weak MJDesignerStoerCtler *ctl = self;
    [self.noDataView removeFromSuperview];
    [self.productListArr removeAllObjects];
    if ([self isConnectToNet]) {
        
        [net getMoJingURL:MJProductList parameters:dict success:^(id obj) {
            //     NSLog(@"%@",dict);
            MJProductListModel *listModel = obj;
          //  NSLog(@"%@",listModel.mj_keyValues);
            MJProductListDataModel *model = listModel.data;
            self.pageCount = model.pageCount;
            // 把字典数组转换成模型数组
            NSMutableArray *arr = [NSMutableArray array];
            arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
            
            // for (MJProductModel *m in ctl.productListArr) {
            //                MJBrandDataModel *brand = m.brandData;
            //                MJCreatedDateModel *create = m.createdDate;
            //                MJModifiedDateModel *modify = m.modifiedDate;
            //   NSLog(@"%ld %@ %@ %@",brand.idNum,m.userID,create.time,modify.day);
            //  }
            
            [ctl.productListArr addObjectsFromArray:arr];
            
            if (ctl.productListArr.count == 0) {
              
                    UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-5, 147, 180, 30) title:@"没有搜到相关的商品" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
                    [ctl addNoDataViewWith:nil];
                    [_noDataView addSubview:lb];
            }
            
            //   NSLog(@"%ld",ctl.productListArr.count);
            [ctl stopCircleLoader];
            
            if (ctl.productListArr.count >= 12) {
                ctl.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                    // 进入刷新状态后会自动调用这个block
                    [ctl loadMoreData:dict];
                }];
                // 默认先隐藏footer
                ctl.collectionView.mj_footer.hidden = YES;
            }else{
                ctl.collectionView.mj_footer = nil;
            }
            
            [ctl.collectionView reloadData];
            [ctl.collectionView.mj_header endRefreshing];
            [ctl.view endEditing:YES];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [ctl stopCircleLoader];
            [ctl.collectionView.mj_header endRefreshing];
        }];
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_header endRefreshing];
    }
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    _pageNumber++;
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    NSString *productTypeID = [dict objectForKey:@"productTypeID"];
    NSString *brandID = [dict objectForKey:@"brandID"];
    NSDictionary *moreDict = @{@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)_pageNumber],@"pageSize" : @12,@"brandID":brandID,@"productTypeID":productTypeID};
    
    __weak MJDesignerStoerCtler *ctl = self;
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            [net getMoJingURL:MJProductList parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                MJProductListModel *listModel = obj;
                NSLog(@"%@",listModel.mj_keyValues);
                MJProductListDataModel *model = listModel.data;
                // 把字典数组转换成模型数组
                NSMutableArray *arr = [NSMutableArray array];
                arr = [MJProductModel mj_objectArrayWithKeyValuesArray:model.productList];
                
                [ctl.productListArr addObjectsFromArray:arr];
                //   NSLog(@"%ld",ctl.productListArr.count);
                [UIView animateWithDuration:0.1 animations:^{
                    [ctl.collectionView reloadData];
                }];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
            }];
            
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        //没有网络
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
    }
}



#pragma mark  <UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.productListArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MJNewProductsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if (self.productListArr.count > 0) {
        MJProductModel *model = self.productListArr[indexPath.row];
        
        NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.imageSuffix,model.imageName,model.imageName,model.imageSuffix]];
        [cell.singleProductImgView  sd_setImageWithURL:bgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //            if (image) {
            //                if (!CGSizeEqualToSize(model.imageSize, image.size)) {
            //                    model.imageSize = image.size;
            //                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
            //                }
            //            }
        }];
        cell.singleProductName.text = model.name;
        cell.singleProductPrice.text = [NSString stringWithFormat:@"￥%ld",model.price];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        
    }
    
    return cell;
}

#pragma mark  <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    MJProductDetailsCtler *ctl = [[MJSecondProductDetailsCtler alloc] init];
    if (self.productListArr.count>0) {
        MJProductModel *model = self.productListArr[indexPath.row];
        ctl.idNum = model.idNum;
        [self.navigationController pushViewController:ctl animated:YES];
    }
    
    //NSLog(@"详情点击%ld",indexPath.row);
}


#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
    //    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
    //        return model.imageSize;
    //    }
    if (collectionView == self.collectionView) {
        return CGSizeMake(256, 275);
    }
    return CGSizeMake(315, 188);
}

#pragma mark - noDataView
- (void)addNoDataViewWith:(NSString *)string
{
    _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
    _noDataView.image = [UIImage imageNamed:@"no_data"];
    _noDataView.center = CGPointMake(self.view.center.x, self.view.center.y+50) ;
    
    
    [self.view addSubview:_noDataView];
}


#pragma mark - <UITextFieldDelegate>
//文本框开始输入的时候
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
    
}// became first responder

//结束输入的时候
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [self.dropdownView removeFromSuperview];
    NSString *productTypeID = [NSString stringWithFormat:@"%d",0];
     NSString *brandID = [NSString stringWithFormat:@"%d",0];
    NSDictionary *footerDict = @{@"productTypeID" : productTypeID,@"searchString" : textField.text,@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber],@"pageSize" : @12,@"brandID":brandID};
    if (textField.text.length == 0) {
        [self.view endEditing:YES];
    }else{
        [self addRefreshWithDict:footerDict];
    }
    return YES;
}
- (void)dealloc
{
    [MJNotificationCenter removeObserver:self];
}

@end
