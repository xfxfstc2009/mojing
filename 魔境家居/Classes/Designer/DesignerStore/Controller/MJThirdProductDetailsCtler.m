//
//  MJThirdProductDetailsCtler.m
//  魔境家居
//
//  Created by lumingliang on 16/2/17.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJThirdProductDetailsCtler.h"

@interface MJThirdProductDetailsCtler ()

@end

@implementation MJThirdProductDetailsCtler

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self hidesTabBar:YES animated:NO];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - 返回
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
    [self.scrollView removeFromSuperview];
    self.scrollView = nil;
//    [_view3d.view removeFromSuperview];
//    [_view3d removeFromParentViewController];
//    _view3d.view = nil;
//    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
