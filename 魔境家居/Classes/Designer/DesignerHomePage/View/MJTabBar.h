//  MJTabBar.h
//  魔境家居
//
//  Created by lumingliang on 16/2/21.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MJTabBar;

@protocol MJTabBarDelegate <NSObject>

@optional
- (void)tabBar:(MJTabBar *)tabBar didSelectedButtonFrom:(NSInteger)from to:(NSInteger)to;

@end

@interface MJTabBar : UIView
- (void)addTabBarButtonWithItem:(UITabBarItem *)item;

@property (nonatomic, weak) id<MJTabBarDelegate> delegate;

@end
