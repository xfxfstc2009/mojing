//
//  MJDesignerHomePageTabBarCtler.m
//  魔境家居
//
//  Created by lumingliang on 16/2/21.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJDesignerHomePageTabBarCtler.h"
#import "MJDesignerDiscoverCtler.h"
#import "MJDesignerStoerCtler.h"
#import "MJTabBar.h"
#import "MJDesignMineCollectionView.h"

@interface MJDesignerHomePageTabBarCtler () <MJTabBarDelegate>
/**
 *  自定义的tabbar
 */
@property (nonatomic, strong) MJTabBar *customTabBar;
@end

@implementation MJDesignerHomePageTabBarCtler

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   // self.tabBar.hidden = NO;
    
    // 删除系统自动生成的UITabBarButton
    for (UIView *child in self.tabBar.subviews) {
        if ([child isKindOfClass:[UIControl class]]) {
            [child removeFromSuperview];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // 初始化tabbar
    [self setupTabbar];
    // 初始化所有的子控制器
    [self setupAllChildViewControllers];
}


/**
 *  初始化tabbar
 */
- (void)setupTabbar
{
    self.customTabBar = [[MJTabBar alloc] init];
    _customTabBar.frame = self.tabBar.bounds;
    _customTabBar.delegate = self;
    [self.tabBar addSubview:self.customTabBar];
}

/**
 *  监听tabbar按钮的改变
 *  @param from   原来选中的位置
 *  @param to     最新选中的位置
 */
- (void)tabBar:(MJTabBar *)tabBar didSelectedButtonFrom:(NSInteger)from to:(NSInteger)to
{
    if (self.selectedIndex == to && to == 0 ) {//双击刷新制定页面的列表
//        UINavigationController *nav = self.viewControllers[0];
//        MJDesignerMineCtler *firstVC = nav.viewControllers[0];
//        [firstVC initUI];
    }
    self.tabBar.hidden = NO;
    self.selectedIndex = to;
}

/**
 *  初始化所有的子控制器
 */
- (void)setupAllChildViewControllers
{
    NSArray *normalImgNameArr = @[@"designer_mine_normal",@"designer_discover_normal",@"designer_store_normal"];
    NSArray *selectImgNameArr = @[@"designer_mine_select",@"designer_discover_select",@"designer_store_select"];
    NSArray *titleArr = @[@"我的",@"发现",@"商城"];
  
    MJDesignMineCollectionView *mine = [[MJDesignMineCollectionView alloc] init];
    MJDesignerDiscoverCtler *discover = [[MJDesignerDiscoverCtler alloc] init];
    MJDesignerStoerCtler *store = [[MJDesignerStoerCtler alloc] init];

    [self setupChildViewController:mine title:titleArr[0] imageName:normalImgNameArr[0] selectedImageName:selectImgNameArr[0]];
    [self setupChildViewController:discover title:titleArr[1] imageName:normalImgNameArr[1] selectedImageName:selectImgNameArr[1]];
    [self setupChildViewController:store title:titleArr[2] imageName:normalImgNameArr[2] selectedImageName:selectImgNameArr[2]];
    
}

/**
 *  初始化一个子控制器
 *
 *  @param childVc           需要初始化的子控制器
 *  @param title             标题
 *  @param imageName         图标
 *  @param selectedImageName 选中的图标
 */
- (void)setupChildViewController:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{
    // 1.设置控制器的属性
    childVc.title = title;
    // 设置图标
    childVc.tabBarItem.image = [UIImage imageNamed:imageName];
    // 设置选中的图标
    UIImage *selectedImage = [UIImage imageNamed:selectedImageName];
//    if (iOS7) {
//        childVc.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    } else {
        childVc.tabBarItem.selectedImage = selectedImage;
   // }
    
    // 2.包装一个导航控制器
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:childVc];
    [self addChildViewController:nav];
    
    // 3.添加tabbar内部的按钮
    [self.customTabBar addTabBarButtonWithItem:childVc.tabBarItem];
}


@end
