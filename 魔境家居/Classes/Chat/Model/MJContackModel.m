//
//  MJContackModel.m
//  魔境家居
//
//  Created by Mac on 15/12/16.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJContackModel.h"

@implementation MJContackModel
 static MJContackModel *instance;
+ (MJContackModel *)getInstance
{
   
    @synchronized(self)
    {
        if (!instance)
            instance = [[MJContackModel alloc] init];
        
        return instance;
    }
}

+ (void)deleteInstance
{
    instance =nil;
}
- (id)init
{
    self = [super init];
    
    if (self) {
        _currentContackId =0;
        _currentSelectIndex =0;
        _unreadMsgCount=0;
    }
    
    return self;
}


@end
