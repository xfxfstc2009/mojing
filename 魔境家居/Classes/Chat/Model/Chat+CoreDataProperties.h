//
//  Chat+CoreDataProperties.h
//  魔境家居
//
//  Created by Mac on 16/1/18.
//  Copyright © 2016年 mojing. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Chat.h"

NS_ASSUME_NONNULL_BEGIN

@interface Chat (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) NSString *image;
@property (nullable, nonatomic, retain) NSNumber *isSelf;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *nickname;
@property (nullable, nonatomic, retain) NSString *time;
@property (nullable, nonatomic, retain) Contack *chat_contack;

@end

NS_ASSUME_NONNULL_END
