//
//  Chat+CoreDataProperties.m
//  魔境家居
//
//  Created by Mac on 16/1/18.
//  Copyright © 2016年 mojing. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Chat+CoreDataProperties.h"

@implementation Chat (CoreDataProperties)

@dynamic content;
@dynamic image;
@dynamic isSelf;
@dynamic name;
@dynamic nickname;
@dynamic time;
@dynamic chat_contack;

@end
