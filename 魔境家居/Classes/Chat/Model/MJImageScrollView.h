//
//  MJImageScrollView.h
//  魔境家居
//
//  Created by Mac on 15/12/15.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJImageScrollView : UIScrollView<UIScrollViewDelegate>
//{
//    UIImageView *_imageView;
//    
//}

//@property (nonatomic, retain)  NSString *imageUrlStr;
@property (nonatomic, copy)UIImageView *imageView;
@end
