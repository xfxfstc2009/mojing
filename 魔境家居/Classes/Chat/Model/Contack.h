//
//  Contack.h
//  魔境家居
//
//  Created by Mac on 15/11/20.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Chat;

NS_ASSUME_NONNULL_BEGIN

@interface Contack : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Contack+CoreDataProperties.h"
