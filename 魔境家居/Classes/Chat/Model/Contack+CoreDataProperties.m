//
//  Contack+CoreDataProperties.m
//  魔境家居
//
//  Created by Mac on 16/1/18.
//  Copyright © 2016年 mojing. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contack+CoreDataProperties.h"

@implementation Contack (CoreDataProperties)

@dynamic contack_id;
@dynamic image;
@dynamic isSelect;
@dynamic msgcount;
@dynamic name;
@dynamic nickname;
@dynamic lasttime;
@dynamic contack_chats;

@end
