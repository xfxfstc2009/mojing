//
//  MJZoomImage.h
//  魔境家居
//
//  Created by Mac on 15/12/15.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJZoomImage.h"

@class MJZoomImage;

@protocol  ZoomImageViewDelegate<NSObject>
@optional

//图片将要放大
- (void)imageWillZoomIn:(MJZoomImage *)imageView;
//图片将要缩小
- (void)imageWillZoomOut:(MJZoomImage *)imageView;
//图片已经放大
//图片已经缩小



@end

@interface MJZoomImage : UIImageView<UIScrollViewDelegate,UIAlertViewDelegate>
{
    UIImageView *_fullImageView;
    UIScrollView *_scrollView;
    
}

@property(nonatomic,weak)id<ZoomImageViewDelegate> delegate;

//@property (nonatomic, copy)  NSString *fullImageUrlString;
@property (nonatomic, retain)  NSString *imageUrlStr;


@end
