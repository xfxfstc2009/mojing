//
//  MJImageScrollView.m
//  魔境家居
//
//  Created by Mac on 15/12/15.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJImageScrollView.h"
#import <UIImageView+WebCache.h>

@implementation MJImageScrollView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        

        _imageView = [[UIImageView alloc]initWithFrame:self.bounds];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_imageView];
        
        self.delegate =self;
        self.maximumZoomScale = 3;
        self.minimumZoomScale = 1;
        
        
//        UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTap)];
//        
//        doubleTap.numberOfTapsRequired = 2;
//        
//        doubleTap.numberOfTouchesRequired = 1;
//        
//        
//        [self addGestureRecognizer:doubleTap];
//        
//        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTap) ];
//        
//        [self addGestureRecognizer:singleTap];
//        
//        [singleTap requireGestureRecognizerToFail:doubleTap];
        
    }
    return self;
}

- (void)singleTap
{
    NSLog(@"singTap");
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"HideNavigationBarNotification" object:nil];
}

- (void)doubleTap
{
    NSLog(@"doubleTap");
}
//- (void)setImageUrlStr:(NSString *)imageUrlStr
//{
//    _imageUrlStr =imageUrlStr;
//    [self setNeedsLayout];
//}
//-(void)layoutSubviews
//{
//    [_imageView sd_setImageWithURL:[NSURL URLWithString:self.imageUrlStr]];
//}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _imageView;
}

@end
