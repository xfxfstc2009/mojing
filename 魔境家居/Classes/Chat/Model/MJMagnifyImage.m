//
//  MJMagnifyImage.m
//  魔境家居
//
//  Created by Mac on 15/12/1.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJMagnifyImage.h"
#import "MJImageScrollView.h"
static CGRect oldframe;
@implementation MJMagnifyImage

+(void)showImage:(UIImageView
                  *)imgView{
    
    UIImage *image=imgView.image;
    
    UIWindow *window=[UIApplication sharedApplication].keyWindow;
    
    MJImageScrollView *backgroundView=[[MJImageScrollView alloc]initWithFrame:CGRectMake(0,
                                                            0,
                                                            [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    oldframe=[imgView convertRect:imgView.bounds toView:window];
    
    backgroundView.backgroundColor=[UIColor blackColor];
    
    backgroundView.alpha=0;
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:oldframe];
    
    imageView.image=image;
    
    imageView.tag=1;
    
    backgroundView.imageView =imageView;
    
    [window addSubview:backgroundView];

    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideImage:)];
    
    [backgroundView addGestureRecognizer: tap];
    
    
    
    [UIView animateWithDuration:0.3
     
     animations:^{
         
         imageView.frame=CGRectMake(0,([UIScreen mainScreen].bounds.size.height-image.size.height*[UIScreen mainScreen].bounds.size.width/image.size.width)/2,
                                    [UIScreen mainScreen].bounds.size.width, image.size.height*[UIScreen mainScreen].bounds.size.width/image.size.width);
         
         backgroundView.alpha=1;
         
     }
     completion:^(BOOL finished) {
         
         
         
     }];
    
}



+(void)hideImage:(UITapGestureRecognizer*)tap{
    
    MJImageScrollView *backgroundView=(MJImageScrollView*)tap.view;
    
    UIImageView *imageView=backgroundView.imageView;
    
    [UIView animateWithDuration:0.3 animations:^{
         
         imageView.frame=oldframe;
         
         backgroundView.alpha=0;
         
     }
     completion:^(BOOL finished) {
         
         [backgroundView removeFromSuperview];
         
     }];
    
}
@end
