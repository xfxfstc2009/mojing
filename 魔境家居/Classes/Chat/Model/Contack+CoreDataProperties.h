//
//  Contack+CoreDataProperties.h
//  魔境家居
//
//  Created by Mac on 16/1/18.
//  Copyright © 2016年 mojing. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contack.h"

NS_ASSUME_NONNULL_BEGIN

@interface Contack (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *contack_id;
@property (nullable, nonatomic, retain) NSString *image;
@property (nullable, nonatomic, retain) NSNumber *isSelect;
@property (nullable, nonatomic, retain) NSNumber *msgcount;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *nickname;
@property (nullable, nonatomic, retain) NSString *lasttime;
@property (nullable, nonatomic, retain) NSSet<Chat *> *contack_chats;

@end

@interface Contack (CoreDataGeneratedAccessors)

- (void)addContack_chatsObject:(Chat *)value;
- (void)removeContack_chatsObject:(Chat *)value;
- (void)addContack_chats:(NSSet<Chat *> *)values;
- (void)removeContack_chats:(NSSet<Chat *> *)values;

@end

NS_ASSUME_NONNULL_END
