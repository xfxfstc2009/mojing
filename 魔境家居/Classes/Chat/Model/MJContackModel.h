//
//  MJContackModel.h
//  魔境家居
//
//  Created by Mac on 15/12/16.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJContackModel : NSObject
//当前联系人ID
@property (nonatomic, assign) NSInteger currentContackId;
//当前联系人在联系人列表的下标
@property (nonatomic, assign) NSInteger currentSelectIndex;
//未读消息
@property (assign, nonatomic) NSInteger unreadMsgCount;
+ (MJContackModel *)getInstance;
+ (void)deleteInstance;
@end
