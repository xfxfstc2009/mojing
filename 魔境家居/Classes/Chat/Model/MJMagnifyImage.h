//
//  MJMagnifyImage.h
//  魔境家居
//
//  Created by Mac on 15/12/1.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJImageScrollView.h"

@interface MJMagnifyImage : NSObject
+(void)showImage:(UIImageView*)imgView;
+(void)hideImage:(UITapGestureRecognizer*)tap;
@end
