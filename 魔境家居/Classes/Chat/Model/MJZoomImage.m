//
//  MJZoomImage.m
//  魔境家居
//
//  Created by Mac on 15/12/15.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJZoomImage.h"

#import "MBProgressHUD.h"
#import <UIImageView+WebCache.h>


@implementation MJZoomImage{

    
    
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ([super initWithCoder:aDecoder]) {
        [self _initTap];
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if ([super initWithFrame:frame]) {
        [self _initTap];
        
    }
    return self;
}

- (instancetype)initWithImage:(UIImage *)image
{
    if ([super initWithImage:image]) {
        [self _initTap];
        
    }
    return self;
}


- (void)_initTap
{
    self.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(zoomIn)];
    
    [self addGestureRecognizer:tap];
    
    self.contentMode = UIViewContentModeScaleAspectFit;
    
}


- (void)setImageUrlStr:(NSString *)imageUrlStr
{
    _imageUrlStr =imageUrlStr;
    [self setNeedsLayout];
}
-(void)layoutSubviews
{


}
//点击放大
- (void)zoomIn{
    
    _fullImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
    _fullImageView.contentMode =UIViewContentModeScaleAspectFit;
    [_fullImageView sd_setImageWithURL:[NSURL URLWithString:self.imageUrlStr]];
    
    //调用代理方法
    if ([self.delegate respondsToSelector:@selector(imageWillZoomIn:)]) {
        
        [self.delegate imageWillZoomOut:self];
        
    }
    
    //隐藏原图
    self.hidden = YES;
    
    //02创建大图浏览_scrollView;
    [self _createView];
    
    //03计算出_fullImageView.frame
    //计算出小图self 相对于 window坐标
    CGRect frame = [self convertRect:self.bounds toView:self.window];
    _fullImageView.frame = frame;
    
    //04放大
    [UIView animateWithDuration:0.5 animations:^{
        _fullImageView.frame =_scrollView.bounds;
    } completion:^(BOOL finished) {

        _scrollView.backgroundColor = [UIColor blackColor];
        
    }];
    

    
}
//缩小
- (void)zoomOut{
    
    //调用代理方法
    if ([self.delegate respondsToSelector:@selector(imageWillZoomOut:)]) {
        
        [self.delegate imageWillZoomOut:self];
        
    }

    
    //缩小动画效果
    self.hidden = NO;
    [UIView animateWithDuration:0.3
                     animations:^{
                         _scrollView.backgroundColor = [UIColor clearColor];
                         CGRect frame = [self convertRect:self.bounds  toView:self.window];
                         _fullImageView.frame = frame;
                         
                         
                     } completion:^(BOOL finished) {
                         
                         [_scrollView removeFromSuperview];
                         _scrollView = nil;
                         _fullImageView = nil;
                         
                         
                     }];
    
}

- (void)_createView
{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.delegate =self;
        _scrollView.maximumZoomScale = 3;
        _scrollView.minimumZoomScale = 1;
        [self.window addSubview:_scrollView];
        
       
        [_scrollView addSubview:_fullImageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(zoomOut)];
        
        [_scrollView addGestureRecognizer:tap];
        
        
        //02长按进度条
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(savePhoto:)];
        //longPress.minimumPressDuration =1.5;
        [_scrollView addGestureRecognizer:longPress];
        

    }
}

- (void)savePhoto:(UILongPressGestureRecognizer *)longPress{
    
    if (longPress.state == UIGestureRecognizerStateBegan) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"是否保存图片" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        [alert show];
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex ==1) {
        
        //1.提示保存
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.window animated:YES];
        hud.labelText = @"正在保存";
        hud.dimBackground = YES;
        
        //2.将大图图片保存相册
        //  - (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
        UIImageWriteToSavedPhotosAlbum(_fullImageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void*)(hud));
        
        
    }
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    //提示保存成功
    MBProgressHUD *hud = (__bridge MBProgressHUD*)(contextInfo);
    
    hud.customView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
    
    //显示模式改为：自定义视图模式
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText =@"保存成功";
    
    [hud hide:YES afterDelay:1.5];
    
    
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _fullImageView;
}

@end
