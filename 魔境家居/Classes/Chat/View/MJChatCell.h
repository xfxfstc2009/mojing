//
//  MJChatCell.h
//  魔境家居
//
//  Created by Mac on 15/11/12.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJZoomImage.h"
#import "Chat.h"
@interface MJChatCell : UITableViewCell<ZoomImageViewDelegate>

@property(nonatomic,strong) Chat* chatMessage;
@property(nonatomic,strong) Chat* previousChatMessage;

@end
