//
//  MJChatCell.m
//  魔境家居
//
//  Created by Mac on 15/11/12.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJChatCell.h"
#import "MJZoomImage.h"
#import <UIImageView+WebCache.h>
#import <MJPrefixHeader.pch>
#import "Contack.h"
#import "MJTransform.h"
#import <MJTimestampTool.h>


#define kWidth  784



@implementation MJChatCell
{
    UIImageView *_iconImageView;
    UIImageView *_bgImageView;
    UILabel *_msgLabel;
    UILabel *_timeLabel;
    
    
    MJZoomImage *_imageView;
    
    //放大用
    UIImageView* imgView;
    
    
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setChatMessage:(Chat *)chatMessage
{
    _chatMessage =chatMessage;
    
    [self setNeedsLayout];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self _createContentView];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}

- (void)_createContentView{
    self.userInteractionEnabled=YES;
    _bgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _iconImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    
    _msgLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _msgLabel.font = [UIFont systemFontOfSize:18];
    _msgLabel.numberOfLines = 0;
    _msgLabel.textColor = [UIColor blackColor];
    _bgImageView.userInteractionEnabled=YES;
    _imageView.userInteractionEnabled=YES;
    _timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _timeLabel.font = [UIFont systemFontOfSize:16];
    _timeLabel.textColor = [UIColor grayColor];
    _timeLabel.textAlignment = NSTextAlignmentCenter;
    
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_bgImageView];
    [self.contentView addSubview:_iconImageView];
    [self.contentView addSubview:_msgLabel];
    [self.contentView addSubview:_timeLabel];
    
}





- (void)layoutSubviews{
    
    [super layoutSubviews];

    [self timeSpecification];
    _timeLabel.frame = CGRectMake(0, 0, self.width, 20);
    
    NSDictionary* dic = [MJTransform dictionaryWithJsonString:_chatMessage.content];
    
    UIImage *imgLeft = [UIImage imageNamed:@"chatto_bg_normal-1"];
    UIImage *imgRight = [UIImage imageNamed:@"chatto_bg_normal-2"];
    
    UIImage *bgImage = [[UIImage alloc]init];
    if ([self.chatMessage.isSelf isEqual:@(1)]) {
        bgImage =imgRight;
        
        NSData* imageurl = [MJUserDefaults objectForKey:@"userAvatarURL"];

        [_iconImageView setImage:[UIImage imageWithData:imageurl]];
        
    } else {
        bgImage =imgLeft;
        
        Contack* contack1 = _chatMessage.chat_contack;
        [_iconImageView sd_setImageWithURL:[NSURL URLWithString:contack1.image]];
        
    }
    bgImage = [bgImage stretchableImageWithLeftCapWidth:bgImage.size.width*0.5  topCapHeight:bgImage.size.height*0.7];
    
    _bgImageView.image = bgImage;
    
    //发图片
    if ([[dic objectForKey:@"type"]isEqualToString:@"2"]) {
        
        
        NSDictionary* imgDic = [dic objectForKey:@"image"];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_1.%@",mainURL,[imgDic objectForKey:@"suffix"],[imgDic objectForKey:@"name"],[imgDic objectForKey:@"name"],[imgDic objectForKey:@"suffix"]]];
        
        _imageView = [[MJZoomImage alloc]init];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        _imageView.clipsToBounds =YES;
        [_imageView sd_setImageWithURL:url];
        _imageView.imageUrlStr =[NSString stringWithFormat:@"%@upload/%@/%@/%@_1.%@",mainURL,[imgDic objectForKey:@"suffix"],[imgDic objectForKey:@"name"],[imgDic objectForKey:@"name"],[imgDic objectForKey:@"suffix"]];
        [_msgLabel setHidden:YES];
        
        
        if ([self.chatMessage.isSelf  isEqual: @(1)]) {//右边
            _iconImageView.frame = CGRectMake(kWidth-50, 30, 40, 40);
            _bgImageView.frame = CGRectMake(kWidth-330-60, 25, 330, 225+20);
            _imageView.frame = CGRectMake(10, 10, 300, 225);
            [_bgImageView addSubview:_imageView];
            
            
            
            
        }else{
            _iconImageView.frame = CGRectMake(10, 30, 40, 40);
            _bgImageView.frame = CGRectMake(60, 20, 325, 225+20);
            _imageView.frame = CGRectMake(10, 10, 300, 225);
            [_bgImageView addSubview:_imageView];
            
            
            
        }
        

        
    }else
    {
        NSString *strData = [dic objectForKey:@"data"];
        
        
        _msgLabel.text = strData;
        CGFloat maxLabelWidth = kWidth - 140;
        
        //iOS 7
        //10+40          40+10
        
        
        NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:18]};
        CGSize contenSize = [strData boundingRectWithSize:CGSizeMake(maxLabelWidth, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        
        if ([self.chatMessage.isSelf  isEqual: @(1)]) {//右边
            _iconImageView.frame = CGRectMake(kWidth-50, 25, 40, 40);
            _bgImageView.frame = CGRectMake(kWidth-contenSize.width-100, 25, contenSize.width+40, contenSize.height+10);
            _msgLabel.frame = CGRectMake(kWidth-contenSize.width-90, 30, contenSize.width, contenSize.height);
            
        }else{
            _iconImageView.frame = CGRectMake(10, 25, 40, 40);
            _bgImageView.frame = CGRectMake(60, 20, contenSize.width+40 , contenSize.height+20);
            _msgLabel.frame = CGRectMake(70, 30, contenSize.width, contenSize.height);
        }
        
        
    
        
    }
    
    
    //角上圆弧的半径
    _iconImageView.layer.cornerRadius = 20;
    //外框边宽度
    _iconImageView.layer.borderWidth = 1;
    //@property CGColorRef borderColor;注：这个是CGColor
    _iconImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    //框框外面多出的部分是否裁掉
    _iconImageView.layer.masksToBounds = YES;
    
    
}

//时间规范 2016-01-22 17:05:18
- (void)timeSpecification {
    
    NSString* currentTimeStamp = [MJTimestampTool getCurrentTimestamp];
    
    //当前时间 2016-01-22 17:05:18
    NSString* currentTime = [MJTimestampTool timestampChangesStandarTime:[currentTimeStamp doubleValue]];
    //聊天时间 2016-01-22 17:05:18
    NSString* chatTime = [MJTimestampTool timestampChangesStandarTime:[_chatMessage.time doubleValue]/1000];
    //上一条聊天时间2016-01-22 17:05:18
    NSString* previousChatTime;
    if (_previousChatMessage) {
        previousChatTime = [MJTimestampTool timestampChangesStandarTime:[_previousChatMessage.time doubleValue]/1000];
    }
    else
    {
        previousChatTime =@"0000000000000000000";
        
    }
    
    
    
    if (currentTime.length <10 || chatTime.length<10  || previousChatTime .length<10) {
        NSLog(@"时间有问题%li%li",currentTime.length,chatTime.length);
    }
    //2016-01-22 17:0
    NSString* currentTimeSubMinute = [currentTime substringToIndex:15];
    NSString* chatTimeSubMinute = [chatTime substringToIndex:15];
    NSString* previousChatTimeSubMinute = [previousChatTime substringToIndex:15];
    
    //2016-01-22
    NSString* currentTimeSubDay = [currentTime substringToIndex:10];
    NSString* chatTimeSubDay = [chatTime substringToIndex:10];
    NSString* previousChatTimeSubDay = [previousChatTime substringToIndex:10];
    
    //2016
    NSString* currentTimeSubyear = [currentTime substringToIndex:4];
    NSString* chatTimeSubyear= [chatTime substringToIndex:4];
    NSString* previousChatTimeSubyear = [previousChatTime substringToIndex:4];
    
    
    //比较前15位数 即是否相差10分钟以内,以内就不显示
    if ([previousChatTimeSubMinute isEqualToString:chatTimeSubMinute]) {
        //        NSRange range = NSMakeRange(10,6);
        //        _timeLabel.text =[chatTime substringWithRange:range];
        
    }else
    {
        if ([currentTimeSubDay isEqualToString:chatTimeSubDay]) {
            NSRange range = NSMakeRange(10,6);
            _timeLabel.text =[chatTime substringWithRange:range];
        }
        else
        {
            //不是当年
            if (![currentTimeSubyear isEqualToString:chatTimeSubyear]) {
                _timeLabel.text =[NSString stringWithFormat:@"%@",[chatTime substringToIndex:10]];
            }
            else
            {
                //20160122
                NSString *x = [currentTimeSubDay stringByReplacingOccurrencesOfString:@"-" withString:@""];
                NSString *y = [chatTimeSubDay stringByReplacingOccurrencesOfString:@"-" withString:@""];
                
                if ([x integerValue] == [y integerValue] +1) {
                    NSRange range = NSMakeRange(10,6);
                    _timeLabel.text =[NSString stringWithFormat:@"昨天 %@",[chatTime substringWithRange:range]];
                }
                else if([currentTimeSubDay integerValue] == [currentTimeSubDay integerValue] +2) {
                    NSRange range = NSMakeRange(10,6);
                    _timeLabel.text =[NSString stringWithFormat:@"前天 %@",[chatTime substringWithRange:range]];
                    
                    
                }else
                {
                    NSRange range = NSMakeRange(5,11);
                    _timeLabel.text =[NSString stringWithFormat:@"%@",[chatTime substringWithRange:range]];
                }
            }
            
        }
        
    }
    
}

- (void)imageWillZoomOut:(MJZoomImage *)imageView{
    
    NSLog(@"缩小");
    
}
- (void)imageWillZoomIn:(MJZoomImage *)imageView{
    
    NSLog(@"放大");
}




@end
