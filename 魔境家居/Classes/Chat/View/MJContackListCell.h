//
//  MJContackListCell.h
//  魔境家居
//
//  Created by Mac on 15/11/12.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contack.h"

@interface MJContackListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *headImage;
@property (weak, nonatomic) IBOutlet UIView *messageCount;

@property(nonatomic,copy)Contack *contackMessage;
@property(nonatomic,copy)UILabel* msgcountLabel;

@end
