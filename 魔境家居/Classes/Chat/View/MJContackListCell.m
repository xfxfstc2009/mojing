//
//  MJContackListCell.m
//  魔境家居
//
//  Created by Mac on 15/11/12.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJContackListCell.h"
#import <UIImageView+WebCache.h>
#import "MJTransform.h"

#define DEFAULT_VOID_COLOR [UIColor whiteColor]

@implementation MJContackListCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        
    }
    return self;
}


- (void)awakeFromNib {
    
}

- (void)setContackMessage:(Contack *)contackMessage
{
    _contackMessage = contackMessage;
    
    [self setNeedsLayout];
}


- (UILabel*)msgcountLabel
{
    if (_msgcountLabel == nil) {
        _msgcountLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 15, 15)];
        _msgcountLabel.textColor = [UIColor whiteColor];
        _msgcountLabel.font = [UIFont systemFontOfSize:12];
        _msgcountLabel.textAlignment = NSTextAlignmentCenter;
        _msgcountLabel.backgroundColor = [UIColor clearColor];
    }
    
    return _msgcountLabel;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
    if (![_contackMessage.msgcount isEqualToNumber:@(0)]) {
        [_messageCount setHidden:NO];
    }else
    {
        [_messageCount setHidden:YES];    }
    
    if (_contackMessage.nickname) {
        _name.text =  [NSString stringWithFormat:@"   %@",_contackMessage.nickname];
    }else{
        _name.text = [NSString stringWithFormat:@"   %@",_contackMessage.name];

    }
    
    
    //角上圆弧的半径
    _headImage.layer.cornerRadius = 19;
    //外框边宽度
    _headImage.layer.borderWidth = 1;
    //@property CGColorRef borderColor;注：这个是CGColor
    _headImage.layer.borderColor = [UIColor whiteColor].CGColor;
    //框框外面多出的部分是否裁掉
    _headImage.layer.masksToBounds = YES;
    if (_contackMessage.image == nil) {
        [_headImage setImage:[UIImage imageNamed:@"mine_head"]];
    }else{
        [_headImage sd_setImageWithURL:[NSURL URLWithString:_contackMessage.image]];
    }
    

    _messageCount.backgroundColor = [MJTransform colorWithHexString:@"ff6600"];
    
    [_messageCount addSubview:self.msgcountLabel];
    
    
    if ([_contackMessage.msgcount isEqualToNumber:@(0)]) {
        [_messageCount setHidden:YES];
    }
    _msgcountLabel.text = [NSString stringWithFormat:@"%@",_contackMessage.msgcount] ;
    
    //角上圆弧的半径
    _messageCount.layer.cornerRadius = 7.5;
    //外框边宽度
    _messageCount.layer.borderWidth = 0.5;
    //@property CGColorRef borderColor;注：这个是CGColor
    _messageCount.layer.borderColor = [UIColor whiteColor].CGColor;
    //框框外面多出的部分是否裁掉
    _messageCount.layer.masksToBounds = YES;

    
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if ([_contackMessage.isSelect isEqualToNumber:@(1)]) {
        selected = YES;
        
    }
    [super setSelected:selected animated:animated];
    
    self.selectionStyle=UITableViewCellSelectionStyleGray;
    // Configure the view for the selected state
}


@end
