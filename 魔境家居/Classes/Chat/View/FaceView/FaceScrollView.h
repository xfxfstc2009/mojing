//
//  FaceScrollView.h
//  魔境家居
//
//  Created by Mac on 15/12/8.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaceView.h"

@interface FaceScrollView : UIView<UIScrollViewDelegate>{
    
    UIScrollView *_scrollView;
    UIPageControl *_pageControl;
    FaceView *_faceView;
    
}

- (void)setFaceViewDelegate:(id<FaceViewDelegate>)delegate;

@end
