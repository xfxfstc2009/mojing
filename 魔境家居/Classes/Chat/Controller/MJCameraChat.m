//
//  MJCameraChat.m
//  魔境家居
//
//  Created by Mac on 16/1/3.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import "MJCameraChat.h"
#import "UIView+Frame.h"
#import "PhotoTrackController.h"
#import "MJSceneCtler.h"

@interface MJCameraChat ()

@end

@implementation MJCameraChat

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSArray *sorts = @[@"相机拍摄",@"场景选择"];
    NSUInteger count = sorts.count;
    CGFloat btnW = 100;
    CGFloat btnH = 30;
    CGFloat btnX = 15;
    CGFloat btnStartY = 15;
    CGFloat btnMargin = 15;
    CGFloat height = 0;
    for (NSUInteger i = 0; i<count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        // 传递模型
        [button setTitle:sorts[i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.width = btnW;
        button.height = btnH;
        button.x = btnX;
        button.y = btnStartY + i * (btnH + btnMargin);
        [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [self.view addSubview:button];
        
        height = CGRectGetMaxY(button.frame);
    }
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 52, self.view.frame.size.width, 1)];
    lineView.backgroundColor = [UIColor grayColor];
    lineView.alpha = 0.2;
    [self.view addSubview:lineView];
    // 设置控制器在popover中的尺寸
    CGFloat width = btnW + 2 * btnX;
    height += btnMargin;
    self.preferredContentSize = CGSizeMake(width, height);
}

- (void)buttonClick:(UIButton *)button
{
    if (button.tag == 0 ) {
        PhotoTrackController *photo = [[PhotoTrackController alloc] init];
        [self presentViewController:photo animated:YES completion:nil];
    }else if (button.tag == 1)
    {
        MJSceneCtler *scene = [[MJSceneCtler alloc] init];
        UINavigationController *sceneNav = [[UINavigationController alloc] initWithRootViewController:scene];
        [self presentModalViewController:sceneNav animated:YES];
    }
    //[MJNotificationCenter postNotificationName:@"cameraClick" object:nil userInfo:@{@"clickTag" : [NSString stringWithFormat:@"%ld",(long)button.tag]}];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
