//
//  MJChatView.m
//  魔境家居
//
//  Created by Mac on 15/11/16.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJChatView.h"
#import "UIBarButtonItem+Extension.h"
#import "UIView+Frame.h"
#import "MJChatCell.h"
#import "MJDBManager.h"
#import "Chat.h"
#import "Contack.h"
#import "MJContackListCell.h"
#import "MJChatCell.h"
#import "FaceScrollView.h"
#import <APService.h>
#import "FaceScrollView.h"
#import <AFNetworking.h>
#import "ImageUpload.h"
#import <SBJson.h>
#import <MJPrefixHeader.pch>
#import <AppDelegate.h>
#import "MJTransform.h"
#import <MJRefresh.h>
#import "MJContackModel.h"
#import "NetConnectURL.h"
#import <MBProgressHUD.h>
#import "MJCameraChat.h"
#import "MJTimestampTool.h"


#define kBottomHeight 60
#define kHeight 728
#define kWidth 984

#define myID [MJUserDefaults objectForKey:@"userID"]
#define kCountDidChangeNofication @"kCountDidChangeNofication"
#define kMessageRefresh @"kMessageRefresh"

#define DEFAULT_VOID_COLOR [UIColor whiteColor]

#define KeyboardHeight 352  //398
#define kNoteViewShowTime 0.5

static NSString *ID = @"Cell1";
static NSString *ID2 = @"Cell2";
static MJChatView *instance = nil;
static dispatch_once_t token;

@implementation MJChatView
{
    
    CGFloat heightForKeyBoard;
    
    //获取的聊天总信息
    NSMutableArray* arrayChat;

    
    MJDBManager* manager;
    
    //当前聊天ID
    NSInteger _contackId;
    
    
    NSMutableArray *_messageArray;
    
    NSMutableArray *_capMessageArray;
    
    //图片表情
    UIView *picture;
    FaceScrollView *faceView;
    UITextField *inputTextField;
    
    //用户名字
    UILabel *_useLabel;
    
    //删除
    NSIndexPath* deleteIndexPath;
    
    //联系人对应信息条数
    NSMutableArray *_messCount;
    
    UIView * _headView;
    
}


+ (instancetype)shareInstance
{

    dispatch_once(&token, ^{

        instance = [[MJChatView alloc]init];
        instance.backgroundColor = [UIColor whiteColor];
        instance.frame = CGRectMake(20, 20, 984, 728);
        

    });

    return instance;

}

+ (void)deleteInstance
{
    token = 0;
    instance = nil;
}


- (void)showWithIndexSelect:(NSUInteger)index
{
    
    [MJDBManager shareInstanceWithCoreDataID:myID];
    
    [MJContackModel getInstance].currentSelectIndex=index;
    
    NSArray *obj = [[NSMutableArray alloc]initWithArray:[self quaryTable:@"Contack" andPredicate:nil]];
    [_contackArray removeAllObjects];
    _contackArray = [NSMutableArray arrayWithArray:[self timeSort:obj]];
    if ([_contackArray count] ==0) {
        [_bottomView setHidden:YES];
        
    }
    else
    {
        [_bottomView setHidden:NO];
    }
    [self.tableViewLeft reloadData];
    
    [UIView animateWithDuration:kNoteViewShowTime animations:^{
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        [window addSubview:[MJChatView shareInstance]];
        self.frame = CGRectMake(20, 20, 984, 728);
        
    } completion:^(BOOL finished) {


        self.clipsToBounds =YES;
        self.hidden =NO;

        NSInteger x=index+1;
        if ([self.contackArray count]>= x) {

                
            [self.tableViewLeft selectRowAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
            [self tableView:self.tableViewLeft didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
            
            [self.tableViewLeft reloadData];
            [self.tableViewRight reloadData];

        }
        
    }];
}



- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _headView = [[UIView alloc]initWithFrame:CGRectMake(0, 60, 200, 40)];
        _headView.backgroundColor = [UIColor grayColor];
        UILabel *tip = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 40)];
        tip.textAlignment = NSTextAlignmentCenter;
        tip.text =@"暂无联系人";
        [_headView addSubview:tip];
        
        
        _currentIndex= 0;
        

        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];

        [defaultCenter addObserver:self
                          selector:@selector(networkDidReceiveMessage:)
                              name:kJPFNetworkDidReceiveMessageNotification
                            object:nil];
        
        [defaultCenter addObserver:self
                          selector:@selector(messageRefresh:)
                              name:kMessageRefresh
                            object:nil];
        
        [MJNotificationCenter addObserver:self selector:@selector(nickNameChange:) name:@"nickNameChange" object:nil];

        //监听键盘事件
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showKeyBoard:) name:UIKeyboardWillShowNotification object:nil];
        //隐藏
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hideKeyBoard:) name:UIKeyboardWillHideNotification object:nil];
        
        _messageArray = [[NSMutableArray alloc]init];
        //打开数据库
        manager=[MJDBManager shareInstanceWithCoreDataID:myID];
        NSArray *obj = [[NSMutableArray alloc]initWithArray:[self quaryTable:@"Contack" andPredicate:nil]];
        
        _contackArray = [[NSMutableArray alloc]initWithArray:[self timeSort:obj]];
        
        _messCount = [[NSMutableArray alloc]initWithCapacity:[_contackArray count]];
        for (NSInteger i =0; i<[_messCount count]; i++) {
            _messCount[i]=@(10);
        }
        
        UILabel* whiteBottom = [[UILabel alloc]initWithFrame:CGRectMake(200, 668, 784, kBottomHeight)];
        whiteBottom.backgroundColor =[UIColor whiteColor];
        [self addSubview:whiteBottom];
        
        [self addSubview:self.tableViewLeft];
        [self addSubview:self.tableViewRight];
        [self addSubview:self.bottomView];
        
        [self setRightButton];
        
        if([_contackArray count]>0)
        {
            
            [self.tableViewLeft selectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];//设置选中第一行（默认有蓝色背景）
            [self tableView:self.tableViewLeft didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];//实现点击第一行所调用的方法
            [_tableViewLeft reloadData];
            [_tableViewRight reloadData];
        }

        
    }
    return self;
    
}


#pragma 懒加载
- (UITableView *)tableViewLeft
{
    
    if (_tableViewLeft == nil) {
        _tableViewLeft = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, 200, 668) style:UITableViewStyleGrouped];
        _tableViewLeft.delegate = self;
        _tableViewLeft.dataSource = self;
        _tableViewLeft.bounces=YES;
        _tableViewLeft.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        UINib *nib = [UINib nibWithNibName:@"MJContackListCell" bundle:nil];
        [_tableViewLeft registerNib:nib forCellReuseIdentifier:ID];
        
        
        _tableViewLeft.tag = 100;

        UIView *foot = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 670)];
        foot.backgroundColor = [MJTransform colorWithHexString:@"ededed"];
        UIView *foot1 = [[UIView alloc] initWithFrame:CGRectMake(199, 0, 1, 670)];
        foot1.backgroundColor = [MJTransform colorWithHexString:@"ededed"];
        
        [_tableViewLeft addSubview:foot];
        [_tableViewLeft addSubview:foot1];
        
    }
    
    return _tableViewLeft;
}

- (UITableView *)tableViewRight
{
    
    if (_tableViewRight == nil) {
        
        //

        
        _tableViewRight = [[UITableView alloc] initWithFrame:CGRectMake(200, 60, 784, 668-kBottomHeight) style:UITableViewStylePlain];
        _tableViewRight.delegate = self;
        _tableViewRight.dataSource = self;
        _tableViewRight.bounces=YES;
        
        _tableViewRight.separatorStyle = UITableViewCellSelectionStyleNone;
        _tableViewRight.editing = YES;
        _tableViewRight.tag =101;
        _capMessageArray = [[NSMutableArray alloc]init];
        _tableViewRight.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            [self _loadMoreData];
            
        }];
        [self.tableViewRight.mj_header beginRefreshing];
        if ([_messageArray count]== 0) {
            return _tableViewRight;
        }
        Contack* contack1 = (Contack*)_contackArray[0];
        _contackId = [contack1.contack_id integerValue];
        
        [MJContackModel getInstance].currentContackId = [contack1.contack_id integerValue];
        
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"contack_id=%@",contack1.contack_id];
        NSArray *array = [self quaryTable:@"Contack" andPredicate:predicate];
        
        
        
        
        if (array == nil) {
            NSLog(@"聊天记录为空");
        }else
        {
            Contack *contack1= array[0];
            for (Chat *chat in contack1.contack_chats) {
                
                [_messageArray addObject:chat];
            }
            if ([_messageArray count]<15) {
                _capMessageArray=_messageArray;
            }
            for (NSInteger y =[_messageArray count]-15; y<15; y++)
            {
                [_capMessageArray addObject:_messageArray[y]];
            }
            [_tableViewRight reloadData];
            
            //tableView滑到最下面
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_capMessageArray.count-1 inSection:0];
            [_tableViewRight scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
        }
        

        
    }


    return _tableViewRight;
}

- (void)getContackMessage
{
    __weak MJChatView *ctrl=self;
    
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    session.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSMutableDictionary *contackDic1 = [[NSMutableDictionary alloc]init];
    [contackDic1 setObject:[NSString stringWithFormat:@"%ld",(long)_contackId] forKey:@"ids"];
    
    [session GET:CHAT_CONTACK parameters:contackDic1 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        
        if ([[dict objectForKey:@"code"] isEqualToNumber:@(0)])
        {
            NSArray *contackInfo =[dict objectForKey:@"data"];
            NSDictionary* dic = contackInfo[0];
            //@"http://api.duc.cn/user/getUserByQuery"
            NSString *headImgUrl = [dic objectForKey:@"avatarPic"];
            
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"contack_id=%li",_contackId];
            
            NSArray *obj=[ctrl quaryTable:@"Contack" andPredicate:predicate];
            if ([obj count] ==0)
            {
                return ;
            }
            Contack *contackA = obj[0];
            
            contackA.image =headImgUrl;
            [manager updateManagerObj:contackA];
            
            [MJDBManager shareInstanceWithCoreDataID:myID];
            NSArray *newArray = [[NSMutableArray alloc]initWithArray:[self quaryTable:@"Contack" andPredicate:nil]];
            [_contackArray removeAllObjects];
            _contackArray = [NSMutableArray arrayWithArray:[self timeSort:newArray]];
            [_tableViewLeft reloadData];
            [_tableViewRight reloadData];
            //                //                                contack1.name =
            //
            //                [managerData saveManagerObj:contack1];
            //
            //                [contackArray addObject:contack1];
            
            
        }else
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"错误" message:[dict objectForKey:@"msg"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"获取联系人信息失败%@",error);
    }];
}

- (void)_loadMoreData{
    [self performSelectorOnMainThread:@selector(appendTableWith) withObject:_messageArray waitUntilDone:NO];
    
}
                                              
-(void) appendTableWith
{
    //一句聊天都没的情况
    if ([_capMessageArray count] ==0) {
        [self.tableViewRight.mj_header endRefreshing];
        return ;
    }
    
    Chat* chat = (Chat*)_capMessageArray[0];
    NSUInteger x=[_messageArray indexOfObject:chat];
    if (x ==0) {
        [self.tableViewRight.mj_header endRefreshing];
        return;
    }
    if (x<15) {
        for (NSInteger i = x-1; i>=0; i--) {
            [_capMessageArray insertObject:_messageArray[i] atIndex:0];
        }
    }else
    {
        for (NSInteger i = 1;i<16 ; i++) {
            [_capMessageArray insertObject:_messageArray[x-i] atIndex:0];
        }
    }
    [_tableViewRight reloadData];
    [self.tableViewRight.mj_header endRefreshing];
    

}

- (UIView *)bottomView{
    
    if (_bottomView == nil) {
    
    
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(200, kHeight-kBottomHeight, kWidth-150-40-6, kBottomHeight)];
        _bottomView.backgroundColor = [UIColor whiteColor];
        //背景视图
        UIImage *bottomImage = [UIImage imageNamed:@"chat_textfield_background"];
        
        UIImageView *bottomImageView = [[UIImageView alloc] initWithFrame:_bottomView.bounds];
        bottomImageView.image = bottomImage;
        
        //左边按钮
        UIButton *voiceButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 15, 30, 30)];
        [voiceButton setImage:[UIImage imageNamed:@"chat_bottom_voice_nor"] forState:UIControlStateNormal];
        
        //右1按钮(kWidth-200-80+5, 15, 30, 30)
        UIButton *upButton = [[UIButton alloc] initWithFrame:CGRectMake(kWidth-200-40+5, 15, 30, 30)];
        [upButton setImage:[UIImage imageNamed:@"chat_image"] forState:UIControlStateNormal];
        [upButton addTarget:self action:@selector(pictureAction) forControlEvents:UIControlEventTouchUpInside];
        
        //右2按钮
        UIButton *smileButton  = [[UIButton alloc] initWithFrame:CGRectMake(kWidth-200-40+5, 15, 30, 30)];
        [smileButton setImage:[UIImage imageNamed:@"chat_bottom_smile_nor"] forState:UIControlStateNormal];
            [smileButton addTarget:self action:@selector(expressionAction) forControlEvents:UIControlEventTouchUpInside];
        
        //输入框(50, 10,
        inputTextField = [[UITextField alloc] initWithFrame:CGRectMake(15, 10,kWidth-200-80-50+70,40)];
        [inputTextField setBackground:[UIImage imageNamed:@"chat_textfield_background"]];
        inputTextField.borderStyle = UITextBorderStyleNone;
        
        //设置光标偏移：其实是设置UILable的leftView为透明视图
        
        UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(0,0,7,26)];
        leftView.backgroundColor = [UIColor clearColor];
        inputTextField.leftView = leftView;
        inputTextField.leftViewMode = UITextFieldViewModeAlways;
        inputTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        inputTextField.delegate = self;
        inputTextField.returnKeyType = UIReturnKeySend;
        
        
        picture = [[UIView alloc]initWithFrame:CGRectMake(kWidth - 120, kHeight-150, 112, 88)];
        UIImageView* imageView = [[UIImageView alloc] initWithFrame:picture.bounds];
        imageView.image = [UIImage imageNamed:@"chat_picture"];
        [picture addSubview:imageView];
            //相机拍摄
        UIButton *btn1= [UIButton buttonWithType:UIButtonTypeCustom];
        
        btn1.frame =CGRectMake(0, 0, 112, 42);
        [btn1 setTitle:@"相机拍摄" forState:UIControlStateNormal];
        [btn1 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btn1 addTarget:self action:@selector(photograph) forControlEvents:UIControlEventTouchUpInside];
            
        //选取图片
        UIButton *btn2=[UIButton buttonWithType:UIButtonTypeCustom];
        [btn2 setTitle:@"选取图片" forState:UIControlStateNormal];
        [btn2 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];

        btn2.frame =CGRectMake(0, 42, 112, 42);
        [btn2 addTarget:self action:@selector(selectPicture) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *foot = [[UIView alloc] initWithFrame:CGRectMake(0, 42, 112, 1)];
        foot.backgroundColor = [MJTransform colorWithHexString:@"ededed"];
        [picture addSubview:foot];
        
        
        [picture addSubview:btn1];
        [picture addSubview:btn2];
            
        faceView = [[FaceScrollView alloc]init];
        [faceView setFaceViewDelegate:self];
        faceView.frame = CGRectMake(kWidth -220, kHeight-170-60, 210, 180);

        [self addSubview:faceView];
        [faceView setHidden:YES];

            
        //添加子视图
        [_bottomView addSubview:bottomImageView];
//        [_bottomView addSubview:voiceButton];
        [_bottomView addSubview:upButton];
//        [_bottomView addSubview:smileButton];
        [_bottomView addSubview:inputTextField];
        
        [self addSubview:picture];
        [picture setHidden:YES];
            
    }
    
    return _bottomView;
}

- (UIButton *)chatCoverView
{
    if (!_chatCoverView) {
        // 遮盖
        _chatCoverView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
        _chatCoverView.backgroundColor = [UIColor blackColor];
        _chatCoverView.alpha = 0.5;
        [MJKeyWindow addSubview:_chatCoverView];
    }
    return _chatCoverView;
}

- (void)setRightButton
{
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, kWidth, 60)];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"chat_top"]];
    imgView.frame = CGRectMake(0, 0, kWidth, 60);
    
    UIButton *back = [[UIButton alloc]initWithFrame:CGRectMake(kWidth-60, 0, 60, 60)];
    [back addTarget:self action:@selector(back1) forControlEvents:UIControlEventTouchUpInside];

    [back setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    
    _useLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 60)];
    _useLabel.font = [UIFont systemFontOfSize:25];
    _useLabel.textColor = [UIColor blackColor];
    NSString* userName = [MJUserDefaults objectForKey:@"nickName"];
    
    _useLabel.text = [NSString stringWithFormat:@"     %@",userName];
    
    [topView addSubview:imgView];
    [topView addSubview:back];
    [topView addSubview:_useLabel];
    [self addSubview:topView];
    
}

- (void)back1
{
    [MJContackModel getInstance].currentSelectIndex = _currentIndex;
   // [self.chatCoverView removeFromSuperview];
    if (_close) {
        _close();
    }
}


#pragma -tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    
    if (tableView.tag == 100) {

        if ([_contackArray count] ==0) {
            
            return 0;
        }
        return [_contackArray count];
    }else
    {
        if ([_contackArray count] ==0) {
            
            return 0;
        }

        return [_capMessageArray count];
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 100) {

        if ([_contackArray count] == 0) {
            
            return nil;
        }
        
        
        MJContackListCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
       // MJContackListCell *cell =[tableView cellForRowAtIndexPath:indexPath];
        
        if (nil == cell) {
            cell = [[MJContackListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        }
        
        cell.contackMessage = _contackArray[indexPath.row];
        

        
        return cell;
        
    }else
    {
        if ([_contackArray count] == 0) {
            return nil;
        }

        
        MJChatCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (cell == nil) {
            cell = [[MJChatCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID2];
            
        }
        
        cell.chatMessage = (Chat *)_capMessageArray[indexPath.row];
        if (indexPath.row == 0) {
            cell.previousChatMessage = nil;
        }
        else{
            cell.previousChatMessage =(Chat *)_capMessageArray[indexPath.row-1];
        }
        cell.userInteractionEnabled = YES;

        return cell;
        
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView.tag == 100) {
        return 0.01;
    }else{
    return 0.01;
    }

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == 100) {
        if ([_contackArray count]==0) {
            return _headView;
        }
        else{
            return nil;
        }
        
    }
    else{
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (tableView.tag == 100) {
        if ([_contackArray count]==0)
        {
            [tableView.tableHeaderView setHidden:NO];

            return 0;
        }else
        {
            if (tableView.tableHeaderView) {
                tableView.tableHeaderView=nil;
            }
            return 44;
        }

    }else
    {
        if ([_messageArray count]==0) {
            return 0;
        }
        
        Chat* chat = _capMessageArray[indexPath.row];
        NSString *chatContent = chat.content;
        
        NSDictionary *dict =[MJTransform dictionaryWithJsonString:chatContent];
        
        //图片
        if ([[dict objectForKey:@"type"] isEqualToString:@"2"]) {
            return 250 +20+20;
        }else
        {
            
            NSString *strData = [dict objectForKey:@"data"];
           
            NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:18]};
            CGSize contenSize = [strData boundingRectWithSize:CGSizeMake(kWidth-200 - 140, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            return contenSize.height + 20 +20+20;
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    if (tableView.tag == 100) {

        
//        NSArray *obj = [[NSMutableArray alloc]initWithArray:[self quaryTable:@"Contack" andPredicate:nil]];
//        if ([obj count] !=0) {
//            [_contackArray removeAllObjects];
//            _contackArray = [NSMutableArray arrayWithArray:obj];
//            [_tableViewLeft reloadData];
//        }

        
        

        [_messageArray removeAllObjects];
        [_capMessageArray removeAllObjects];
       
        
        [MJContackModel getInstance].currentSelectIndex = indexPath.row;

        
        Contack* contack1 = (Contack*)_contackArray[indexPath.row];
        
        
        //如果没有未读信息就不置顶
//        if (contack1.msgcount == 0) {
            _currentIndex = indexPath.row;
//        }
//        else{
//            _currentIndex = 0;
//        }
        
        _contackId = [contack1.contack_id integerValue];
        
        if ([myID isEqualToString:[NSString stringWithFormat:@"%li",_contackId]]) {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"您不能与自己聊天"
                                                           delegate:self
                                                  cancelButtonTitle:@"确定"
                                                  otherButtonTitles: nil];
            alert.tag=106;
            [alert show];
        }
        
        
        [self getContackMessage];
        [manager updateZeroManagerObj:contack1];
        
        [_contackArray removeAllObjects];
        NSArray *arrayContack = [self quaryTable:@"Contack" andPredicate:nil];
        _contackArray =[NSMutableArray arrayWithArray:[self timeSort:arrayContack]];
        
        
        [_tableViewLeft reloadData];
        
        
        [MJContackModel getInstance].currentContackId = [contack1.contack_id integerValue];
        
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"contack_id=%@",contack1.contack_id];
        NSArray *array = [self quaryTable:@"Contack" andPredicate:predicate];
        
        if (array == nil) {
            NSLog(@"聊天记录为空");
        }else
        {
            
            Contack *contack1= array[0];
            
            NSMutableArray* setArr = [[NSMutableArray alloc]init];
            for (Chat *chat in contack1.contack_chats) {
                
                [setArr addObject:chat];
            }
            
            NSComparator cmptr = ^(id obj1, id obj2){
                Chat* chat1 = (Chat*)obj1;
                Chat* chat2 = (Chat*)obj2;

                    if ([chat1.time doubleValue]> [chat2.time doubleValue]) {
                        return (NSComparisonResult)NSOrderedDescending;
                    }
    
                    if ([chat1.time doubleValue] < [chat2.time doubleValue]) {
                        return (NSComparisonResult)NSOrderedAscending;
                    }
                return (NSComparisonResult)NSOrderedSame;
            };
            
            NSArray *sortArray = [[NSArray alloc] initWithArray:setArr];
            
            //第一种排序
            NSArray *array = [[NSArray alloc]init];
            array =[sortArray sortedArrayUsingComparator:cmptr];
            [_messageArray removeAllObjects];
            _messageArray = [NSMutableArray arrayWithArray:array];
            
//            for (int i=0; i<[_messageArray count]; i++) {
//                Chat* x=_messageArray[i];
//                NSLog(@"%@%@",x.time,x.content);
//            }
            
            if ([_messageArray count]<15) {
                _capMessageArray=_messageArray;
            }else
            {
                for (NSInteger i = [_messageArray count] -15; i<[_messageArray count]; i++) {
                    [_capMessageArray addObject:_messageArray[i]];
                }
            }
            [_tableViewRight reloadData];
            
            if ([_capMessageArray count] != 0) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_capMessageArray.count-1 inSection:0];
                [_tableViewRight scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            }
        }
        [self.bottomView setHidden:NO];
    }else if (tableView.tag == 101)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
    
}


#pragma -联系人删除
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag ==100) {
        return YES;
    }else
    {
        return NO;
    }
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag ==100) {
        return UITableViewCellEditingStyleDelete;
    }else
    {
        return UITableViewCellEditingStyleNone;
    }
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        deleteIndexPath = [[NSIndexPath alloc]init];
        deleteIndexPath =indexPath;
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@""
                                           message:@"是否删除联系人"
                                          delegate:self
                                 cancelButtonTitle:@"取消"
                                 otherButtonTitles:@"确定", nil];
        alert.tag=100;
        [alert show];

    }
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    _currentIndex = [MJContackModel getInstance].currentSelectIndex;
    if (buttonIndex == 1 &&alertView.tag ==100) {
        //要删除的联系人
        Contack* contack1 = (Contack*)_contackArray[deleteIndexPath.row];
        
        
        if ([MJContackModel getInstance].currentContackId == [contack1.contack_id integerValue])
        {
            
            [MJContackModel getInstance].unreadMsgCount=[MJContackModel getInstance].unreadMsgCount-[contack1.msgcount integerValue];
            if ([MJContackModel getInstance].unreadMsgCount < 0) {
                NSLog(@"%li",[MJContackModel getInstance].unreadMsgCount);
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kCountDidChangeNofication object:nil];
            
            
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"contack_id=%@",contack1.contack_id];
            NSArray *array = [self quaryTable:@"Contack" andPredicate:predicate];
            
            if (array == nil) {
                NSLog(@"未找到contack_id=%@的联系人",contack1.contack_id);
            }else
            {
                Contack *contack1= array[0];
                [manager removeManagerObj:contack1];
                
                NSArray *remainArray = [self quaryTable:@"Contack" andPredicate:nil];
                
                if ([remainArray count] ==0) {
                    [_bottomView setHidden:YES];
                }
                
            }
            [_messageArray removeAllObjects];
            
            [_tableViewRight reloadData];
            [_bottomView setHidden:YES];
            
        }else if(_currentIndex>deleteIndexPath.row)
        {
            _currentIndex =_currentIndex-1;
            [MJContackModel getInstance].currentSelectIndex =_currentIndex;
        }else
        {
            
        }
        
        //1.删除indexPath对应的数据
        [_contackArray removeObjectAtIndex:deleteIndexPath.row];
        [manager removeManagerObj:contack1];
        
        [_tableViewLeft deleteRowsAtIndexPaths:@[deleteIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [_tableViewLeft reloadData];
        [_tableViewRight reloadData];
        
        
//        _contackId = [contack1.contack_id integerValue];
//        [MJContackModel getInstance].currentContackId = [contack1.contack_id integerValue];
//        
//        [MJContackModel getInstance].unreadMsgCount=[MJContackModel getInstance].unreadMsgCount-[contack1.msgcount integerValue];
//        if ([MJContackModel getInstance].unreadMsgCount < 0) {
//            NSLog(@"%li",[MJContackModel getInstance].unreadMsgCount);
//        }
//        
//        [[NSNotificationCenter defaultCenter] postNotificationName:kCountDidChangeNofication object:nil];
//        
//        
//        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"contack_id=%@",contack1.contack_id];
//        NSArray *array = [self quaryTable:@"Contack" andPredicate:predicate];
//        
//        if (array == nil) {
//            NSLog(@"未找到contack_id=%@的联系人",contack1.contack_id);
//        }else
//        {
//            Contack *contack1= array[0];
//            [manager removeManagerObj:contack1];
//            
//            NSArray *remainArray = [self quaryTable:@"Contack" andPredicate:nil];
//            
//            if ([remainArray count] ==0) {
//                [_bottomView setHidden:YES];
//            }
//            
//            
//            
//        }
//        if (_currentIndex>deleteIndexPath.row) {
//            _currentIndex =_currentIndex-1;
//            [MJContackModel getInstance].currentSelectIndex =_currentIndex;
//        }
//        
//        //1.删除indexPath对应的数据
//        [_contackArray removeObjectAtIndex:deleteIndexPath.row];
//        [_messageArray removeAllObjects];
//        
//        [manager removeManagerObj:contack1];
//        
//        [_tableViewLeft deleteRowsAtIndexPaths:@[deleteIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
//        [_tableViewLeft reloadData];
//        [_tableViewRight reloadData];
    }
    else
    {
        //[self tableView:<#(nonnull UITableView *)#> didSelectRowAtIndexPath:<#(nonnull NSIndexPath *)#>]
        [_tableViewLeft reloadData];
    }
}

#pragma mark - 键盘点击
- (void)hideKeyBoard:(NSNotification *)notification{
    
    _tableViewRight.transform = CGAffineTransformIdentity;
    _bottomView.transform = CGAffineTransformIdentity;
    faceView.transform = CGAffineTransformIdentity;
    picture.transform = CGAffineTransformIdentity;
    [self endEditing:YES];
    
}


- (void)showKeyBoard:(NSNotification *)notification{
    
    NSValue *rectValue = [notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect frame = [rectValue CGRectValue];
    
    CGFloat height = frame.size.height;
    
    heightForKeyBoard = height;
    _tableViewRight.transform = CGAffineTransformMakeTranslation(0 ,-KeyboardHeight-25);
    _bottomView.transform = CGAffineTransformMakeTranslation(0, -KeyboardHeight-25);
    faceView.transform = CGAffineTransformMakeTranslation(0, -KeyboardHeight-25);
    picture.transform = CGAffineTransformMakeTranslation(0, -KeyboardHeight-25);    ;
    
    if ([_capMessageArray count] != 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_capMessageArray.count-1 inSection:0];
        
        [_tableViewRight scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
    }
    
}


#pragma mark - TextFieldDelegate

- (BOOL) isBlankString:(NSString *)string {
    if (string == nil || string == NULL) {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    _currentIndex =0;
    if ([self isBlankString:textField.text] ) {
        return NO;
    }

    if (textField.text.length ==0) {
        return NO;
    }
    
    if ([MJTransform isContainsEmoji:textField.text] ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"输入暂时不支持输入表情" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        alert.tag =102;
        [alert show];
    }else if(textField.text.length >1000)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"输入字数过多" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
        alert.tag =103;
        [alert show];
    }else
    {
        
        NSMutableDictionary* dict = [[NSMutableDictionary alloc]init];
        [dict setObject:@"1" forKey:@"type"];
        [dict setObject:textField.text forKey:@"data"];
        NSString* str = [MJTransform dictionaryToJson:dict];
        
        NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
        [parameter setObject:[NSNumber numberWithInteger:_contackId] forKey:@"receiverUserID"];
        [parameter setObject:str forKey:@"content"];
        [parameter setObject:isApnsProduction forKey:@"isApnsProduction"];
        
        
        
        textField.text = @"";
        Chat *chat0 = (Chat *)[manager createMO:@"Chat"];
        chat0.content = str;
        chat0.isSelf = @(1);
        NSString* strTime = [MJTimestampTool getCurrentTimestamp];
        chat0.time =  [NSString stringWithFormat:@"%f",[strTime doubleValue]*1000] ;
        
        
        [_capMessageArray addObject:chat0];
        [_tableViewRight reloadData];
        if ([_capMessageArray count] != 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_capMessageArray.count-1 inSection:0];
            
            [_tableViewRight scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            
        }
        
        _tableViewRight.transform =CGAffineTransformIdentity;
        _bottomView.transform = CGAffineTransformIdentity;
        faceView.transform = CGAffineTransformIdentity;
        picture.transform =CGAffineTransformIdentity;
        
        [self endEditing:YES];
        
        
        if (![self isBlankString:[dict objectForKey:@"data"]]) {
            

            AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
            session.responseSerializer = [AFHTTPResponseSerializer serializer];

            [session POST:CHAT_SEND parameters:parameter success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                
                NSLog(@"输入文字发送成功");
                NSLog(@"%@",CHAT_SEND);
                NSDictionary *dict1 = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                
                if ([[dict1 objectForKey:@"code"] isEqualToNumber:@(200)])
                {


                    
                    NSDictionary* dict2 = [dict1 objectForKey:@"data"];
                    [self addChatWithName:@"本人" andContent:str andIsSelf:@(1) andImage:nil andContack_id:_contackId andTime:[dict2 objectForKey:@"currentTime"]];
                }
                else
                {
                    NSLog(@"%@",[dict1 objectForKey:@"msg"]);
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:[dict1 objectForKey:@"msg"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
                    alert.tag =101;
                    [alert show];

                    
                }
                

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
            }];

            
        }else{
            
        };
        return YES;
    }
    return YES;
}



#pragma mark- DataSourceTool
- (NSArray *)quaryTable:(NSString *)table andPredicate:(NSPredicate *)predicate
{
    NSArray *array=[[MJDBManager shareInstanceWithCoreDataID:myID] query:table predicate:predicate];

    return array;
}


- (Chat*)addChatWithName:(NSString *)name andContent:(NSString *)content andIsSelf:(NSNumber *)isSelf  andImage:(NSString *)image andContack_id:(NSInteger)contack_id andTime:(NSNumber*)time
{
    
    MJDBManager* manager1= [MJDBManager shareInstanceWithCoreDataID:myID];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"contack_id=%li",contack_id];
    
    NSArray *obj=[self quaryTable:@"Contack" andPredicate:predicate];
    if ([obj count] ==0) {
        return nil;
    }
    
    Contack *contack = obj[0];
    if (![contack.nickname isEqualToString:name] && [isSelf isEqualToNumber:@(0)] ) {
        contack.nickname =name;
    }
    
    double lastT = [contack.lasttime doubleValue];
    double timeT = [time doubleValue];
    if (lastT <= timeT) {
        contack.lasttime = [time stringValue];
    }
    
    Chat *chat = (Chat *)[manager1 createMO:@"Chat"];
    chat.name = name;
    chat.content = content;
    chat.isSelf = isSelf;
    chat.image =image ;
    chat.chat_contack =contack;
    chat.time =[NSString stringWithFormat:@"%@",time];
    
    
    [manager1 saveManagerObj:chat];
    
    
    if ([isSelf isEqualToNumber:@(0)]) {
    
//        AppDelegate* delegate = MJAppDelegate;
        [MJContackModel getInstance].unreadMsgCount++;
        [[NSNotificationCenter defaultCenter] postNotificationName:kCountDidChangeNofication object:nil ];
        
        NSInteger x = [contack.msgcount integerValue];
        x++;
        contack.msgcount = [NSNumber numberWithInteger:x];
        
    }
    
    [manager1 updateManagerObj:contack];

    if ([isSelf isEqualToNumber:@(0)]) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kMessageRefresh object:nil userInfo:@{@"contack" : contack,
                        @"chat":chat}];

    }
    else{
        NSArray *objArr = [[NSMutableArray alloc]initWithArray:[self quaryTable:@"Contack" andPredicate:nil]];
        if ([obj count] !=0) {
            _contackArray = [NSMutableArray arrayWithArray:[self timeSort:objArr]];
        }
    }
    
    
    [_tableViewLeft reloadData];
    [_tableViewRight reloadData];
    return chat;
    
}
+ (Chat*)addChatWithName:(NSString *)name andContent:(NSString *)content andIsSelf:(NSNumber *)isSelf  andImage:(NSString *)image andContack_id:(NSInteger)contack_id andTime:(NSNumber*)time
{

    
    return [[self alloc] addChatWithName:name andContent:content andIsSelf:isSelf andImage:image andContack_id:contack_id andTime:time];
}

#pragma mark -bottomAction

- (void)pictureAction
{
    [faceView setHidden:YES];;
    if ([picture isHidden]) {
        [picture setHidden:NO];
        

    }else{
    [picture setHidden:YES];
    }
    
//    [faceView setHidden:YES];
//    [picture setHidden:YES];
//    //self addSubview:self.
//    self.cameraPop = [[UIPopoverController alloc] initWithContentViewController:[[MJCameraChat alloc] init]];
//    
//    CGRect rect = CGRectMake(self.width-112, 42, 1, 1);
//    [self.cameraPop presentPopoverFromRect:rect inView:self permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}
- (void)expressionAction
{
//    [picture setHidden:YES];
//    if ([faceView isHidden]) {
//        [faceView setHidden:NO];
//        
//    }else{
//        [faceView setHidden:YES];
//    }
    
}

#pragma mark - 表情处理
- (void)faceDidSelect:(NSString *)text{
    
    NSMutableString *mString=[[NSMutableString alloc]initWithString:inputTextField.text];
    inputTextField.text = [mString stringByAppendingString:text];
 
}


#pragma mark -选择照片
- (UIViewController*)popctl{
    if (_popctl == nil) {
        _popctl = [[UIViewController alloc]init];
        _popctl.view.frame = CGRectMake(0, 0, kWidth, kHeight);
        
        _popctl.view.backgroundColor = [UIColor clearColor];
        [self addSubview:self.popctl.view];
    }
    return _popctl;
}


- (void)selectPicture
{

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary]) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
        picker.delegate = self;
        picker.allowsEditing = YES;//是否允许编辑
        picker.modalPresentationStyle = UIModalPresentationFormSheet;
        [self.popctl presentModalViewController:picker animated:YES];
        [_popctl.view setHidden:YES];
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Error accessing photo library!" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [alert show];
    }

}

- (void)photograph
{
    
    
    
    
    UIImagePickerControllerSourceType sourceType;
    sourceType = UIImagePickerControllerSourceTypeCamera;
    BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
    if (!isCamera) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"摄像头无法使用" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
        [alert show];
        sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }

    
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = sourceType;
    picker.allowsImageEditing = YES;
    picker.delegate = self;
    

    [self.popctl presentViewController:picker animated:YES completion:nil];
    [_popctl.view setHidden:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    _currentIndex =0;
    NSMutableDictionary * imgDic = [[NSMutableDictionary alloc]init];
    [picture setHidden:YES];
    //弹出相册控制器
    UIImage *image = [[UIImage alloc]init];
    image = info[UIImagePickerControllerEditedImage];
    
    __block NSString *imgStr =[[NSString alloc]init];
    NSDictionary *img = @{@"image":image, @"name":@"aa.jpg"};
    [ImageUpload requestWithUrl:UPLOAD_IMAGE postData:nil imgList:@[img] userInfo:nil
                       callBack:^(BOOL b, NSDictionary *data, NSDictionary *userInfo)
     {
         
         if (b) {
             
             NSDictionary* imageData =  [data objectForKey:@"data"];
             
             [imgDic setObject:@"2" forKey:@"type"];
             NSMutableDictionary* x = [[NSMutableDictionary alloc]init];
             [x setObject:[imageData objectForKey:@"suffix"] forKey:@"suffix"];
             [x setObject:[imageData objectForKey:@"name"] forKey:@"name"];
             [x setObject:[imageData objectForKey:@"id"] forKey:@"id"];
             [imgDic setObject:x forKey:@"image"];
             
             imgStr =[MJTransform dictionaryToJson:imgDic];

             NSMutableDictionary *parameter = [[NSMutableDictionary alloc]init];
             [parameter setObject:[NSNumber numberWithInteger:_contackId] forKey:@"receiverUserID"];
             [parameter setObject:imgStr forKey:@"content"];
             [parameter setObject:isApnsProduction forKey:@"isApnsProduction"];
             
             Chat *chat0 = (Chat *)[manager createMO:@"Chat"];
             chat0.content = imgStr;
             chat0.isSelf = @(1);
             NSString* strTime = [MJTimestampTool getCurrentTimestamp];
             chat0.time =  [NSString stringWithFormat:@"%f",[strTime doubleValue]*1000] ;
             
             [_capMessageArray addObject:chat0];
             [_tableViewRight reloadData];
             if ([_capMessageArray count] != 0) {
                 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_capMessageArray.count-1 inSection:0];
                 [_tableViewRight scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
             }
             

             if (_contackId== 0) {
                 NSLog(@"应该跳不进来吧");
                 return ;
             }
             
             AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
             session.responseSerializer = [AFHTTPResponseSerializer serializer];

             
             __block NSMutableDictionary *dataDic = [[NSMutableDictionary alloc]init];
             
             [session POST:CHAT_SEND parameters:parameter success:^(NSURLSessionDataTask *task, id responseObject) {
                 

                 NSDictionary *result = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];


                 if ([[result objectForKey:@"code"] isEqualToNumber:@(200)])
                 {
                     dataDic =[result objectForKey:@"data"];
                     [self addChatWithName:@"随便留得" andContent:imgStr andIsSelf:@(1) andImage:nil andContack_id:_contackId andTime:[dataDic objectForKey:@"currentTime"]];
                 }
                 else
                 {
                     NSLog(@"%@",[result objectForKey:@"msg"]);
                     UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"提示" message:[result objectForKey:@"msg"] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                     alert1.tag =102;
                     [alert1 show];
                     
                 }

             } failure:^(NSURLSessionDataTask *task, NSError *error) {
                 NSLog(@"图片上传失败");
             }];
             
         }

     }];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [_popctl.view setHidden:YES];
    }];
}



#pragma mark - 极光推送
- (void)dealloc {
    [self unObserveAllNotifications];
}

- (void)messageRefresh:(NSNotification *)notification{

    BOOL isChatView =NO;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;

    for (UIView *x in window.subviews) {
    
        if ([x isKindOfClass:[MJChatView class]]) {
            isChatView =[x isKindOfClass:[MJChatView class]];
            break;
        }
        
    }
    
    if (isChatView) {

        Contack* contack = (Contack*)notification.userInfo[@"contack"];
        Chat* chat = (Chat*)notification.userInfo[@"chat"];
//        manager=[MJDBManager shareInstanceWithCoreDataID:myID];
//        NSArray *obj = [[NSMutableArray alloc]initWithArray:[self quaryTable:@"Contack" andPredicate:nil]];
//        if ([obj count] !=0) {
//            _contackArray = [NSMutableArray arrayWithArray:obj];
//        }
        NSInteger z =_currentIndex;
        
        Contack* contack1 = _contackArray[z];//当前点击
        NSNumber* x= contack1.contack_id;
        NSNumber* y= contack.contack_id;
        NSLog(@"%@  %@",x,y);
        NSArray *obj = [[NSMutableArray alloc]initWithArray:[self quaryTable:@"Contack" andPredicate:nil]];
        if ([x isEqualToNumber:y])
        {
            [_contackArray removeAllObjects];
            //1月18新加的
            _contackArray = [NSMutableArray arrayWithArray:[self timeSort:obj]];
            [manager updateZeroManagerObj:contack1];
            [_capMessageArray addObject:chat];
            [_bottomView setHidden:NO];
            _currentIndex=0;
        }else
        {
            NSUInteger currentClick = [_contackArray indexOfObject:contack1];
            NSUInteger newMsg = [_contackArray indexOfObject:contack];
            
            
            [_contackArray removeAllObjects];
            //1月19新加的
            _contackArray = [NSMutableArray arrayWithArray:[self timeSort:obj]];
            
            
            
            if (currentClick>newMsg) {
                
                
            }else{
                
                _currentIndex=_currentIndex+1;
                NSLog(@"%li   +++++++1了",(long)_currentIndex);
                
//                [_tableViewLeft selectRowAtIndexPath:[NSIndexPath indexPathWithIndex:_currentIndex]  animated:YES scrollPosition:UITableViewScrollPositionBottom];

            }
            [MJContackModel getInstance].currentSelectIndex = _currentIndex;
//            [self tableView:_tableViewLeft didSelectRowAtIndexPath:[NSIndexPath indexPathWithIndex:_currentIndex]];
        }
        
        
        
        
        
        [_tableViewRight reloadData];
        [_tableViewLeft reloadData];
        if ([_capMessageArray count] != 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_capMessageArray.count-1 inSection:0];
            [_tableViewRight scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }

    }else
    {
        NSArray *obj = [[NSMutableArray alloc]initWithArray:[self quaryTable:@"Contack" andPredicate:nil]];
        _contackArray = [NSMutableArray arrayWithArray:[self timeSort:obj]];
        
        
    }
    
}

- (void)unObserveAllNotifications {
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];

    [defaultCenter removeObserver:self
                             name:kJPFNetworkDidReceiveMessageNotification
                           object:nil];

}


- (void)networkDidReceiveMessage:(NSNotification *)notification {
//    [self getUnreadContack];

}


- (void)nickNameChange:(NSNotification*)notification
{
    NSString *nickName=notification.userInfo[@"nickName"];
    _useLabel.text =[NSString stringWithFormat:@"     %@",nickName];
}


- (NSArray*)timeSort:(NSArray* )obj
{
    
    
    NSComparator cmptr = ^(id obj1, id obj2){
        Contack* contack1 = (Contack*)obj1;
        Contack* contack2 = (Contack*)obj2;
        if ([contack1.lasttime doubleValue]> [contack2.lasttime doubleValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        if ([contack1.lasttime doubleValue] < [contack2.lasttime doubleValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    NSArray *array = [[NSArray alloc]init];
    array =[obj sortedArrayUsingComparator:cmptr];
    

    
    return array;
}

@end
