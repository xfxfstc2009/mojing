//
//  MJDBManager.m
//  魔境家居
//
//  Created by Mac on 15/11/12.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJDBManager.h"
#import <CoreData/CoreData.h>
#import "Chat.h"
#import "Contack.h"
#import "AppDelegate.h"
#import "MJContackModel.h"
#define kCountDidChangeNofication @"kCountDidChangeNofication"


@implementation MJDBManager


+ (instancetype)shareInstanceWithCoreDataID:(NSString*)coreDataID
{
    static MJDBManager *instance = nil;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        
        instance = [[[self class] alloc]init];
        [instance openDBWithCoreDataID:coreDataID];
        instance.lastCoreData=coreDataID;
        
    });
    
    if (![instance.lastCoreData isEqualToString:coreDataID]) {
        instance.lastCoreData=coreDataID;
        [instance openDBWithCoreDataID:coreDataID];
    }
    
    return instance;
    
}

- (void)openDBWithCoreDataID:(NSString*)coreDataID{
    //01 加载数据模型文件
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    NSManagedObjectModel *dataModel = [[NSManagedObjectModel alloc]initWithContentsOfURL:url];
    
    //02 创建coordinator 协调器,并与 数据模型关联
    self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:dataModel];
    
    //03 打开或者创建数据库文件
    //>>01 文件路径
//     NSString *filePath = [NSHomeDirectory() stringByAppendingString:@"/Documents/test.sqlite"];
    NSString *filePath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.sqlite",coreDataID]];
    NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
    NSLog(@"%@",filePath);
    
    //>>02 添加 PersistentStore
    // 如果 data.sqlite 不存在，则创建并打开
    // 如果存在 则打开
    //通过 coordinator  添加一个 PersistentStore（与文件相关联）
    NSError *error = nil;
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:fileUrl options:options error:&error]) {
        NSLog(@"打开数据库失败  %@",error);
    }
    
    
    if (error) {
        NSLog(@"打开数据库失败  %@",error);
    }else{
        //NSLog(@"打开数据库成功");
    }
    
    
    //04 创建 context并与NSPersistentStoreCoordinator 关联
    self.context = [[NSManagedObjectContext alloc] init];
    self.context.persistentStoreCoordinator = _persistentStoreCoordinator;
    
}


//创建MO对象
- (NSManagedObject *)createMO:(NSString *)entityName
{
    if (entityName.length == 0) {
        return nil;
    }
    NSManagedObject *mo = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:self.context];
    
    return mo;
    
}

//1.添加对象
- (void)saveManagerObj:(NSManagedObject *)mo
{
#warning 添加联系人
    if (mo == nil) {
        return;
    }
    [self.context insertObject:mo];
    NSError *error = nil;
    [self.context save:&error];
    if (error) {
        NSLog(@"保存失败 %@",error);
    }else {
        NSLog(@"保存成功");

    }
    

}

//2.查询
- (NSArray *)query:(NSString *)entifyName
         predicate:(NSPredicate *)predicate
{
    if (entifyName.length == 0) {
        return nil;
    }
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entifyName];
    //
    if (predicate !=nil) {
        request.predicate = predicate;
    }
    

    
    NSArray *arrayOrder = [_context executeFetchRequest:request error:nil];
    
    NSArray *array =[[NSArray alloc]init];
    if ([arrayOrder count] >0)
    {
        
        if ([entifyName isEqualToString:@"Contack"])
        {
            NSMutableArray * arr = [[NSMutableArray alloc]init];
            for (Contack* contack in arrayOrder) {
                [arr insertObject:contack atIndex:0];
            }
            array =arr;
        }else if([entifyName isEqualToString:@"Chat"])
        {
            array = arrayOrder;
        }
        
    }
    
    return array;

    
}

//3.修改mo 插入聊天
- (void)updateManagerObj:(NSManagedObject *)mo
{
    
    
    if (mo == nil) {
        return;
    }
    

    NSError *error = nil;
    [self.context save:&error];
    if (error) {
        NSLog(@"修改失败 %@",error);
    }else {
        //NSLog(@"修改成功");
    }

   
}

- (void)updateZeroManagerObj:(NSManagedObject *)mo
{
    
    
    if (mo == nil) {
        return;
    }
    
    NSArray *obj = [[NSMutableArray alloc]initWithArray:[self query:@"Contack" predicate:nil]];
                    
    for (Contack* contack in obj)
    {
        contack.isSelect =@(0);
    }
                    
                    
    Contack* contack1 = (Contack*)mo;
//    AppDelegate* delegate = MJAppDelegate;
    [MJContackModel getInstance].unreadMsgCount=[MJContackModel getInstance].unreadMsgCount-[contack1.msgcount integerValue];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kCountDidChangeNofication object:nil];
    contack1.msgcount = @(0);
    contack1.isSelect =@(1);
    
    

    
    
    NSError *error = nil;
    [self.context save:&error];
    if (error) {
        NSLog(@"归零修改失败 %@",error);
    }else {
        //NSLog(@"归零修改成功");
    }
    
}


//4.删除
- (void)removeManagerObj:(NSManagedObject *)mo
{
    if (mo == nil) {
        return;
    }
#warning 删除联系人
    [self.context deleteObject:mo];
    [self.context save:nil];
    
}

@end
