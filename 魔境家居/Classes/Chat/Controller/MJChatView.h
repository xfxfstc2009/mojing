//
//  MJChatView.h
//  魔境家居
//
//  Created by Mac on 15/11/16.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FaceScrollView.h"
#import "Contack.h"

typedef void (^closeClick)();


@interface MJChatView : UIView<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,FaceViewDelegate,UIAlertViewDelegate >


@property (nonatomic, strong) UITableView *tableViewLeft;
@property (nonatomic, strong) UITableView *tableViewRight;
@property (nonatomic, strong) UIView *bottomView;
@property (nonatomic, copy) closeClick close;
@property (nonatomic, copy)NSMutableArray* contackArray;
@property (nonatomic, assign)NSInteger currentIndex;
/** 聊天覆盖视图*/
@property (nonatomic,strong) UIButton *chatCoverView;
@property (nonatomic, strong) UIPopoverController *cameraPop;

//相册
@property (nonatomic, copy)UIViewController *popctl;

- (Chat*)addChatWithName:(NSString *)name andContent:(NSString *)content andIsSelf:(NSNumber *)isSelf  andImage:(NSString *)image andContack_id:(NSInteger)contack_id andTime:(NSNumber*)time;
+ (Chat*)addChatWithName:(NSString *)name andContent:(NSString *)content andIsSelf:(NSNumber *)isSelf  andImage:(NSString *)image andContack_id:(NSInteger)contack_id andTime:(NSNumber*)time;
+ (instancetype)shareInstance;
- (void)showWithIndexSelect:(NSUInteger)index;
+ (void)deleteInstance;

@end
