//
//  MJDBManager.h
//  魔境家居
//
//  Created by Mac on 15/11/12.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface MJDBManager : NSObject
@property(nonatomic,strong)NSManagedObjectContext *context;
@property(nonatomic,strong)NSString *lastCoreData;
@property(nonatomic,strong)NSPersistentStoreCoordinator *persistentStoreCoordinator;


+ (instancetype)shareInstanceWithCoreDataID:(NSString*)coreDataID;

//创建MO对象
- (NSManagedObject *)createMO:(NSString *)entityName;

//1.添加对象
- (void)saveManagerObj:(NSManagedObject *)mo;

//2.查询
- (NSArray *)query:(NSString *)entifyName
         predicate:(NSPredicate *)predicate;

//查询联系人
//- (NSArray *)query2:(NSString *)entifyName
//         predicate:(NSPredicate *)predicate;
//3.修改mo
- (void)updateManagerObj:(NSManagedObject *)mo;

//5.归零角标
- (void)updateZeroManagerObj:(NSManagedObject *)mo;

//4.删除
- (void)removeManagerObj:(NSManagedObject *)mo;
@end
