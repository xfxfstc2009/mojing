//
//  MJMatchUserDataModel.h
//  魔境家居
//
//  Created by mojing on 15/12/1.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJMatchUserDataModel : NSObject

@property (nonatomic,assign) NSInteger  idNum ;//90029,
@property (nonatomic,copy) NSString * email ;//  ,
@property (nonatomic,copy) NSString * mobile ;//  ,
@property (nonatomic,copy) NSString * avatar ;// http;////static.duc.cn//user/default.jpg ,
@property (nonatomic,copy) NSString * avatarPic ;// http;////static.duc.cn//user/default.jpg ,
@property (nonatomic,copy) NSString * failCount ;//0,
@property (nonatomic,copy) NSString * name ;// 商家设计师测试 ,
@property (nonatomic,copy) NSString * type ;// 2 ,
@property (nonatomic,copy) NSString * uid ;// 90029 ,
@property (nonatomic,assign) NSInteger  systemTag ;//0,
@property (nonatomic,assign) NSInteger  userTag ;//0,
@property (nonatomic,assign) NSInteger  status ;//0,
@property (nonatomic,copy) NSString * realName ;// 商家设计师测试 ,
@property (nonatomic,copy) NSString * serviceMobile ;// 400-853-0588 ,
@property (nonatomic,copy) NSString * serviceQQ ;// 2946546811 ,
@property (nonatomic,assign) NSInteger  identityTag ;//6

@end
