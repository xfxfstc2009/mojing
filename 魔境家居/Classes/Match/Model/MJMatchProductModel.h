//
//  MJMatchProductModel.h
//  魔境家居
//
//  Created by mojing on 15/11/16.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJMatchTopDataModel.h"
#import "MJModifiedDateModel.h"
#import "MJCreatedDateModel.h"
#import "MJTopDataModel.h"
#import "MJCollocationDataModel.h"
@interface MJMatchProductModel : NSObject

@property (nonatomic,copy) NSString *isDeleted ;// 0,
@property (nonatomic,copy) NSString *collocationImageSuffix ;// png,
@property (nonatomic,copy) NSString *createdDateTime ;// 1448618841000,
@property (nonatomic,strong) NSArray *productList ;// [
@property (nonatomic,assign) NSInteger userID ;// 155588,
@property (nonatomic,strong) MJTopDataModel *topDate ;//
@property (nonatomic,assign) NSInteger bgImageID ;// 30,
@property (nonatomic,copy) NSString *name ;// 客厅1,
@property (nonatomic,strong) MJModifiedDateModel *modifiedDate ;//
@property (nonatomic,copy) NSString *spaceTypeName ;// 客厅,
@property (nonatomic,copy) NSString *isTop ;// 0,
@property (nonatomic,assign) NSInteger idNum ;// 1,
@property (nonatomic,copy) NSString *isFavorited ;// ,
@property (nonatomic,assign) NSInteger productIDs ;// 3,
@property (nonatomic,assign) NSInteger spaceTypeID ;// 1,
@property (nonatomic,strong) MJCreatedDateModel *createdDate ;//
@property (nonatomic,copy) NSString *bgImageSuffix ;// png,
@property (nonatomic,copy) NSString *bgImageName ;// 1448617153902,
@property (nonatomic,assign) NSInteger favoriteCount ;// 0,
@property (nonatomic,copy) NSString *collocationImageName ;// 1448618916342,
@property (nonatomic,assign) NSInteger collocationImageID ;// 31,
@property (nonatomic,strong) MJCollocationDataModel *collocationData ;// {"v":"1.0","data":{"state":2,"quality":1,"rotation":[0.49662241339683533,-0.86790412664413452,0.010421603918075562,0.1460610032081604,0.095401018857955933,0.98466479778289795,-0.85558891296386719,-0.48748442530632019,0.17414523661136627],"timestampInSeconds":109754.78125,"sensor":"GPSCompassSensorSource","cosName":"OrientationFloorTracker","translation":{"x":-13.54640007019043,"y":-1280.0650634765625,"z":-226.38865661621094},"viewport":{"x":-10000,"y":-10000},"additionalValues":"","trackingTimeMs":0.019958330318331718,"timeElapsed":1293918.375,"coordinateSystemID":1,"rect":{"width":1136,"height":640}},"models":[{"rotation":[1,0,0,0,-3.5762786865234375e-07,-1,0,1,-3.5762786865234375e-07],"id":3,"scale":{"x":1.5448249578475952,"y":1.5448249578475952,"z":1.5448249578475952},"name":"3_0","location":{"x":368,"y":530},"translation":{"x":2441.833984375,"y":2123.255615234375,"z":0}}]},
@property (nonatomic,copy) NSString *modifiedDateTime ;// 1448618841000,
@property (nonatomic,copy) NSString *desc ;//
@property (nonatomic,copy) NSString *imageSuffix;
@property (nonatomic,assign) NSInteger imageID;
@property (nonatomic,copy) NSString *imageName;
/*是否选中打钩*/
@property (nonatomic,assign,getter=isSelected) BOOL selected;
/*是否需要显示白色背景*/
@property (nonatomic,assign,getter=isShowWhiteColor) BOOL showWhiteColor;
@property(nonatomic,assign)CGSize imageSize;

@end
