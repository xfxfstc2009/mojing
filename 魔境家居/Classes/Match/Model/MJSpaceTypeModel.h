//
//  MJSpaceTypeModel.h
//  魔境家居
//
//  Created by mojing on 15/11/13.
//  Copyright © 2015年 mojing. All rights reserved.
//  空间类型/商品类型共用

#import <Foundation/Foundation.h>

@interface MJSpaceTypeModel : NSObject

@property (nonatomic,assign) NSInteger idNum;
@property (nonatomic,assign) NSInteger parentID;
@property (nonatomic,copy) NSString *isDeleted;
@property (nonatomic,copy) NSString *name;

@end
