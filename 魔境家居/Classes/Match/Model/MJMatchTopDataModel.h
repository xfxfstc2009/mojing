//
//  MJMatchTopDataModel.h
//  魔境家居
//
//  Created by mojing on 15/11/16.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJMatchTopDataModel : NSObject

@property (nonatomic,copy) NSNumber *date;
@property (nonatomic,copy) NSNumber *day;
@property (nonatomic,copy) NSNumber *hours;
@property (nonatomic,copy) NSNumber *minutes;
@property (nonatomic,copy) NSNumber *month;
@property (nonatomic,copy) NSNumber *nanos;
@property (nonatomic,copy) NSNumber *seconds;
@property (nonatomic,copy) NSString *time;
@property (nonatomic,copy) NSString *timezoneOffset;
@property (nonatomic,copy) NSNumber *year;

@end
