//
//  MJMatchProductModel.m
//  魔境家居
//
//  Created by mojing on 15/11/16.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJMatchProductModel.h"
#import "MJExtension.h"
#import "MJProductModel.h"
@implementation MJMatchProductModel

+ (NSDictionary *)replacedKeyFromPropertyName
{
    
    return @{@"idNum" : @"id",@"desc" : @"description"};
    
}

- (NSDictionary *)objectClassInArray
{
    return @{@"productList":[MJProductModel class]};
}


@end
