//
//  MJMatchWorksListModel.m
//  魔境家居
//
//  Created by mojing on 15/11/16.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJMatchWorksListModel.h"
#import "MJExtension.h"
#import "MJMatchProductModel.h"
@implementation MJMatchWorksListModel

- (NSDictionary *)objectClassInArray
{
    return @{@"worksList":[MJMatchProductModel class],@"sceneList":[MJMatchProductModel class]};
}

@end
