//
//  MJMatchSelectView.h
//  魔境家居
//
//  Created by mojing on 15/11/5.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^selectBtn)(NSInteger index,UIButton *button);
@interface MJMatchSelectView : UIView

@property (nonatomic,copy) selectBtn cb;
@property (nonatomic,strong) UIButton *selectBtn;
@property (weak, nonatomic) IBOutlet UIButton *allMatchBtn;

@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
+ (instancetype)select;
- (void)setBtnTitleWithArray:(NSArray *)arr;
- (void)setBtnNotSelected;
@end
