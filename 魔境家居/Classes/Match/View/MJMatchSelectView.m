//
//  MJMatchSelectView.m
//  魔境家居
//
//  Created by mojing on 15/11/5.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJMatchSelectView.h"

@implementation MJMatchSelectView
{

    __weak IBOutlet UIButton *_secondBtn;
    __weak IBOutlet UIButton *_thirdBtn;
    __weak IBOutlet UIButton *_fourthBtn;
    __weak IBOutlet UIButton *_fifthBtn;
    __weak IBOutlet UIButton *_sixthBtn;
    __weak IBOutlet UIButton *_seventhBtn;
    __weak IBOutlet UIButton *_eighthBtn;
    
}
+ (instancetype)select
{
    return [[[NSBundle mainBundle] loadNibNamed:@"MJMatchSelectView" owner:nil options:nil] firstObject];
}

- (void)setBtnNotSelected
{
    _firstBtn.selected = NO;
    _secondBtn.selected = NO;
    _thirdBtn.selected = NO;
    _fourthBtn.selected = NO;
    _fifthBtn.selected = NO;
    _sixthBtn.selected = NO;
    _seventhBtn.selected = NO;
    _eighthBtn.selected = NO;
}

- (IBAction)buttonClick:(UIButton *)button {
    
    self.allMatchBtn.selected = NO;
    self.selectBtn.selected = NO;
    button.selected = YES;
    self.selectBtn = button;
    
    
    if (_cb) {
        _cb(button.tag,button);
    }
}

#pragma mark - 设置按钮名称
- (void)setBtnTitleWithArray:(NSArray *)arr
{
    [_firstBtn setTitle:arr[0] forState:UIControlStateNormal];
    [_secondBtn setTitle:arr[1] forState:UIControlStateNormal];
    [_thirdBtn setTitle:arr[2] forState:UIControlStateNormal];
    [_fourthBtn setTitle:arr[3] forState:UIControlStateNormal];
    [_fifthBtn setTitle:arr[4] forState:UIControlStateNormal];
    [_sixthBtn setTitle:arr[5] forState:UIControlStateNormal];
    [_seventhBtn setTitle:arr[6] forState:UIControlStateNormal];
    [_eighthBtn setTitle:arr[7] forState:UIControlStateNormal];
}

@end
