//
//  MJAllMatchCell.h
//  魔境家居
//
//  Created by mojing on 15/11/5.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJMatchProductModel.h"
typedef void (^matchWorksEdit)();
@interface MJAllMatchCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView *bigFurnitureImageView;

@property (strong, nonatomic) UILabel *matchNameLabel;

@property (strong, nonatomic) UIButton *editButton;

@property (strong, nonatomic) MJMatchProductModel *model;

@property (copy, nonatomic) matchWorksEdit worksEdit;
@end
