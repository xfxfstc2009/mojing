//
//  MJAllMatchCell.m
//  魔境家居
//
//  Created by mojing on 15/11/5.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJAllMatchCell.h"
#import "UIImageView+WebCache.h"
#import "MJMatchUserDataModel.h"
@implementation MJAllMatchCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self uiConfig];
    }
    return self;
}

-(void)uiConfig
{
    CGFloat width = 328;
    CGFloat height = 315;
    _bigFurnitureImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width,height-50)];
    
    _matchNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, height-50, width, 50)];
    _matchNameLabel.textAlignment = NSTextAlignmentCenter;
    _matchNameLabel.font = [UIFont systemFontOfSize:20];
    
    _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _editButton.frame = CGRectMake(width - 40, 0, 40, 40);
    [_editButton setImage:[UIImage imageNamed:@"match_edit_delete"] forState:UIControlStateNormal];
    [_editButton addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    _editButton.hidden = YES;
    
    [self.contentView addSubview:_editButton];
    [self.contentView addSubview:_bigFurnitureImageView];
    [self.contentView addSubview:_matchNameLabel];
    
    [self.contentView bringSubviewToFront:self.editButton];
}

- (void)click
{
   // NSLog(@"删除");
    // 设置模型
    //self.model.selected = !self.deal.isChecking;
    // 直接修改状态
    
    if(_worksEdit)
    {
        _worksEdit();
    }
}

- (void)awakeFromNib {
    // Initialization code
}

@end
