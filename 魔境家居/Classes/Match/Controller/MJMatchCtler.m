//
//  MJMatchCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/27.
//  Copyright © 2015年 mojing. All rights reserved.
//
#import "MJMatchCtler.h"
#import "MJCategory.h"
#import "UIView+Frame.h"
#import "MJPlistTool.h"
#import "MJConmon.h"
#import "MJAllMatchCell.h"
#import "MJMatchSelectView.h"
#import "MJSearchBar.h"
#import "MJNetworkTool.h"
#import "MJSpaceTypeModel.h"
#import "MJMatchModel.h"
#import "MJMatchProductModel.h"
#import "MJExtension.h"
#import "MJRefresh.h"
#import "MJProductGroupDetailsCtler.h"
#import "GMDCircleLoader.h"
#import "MJMatchUserDataModel.h"
#import "MJUIClassTool.h"
#import "UIBarButtonItem+Extension.h"
#import <CHTCollectionViewWaterfallLayout.h>
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
@interface MJMatchCtler ()<UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,CHTCollectionViewDelegateWaterfallLayout>
@property (nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic,strong) MJMatchSelectView *selectView;
/**
 *  空间类型列表数组
 */
@property (nonatomic,strong) NSMutableArray *spaceTypeArr;
/** 搭配作品列表数组 */
@property (nonatomic,strong) NSMutableArray *matchProductArr;
/** 搭配作品详情参数数组 */
@property (nonatomic,strong) NSMutableArray *matchDetailParaArr;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
/* 用户头像数组*/
@property (nonatomic, strong) NSMutableArray *userPhotoArr;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
/* 转圈*/
@property (nonatomic, strong) GMDCircleLoader *loader;
/* tempBtn*/
@property (nonatomic, strong) UIButton *tempBtn;
/* 搭配作品编辑按钮*/
@property (strong, nonatomic) UIButton *hotListEditButton;
@end

@implementation MJMatchCtler

static NSString * const reuseIdentifier = @"Cell";

- (UIImageView *)noDataView
{
    if (_noDataView == nil) {
        
        _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
        _noDataView.image = [UIImage imageNamed:@"no_data"];
        _noDataView.center = self.view.center;
        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(-20, 147, 200, 30) title:@"没有搜到相关的搭配作品" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:18];
        [_noDataView addSubview:lb];
     //   [self.view addSubview:_noDataView];
        
    }
    return _noDataView;
}

- (NSMutableArray *)userPhotoArr
{
    if (_userPhotoArr == nil) {
        _userPhotoArr = [NSMutableArray array];
    }
    return _userPhotoArr;
}

#pragma mark - 空间类型选择
- (MJMatchSelectView *)selectView
{
    if (_selectView == nil) {
        _selectView = [MJMatchSelectView select];
        _selectView.frame = CGRectMake(0, 64, Screen_Width, 44);
        _selectView.allMatchBtn.selected = YES;
        MJSearchBar *searchBar = [[MJSearchBar alloc] initWithFrame:CGRectMake(Screen_Width - 278, 5, 258, 34)];
        searchBar.placeholder = @"搜索搭配作品";
        searchBar.returnKeyType = UIReturnKeySearch;
        searchBar.delegate = self;
        [_selectView addSubview:searchBar];
        //空间类型按钮点击
        __weak MJMatchCtler *ctl = self;
        _selectView.cb = ^(NSInteger index,UIButton *button){

            NSDictionary *headerDict = @{@"spaceTypeID" : [NSString stringWithFormat:@"%ld",index-10+1],@"pageNumber" : [NSString stringWithFormat:@"%ld",ctl.pageNumber],@"pageSize" : @6,@"type" : @1};
            
            switch (index-10) {
                case 0:
                {
                    [ctl addRefreshHeader:headerDict];
                    //  [ctl btnDone:button dict:headerDict];
                    
                    break;
                }
                case 1:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 2:
                {
                    [ctl addRefreshHeader:headerDict];
                    break;
                }
                case 3:
                {
                    [ctl addRefreshHeader:headerDict];
                    break;
                }
                case 4:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 5:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 6:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 7:
                {
                    [ctl addRefreshHeader:headerDict];
                    
                    break;
                }
                case 8:
                {
                    NSDictionary *headerDict1 = @{@"spaceTypeID" : @0,@"pageSize" : @6,@"pageNumber" : [NSString stringWithFormat:@"%ld",ctl.pageNumber],@"type" : @1};
                    [ctl addRefreshHeader:headerDict1];
                    
                    break;
                }
                default:
                    break;
            }
            
        };
    }
    return _selectView;
}

- (NSMutableArray *)matchDetailParaArr
{
    if (_matchDetailParaArr == nil) {
        _matchDetailParaArr = [NSMutableArray array];
    }
    return _matchDetailParaArr;
}

- (NSMutableArray *)matchProductArr
{
    if (_matchProductArr == nil) {
        _matchProductArr = [NSMutableArray array];
        
    }
    return _matchProductArr;
}

- (NSMutableArray *)spaceTypeArr
{
    if (_spaceTypeArr == nil) {
        _spaceTypeArr = [NSMutableArray array];
    }
    return _spaceTypeArr;
}

- (UICollectionView *)collectionView
{
    if (!_collectionView) {
    
        CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
        
        layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
      //  layout.minimumLineSpacing = inset;
        layout.columnCount = 3;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 108, Screen_Width, Screen_Height-108) collectionViewLayout:layout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.alwaysBounceVertical = YES;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.backgroundColor = RGB(220, 220, 220);
        _collectionView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
        [_collectionView registerClass:[MJAllMatchCell class] forCellWithReuseIdentifier:reuseIdentifier];
        [self.view addSubview:_collectionView];
    }
    return _collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    
    self.pageNumber = 1;
    [self setTitle:@"搭配列表页"];
    self.view.backgroundColor = RGB(246, 246, 246);
    [self.view addSubview:self.selectView];
   // _collectionViewFrame = self.collectionView.frame;

    //搭配编辑按钮
    self.hotListEditButton = [MJUIClassTool createBtnWithFrame:CGRectMake(Screen_Width-268-100, 7, 75, 30) title:@"" image:[UIImage imageNamed:@"store_edit_button"] target:self action:@selector(selectEditBtnClick:)];
    
    _hotListEditButton.tag = 23;
    [self.selectView addSubview:self.hotListEditButton];
    
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    
    __weak MJMatchCtler *ctl = self;
    
    //获取空间类型列表
    [netTool getMoJingURL:MJSpaceTypeList parameters:nil success:^(id obj) {
        [ctl.spaceTypeArr addObjectsFromArray:obj];
        NSMutableArray *btnNameArr = [NSMutableArray array];
        for (MJSpaceTypeModel *m in ctl.spaceTypeArr) {
            NSString *name = m.name;
            [btnNameArr addObject:name];
        }
        [ctl.selectView setBtnTitleWithArray:btnNameArr];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
   
    NSDictionary *headerDict = @{@"spaceTypeID" : @0,@"pageSize" : @6,@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber],@"type" : @1};
    
    [self addRefreshHeader:headerDict];
}


#pragma mark - 返回
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
    ;
    self.noteView = nil;
}

#pragma mark - 编辑按钮
- (void)selectEditBtnClick:(UIButton *)button
{
    button.selected = !button.isSelected;
    if (button.selected) {
        [_hotListEditButton setImage:[UIImage imageNamed:@"store_quit_edit"] forState:UIControlStateSelected];
        // 进入编辑状态
        for (MJMatchProductModel *model in self.matchProductArr) {
            model.selected = YES;
        }
        
    } else {
        for (MJMatchProductModel *model in self.matchProductArr) {
            model.selected = NO;
        }
       
    }
     [self.collectionView reloadData];
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.collectionView.frame];
    _circulateBackgroungView.backgroundColor = RGB(246, 246, 246);
    [self.view addSubview:self.circulateBackgroungView];
    self.selectView.userInteractionEnabled = NO;
   self.loader = [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    self.selectView.userInteractionEnabled = YES;
    [GMDCircleLoader hideFromView:self.view animated:YES];
    [self.circulateBackgroungView removeFromSuperview];
}

#pragma mark - 添加上下拉加载数据
- (void)addRefreshHeader:(NSDictionary *)dict
{
    __weak MJMatchCtler *ctl = self;
    [self loadingData];
    //下拉刷新
    self.collectionView.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        [ctl loadNewData:dict];
    }];
    // 马上进入刷新状态
    [self.collectionView.mj_header beginRefreshing];
    
}

#pragma mark - 下拉加载数据
- (void)loadNewData:(NSDictionary *)dict
{
    self.pageNumber = 1;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJMatchCtler *ctl = self;
    [self.noDataView removeFromSuperview];
    [self.matchProductArr removeAllObjects];
    [self.userPhotoArr removeAllObjects];
    
    //获取搭配作品列表
    if ([self isConnectToNet]) {
    
            [netTool getMoJingURL:MJWorksList parameters:dict success:^(id obj) {
              //  NSLog(@"%@",dict);
                MJMatchModel *model = obj;
                NSMutableArray *arr= [NSMutableArray array];
                arr = [MJMatchProductModel mj_objectArrayWithKeyValuesArray:model.data.worksList];
               // for (int i = 0; i < 2; i++) {
                    [ctl.matchProductArr addObjectsFromArray:arr];
               // }
                self.pageCount = model.data.pageCount;
                if (ctl.matchProductArr.count == 0) {
                    [ctl.view addSubview:ctl.noDataView];
                }
                
                if (ctl.matchProductArr.count >= 6) {
                    // 上拉刷新
                        ctl.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                            // 进入刷新状态后会自动调用这个block
                            [ctl loadMoreData:dict];
                        }];
                        // 默认先隐藏footer
                        ctl.collectionView.mj_footer.hidden = YES;
                }else{
                    ctl.collectionView.mj_footer = nil;
                }
                
                [ctl.collectionView reloadData];
            //    [ctl stopDisplayLink];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_header endRefreshing];
                [ctl.view endEditing:YES];
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.collectionView.mj_header endRefreshing];
                [ctl stopCircleLoader];
            }];
       
       }else
    {
        [ctl stopCircleLoader];
        [ctl.collectionView.mj_header endRefreshing];

    }
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.collectionView];
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    _pageNumber++;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJMatchCtler *ctl = self;
    //[self.matchProductArr removeAllObjects];
    NSString *str = [dict objectForKey:@"spaceTypeID"];
    NSDictionary *moreDict = @{@"spaceTypeID" : str,@"pageNumber" : [NSString stringWithFormat:@"%ld",_pageNumber],@"pageSize" : @6,@"type" : @1};
    //获取搭配作品列表
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            
            [netTool getMoJingURL:MJWorksList parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                MJMatchModel *model = obj;
                NSMutableArray *arr= [NSMutableArray array];
                arr = [MJMatchProductModel mj_objectArrayWithKeyValuesArray:model.data.worksList];
                for (MJMatchProductModel *m in arr) {
                    NSString *userID = [NSString stringWithFormat:@"%ld",m.userID];
                    NSDictionary *para = @{@"ids":userID};
                    [netTool getMoJingURL:MJUsersDetailData parameters:para success:^(id obj) {
                        //用户信息
                        MJMatchUserDataModel *userModel = obj[0];
                        NSURL *url = [NSURL URLWithString:userModel.avatar];
                        [self.userPhotoArr addObject:url];
                        
                    } failure:^(NSError *error) {
                    }];
                }
                [ctl.matchProductArr addObjectsFromArray:arr];
                [ctl.collectionView reloadData];
                [ctl stopCircleLoader];
                [ctl.collectionView.mj_footer endRefreshing];
             //   [ctl stopDisplayLink];
                
                for (MJMatchProductModel *m in ctl.matchProductArr) {
                    NSDictionary *dict = @{@"id" : [NSString stringWithFormat:@"%ld",m.idNum]};
                    [ctl.matchDetailParaArr addObject:dict];
                    [netTool getMoJingURL:MJWorksDetail parameters:dict success:^(id obj) {
                        
                    } failure:^(NSError *error) {
                        NSLog(@"%@",error);
                        [ctl.collectionView.mj_footer endRefreshing];
                        [ctl stopCircleLoader];
                    //    [ctl stopDisplayLink];
                    }];
                }
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.collectionView.mj_footer endRefreshing];
                [ctl stopCircleLoader];
             //   [ctl stopDisplayLink];
            }];
        }else
        {
            // 变为没有更多数据的状态
            [ctl.collectionView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        [self stopCircleLoader];
        [self.collectionView.mj_footer endRefreshing];
         self.collectionView.mj_footer.hidden = YES;
    }

}

#pragma mark  <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return self.matchProductArr.count;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MJAllMatchCell *cell = (MJAllMatchCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    // Configure the cell
    if (self.matchProductArr.count > 0) {
        MJMatchProductModel *model = self.matchProductArr[indexPath.row];
        cell.model = model;
        NSURL *bgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@upload/%@/%@/%@_300_300.%@",mainURL,model.collocationImageSuffix,model.collocationImageName,model.collocationImageName,model.collocationImageSuffix]];
        [cell.bigFurnitureImageView  sd_setImageWithURL:bgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            if (image) {
//                if (!CGSizeEqualToSize(model.imageSize, image.size)) {
//                    model.imageSize = image.size;
//                    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
//                }
//            }
        }];
        cell.matchNameLabel.text = model.name;
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.editButton.hidden = !model.isSelected;
        __weak MJMatchCtler *ctl = self;
        
        NSMutableArray *tempArr = [NSMutableArray array];
        cell.worksEdit = ^{
            for (MJMatchProductModel *m in self.matchProductArr) {
                if (m.idNum == cell.model.idNum) {
                    [tempArr addObject:m];
                }
            }
            
            // 删除模型
            [ctl.matchProductArr removeObjectsInArray:tempArr];
            
            if (ctl.matchProductArr.count < 4) {
                ctl.collectionView.mj_footer = nil;
            }

            //掉接口 保存到服务器
            MJNetworkTool *net = [MJNetworkTool shareInstance];
            NSDictionary *para = @{@"id" : @(model.idNum)};
            [net getMoJingURL:MJWorksDelete parameters:para success:^(id obj) {
                
                [ctl.collectionView reloadData];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:ctl.view animated:YES];
                hud.removeFromSuperViewOnHide =YES;
                hud.mode = MBProgressHUDModeDeterminate;
                // hud.animationType = MBProgressHUDAnimationZoom;
                hud.labelText = NSLocalizedString(@"删除成功", nil);
                // hud.minSize = CGSizeMake(100, 100);
                hud.layer.cornerRadius = 50;
                [hud hide:YES afterDelay:1.5];
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
        };
        
    }
    
    return cell;

}

#pragma mark  <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MJProductGroupDetailsCtler *detail = [[MJProductGroupDetailsCtler alloc] init];
    MJMatchProductModel *model = self.matchProductArr[indexPath.row];
    detail.idNum = model.idNum;
    if (!model.selected) {
       [self.navigationController pushViewController:detail animated:YES];
    }
    
   // NSLog(@"详情点击%ld",indexPath.row);
}

#pragma mark - <UITextFieldDelegate>
//文本框开始输入的时候
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
}// became first responder

//结束输入的时候
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{

    NSDictionary *footerDict = @{@"searchString" : textField.text,@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber],@"pageSize" : @6,@"type" : @1};
    if (textField.text.length > 0) {
        
        [self addRefreshHeader:footerDict];
        [self.selectView setBtnNotSelected];
         self.selectView.allMatchBtn.selected = YES;
        
    }else{
        [self.view endEditing:YES];
    }
    return YES;
}

#pragma mark - CHTCollectionViewDelegateWaterfallLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
   // MJMatchProductModel *model = [self.matchProductArr objectAtIndex:indexPath.row];
//    if (!CGSizeEqualToSize(model.imageSize, CGSizeZero)) {
//        return model.imageSize;
//    }
    return CGSizeMake(328, 315);
}


@end
