#import <Foundation/Foundation.h>

#ifdef  DEBUG

#define isApnsProduction @"0"

#pragma mark - 主URL
#define mainURL @"http://172.16.35.23:9292/"
//http://172.16.35.23:9292/
#pragma mark - 文件操作
/** 文件上传*/
#define fileUploadURL @"http://172.16.35.23:9292/file/upload"
/*	MultipartFile file 必需。文件流。*/

/** 文件删除*/
#define fileDeleteURL @"http://172.16.35.23:9292/file/delete"
/* long id 必需。文件ID。*/


#pragma mark - 账号管理
/* 供应商保存*/
#define supplierSaveURL @"http://172.16.35.23:9292/supplier/save"
/* 参数查看文档*/

/* 经销商保存*/
#define dealerSaveURL @"http://172.16.35.23:9292/dealer/save"
/* 参数查看文档*/

/* 导购员保存*/
#define guideSaveURL @"http://172.16.35.23:9292/guide/save"
/* 参数查看文档*/

/* 用户删除*/
#define userDeleteURL @"http://172.16.35.23:9292/user/delete"
/* 参数查看文档*/

/* 获取用户详情*/
#define userDetailURL @"http://172.16.35.23:9292/user/getDetail"
/* 参数查看文档*/

/* 获取用户列表*/
#define userListURL @"http://172.16.35.23:9292/user/getList"
/* 参数查看文档*/

/* 获取多个用户id*/
#define userByIDListURL @"http://172.16.35.23:9292/user/getUserByIDList"
/* 参数查看文档*/

/* 获取多个用户信息*/
#define multiUserListURL @"http://api.duc.cn/user/getUserByQuery"
/* 参数传用户id  ids=90028&ids=90029*/

/* 用户登录*/
#define userLoginURL @"http://172.16.35.23:9292/user/login"
//http://172.16.35.23:9292/

/* 用户注销*/
#define userLogoffURL @"http://172.16.35.23:9292/user/logoff"

/* 修改用户昵称*/
#define userNickNameUpdateURL @"http://172.16.35.23:9292/user/updateNickName"

/* 经销商列表*/
#define dealerListURL @"http://172.16.35.23:9292/dealer/getDealerListBySupplierID"

/* 二次登录*/
#define Login_DUC @"http://login.duc.cn/"

/* 头像上传*/
#define SETTING_AVATAR @"http://i.duc.cn/setting/setUserInfo"

/* 密码上传*/
#define SETTING_UPDATE_PWD @"http://login.duc.cn/setting/setForgetPwd"

/* 设置用户信息*/
#define SETTING_USERINFO @"http://i.duc.cn/setting/getUserInfo"



#pragma mark - App封面
/* 封面保存*/
#define coverSaveURL @"http://172.16.35.23:9292/cover/save"

/* 封面删除*/
#define coverDeleteURL @"http://172.16.35.23:9292/cover/delete"

/* 封面详情*/
#define coverDetailURL @"http://172.16.35.23:9292/cover/getDetail"

/* 封面列表*/
#define coverListURL @"http://172.16.35.23:9292/cover/getList"


#pragma mark - 空间类型
/* 空间类型保存*/
#define spaceTypeSaveURL @"http://172.16.35.23:9292/spaceType/save"

/* 空间类型删除*/
#define spaceTypeDeleteURL @"http://172.16.35.23:9292/spaceType/delete"

/** 空间类型列表*/
#define spaceTypeListURL @"http://172.16.35.23:9292/spaceType/getList"


#pragma mark - 商品类型
/* 商品类型保存*/
#define productTypeSaveURL @"http://172.16.35.23:9292/productType/save"

/* 商品类型删除*/
#define productTypeDeleteURL @"http://172.16.35.23:9292/productType/delete"

/* 商品类型列表*/
#define productTypeListURL @"http://172.16.35.23:9292/productType/getList"


#pragma mark - 品牌
/* 品牌保存*/
#define brandSaveURL @"http://172.16.35.23:9292/brand/save"

/* 品牌删除*/
#define brandDeleteURL @"http://172.16.35.23:9292/brand/delete"

/* 获取品牌列表*/
#define brandListURL @"http://172.16.35.23:9292/brand/getPageList"

/* 获取品牌完整信息*/
#define brandDetailURL @"http://172.16.35.23:9292/brandDetail/getDetail"

/* 获取品牌经销商列表*/
#define brandDealerListURL @"http://172.16.35.23:9292/brand/getDealerList"

/* 获取热销信息保存*/
#define brandDetailSpreadSaveURL @"http://172.16.35.23:9292/brandDetail/spread/save"

#pragma mark - 场景
/* 场景保存*/
#define sceneSaveURL @"http://172.16.35.23:9292/scene/save"

/* 场景删除*/
#define sceneDeleteURL @"http://172.16.35.23:9292/scene/delete"

/* 获取场景列表*/
#define sceneListURL @"http://172.16.35.23:9292/scene/getList"

/* 获取场景详细信息*/
#define sceneDetailURL @"http://172.16.35.23:9292/scene/getDetail"


#pragma mark - 商品
/** 商品保存*/
#define productSaveURL @"http://172.16.35.23:9292/product/save"

/** 商品删除*/
#define productDeleteURL @"http://172.16.35.23:9292/product/delete"

/** 置新/取消置新*/
#define productNewURL @"http://172.16.35.23:9292/product/update/new"

/** 获取商品列表*/
#define productListURL @"http://172.16.35.23:9292/product/getList"

/** 获取商品详细信息*/
#define productDetailURL @"http://172.16.35.23:9292/product/getDetail"


#pragma mark - 收藏
/** 收藏/取消收藏资源*/
#define resourceFavoriteURL @"http://172.16.35.23:9292/favorite/update"


#pragma mark - 搭配
/** 搭配作品保存*/
#define worksSaveURL @"http://172.16.35.23:9292/works/save"

/** 搭配作品删除*/
#define worksDeleteURL @"http://172.16.35.23:9292/works/delete"

/** 获取搭配作品列表*/
#define worksListURL @"http://172.16.35.23:9292/works/getList"

/** 获取搭配作品详细信息*/
#define worksDrtailURL @"http://172.16.35.23:9292/works/getDetail"


#pragma mark - 客户
/** 获取客户列表*/
#define customerListURL @"http://172.16.35.23:9292/browse/brand/getList"

/** 获取客户浏览详情*/
#define customerBrowseDetailURL @"http://172.16.35.23:9292/browse/brand/getDetail"

/** 保存客户进/离店信息*/
#define browseShopSaveURL @"http://172.16.35.23:9292/browse/brand/save"

/** 保存客户商品浏览信息*/
#define browseProductSavelURL @"http://172.16.35.23:9292/browse/product/save"


#pragma mark - 品牌完整信息
/** 品牌相册保存*/
#define brandDetailAlbumSaveURL @"http://172.16.35.23:9292/brandDetail/album/save"

/** 品牌视频保存*/
#define brandDetailVideoSaveURL @"http://172.16.35.23:9292/brandDetail/video/save"

/** 品牌简介保存*/
#define brandDetailIntroduceSaveURL @"http://172.16.35.23:9292/brandDetail/introduce/save"

/** 品牌360全景保存*/
#define brandDetailPanoramaSaveURL @"http://172.16.35.23:9292/brandDetail/panorama/save"

/** 品牌案例保存*/
#define brandDetailCaseSaveURL @"http://172.16.35.23:9292/brandDetail/case/save"

/** 品牌推广数据保存*/
#define brandDetailSpreadSaveURL @"http://172.16.35.23:9292/brandDetail/spread/save"

/** 获取品牌完整信息*/
#define brandDetailDetailURL @"http://172.16.35.23:9292/brandDetail/getDetail"


#pragma mark - 统计
/** 商品总体统计*/
#define productTotalStatisticsURL @"http://172.16.35.23:9292/statistics/product/getTotal"
/** 商品统计列表*/
#define productStatisticsListURL @"http://172.16.35.23:9292/statistics/product/getList"

/** 顾客统计列表*/
#define customerStatisticsListURL @"http://172.16.35.23:9292/statistics/customer/getList"


#pragma mark - 多个用户头像等数据解析
#define usersDetailDataURL @"http://api.duc.cn/user/getUserByQuery"
//http://i.duc.cn/setting/getUserInfo?

#pragma mark - 二维码解析地址

#define mjQrcodeURL @"http://172.16.35.23:9292/app/download/index.html"
//http://172.16.35.23:9292/app/download/index.html?productID=3

/**
 *  productID=****；                 商品
 *  userID=****&type=1&brandID=****；供应商
 *  userID=****&type=2&brandID=****；经销商
 *  userID=****&type=3&brandID=****；导购员
 *  userID=****&type=0；             普通用户
 */
/**
 *  供应商 经销商 导购员二维码图片地址格式为: upload/png/qr/{userID}_qr_1.png
 *  普通用户二维码图片地址格式为: upload/png/qr/{userID}_qr_1.png
 *  商品的二维码图片地址格式为: upload/png/qr/{productID}_qr_1.png
 */

#pragma mark - 聊天

#define UPLOAD_IMAGE @"http://172.16.35.23:9292/file/upload"
#define CHAT_SEND @"http://172.16.35.23:9292/chat/send"
#define CHAT_UNREAD @"http://172.16.35.23:9292/chat/unread/getList"
#define CHAT_CONTACK @"http://api.duc.cn/user/getUserByQuery"

#pragma mark - 版本更新
#define mjVersionURL @"http://itunes.apple.com/lookup?id=1073852753"
#define mjVersionUpdateURL @"https://itunes.apple.com/us/app/mo-jing-jia-ju-zhu-shou-shang/id1073852753?l=zh&ls=1&mt=8"

#pragma mark - 选材搭配
#define planeWorksSaveURL @"http://172.16.35.23:9292/planeWorks/save"

#define planeWorksGetListURL @"http://172.16.35.23:9292/planeWorks/getList"

#else

#define isApnsProduction @"1"
#pragma mark - 主URL
#define mainURL @"http://mj.duc.cn/"
//http://mj.duc.cn/
#pragma mark - 文件操作
/** 文件上传*/
#define fileUploadURL @"http://mj.duc.cn/file/upload"
/*	MultipartFile file 必需。文件流。*/

/** 文件删除*/
#define fileDeleteURL @"http://mj.duc.cn/file/delete"
/* long id 必需。文件ID。*/


#pragma mark - 账号管理
/* 供应商保存*/
#define supplierSaveURL @"http://mj.duc.cn/supplier/save"
/* 参数查看文档*/

/* 经销商保存*/
#define dealerSaveURL @"http://mj.duc.cn/dealer/save"
/* 参数查看文档*/

/* 导购员保存*/
#define guideSaveURL @"http://mj.duc.cn/guide/save"
/* 参数查看文档*/

/* 用户删除*/
#define userDeleteURL @"http://mj.duc.cn/user/delete"
/* 参数查看文档*/

/* 获取用户详情*/
#define userDetailURL @"http://mj.duc.cn/user/getDetail"
/* 参数查看文档*/

/* 获取用户列表*/
#define userListURL @"http://mj.duc.cn/user/getList"
/* 参数查看文档*/

/* 获取多个用户id*/
#define userByIDListURL @"http://mj.duc.cn/user/getUserByIDList"

/* 获取多个用户信息*/
#define multiUserListURL @"http://api.duc.cn/user/getUserByQuery"
/* 参数传用户id  ids=90028&ids=90029*/

/* 用户登录*/
#define userLoginURL @"http://mj.duc.cn/user/login"
//http://mj.duc.cn/

/* 用户注销*/
#define userLogoffURL @"http://mj.duc.cn/user/logoff"

/* 修改用户昵称*/
#define userNickNameUpdateURL @"http://mj.duc.cn/user/updateNickName"

#define dealerListURL @"http://mj.duc.cn/dealer/getDealerListBySupplierID"

/* 二次登录*/
#define Login_DUC @"http://login.duc.cn/"

/* 头像上传*/
#define SETTING_AVATAR @"http://i.duc.cn/setting/setUserInfo"

/* 密码上传*/
#define SETTING_UPDATE_PWD @"http://login.duc.cn/setting/setForgetPwd"

/* 设置用户信息*/
#define SETTING_USERINFO @"http://i.duc.cn/setting/getUserInfo"



#pragma mark - App封面
/* 封面保存*/
#define coverSaveURL @"http://mj.duc.cn/cover/save"

/* 封面删除*/
#define coverDeleteURL @"http://mj.duc.cn/cover/delete"

/* 封面详情*/
#define coverDetailURL @"http://mj.duc.cn/cover/getDetail"

/* 封面列表*/
#define coverListURL @"http://mj.duc.cn/cover/getList"


#pragma mark - 空间类型
/* 空间类型保存*/
#define spaceTypeSaveURL @"http://mj.duc.cn/spaceType/save"

/* 空间类型删除*/
#define spaceTypeDeleteURL @"http://mj.duc.cn/spaceType/delete"

/** 空间类型列表*/
#define spaceTypeListURL @"http://mj.duc.cn/spaceType/getList"


#pragma mark - 商品类型
/* 商品类型保存*/
#define productTypeSaveURL @"http://mj.duc.cn/productType/save"

/* 商品类型删除*/
#define productTypeDeleteURL @"http://mj.duc.cn/productType/delete"

/* 商品类型列表*/
#define productTypeListURL @"http://mj.duc.cn/productType/getList"


#pragma mark - 品牌
/* 品牌保存*/
#define brandSaveURL @"http://mj.duc.cn/brand/save"

/* 品牌删除*/
#define brandDeleteURL @"http://mj.duc.cn/brand/delete"

/* 获取品牌列表*/
#define brandListURL @"http://mj.duc.cn/brand/getPageList"

/* 获取品牌完整信息*/
#define brandDetailURL @"http://mj.duc.cn/brandDetail/getDetail"

/* 获取品牌经销商列表*/
#define brandDealerListURL @"http://mj.duc.cn/brand/getDealerList"

/* 获取热销信息保存*/
#define brandDetailSpreadSaveURL @"http://mj.duc.cn/brandDetail/spread/save"


#pragma mark - 场景
/* 场景保存*/
#define sceneSaveURL @"http://mj.duc.cn/scene/save"

/* 场景删除*/
#define sceneDeleteURL @"http://mj.duc.cn/scene/delete"

/* 获取场景列表*/
#define sceneListURL @"http://mj.duc.cn/scene/getList"

/* 获取场景详细信息*/
#define sceneDetailURL @"http://mj.duc.cn/scene/getDetail"


#pragma mark - 商品
/** 商品保存*/
#define productSaveURL @"http://mj.duc.cn/product/save"

/** 商品删除*/
#define productDeleteURL @"http://mj.duc.cn/product/delete"

/** 商品置新*/
#define productNewURL @"http://mj.duc.cn/product/update/new"

/** 获取商品列表*/
#define productListURL @"http://mj.duc.cn/product/getList"

/** 获取商品详细信息*/
#define productDetailURL @"http://mj.duc.cn/product/getDetail"


#pragma mark - 收藏
/** 收藏/取消收藏资源*/
#define resourceFavoriteURL @"http://mj.duc.cn/favorite/update"


#pragma mark - 搭配
/** 搭配作品保存*/
#define worksSaveURL @"http://mj.duc.cn/works/save"

/** 搭配作品删除*/
#define worksDeleteURL @"http://mj.duc.cn/works/delete"

/** 获取搭配作品列表*/
#define worksListURL @"http://mj.duc.cn/works/getList"

/** 获取搭配作品详细信息*/
#define worksDrtailURL @"http://mj.duc.cn/works/getDetail"


#pragma mark - 客户
/** 获取客户列表*/
#define customerListURL @"http://mj.duc.cn/browse/brand/getList"

/** 获取客户浏览详情*/
#define customerBrowseDetailURL @"http://mj.duc.cn/browse/brand/getDetail"

/** 保存客户进/离店信息*/
#define browseShopSaveURL @"http://mj.duc.cn/browse/brand/save"

/** 保存客户商品浏览信息*/
#define browseProductSavelURL @"http://mj.duc.cn/browse/product/save"


#pragma mark - 品牌完整信息
/** 品牌相册保存*/
#define brandDetailAlbumSaveURL @"http://mj.duc.cn/brandDetail/album/save"

/** 品牌视频保存*/
#define brandDetailVideoSaveURL @"http://mj.duc.cn/brandDetail/video/save"

/** 品牌简介保存*/
#define brandDetailIntroduceSaveURL @"http://mj.duc.cn/brandDetail/introduce/save"

/** 品牌360全景保存*/
#define brandDetailPanoramaSaveURL @"http://mj.duc.cn/brandDetail/panorama/save"

/** 品牌案例保存*/
#define brandDetailCaseSaveURL @"http://mj.duc.cn/brandDetail/case/save"

/** 品牌推广数据保存*/
#define brandDetailSpreadSaveURL @"http://mj.duc.cn/brandDetail/spread/save"

/** 获取品牌完整信息*/
#define brandDetailDetailURL @"http://mj.duc.cn/brandDetail/getDetail"


#pragma mark - 统计
/** 商品总体统计*/
#define productTotalStatisticsURL @"http://mj.duc.cn/statistics/product/getTotal"
/** 商品统计列表*/
#define productStatisticsListURL @"http://mj.duc.cn/statistics/product/getList"

/** 顾客统计列表*/
#define customerStatisticsListURL @"http://mj.duc.cn/statistics/customer/getList"


#pragma mark - 多个用户头像等数据解析
#define usersDetailDataURL @"http://api.duc.cn/user/getUserByQuery"
//http://i.duc.cn/setting/getUserInfo?

#pragma mark - 二维码解析地址

#define mjQrcodeURL @"http://mj.duc.cn/app/download/index.html"
//http://mojing.duc.cn/app/download/index.html?productID=3

/**
 *  productID=****；                 商品
 *  userID=****&type=1&brandID=****；供应商
 *  userID=****&type=2&brandID=****；经销商
 *  userID=****&type=3&brandID=****；导购员
 *  userID=****&type=0；             普通用户
 */
/**
 *  供应商 经销商 导购员二维码图片地址格式为: upload/png/qr/{userID}_qr_1.png
 *  普通用户二维码图片地址格式为: upload/png/qr/{userID}_qr_1.png
 *  商品的二维码图片地址格式为: upload/png/qr/{productID}_qr_1.png
 */

#pragma mark - 聊天

#define UPLOAD_IMAGE @"http://mj.duc.cn/file/upload"
#define CHAT_SEND @"http://mj.duc.cn/chat/send"
#define CHAT_UNREAD @"http://mj.duc.cn/chat/unread/getList"
#define CHAT_CONTACK @"http://api.duc.cn/user/getUserByQuery"

#pragma mark - 版本跟新
#define mjVersionURL @"http://itunes.apple.com/lookup?id=1073852753"
#define mjVersionUpdateURL @"https://itunes.apple.com/us/app/mo-jing-jia-ju-zhu-shou-shang/id1073852753?l=zh&ls=1&mt=8"

#pragma mark - 选材搭配
#define planeWorksSaveURL @"http://mj.duc.cn/planeWorks/save"

#define planeWorksGetListURL @"http://172.16.35.23:9292/planeWorks/getList"

#endif



