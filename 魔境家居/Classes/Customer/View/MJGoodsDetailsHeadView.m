//
//  MJGoodsDetailsHeadView.m
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJGoodsDetailsHeadView.h"

@implementation MJGoodsDetailsHeadView

+ (instancetype)head
{
    return [[[NSBundle mainBundle] loadNibNamed:@"MJGoodsDetailsHeadView" owner:nil options:nil] firstObject];
}

@end
