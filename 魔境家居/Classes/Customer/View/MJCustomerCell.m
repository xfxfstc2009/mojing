//
//  MJCustomerCell.m
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCustomerCell.h"
#import "MJTimestampTool.h"
#import "MJNetworkTool.h"
#import "MJMatchUserDataModel.h"
#import "MJExtension.h"
#import "MJCustomerDetailDataModel.h"
@implementation MJCustomerCell
{
    __weak IBOutlet UILabel *_userIDLabel;
    __weak IBOutlet UILabel *_nickNameLabel;
    __weak IBOutlet UILabel *_phoneNumLabel;
    __weak IBOutlet UILabel *_startDateLabel;
    __weak IBOutlet UILabel *_endDateLabel;
}
- (void)setModel:(MJCustomerModel *)model
{
    _model =model;
    if (model.orderCount < 1000) {
        _userIDLabel.text = [NSString stringWithFormat:@"%03ld",(long)model.orderCount];
    }else
    {
        _userIDLabel.text = [NSString stringWithFormat:@"%ld",(long)model.orderCount];
    }
    
    NSString *startTime = [MJTimestampTool timestampChangesStandarTime:model.enterDateTime/1000];
    _startDateLabel.text = startTime;
    

    if (![model.isPhoneAllowed isEqualToString:@"0"])
    {
       
        _phoneNumLabel.text = model.customerPhoneNumber;
    
    }else
    {
        _phoneNumLabel.text = @"待客户确认";
    }
   
    _nickNameLabel.text = model.customerNickName;
    
    if (model.leaveDateTime == 0) {
        _endDateLabel.text = @"";
    }else
    {
        NSString *endTime = [MJTimestampTool timestampChangesStandarTime:model.leaveDateTime/1000];
        _endDateLabel.text = endTime;
    }
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
