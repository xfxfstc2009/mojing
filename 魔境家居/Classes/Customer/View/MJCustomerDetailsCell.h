//
//  MJCustomerDetailsCell.h
//  魔境家居
//
//  Created by mojing on 15/11/5.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJCustomerScanDetailsModel.h"

@interface MJCustomerDetailsCell : UITableViewCell

@property (nonatomic,strong) MJCustomerScanDetailsModel *model;

- (void)setTimeText:(NSString *)text product:(NSString *)product;
@end
