//
//  MJCustomerDetailsHeadView.m
//  魔境家居
//
//  Created by mojing on 15/11/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCustomerDetailsHeadView.h"

@implementation MJCustomerDetailsHeadView
{
    __weak IBOutlet UILabel *_nickNameLabel;
    __weak IBOutlet UILabel *_phoneNumLabel;
    /// 修改资料按钮
    __weak IBOutlet UIButton *_organizingDataButton;
    
}

+ (id)customerDetailsHead
{
    return [[[NSBundle mainBundle] loadNibNamed:@"MJCustomerDetailsHeadView" owner:nil options:nil] firstObject];
}

- (void)setNickName:(NSString *)nickName phoneNum:(NSString *)phoneNum
{
    _nickNameLabel.text = nickName;
    _phoneNumLabel.text = phoneNum;
}

- (void)addTarget:(id)target action:(SEL)action
{
    [_organizingDataButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}


@end
