//
//  MJCustomerHeadView.m
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCustomerHeadView.h"

@implementation MJCustomerHeadView

+ (instancetype)head
{
    return [[[NSBundle mainBundle] loadNibNamed:@"MJCustomerHeadView" owner:nil options:nil] firstObject];
}
@end
