//
//  MJCustomerDetailsCell.m
//  魔境家居
//
//  Created by mojing on 15/11/5.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCustomerDetailsCell.h"
#import "UIView+Frame.h"
#import "MJTimestampTool.h"
#import "MJExtension.h"
#import "MJCustomerRemarkModel.h"
@interface MJCustomerDetailsCell()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remarkLabelLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *productWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remarkWidth;


@end
@implementation MJCustomerDetailsCell
{
    __weak IBOutlet UILabel *_timeLabel;
    __weak IBOutlet UILabel *_productNameLabel;
    __weak IBOutlet UILabel *_remarkLabel;
    __weak IBOutlet UIImageView *_firstStarView;
    __weak IBOutlet UIImageView *_secondStarView;
    __weak IBOutlet UIImageView *_thirdStarView;
    __weak IBOutlet UIImageView *_fourthStarView;
    __weak IBOutlet UIImageView *_fifthStarView;
    __weak IBOutlet UIImageView *_playBtn;
    
}

- (void)setModel:(MJCustomerScanDetailsModel *)model
{
    _model = model;
    NSString *browseDateTime = [MJTimestampTool timestampChangesStandarTime:model.browseDateTime/1000];
    _timeLabel.text = browseDateTime;
    _productNameLabel.text = model.productName;
    NSDictionary *dict1 = model.mj_keyValues;
    NSDictionary *remarkDict = [dict1 objectForKey:@"remark"];
    MJCustomerRemarkModel *remark = [MJCustomerRemarkModel mj_objectWithKeyValues:remarkDict];
    NSString *type = remark.type;
   
    if ([type isEqualToString:@"1"]) {
         _remarkLabel.text = remark.data;
    }else if ([type isEqualToString:@"2"])
    {
        _remarkLabel.text =[NSString stringWithFormat:@"语音"];
        _playBtn.alpha = 1;
    }
   
    
   // NSLog(@"%lf %lf",self.productWidth.constant,self.remarkWidth.constant);
        switch (model.degree) {
            case 0:
            {
                _firstStarView.hidden = YES;
                _secondStarView.hidden = YES;
                _thirdStarView.hidden = YES;
                _fourthStarView.hidden= YES;
                _fifthStarView.hidden = YES;
                break;
            }
            case 1:
            {
                _firstStarView.hidden = NO;
                _secondStarView.hidden = YES;
                _thirdStarView.hidden = YES;
                _fourthStarView.hidden= YES;
                _fifthStarView.hidden = YES;
                break;
            }
            case 2:
            {
                _firstStarView.hidden = NO;
                _secondStarView.hidden = NO;
                _thirdStarView.hidden = YES;
                _fourthStarView.hidden= YES;
                _fifthStarView.hidden = YES;
                break;
            }
            case 3:
            {
                _firstStarView.hidden = NO;
                _secondStarView.hidden = NO;
                _thirdStarView.hidden = NO;
                _fourthStarView.hidden= YES;
                _fifthStarView.hidden = YES;
                break;
            }
            case 4:
            {
                _firstStarView.hidden = NO;
                _secondStarView.hidden = NO;
                _thirdStarView.hidden = NO;
                _fourthStarView.hidden= NO;
                _fifthStarView.hidden = YES;
                break;
            }
            case 5:
            {
                _firstStarView.hidden = NO;
                _secondStarView.hidden = NO;
                _thirdStarView.hidden = NO;
                _fourthStarView.hidden= NO;
                _fifthStarView.hidden = NO;
                break;
            }
            default:
            {
                
            }
                break;
        }
    
}

- (void)setTimeText:(NSString *)text product:(NSString *)product
{
    _firstStarView.hidden = YES;
    _secondStarView.hidden = YES;
    _thirdStarView.hidden = YES;
    _fourthStarView.hidden= YES;
    _fifthStarView.hidden = YES;
    _timeLabel.text = text;
    _productNameLabel.text = product;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
