//
//  MJCustomerDetailsHeadView.h
//  魔境家居
//
//  Created by mojing on 15/11/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJCustomerDetailsHeadView : UIView
+ (id)customerDetailsHead;
- (void)setNickName:(NSString *)nickName phoneNum:(NSString *)phoneNum;
- (void)addTarget:(id)target action:(nonnull SEL)action;
@end
