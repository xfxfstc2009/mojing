//
//  MJGoodsDetailsHeadView.h
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJGoodsDetailsHeadView : UIView

+ (instancetype)head;
@end
