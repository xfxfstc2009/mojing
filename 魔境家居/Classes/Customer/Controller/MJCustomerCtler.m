//
//  MJCustomerCtler.m
//  魔境家居
//
//  Created by mojing on 15/10/28.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCustomerCtler.h"
#import "MJCustomerCell.h"
#import "MJCustomerHeadView.h"
#import "MJCustomerDetailsCtler.h"
#import "MJRefresh.h"
#import "UIBarButtonItem+Extension.h"
#import "MJNetworkTool.h"
#import "GMDCircleLoader.h"
#import "MJCustomerListModel.h"
#import "MJCustomerDataModel.h"
#import "MJCustomerModel.h"
#import "MJExtension.h"
#import "MJUIClassTool.h"
@interface MJCustomerCtler ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *tableBackView;
@property (nonatomic, strong) MJCustomerHeadView *headView;
@property (nonatomic, strong) NSMutableArray *dataArr;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
@property (nonatomic, strong) GMDCircleLoader *loader;
@end

@implementation MJCustomerCtler

static NSString * const reuseIdentifier = @"Cell";

- (UIImageView *)noDataView
{
    if (_noDataView == nil) {
        
        _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
        _noDataView.image = [UIImage imageNamed:@"no_data"];
        _noDataView.center = self.view.center;
        
        UILabel *lb = [MJUIClassTool createLbWithFrame:CGRectMake(17, 147, 150, 30) title:@"暂无顾客进店!" aliment:NSTextAlignmentLeft color:[UIColor blackColor] size:20];
        [_noDataView addSubview:lb];
        //   [self.view addSubview:_noDataView];
        
    }
    return _noDataView;
}

- (NSMutableArray *)dataArr
{
    if (_dataArr == nil) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UIView *)tableBackView
{
    if (_tableBackView == nil) {
        _tableBackView = [[UIView alloc] initWithFrame:CGRectMake(10, 74, Screen_Width-20, Screen_Height-84)];
        _tableBackView.backgroundColor = RGB(242, 245, 248);
        _tableBackView.alpha = 0.8;
        [_tableBackView addSubview:self.tableView];
        [self.view addSubview:_tableBackView];
    }
    return _tableBackView;
}

- (MJCustomerHeadView *)headView
{
    if (_headView == nil) {
        _headView = [MJCustomerHeadView head];
    }
    return _headView;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 1004, 684) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.headView;
        _tableView.separatorStyle = NO;
        _tableView.rowHeight = 50;
        [_tableView registerNib:[UINib nibWithNibName:@"MJCustomerCell" bundle:nil] forCellReuseIdentifier:reuseIdentifier];
    }
    return _tableView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
  
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //customerListReloadData
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.pageNumber = 1;
    self.navigationItem.leftBarButtonItem = back;
    [self setTitle:@"我的顾客"];
    UIImageView *backgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, Screen_Width, Screen_Height-64)];
    backgroundView.image = [UIImage imageNamed:@"customer_background"];
    [self.view addSubview:backgroundView];
 //   [self.view addSubview:self.tableBackView];
    [self.view bringSubviewToFront:self.tableBackView];
    
    NSDictionary *headerDict = @{@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber],@"pageSize" : @13};
    
    [self addRefreshHeader:headerDict];
    
    [MJNotificationCenter addObserver:self selector:@selector(customerListReloadData) name:@"customerListReloadData" object:nil];
}

- (void)customerListReloadData
{
    NSDictionary *headerDict = @{@"pageNumber" : [NSString stringWithFormat:@"%d",1],@"pageSize" : @13};
    
    [self addRefreshHeader:headerDict];
    [self.tableView reloadData];
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.view.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_circulateBackgroungView];
    self.loader = [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    [GMDCircleLoader hideFromView:self.circulateBackgroungView animated:YES];
    [self.circulateBackgroungView removeFromSuperview];
}


#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}


#pragma mark - 添加下拉刷新
- (void)addRefreshHeader:(NSDictionary *)dict
{
    __weak MJCustomerCtler *ctl = self;
    [self loadingData];
    //下拉刷新
    self.tableView.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        [ctl loadNewData:dict];
    }];
    // 马上进入刷新状态
    [self.tableView.mj_header beginRefreshing];
}


#pragma mark - 下拉加载数据
- (void)loadNewData:(NSDictionary *)dict
{
    _pageNumber = 1;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJCustomerCtler *ctl = self;
    [self.noDataView removeFromSuperview];
    [self.dataArr removeAllObjects];
    //获取客户列表
    if ([self isConnectToNet]) {
            [netTool getMoJingURL:MJCustomerList parameters:dict success:^(id obj) {
                MJCustomerDataModel *dataModel = obj;
                NSMutableArray *arr = [NSMutableArray array];
                arr =  [MJCustomerModel mj_objectArrayWithKeyValuesArray: dataModel.brandBrowseList];
                ctl.pageCount = dataModel.pageCount;
                NSInteger i = 1;
                for (MJCustomerModel *m in arr) {
                    m.orderCount = i;
                    [ctl.dataArr addObject:m];
                    i++;
                }
                
              //  NSLog(@"%ld",ctl.dataArr.count);
                if (ctl.dataArr.count == 0) {
                    [ctl.view addSubview:ctl.noDataView];
                }
                
                if (ctl.dataArr.count >= 13) {
                    // 上拉刷新
                    ctl.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        // 进入刷新状态后会自动调用这个block
                        [ctl loadMoreData:dict];
                    }];
                    
                    // 默认先隐藏footer
                    self.tableView.mj_footer.hidden = YES;
                }

                [ctl.tableView reloadData];
                [ctl stopCircleLoader];
                [ctl.tableView.mj_header endRefreshing];
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.tableView.mj_header endRefreshing];
                [ctl stopCircleLoader];
            }];
      
    }else
    {
        [ctl stopCircleLoader];
        [ctl.tableView.mj_header endRefreshing];
    }
}


#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    _pageNumber++;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    NSDictionary *moreDict = @{@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber],@"pageSize" : @13};
    __weak MJCustomerCtler *ctl = self;
    //获取客户列表
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            [netTool getMoJingURL:MJCustomerList parameters:moreDict success:^(id obj) {
                MJCustomerDataModel *dataModel = obj;
                NSMutableArray *arr = [NSMutableArray array];
                arr =  [MJCustomerModel mj_objectArrayWithKeyValuesArray: dataModel.brandBrowseList];
             //   NSLog(@"%@",moreDict);
                NSInteger i = ctl.dataArr.count+1;
                for (MJCustomerModel *m1 in arr) {
                    m1.orderCount = i;
                    [ctl.dataArr addObject:m1];
                    i++;
                }
             //   NSLog(@"%ld",ctl.dataArr.count);
                [ctl.tableView reloadData];
                [ctl stopCircleLoader];
                [ctl.tableView.mj_footer endRefreshing];
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.tableView.mj_footer endRefreshing];
                [ctl stopCircleLoader];
            }];
        }else
        {
            // 变为没有更多数据的状态
            [ctl.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        
        
    }else
    {
        [ctl stopCircleLoader];
        [ctl.tableView.mj_footer endRefreshing];
        self.tableView.mj_footer.hidden = YES;
    }
    
}

#pragma mark - UITableViewDataSource,UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MJCustomerCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(self.dataArr.count>0)
    {
        cell.model = self.dataArr[indexPath.row];
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MJCustomerDetailsCtler *details = [[MJCustomerDetailsCtler alloc] init];
    details.model = self.dataArr[indexPath.row];
   // NSLog(@"%i",details.model.idNum);
    [self.navigationController pushViewController:details animated:YES];
     self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width, 64);
}

- (void)back
{
    [self stopCircleLoader];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [MJNotificationCenter removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self stopCircleLoader];
}

@end
