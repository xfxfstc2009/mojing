//
//  MJGoodsDetailsCtler.m
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJGoodsDetailsCtler.h"
#import "UIBarButtonItem+Extension.h"
#import "MJStatisticsDetailsCell.h"
#import "MJGoodsDetailsHeadView.h"
#import "MJSearchBar.h"
#import "MJRefresh.h"
#import "GMDCircleLoader.h"
#import "MJNetworkTool.h"
#import "AppDelegate.h"
#import "MJAllGoodsStatisticsListModel.h"
#import "MJExtension.h"
#import "MJGoodsStatisticsListModel.h"
#import "MJProductDetailsCtler.h"
@interface MJGoodsDetailsCtler ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) MJGoodsDetailsHeadView *headView;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;
/* 没有数据时的view*/
@property (nonatomic, strong) UIImageView *noDataView;
@end

@implementation MJGoodsDetailsCtler

static NSString * const reuseIdentifier = @"Cell";

- (UIImageView *)noDataView
{
    if (_noDataView == nil) {
        
        _noDataView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 147, 147)];
        _noDataView.image = [UIImage imageNamed:@"no_data"];
        _noDataView.center = self.view.center;
        
        //   [self.view addSubview:_noDataView];
        
    }
    return _noDataView;
}


- (NSMutableArray *)dataArr
{
    if (_dataArr == nil) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (MJGoodsDetailsHeadView *)headView
{
    if (_headView == nil) {
        _headView = [MJGoodsDetailsHeadView head];
    }
    return _headView;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64,Screen_Width, Screen_Height-64) style:UITableViewStylePlain];
        _tableView.separatorStyle = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.headView;
        _tableView.rowHeight = 50;
        [_tableView registerNib:[UINib nibWithNibName:@"MJStatisticsDetailsCell" bundle:nil] forCellReuseIdentifier:reuseIdentifier];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
//    MJSearchBar *searchBar = [[MJSearchBar alloc] initWithFrame:CGRectMake(0, 0, Screen_Width-400, 35)];
//    searchBar.placeholder = @"搜商品";
//    searchBar.keyboardType = UIKeyboardTypeWebSearch;

}

- (void)initUI
{
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.pageNumber = 1;
    self.title = @"商品统计";
    
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    //statisticsType pageSize pageNumber brandID
    
    NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
    NSDictionary *dict = @{@"brandID" : brandID,@"statisticsType" : @1,@"pageSize" : @13,@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber]};
    [self addRefreshHeader:dict];
}

#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.tableView];
}


#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:self.view.frame];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_circulateBackgroungView];
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    [self.circulateBackgroungView removeFromSuperview];
    [GMDCircleLoader hideFromView:self.view animated:YES];
}


#pragma mark - 添加上下拉刷新
- (void)addRefreshHeader:(NSDictionary *)dict
{
    __weak MJGoodsDetailsCtler *ctl = self;
    [self loadingData];
    //下拉刷新
    self.tableView.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        [ctl loadNewData:dict];
    }];
    // 马上进入刷新状态
    [self.tableView.mj_header beginRefreshing];
    
}

#pragma mark - 下拉加载数据
- (void)loadNewData:(NSDictionary *)dict
{
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJGoodsDetailsCtler *ctl = self;
    [self.noDataView removeFromSuperview];
    [self.dataArr removeAllObjects];
    //获取商品统计
    if ([self isConnectToNet]) {
                  [netTool getMoJingURL:MJProductStatisticsList parameters:dict success:^(id obj) {
              //  NSLog(@"%@",dict);
                MJAllGoodsStatisticsListModel *model2 = [MJAllGoodsStatisticsListModel mj_objectWithKeyValues:obj];
                self.pageCount = model2.data.pageCount;
                NSMutableArray *arr = [NSMutableArray array];
                      
                arr = [MJGoodsStatisticsModel mj_objectArrayWithKeyValuesArray:model2.data.productList];
             
                  //  [ctl.dataArr addObjectsFromArray:arr];
                NSInteger i = 1;
                      for (MJGoodsStatisticsModel *m in arr) {
                          m.orderCount = i;
                          [ctl.dataArr addObject:m];
                          i++;
                      }
                    
                if (ctl.dataArr.count == 0) {
                    [ctl.view addSubview:ctl.noDataView];
                }
                if (ctl.dataArr.count >= 13) {
                    // 上拉刷新
                    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                        // 进入刷新状态后会自动调用这个block
                        [ctl loadMoreData:dict];
                    }];
                    
                    // 默认先隐藏footer
                    self.tableView.mj_footer.hidden = YES;
                }
                
                [ctl.tableView reloadData];
                [ctl stopCircleLoader];
                [ctl.tableView.mj_header endRefreshing];
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.tableView.mj_header endRefreshing];
                [ctl stopCircleLoader];
            }];
    }else
    {
        [ctl stopCircleLoader];
        [ctl.tableView.mj_header endRefreshing];
    }
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    self.pageNumber++;
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJGoodsDetailsCtler *ctl = self;
    NSString *brandID = [MJUserDefaults objectForKey:@"brandID"];
    NSString *statisticsType = [dict objectForKey:@"statisticsType"];
    NSString *pageSize = [dict objectForKey:@"pageSize"];
    NSDictionary *moreDict = @{@"brandID" : brandID,@"statisticsType" : statisticsType,@"pageSize" : pageSize,@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber]};
    //获取搭配作品列表
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            
            [netTool getMoJingURL:MJProductStatisticsList parameters:moreDict success:^(id obj) {
                //NSLog(@"%@",moreDict);
                MJAllGoodsStatisticsListModel *model2 = [MJAllGoodsStatisticsListModel mj_objectWithKeyValues:obj];
                NSMutableArray *arr = [NSMutableArray array];
                arr = [MJGoodsStatisticsModel mj_objectArrayWithKeyValuesArray:model2.data.productList];
                NSInteger i = ctl.dataArr.count;
                for (MJGoodsStatisticsModel *m1 in arr) {
                    m1.orderCount = i;
                    [ctl.dataArr addObject:m1];
                    i++;
                }
                
                [ctl.tableView reloadData];
                [ctl stopCircleLoader];
                [ctl.tableView.mj_footer endRefreshing];
                
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.tableView.mj_footer endRefreshing];
                [ctl stopCircleLoader];
            }];

        }else
        {
            // 变为没有更多数据的状态
            [ctl.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        [ctl stopCircleLoader];
        [ctl.tableView.mj_footer endRefreshing];
        self.tableView.mj_footer.hidden = YES;
    }
    
}

- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - <UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MJStatisticsDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (self.dataArr.count>0) {
        MJGoodsStatisticsModel *model = self.dataArr[indexPath.row];
       // model.orderCount = indexPath.row + 1;
//        if (model.orderCount) {
//            
//        }
        cell.model = model;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     MJGoodsStatisticsModel *model = self.dataArr[indexPath.row];
    MJProductDetailsCtler *detail = [[MJProductDetailsCtler alloc] init];
    detail.idNum = model.productID;
    [self.navigationController pushViewController:detail animated:YES];
}

@end
