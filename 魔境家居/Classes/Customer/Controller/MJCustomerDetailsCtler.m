//
//  MJCustomerDetailsCtler.m
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCustomerDetailsCtler.h"
#import "MJCustomerDetailsHeadView.h"
#import "UIBarButtonItem+Extension.h"
#import "MJCustomerModel.h"
#import "MJCustomerScanDetailsModel.h"
#import "MJCustomerDetailsCell.h"
#import "MJChatCtler.h"
#import "MJNetworkTool.h"
#import "GMDCircleLoader.h"
#import "MJTimestampTool.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "MJRefresh.h"
#import "MJChatView.h"
#import "Contack.h"
#import "MJDBManager.h"
#import <AFNetworking.h>
#import "MJProductDetailsCtler.h"
#import "MJContackModel.h"
#import "MJUserDataModel.h"

#define myID [MJUserDefaults objectForKey:@"userID"]
#define CHAT_CONTACK @"http://api.duc.cn/user/getUserByQuery"

#define kBackBtnWidth 60
@interface MJCustomerDetailsCtler ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *bigBackgroundView;
@property (nonatomic,strong) MJCustomerDetailsHeadView *headView;
@property (nonatomic,strong) UITableView  *tableView;
@property (nonatomic, strong) NSMutableArray *dataArr;
/* 转圈的背景View*/
@property (nonatomic,strong) UIView *circulateBackgroungView;
/* 播放器*/
@property (retain, nonatomic) AVAudioPlayer *audioPlayer;
@property (nonatomic,strong) NSURL *urlPath;

/* 当前页码数*/
@property (nonatomic,assign) NSInteger pageNumber;
/* 总共页码数*/
@property (nonatomic,assign) NSInteger pageCount;

@property (nonatomic,strong) MJChatView *noteView;
@property (nonatomic, strong) UIView *coverView;
@property (nonatomic, strong) GMDCircleLoader *loader;
#define kNoteViewWidth 984
#define kNoteViewHeight 728
//弹出时间
#define kNoteViewShowTime 0.5

@end

@implementation MJCustomerDetailsCtler


static NSString * const reuseIdentifier = @"Cell";


-(AVAudioPlayer *)audioPlayer{
    if (!_audioPlayer) {
        
        NSError *error=nil;
        //初始化播放器，注意这里的Url参数只能时文件路径，不支持HTTP Url
        _audioPlayer=[[AVAudioPlayer alloc]initWithContentsOfURL:self.urlPath error:&error];
       // _audioPlayer = [[AVAudioPlayer alloc] init];
        
       // NSLog(@"%@",self.urlPath);
        //设置播放器属性
        _audioPlayer.numberOfLoops=0;//设置为0不循环
        //_audioPlayer.delegate=self;
        [_audioPlayer prepareToPlay];//加载音频文件到缓存
        if(error){
            NSLog(@"初始化播放器过程发生错误,错误信息:%@",error.localizedDescription);
            return nil;
        }
    }
    return _audioPlayer;
}

- (NSMutableArray *)dataArr
{
    if (_dataArr == nil) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (UITableView *)tableView
{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,Screen_Width-kBackBtnWidth, Screen_Height-130) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableHeaderView = self.headView;

        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.backgroundColor = RGB(225, 225, 225);
      //  _tableView.scrollEnabled = YES;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
      // _tableView.rowHeight = 50;
        [_tableView registerNib:[UINib nibWithNibName:@"MJCustomerDetailsCell" bundle:nil] forCellReuseIdentifier:reuseIdentifier];
    }
    return _tableView;
}


- (MJCustomerDetailsHeadView *)headView
{
    if (_headView == nil) {
        _headView = [MJCustomerDetailsHeadView customerDetailsHead];
        //_headView.frame = CGRectMake(0, 0, Screen_Width-64, 50);
        // _headView.backgroundColor = [UIColor redColor];
        if ([_model.isPhoneAllowed isEqualToString:@"1"]) {
            [_headView setNickName:_model.customerNickName phoneNum:_model.customerPhoneNumber];
        }else
        {
            [_headView setNickName:_model.customerNickName phoneNum:@""];
        }
        
        [_headView addTarget:self action:@selector(organizingData)];
    }
    return _headView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
}

- (void)initUI
{
    self.automaticallyAdjustsScrollViewInsets=NO;
    self.pageNumber = 1;
    [self.bigBackgroundView addSubview:self.tableView];
    [self.bigBackgroundView bringSubviewToFront:self.tableView];
   // [self.bigBackgroundView addSubview:self.headView];

    //在线联系客服
    UIButton *onlineContact = [UIButton buttonWithType:UIButtonTypeCustom];
    [onlineContact setFrame:CGRectMake((Screen_Width-kBackBtnWidth)/2-(Screen_Width-kBackBtnWidth)/4, Screen_Height-100, (Screen_Width-kBackBtnWidth)/2-10, 50)];
    [onlineContact setImage:[UIImage imageNamed:@"online"] forState:UIControlStateNormal];
    [onlineContact addTarget:self action:@selector(onlineContact) forControlEvents:UIControlEventTouchUpInside];
    [self.bigBackgroundView addSubview:onlineContact];
    
    //电话联系客服
    UIButton *phoneContact = [UIButton buttonWithType:UIButtonTypeCustom];
    [phoneContact setFrame:CGRectMake((Screen_Width-kBackBtnWidth)/2+10, Screen_Height-100, (Screen_Width-kBackBtnWidth)/4-10, 50)];
    [phoneContact setImage:[UIImage imageNamed:@"dial"] forState:UIControlStateNormal];
    [phoneContact addTarget:self action:@selector(phoneContact) forControlEvents:UIControlEventTouchUpInside];
  //  [self.bigBackgroundView addSubview:phoneContact];
   
    
    
    NSDictionary *dict = @{@"browseID" : [NSString stringWithFormat:@"%ld",(long)self.model.idNum],@"pageSize" : @12,@"pageNumber" : [NSString stringWithFormat:@"%ld",(long)self.pageNumber]};
    
    [self addRefreshHeader:dict];
}

#pragma mark - 加载转圈
- (void)loadingData
{
    self.circulateBackgroungView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
    _circulateBackgroungView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_circulateBackgroungView];
    [GMDCircleLoader setOnView:_circulateBackgroungView withTitle:@"加载中..." animated:YES];
  //  MJNetworkTool *net = [MJNetworkTool shareInstance];
    
//    NSDictionary *para1 = @{@"userID" : [NSString stringWithFormat:@"%d",148279]};
//   // NSLog(@"%ld",_model.userID);
//    [net getMoJingURL:MJUserDetailData parameters:para1 success:^(id obj) {
//        NSDictionary *data = [obj objectForKey:@"data"];
//
//        MJUserDataModel *dataModel = [MJUserDataModel mj_objectWithKeyValues:data];
////
//        NSLog(@"%@ %@",dataModel.userName,dataModel.nickName);
//        
//    } failure:^(NSError *error) {
//        NSLog(@"%@",error);
//    }];
    
}

#pragma mark - 停止转圈
- (void)stopCircleLoader {
    [self.circulateBackgroungView removeFromSuperview];
    //[GMDCircleLoader hideFromView:self.view animated:YES];
}


#pragma mark - 是否有网络
- (BOOL)isConnectToNet
{
    MJNetworkTool *net = [MJNetworkTool shareInstance];
    return  [net isConnectionAvailableInView:self.view];
}

#pragma mark - 添加上下拉加载数据
- (void)addRefreshHeader:(NSDictionary *)dict
{
    
    __weak MJCustomerDetailsCtler *ctl = self;
    [self loadingData];
    //下拉刷新
    self.tableView.mj_header =  [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        // 进入刷新状态后会自动调用这个block
        [ctl loadNewData:dict];
    }];
    // 马上进入刷新状态
    [self.tableView.mj_header beginRefreshing];
    
}

#pragma mark - 数据获取
- (void)loadNewData:(NSDictionary *)dict
{
    self.pageNumber = 1;
    [self.dataArr removeAllObjects];
     __weak MJCustomerDetailsCtler *ctl = self;
    if ([self isConnectToNet]) {

        MJNetworkTool *net = [MJNetworkTool shareInstance];
        [net getMoJingURL:MJCustomerBrowseDetail parameters:dict success:^(id obj) {
            
            NSDictionary *para1 = @{@"userID" : [NSString stringWithFormat:@"%ld",(long)self.model.idNum]};
        
            
            NSDictionary *browseDict = obj[@"data"];
            NSArray *arr = [browseDict objectForKey:@"productBrowseList"];
            self.pageCount = [[browseDict objectForKey:@"pageCount"] integerValue];
            NSArray *modelArr = [MJCustomerScanDetailsModel mj_objectArrayWithKeyValuesArray:arr];
            
            
            [ctl.dataArr addObject:ctl.model];
            [ctl.dataArr addObjectsFromArray:modelArr];
            [ctl.dataArr addObject:ctl.model];
            
            if (self.dataArr.count >= 12)  {
                // 上拉刷新
                ctl.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
                    // 进入刷新状态后会自动调用这个block
                    [ctl loadMoreData:dict];
                }];
                // 默认先隐藏footer
                ctl.tableView.mj_footer.hidden = YES;
            }else{
                ctl.tableView.mj_footer = nil;
            }
            
            [ctl.tableView reloadData];
            [ctl stopCircleLoader];
            [ctl.tableView.mj_header endRefreshing];
            [ctl.view endEditing:YES];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [ctl.tableView.mj_header endRefreshing];
            [ctl stopCircleLoader];
        }];
    }
}

#pragma mark - 上拉加载更多数据
- (void)loadMoreData:(NSDictionary *)dict
{
    _pageNumber++;
    //下拉之前先移除数组最后一个数据
    [self.dataArr removeObjectAtIndex:self.dataArr.count-1];
    
    MJNetworkTool *netTool = [MJNetworkTool shareInstance];
    __weak MJCustomerDetailsCtler *ctl = self;
   
    NSDictionary *moreDict = @{@"browseID" : [NSString stringWithFormat:@"%ld",(long)self.model.idNum],@"pageSize" : @12,@"pageNumber" : [NSString stringWithFormat:@"%ld",self.pageNumber]};
    //获取搭配作品列表
    if ([self isConnectToNet]) {
        if (self.pageNumber <= self.pageCount) {
            
            [netTool getMoJingURL:MJCustomerBrowseDetail parameters:moreDict success:^(id obj) {
                // NSLog(@"%@",moreDict);
                NSDictionary *browseDict = obj[@"data"];
                NSArray *arr = [browseDict objectForKey:@"productBrowseList"];
            
                NSArray *modelArr = [MJCustomerScanDetailsModel mj_objectArrayWithKeyValuesArray:arr];
                
                [ctl.dataArr addObjectsFromArray:modelArr];
                [ctl.dataArr addObject:ctl.model];
                [ctl.tableView reloadData];
                [ctl stopCircleLoader];
                [ctl.tableView.mj_footer endRefreshing];
                
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [ctl.tableView.mj_footer endRefreshing];
                [ctl stopCircleLoader];
            }];
        }else
        {
            // 变为没有更多数据的状态
            [ctl.tableView.mj_footer endRefreshingWithNoMoreData];
        }
        
    }else
    {
        [self stopCircleLoader];
        [self.tableView.mj_footer endRefreshing];
        self.tableView.mj_footer.hidden = YES;
    }
    
}


#pragma mark - 在线联系客服
- (UIView *)noteView
{
    if (_noteView == nil) {
        _noteView =[MJChatView shareInstance];
        _noteView.backgroundColor = [UIColor blackColor];
        _noteView.tag =200;
        
        __block MJCustomerDetailsCtler *ctl = self;
        
        _noteView.close= ^{
            [UIView animateWithDuration:kNoteViewShowTime animations:^{
                [ctl.noteView setFrame:CGRectMake(20, Screen_Height, kNoteViewWidth, kNoteViewHeight)];
            }completion:^(BOOL finished) {
                [ctl.noteView removeFromSuperview];
                [ctl.coverView removeFromSuperview];
                ctl.coverView = nil;
            }];
            
        };
        
    }
    
    return _noteView;
}


- (UIView *)coverView
{
    if (!_coverView) {
        _coverView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Screen_Width/*-kMoreViewWidth*/, Screen_Height)];
        _coverView.backgroundColor = [UIColor blackColor];
        _coverView.alpha = 0.5;
       
        // [self.view addSubview:_coverView];
    }
    return _coverView;
}

- (void)onlineContact
{

    [self.view addSubview:self.coverView];
    MJDBManager* managerData = [MJDBManager shareInstanceWithCoreDataID:myID];
    
    NSString* useId =[NSString stringWithFormat:@"%li",_model.customerID];
    NSArray *objX = [[NSMutableArray alloc]initWithArray:[managerData query:@"Contack" predicate:nil]];
    NSArray *obj = [self timeSort:objX];
    
    NSMutableArray *localContact = [[NSMutableArray alloc]init];

    for (Contack* contack in obj) {
        [localContact addObject:[contack.contack_id stringValue]];
    }

    
    if ([localContact indexOfObject:useId]== NSNotFound)
    {
        AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
        session.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSMutableDictionary *contackDic1 = [[NSMutableDictionary alloc]init];
        
        [contackDic1 setObject:useId forKey:@"ids"];
        
        [session GET:CHAT_CONTACK parameters:contackDic1 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            NSArray *contackInfo =[dict objectForKey:@"data"];
            
            for (NSDictionary* dic in contackInfo) {
                
                
                
                NSString *headImgUrl = [dic objectForKey:@"avatarPic"];
                NSString *name = _model.customerNickName;
                NSNumber *contack_id = [dic objectForKey:@"id"];
                
                
                Contack *contack1 = (Contack *)[managerData createMO:@"Contack"];
                contack1.image =headImgUrl;
                contack1.nickname = name;
                contack1.contack_id = contack_id;
                
                [managerData saveManagerObj:contack1];
                
            }
            NSArray *obj = [self timeSort:[[NSMutableArray alloc]initWithArray:[managerData query:@"Contack" predicate:nil]]];
            [self.noteView showWithIndexSelect:[obj count]-1];
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"添加联系人信息失败%@",error);
            
        }];
    }else//联系人已经存在
    {

        NSUInteger indexSelect = [localContact indexOfObject:useId];
        
        [self.noteView showWithIndexSelect:indexSelect];

        
    }
    
}

- (NSArray*)timeSort:(NSArray* )obj
{
    
    
    NSComparator cmptr = ^(id obj1, id obj2){
        Contack* contack1 = (Contack*)obj1;
        Contack* contack2 = (Contack*)obj2;
        if ([contack1.lasttime doubleValue]> [contack2.lasttime doubleValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        if ([contack1.lasttime doubleValue] < [contack2.lasttime doubleValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        return (NSComparisonResult)NSOrderedSame;
    };
    
    NSArray *array = [[NSArray alloc]init];
    array =[obj sortedArrayUsingComparator:cmptr];
    
    
    
    return array;
}

//- (void)addContack
//{
//    MJDBManager* managerData = [MJDBManager shareInstanceWithCoreDataID:myID];
//    
//    NSString* useId =[NSString stringWithFormat:@"%li",_model.customerID];
//    NSArray *obj = [[NSMutableArray alloc]initWithArray:[managerData query:@"Contack" predicate:nil]];
//
//    NSMutableArray *localContact = [[NSMutableArray alloc]init];
//    for (Contack* contack in obj) {
//        [localContact addObject:contack.contack_id];
//    }
//
//
//
//    if ([localContact indexOfObject:useId]== NSNotFound)
//    {
//        AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
//        session.responseSerializer = [AFHTTPResponseSerializer serializer];
//        NSMutableDictionary *contackDic1 = [[NSMutableDictionary alloc]init];
//        
//        [contackDic1 setObject:useId forKey:@"ids"];
//        
//        [session GET:CHAT_CONTACK parameters:contackDic1 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
//            
//            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
//            NSArray *contackInfo =[dict objectForKey:@"data"];
//            
//            for (NSDictionary* dic in contackInfo) {
//                
//                
//                NSString *headImgUrl = [dic objectForKey:@"avatarPic"];
//                NSString *name = [dic objectForKey:@"name"];
//                NSNumber *contack_id = [dic objectForKey:@"id"];
//                
//                
//                Contack *contack1 = (Contack *)[managerData createMO:@"Contack"];
//                contack1.image =headImgUrl;
//                contack1.nickname = name;
//                contack1.contack_id = contack_id;
//                
//                [managerData saveManagerObj:contack1];
//                
//            }
//            
//            NSUInteger indexSelect = [localContact indexOfObject:useId];
//
//            [self.noteView.tableViewLeft selectRowAtIndexPath:[NSIndexPath indexPathForItem:indexSelect inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];//设置选中第一行（默认有蓝色背景）
//            [self.noteView tableView:self.noteView.tableViewLeft didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:indexSelect inSection:0]];//实现点击第一行所调用的方法
//            [self.noteView.tableViewLeft reloadData];
//            [self.noteView.tableViewRight reloadData];
//            
//        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//            NSLog(@"添加联系人信息失败%@",error);
//
//        }];
//    }else//联系人已经存在
//    {
//        NSUInteger indexSelect = [localContact indexOfObject:useId];
//        
//        [self.noteView.tableViewLeft selectRowAtIndexPath:[NSIndexPath indexPathForItem:indexSelect inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];//设置选中第一行（默认有蓝色背景）
//        [self.noteView tableView:self.noteView.tableViewLeft didSelectRowAtIndexPath:[NSIndexPath indexPathForItem:indexSelect inSection:0]];//实现点击第一行所调用的方法
//        [self.noteView.tableViewLeft reloadData];
//        [self.noteView.tableViewRight reloadData];
//        
//    }
//}

#pragma mark - 电话联系客服
- (void)phoneContact
{
    NSLog(@"客户电话%@",self.model.customerPhoneNumber);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"拨打电话给客户" message:@"" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
   
}

#pragma mark - 完善资料
- (void)organizingData
{
    NSLog(@"完善资料");
}
- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self.loader removeFromSuperview];
    self.loader = nil;
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MJCustomerDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    cell.backgroundColor =RGB(225, 225, 225);
    if (indexPath.row == 0) {
        NSString *startTime = [MJTimestampTool timestampChangesStandarTime:_model.enterDateTime/1000];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setTimeText:startTime  product:@"进店"];
    }
    if(indexPath.row == self.dataArr.count-1)
    {
        if (_model.leaveDateTime == 0) {
            cell.hidden = YES;
           // cell.contentView.hidden = YES;
           
           // [cell setTimeText:@"" product:@""];
        }else{
         NSString *endTime = [MJTimestampTool timestampChangesStandarTime:(_model.leaveDateTime/1000)];
            [cell setTimeText:endTime product:@"离店"];}
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if ((indexPath.row !=0) && (indexPath.row != (self.dataArr.count-1))) {
        cell.model = self.dataArr[indexPath.row];
      //  [self.view bringSubviewToFront:cell.contentView];
      //  cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    }
    
    return cell;
}

#pragma mark - <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
   // NSLog(@"%ld",indexPath.row);
    if (indexPath.row != 0) {
        MJCustomerScanDetailsModel *model = self.dataArr[indexPath.row];
        NSDictionary *dict1 = model.mj_keyValues;
        NSDictionary *remarkDict = [dict1 objectForKey:@"remark"];
        MJCustomerRemarkModel *remark = [MJCustomerRemarkModel mj_objectWithKeyValues:remarkDict];
        NSString *type = remark.type;
       // NSLog(@"%@",remarkDict);
        if ([type isEqualToString:@"1"]) {
            self.audioPlayer = nil;
       //     [self.audioPlayer stop];
            MJProductDetailsCtler *detail = [[MJProductDetailsCtler alloc] init];
            detail.idNum = model.productID;
            [self.navigationController pushViewController:detail animated:YES];
            
        }
        if ([type isEqualToString:@"2"])
        {
            NSString *path = [NSString stringWithFormat:@"%@/Documents/%@.caf",NSHomeDirectory(),remark.sound];
            self.urlPath = [NSURL fileURLWithPath:path];
            NSLog(@"%@",_urlPath);
            
            [self playRecordSound];
            
        }
    }
    
}


#pragma mark - <UIAlertViewDelegate>
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.model.customerPhoneNumber]];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)playRecordSound
{
    if (self.audioPlayer.playing) {
        [self.audioPlayer stop];
        return;
    }
    [self.audioPlayer play];
}
@end
