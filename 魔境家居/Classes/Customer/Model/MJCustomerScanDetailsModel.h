//
//  MJCustomerScanDetailsModel.h
//  魔境家居
//
//  Created by mojing on 15/11/5.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJCustomerRemarkModel.h"
@interface MJCustomerScanDetailsModel : NSObject

@property (nonatomic,assign) NSInteger productID  ; //1001,
@property (nonatomic,copy) NSString * productName  ;//  商品名称 ,
@property (nonatomic,assign) NSInteger degree  ; //123450,
@property (nonatomic,strong) MJCustomerRemarkModel * remark  ; // 备注信息 ,
@property (nonatomic,assign) double browseDateTime ;//  2015-10-10 11 ;11 ;11

- (instancetype)initWithProductName:(NSString *)productName degree:(NSInteger)degree browseDate:(double)browseDate;

+ (instancetype)modelWithProductName:(NSString *)productName degree:(NSInteger)degree browseDate:(double)browseDate;
@end
