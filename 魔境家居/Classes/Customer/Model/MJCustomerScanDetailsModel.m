//
//  MJCustomerScanDetailsModel.m
//  魔境家居
//
//  Created by mojing on 15/11/5.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCustomerScanDetailsModel.h"

@implementation MJCustomerScanDetailsModel

- (instancetype)initWithProductName:(NSString *)productName degree:(NSInteger)degree browseDate:(double)browseDate
{
    if (self = [super init]) {
        self.productName = productName;
        self.degree = degree;
        self.browseDateTime = browseDate;
    }
    return self;
}

+ (instancetype)modelWithProductName:(NSString *)productName degree:(NSInteger)degree  browseDate:(double)browseDate
{
    return [[self alloc] initWithProductName:productName degree:degree browseDate:browseDate];
}
@end
