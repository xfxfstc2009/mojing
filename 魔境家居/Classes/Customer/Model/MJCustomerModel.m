//
//  MJCustomerModel.m
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCustomerModel.h"
#import "MJCustomerScanDetailsModel.h"
#import "MJExtension.h"
@implementation MJCustomerModel

+ (NSDictionary *)replacedKeyFromPropertyName
{
    return @{@"idNum" : @"id"};
}

- (NSDictionary *)objectClassInArray
{
    return @{@"productBrowseList":[MJCustomerScanDetailsModel class]};
}

- (instancetype)initWithUserID:(NSInteger)userID browseID:(NSInteger)browseID nickName:(NSString *)nickName startDate:(double)startDate endDate:(double)endDate phoneNumber:(NSString *)phoneNumber
{
    if (self = [super init]) {
        self.userID = userID;
        self.customerNickName = nickName;
        self.enterDateTime = startDate;
        self.leaveDateTime = endDate;
        self.customerPhoneNumber = phoneNumber;
       // self.browseID = browseID;
    }
    return self;
}

+ (instancetype)modelWithUserID:(NSInteger)userID browseID:(NSInteger)browseID nickName:(NSString *)nickName startDate:(double)startDate endDate:(double)endDate phoneNumber:(NSString *)phoneNumber
{
    return [[self alloc] initWithUserID:userID browseID:browseID nickName:nickName startDate:startDate endDate:endDate phoneNumber:phoneNumber];
}
@end
