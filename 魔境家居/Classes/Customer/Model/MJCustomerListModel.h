//
//  MJCustomerList.h
//  魔境家居
//
//  Created by mojing on 15/11/21.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJCustomerDataModel.h"

@interface MJCustomerListModel : NSObject
@property (nonatomic,assign) NSInteger code  ;//200,
@property (nonatomic,copy) NSString * msg  ;// 可以直接向用户提示的错误信息 ,
@property (nonatomic,copy) NSString * debugMsg  ;// 用于确定错误原因的调试错误信息，不能向用户提示 ,
@property (nonatomic,strong) MJCustomerDataModel *data;
@end
