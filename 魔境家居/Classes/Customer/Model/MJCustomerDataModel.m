//
//  MJCustomerDataModel.m
//  魔境家居
//
//  Created by mojing on 15/11/21.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import "MJCustomerDataModel.h"
#import "MJExtension.h"
#import "MJCustomerModel.h"
@implementation MJCustomerDataModel

- (NSDictionary *)objectClassInArray
{
    return @{@"brandBrowseList":[MJCustomerModel class]};
}

@end
