//
//  MJQRDataModel.h
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//  二维码信息

#import <Foundation/Foundation.h>

@interface MJQRDataModel : NSObject

@property (nonatomic,copy) NSString *qrImageID;
@property (nonatomic,copy) NSString *qrImageName;
@property (nonatomic,copy) NSString *qrImageSuffix;
@end
