//
//  MJCustomerDetailDataModel.h
//  魔境家居
//
//  Created by lumingliang on 16/1/7.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJCustomerDetailDataModel : NSObject


@property (nonatomic,assign) NSInteger idNum ;// 125,
@property (nonatomic,copy) NSString *phoneNumber ;// 13588729692,

@property (nonatomic,assign) NSInteger userID ;// 147862,

@property (nonatomic,copy) NSString *permission ;// ,

@property (nonatomic,copy) NSString *userAvatarURL ;// ,
@property (nonatomic,copy) NSString *isNickNameUpdate ;// 1,
@property (nonatomic,copy) NSString *isDeleted ;// 0,

@property (nonatomic,copy) NSString *type ;// 0,
@property (nonatomic,copy) NSString *nickName ;// 是的,
@property (nonatomic,copy) NSString *userName ;// a13588729692,
@property (nonatomic,copy) NSString *oldPassword ;// ,
@property (nonatomic,copy) NSString *isTest ;// 0


@end
