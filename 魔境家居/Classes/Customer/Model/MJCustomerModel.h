//
//  MJCustomerModel.h
//  魔境家居
//
//  Created by mojing on 15/11/3.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJQRDataModel.h"
#import "MJExtension.h"
@interface MJCustomerModel : NSObject

/* 编号*/
@property (nonatomic,assign) NSInteger userID;
/* 浏览编号*/
@property (nonatomic,assign) NSInteger browseID;
/* 用户名字*/
@property (nonatomic,copy) NSString *customerUserName;
/* 用户昵称*/
@property (nonatomic,copy) NSString *customerNickName;
/* 进店时间*/
@property (nonatomic,assign) double  enterDateTime;
/* 离店时间*/
@property (nonatomic,assign) double leaveDateTime;
/* 客户头像URL*/
@property (nonatomic,copy) NSString *userAvatarURL;
/* 电话号码*/
@property (nonatomic,copy) NSString *customerPhoneNumber;
/* 浏览详情数组*/
@property (nonatomic,strong) NSArray *productBrowseList;
/* 用户编号*/
@property (nonatomic,assign) NSInteger customerID;
/* 导购编号*/
@property (nonatomic,assign) NSInteger guideID;
/* id*/
@property (nonatomic,assign) NSInteger idNum;
/* 序列数*/
@property (nonatomic, assign) NSInteger orderCount;
/* 是否同意显示手机号*/
@property (nonatomic, assign) NSString *isPhoneAllowed;

- (instancetype)initWithUserID:(NSInteger)userID browseID:(NSInteger)browseID nickName:(NSString *)nickName startDate:(double)startDate endDate:(double)endDate phoneNumber:(NSString *)phoneNumber;
+ (instancetype)modelWithUserID:(NSInteger)userID browseID:(NSInteger)browseID nickName:(NSString *)nickName startDate:(double)startDate endDate:(double)endDate phoneNumber:(NSString *)phoneNumber;
@end
