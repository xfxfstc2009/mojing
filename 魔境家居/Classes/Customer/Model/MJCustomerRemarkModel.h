//
//  MJCustomerRemarkModel.h
//  魔境家居
//
//  Created by mojing on 15/12/4.
//  Copyright © 2015年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MJCustomerRemarkModel : NSObject

@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *data;
@property (nonatomic,copy) NSString *sound;

@end
