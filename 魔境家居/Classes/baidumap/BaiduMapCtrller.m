//
//  BaiduMapCtrller.m
//  PhotoAR
//
//  Created by 施正士 on 15/11/25.
//  Copyright © 2015年 施正士. All rights reserved.
//


#import "BaiduMapCtrller.h"
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "MJMapAnnotation.h"
#import "UIBarButtonItem+Extension.h"

#define kLatitudeDelta 0.002703
#define kLongitudeDelta 0.001717

@interface BaiduMapCtrller ()<CLLocationManagerDelegate,MKMapViewDelegate>
{
    CLGeocoder *_geocoder;
    MKMapView *_mapView;
    CLLocation *_location;
}
@end

@implementation BaiduMapCtrller

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor cyanColor];
    //返回
    UIBarButtonItem *back = [UIBarButtonItem itemWithTarget:self action:@selector(back) image:@"back" highImage:@""];
    back.customView.width = 44;
    back.customView.height = 44;
    self.navigationItem.leftBarButtonItem = back;
    
    _mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 64, Screen_Width, Screen_Height-64)];
    _mapView.delegate = self;
    // 跟踪用户的位置
    _mapView.userTrackingMode = MKUserTrackingModeFollow;
    [self.view addSubview:_mapView];
    
    _geocoder=[[CLGeocoder alloc]init];
    NSString *address = [NSString stringWithFormat:@"%@%@%@%@",self.mapDict[@"provinceName"],self.mapDict[@"cityName"],self.mapDict[@"address"],self.mapDict[@"shopName"]];
    [self getCoordinateByAddress:address];
  
}


#pragma mark 根据地名确定地理坐标
-(void)getCoordinateByAddress:(NSString *)address{
    //地理编码
    [_geocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        //取得第一个地标，地标中存储了详细的地址信息，注意：一个地名可能搜索出多个地址
        CLPlacemark *placemark=[placemarks firstObject];
        
        CLLocation *location=placemark.location;//位置
        MKCoordinateSpan span = MKCoordinateSpanMake(kLatitudeDelta, kLongitudeDelta);
        MKCoordinateRegion region = MKCoordinateRegionMake(location.coordinate,span);
       [_mapView setRegion:region animated:YES];
      // CLRegion *region=placemark.region;//区域
       
        MJMapAnnotation *anno1 = [[MJMapAnnotation alloc] init];
        anno1.coordinate = location.coordinate;
        anno1.title = self.mapDict[@"address"];
        anno1.subtitle = self.mapDict[@"shopName"];
        
        [_mapView addAnnotation:anno1];
        NSLog(@"%f %f",location.coordinate.latitude,location.coordinate.longitude);
     
    }];
}

#pragma mark - 返回
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    self.navigationController.navigationBar.frame = CGRectMake(0, 0, Screen_Width,64);
}
@end
